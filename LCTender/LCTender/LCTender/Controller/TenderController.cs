﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using WsLCTender;
using System.Web.Http;
using LCTender.Models;
using System.Data;

namespace LCTender.Controller
{
    
    public class TenderController : ApiController
    {
      
        public static List<JoinTender> tenderList = new List<JoinTender>();
        dbcReader dbcInsVendReg = new dbcReader();   
        public List<JoinTender> Get()
        {
            DataSet ds = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_join_tender ''", 0, 0);
            DataTable dt = ds.Tables[0];
            tenderList.Clear();          
            if (dt != null)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    JoinTender d = new JoinTender();
                    d.t_vendor_register_id = Convert.ToInt32(dt.Rows[i]["t_vendor_register_id"]);
                    d.personal_id = dt.Rows[i]["personal_id"].ToString();
                    d.personal_no = dt.Rows[i]["personal_no"].ToString();
                    d.p_project_tender_id = Convert.ToInt32(dt.Rows[i]["p_project_tender_id"]);
                    d.tender_no = dt.Rows[i]["tender_no"].ToString();
                    d.vendor_name = dt.Rows[i]["vendor_name"].ToString();
                    d.t_vendor_tender_id = Convert.ToInt32(dt.Rows[i]["t_vendor_tender_id"]);
                    d.join_date = dt.Rows[i]["join_date"].ToString();
                    d.join_date_epoch = Convert.ToDouble(dt.Rows[i]["join_date_epoch"]);
                    d.project_name = dt.Rows[i]["project_name"].ToString();
                    d.request_status = dt.Rows[i]["request_status"].ToString();
                    d.amt_submitted = Convert.ToInt32(dt.Rows[i]["amt_submitted"]);
                    d.status = dt.Rows[i]["status"].ToString();
                    d.service_unit_name = dt.Rows[i]["service_unit_name"].ToString();
                    d.service_unit_no = dt.Rows[i]["service_unit_no"].ToString();
                    d.entity_name = dt.Rows[i]["entity_name"].ToString();
                    d.department_name = dt.Rows[i]["department_name"].ToString();
                    d.department_no = dt.Rows[i]["department_no"].ToString();
                    d.division_no = dt.Rows[i]["division_no"].ToString();
                    //d.vendorNo = dt.Rows[i]["vendorNo"].ToString();
                    tenderList.Add(d);
                }
            }
            return tenderList;
        }
        public List<JoinTender> Get(string tender_no)
        {
            DataSet ds = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_join_tender "+ "'" + tender_no + "'" + "", 0, 0);
            DataTable dt = ds.Tables[0];
            tenderList.Clear();
            if (dt != null)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    JoinTender d = new JoinTender();
                    d.t_vendor_register_id = Convert.ToInt32(dt.Rows[i]["t_vendor_register_id"]);
                    d.personal_id = dt.Rows[i]["personal_id"].ToString();
                    d.personal_no = dt.Rows[i]["personal_no"].ToString();
                    d.p_project_tender_id = Convert.ToInt32(dt.Rows[i]["p_project_tender_id"]);
                    d.tender_no = dt.Rows[i]["tender_no"].ToString();
                    d.vendor_name = dt.Rows[i]["vendor_name"].ToString();
                    d.t_vendor_tender_id = Convert.ToInt32(dt.Rows[i]["t_vendor_tender_id"]);
                    d.join_date = dt.Rows[i]["join_date"].ToString();
                    d.join_date_epoch = Convert.ToDouble(dt.Rows[i]["join_date_epoch"]);
                    d.project_name = dt.Rows[i]["project_name"].ToString();
                    d.request_status = dt.Rows[i]["request_status"].ToString();
                    d.amt_submitted = Convert.ToInt32(dt.Rows[i]["amt_submitted"]);
                    d.status = dt.Rows[i]["status"].ToString();
                    d.service_unit_name = dt.Rows[i]["service_unit_name"].ToString();
                    d.service_unit_no = dt.Rows[i]["service_unit_no"].ToString();
                    d.entity_name = dt.Rows[i]["entity_name"].ToString();
                    d.department_name = dt.Rows[i]["department_name"].ToString();
                    d.department_no = dt.Rows[i]["department_no"].ToString();
                    d.division_no = dt.Rows[i]["division_no"].ToString();
                    //d.vendorNo = dt.Rows[i]["vendorNo"].ToString();
                    tenderList.Add(d);
                }
            }
            return tenderList;
        }
    }
}