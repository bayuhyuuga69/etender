﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using WsLCTender;
using System.Web.Http;
using LCTender.Models;
using System.Data;

namespace LCTender.Controller
{
    
    public class RecomVendorController : ApiController
    {
      
        public static List<RecomVendor> RecomVendList = new List<RecomVendor>();
        dbcReader dbcInsVendReg = new dbcReader();   
        public List<RecomVendor> Get()
        {
            DataSet ds = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_recommend_tender_api ''", 0, 0);
            DataTable dt = ds.Tables[0];
            RecomVendList.Clear();          
            if (dt != null)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    RecomVendor d = new RecomVendor();
                    d.project_name = dt.Rows[i]["project_name"].ToString();
                    d.register_no = dt.Rows[i]["register_no"].ToString();
                    d.vendor_name = dt.Rows[i]["vendor_name"].ToString();
                    d.tender_no = dt.Rows[i]["tender_no"].ToString();
                    d.personalid = dt.Rows[i]["personalid"].ToString();
                    d.personalno = dt.Rows[i]["personalno"].ToString();
                    d.tender_date = dt.Rows[i]["join_date_epoch"].ToString();
                    d.tender_date_epoch = dt.Rows[i]["tender_date"].ToString();
                    d.estimated_cost_amt = dt.Rows[i]["estimated_cost_amt"].ToString();
                    d.offering_cost_amt = dt.Rows[i]["offering_cost_amt"].ToString();
                    //d.vendorNo = dt.Rows[i]["vendorNo"].ToString();
                    RecomVendList.Add(d);
                }
            }
            return RecomVendList;
        }
        public List<RecomVendor> Get(string tender_no)
        {
            DataSet ds = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_recommend_tender_api "+ "'"+tender_no+"'", 0, 0);
            DataTable dt = ds.Tables[0];
            RecomVendList.Clear();
            if (dt != null)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    RecomVendor d = new RecomVendor();
                    d.project_name = dt.Rows[i]["project_name"].ToString();
                    d.register_no = dt.Rows[i]["register_no"].ToString();
                    d.vendor_name = dt.Rows[i]["vendor_name"].ToString();
                    d.tender_no = dt.Rows[i]["tender_no"].ToString();
                    d.personalid = dt.Rows[i]["personalid"].ToString();
                    d.personalno = dt.Rows[i]["personalno"].ToString();
                    d.tender_date = dt.Rows[i]["tender_date"].ToString();
                    d.tender_date_epoch = dt.Rows[i]["join_date_epoch"].ToString();
                    d.estimated_cost_amt = dt.Rows[i]["estimated_cost_amt"].ToString();
                    d.offering_cost_amt = dt.Rows[i]["offering_cost_amt"].ToString();
                    //d.vendorNo = dt.Rows[i]["vendorNo"].ToString();
                    if (!DBNull.Value.Equals(dt.Rows[i]["file_dir"]))
                    {
                        d.file_penawaran = dt.Rows[i]["file_dir"].ToString();
                    }
                    if (!DBNull.Value.Equals(dt.Rows[i]["file_type"]))
                    {
                        d.file_type = dt.Rows[i]["file_type"].ToString();
                    }
                    RecomVendList.Add(d);
                }
            }
            return RecomVendList;
        }
    }
}