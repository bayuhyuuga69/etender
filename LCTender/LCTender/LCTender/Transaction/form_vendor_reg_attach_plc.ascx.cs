﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;

namespace LCTender.Transaction
{
    public partial class form_vendor_reg_attach_plc : System.Web.UI.UserControl
    {
        private string strCallee;
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 20;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                //strCallee = "aceng.koswara2010@gmail.com";
                int intCurrPage = 1;               
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                txtSelectID.Text = Request["t_vendor_register_id"];
                BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_register_id"]),0);
            }
            if (Request["t_vendor_register_id"].ToString() == "0")
            {
                imbAdd.Visible = false;
                lblAdd.Visible = false;
            }
            else
            {
                imbAdd.Visible = true;
                lblAdd.Visible = true;
            }

        }
        protected void BindDataList(int intCurrPage,int intVendorID,int intHolderStockID)
        {
            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            int vendorId = Convert.ToInt32(Request["t_vendor_register_id"]);
            DataSet dsresult = dbcVendorList.db_get_data_reg_attachment(intPage, vendorId, Session["UserName"].ToString(), PER_PAGE, 0, ViewState["filter"].ToString(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;            
            gvTask.DataSource = dtresult;
            gvTask.DataBind();
       
            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row' style='padding-top:20px'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NAMA FILE</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>DESKRIPSI</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>DIBUAT OLEH</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]),imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String AttachmentId = "";
            AttachmentId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            String FileDir = "";
            FileDir = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[1].Text;
            switch (e.CommandName)
            {
                case "Hapus":

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_reg_attachment";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Convert.ToInt32(AttachmentId), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_register_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "1", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", 0, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "DELETE DOC", " ");

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        //cstext1.Append("bootbox.alert({ ");
                        //cstext1.Append("message: 'Terhapus',");
                        //cstext1.Append("size: 'small',");
                        //cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());

                        File.Delete(FileDir);
                        FileInfo fInfoEvent;
                        fInfoEvent = new FileInfo(FileDir);
                        fInfoEvent.Delete();
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                    break;
            }
            

        } 
       
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                Label lblFileNameVal = (Label)e.Row.FindControl("lblFileName");
                                
                String strFileName = "";

                String FileName = (((DataRowView)e.Row.DataItem)["FILE_NAME"]).ToString();
                String FileDir = (((DataRowView)e.Row.DataItem)["FILE_DIR"]).ToString();
                String FileMime = (((DataRowView)e.Row.DataItem)["FILE_TYPE"]).ToString();
                String FileType = (((DataRowView)e.Row.DataItem)["ATTACHMENT_TYPE"]).ToString();

                strFileName += String.Format(@"<div class='row'>");
                strFileName += String.Format(@"<div class='col-sm-2'><a href=""javascript:clickView('{1}','{2}')""><span class='glyphicon glyphicon-download'>{0}</span></a></div>", FileName.ToUpper(), FileDir, FileMime);
                strFileName += String.Format(@"</div>");

                lblFileNameVal.Text = strFileName;
            }
        }

        public string StrCallee
        {
            get
            {
                return strCallee;
            }
            set
            {
                strCallee = value;
            }
        }

        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_register_id"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_register_id"]),0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>clickAdd(" + 0 + ",1)</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }
    }
}