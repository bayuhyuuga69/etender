﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;

namespace LCTender.Transaction
{
    public partial class form_vendor_reg_plc : System.Web.UI.UserControl
    {
        private string strCallee;
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 20;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
               
                int intCurrPage = 1;               
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["t_vendor_register_id"]==null)
                {
                    BindDataList(intCurrPage, Session["UserName"].ToString(), 0);

                }else
                {
                    BindDataList(intCurrPage, Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]));
                }


            }
         

        }
        protected void BindDataList(int intCurrPage,string strUserName, int intVendorID)
        {

            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_data_vendor(intPage, strUserName, PER_PAGE, 0, ViewState["filter"].ToString(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;            
            gvTask.DataSource = dtresult;
            gvTask.DataBind();
            //gvTask.HeaderRow.TableSection = TableRowSection.TableHeader;

            String strAmt = dbcVendorList.ExecuteReader("Select Count(*) Amt from dbo.T_VENDOR_REGISTER where p_app_user_id=" + Session["UserId"].ToString(), "Amt");
            if (Convert.ToInt32(strAmt) > 0)
            {
                imbAdd.Visible = false;
                lblAdd.Visible = false;
            }
            else
            {
                imbAdd.Visible = true;
                lblAdd.Visible = true;
            }


            for (int i = 0; i < dtresult.Rows.Count; i++)
            {
                
                ImageButton imbRdoSelect = (ImageButton)gvTask.Rows[i].FindControl("imbRdoRow");
                gvTask.Rows[i].Cells[11].Text = String.Format("{0:n0}", Convert.ToDecimal(gvTask.Rows[i].Cells[11].Text));
                gvTask.Rows[i].Cells[12].Text = String.Format("{0:n0}", Convert.ToDecimal(gvTask.Rows[i].Cells[12].Text));

                if (intVendorID==0)
                {                   
                    if(i==0) {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        // gvTask.Rows[i].CssClass = "Row";
                    }
                    intVendorID = Convert.ToInt32(gvTask.Rows[i].Cells[0].Text);
                }else
                {
                    if (Convert.ToInt32(gvTask.Rows[i].Cells[0].Text) == intVendorID)
                    {
                       imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        //gvTask.Rows[i].CssClass = "AltRow";
                    }
                }
              
             
            }
            txtSelectID.Text = intVendorID.ToString();

            if(intMaxRec == 0){
                lblNoRow.Text = "" +
                 "<div class='Row' style='padding-top:20px'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>REGISTER NO</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VENDOR NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>SITE ADDRESS</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = ""; 
            }
           

            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]),imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String VendorRegId = "";
            VendorRegId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    //Response.Write("Vendorid:" + VendorRegId);
                    BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(VendorRegId));
                    break;
                case "Detail":
                    //load_maint(VendorRegId, 2)
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>clickEdit("+ VendorRegId + ",2)</");
                    cstext1.Append("script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    break;
            }
            txtSelectID.Text = VendorRegId;

        }
       
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblRegVal = (Label)e.Row.FindControl("lblReg");
                Label lblVendorVal = (Label)e.Row.FindControl("lblVendor");
                Label lblSiteVal = (Label)e.Row.FindControl("lblSiteAddr");

                String strReg = "";
                String strVendor = "";
                String strSite = "";

                String regno = (((DataRowView)e.Row.DataItem)["REGISTER_NO"]).ToString();
                String vendor = (((DataRowView)e.Row.DataItem)["VENDOR_NAME"]).ToString();
                String site = (((DataRowView)e.Row.DataItem)["SITE_ADDR"]).ToString();
                String created = (((DataRowView)e.Row.DataItem)["CREATED_BY"]).ToString();

                strReg += String.Format(@"<div class='row'>");
                strReg += String.Format(@"<div class='col-sm-6'>{0}</div>", regno.ToUpper());
                strReg += String.Format(@"</div>");

                lblRegVal.Text = strReg;

                strVendor += String.Format(@"<div class='row'>");
                strVendor += String.Format(@"<div class='col-sm-6'>{0}</div>", vendor.ToUpper());
                strVendor += String.Format(@"</div>");

                lblVendorVal.Text = strVendor;

                strSite += String.Format(@"<div class='row'>");
                strSite += String.Format(@"<div class='col-sm-6'>{0}</div>", site.ToUpper());
                strSite += String.Format(@"</div>");

                lblSiteVal.Text = strSite;


            }
        }

        public string StrCallee
        {
            get
            {
                return strCallee;
            }
            set
            {
                strCallee = value;
            }
        }

        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), 0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), 0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), 0);
        }

        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>clickAdd(" + 0 + ",1)</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }
    }
}