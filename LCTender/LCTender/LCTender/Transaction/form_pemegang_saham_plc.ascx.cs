﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LCTender.Transaction
{
    public partial class form_pemegang_saham_plc : System.Web.UI.UserControl
    {
        public TextBox EmailSaham
        {
            get
            {
                return txtEmailSaham;
            }
            set
            {
                txtEmailSaham = value;
            }
        }
        private string strCallee = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Find UserControl1 user control.             

                txtEmailSaham.Text = strCallee;
            }

        }


        public string StrCallee
        {
            get
            {
                return strCallee;
            }
            set
            {
                strCallee = value;
            }
        }
    }
}