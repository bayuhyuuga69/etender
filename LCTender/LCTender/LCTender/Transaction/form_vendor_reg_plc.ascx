﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="form_vendor_reg_plc.ascx.cs" Inherits="LCTender.Transaction.form_vendor_reg_plc" %> 
<div class="table-responsive">
<div class="row">
   <div class="col-sm-12">
    <div class="input-group input-group-sm">
        <label for="email" class="col-sm-2">Filter:</label>
        <div class="col-sm-8">
            <asp:TextBox id="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="col-sm-2">
            <div class="btn btn-default glyphicon glyphicon-search">
                    <asp:Button ID="btnCari" runat="server" Text="Cari" BackColor="Transparent" BorderWidth="0" OnClick="btnCari_Click" />
                </div>
        </div>
    </div>    
   </div>
</div>
<div class="row" style="padding-top:20px">
    <div class="col-sm-12">
         <a><asp:ImageButton ID="imbAdd" runat="server" border ="0" ImageUrl="~/images/btn_add.png" OnClick="imbAdd_Click"/>&nbsp;<asp:Label ID="lblAdd" runat="server" Text="Add New Data"></asp:Label></a>
    </div>
</div>
<div class="row">
   <div class="col-sm-12">
    <asp:Panel ID="pnlGridVend" runat="server">    
        <asp:GridView ID="gvTask" runat="server" Width="100%"
            CssClass="mGrid table table-responsive table-striped table-hover"
            PagerStyle-CssClass="pgr"
            AlternatingRowStyle-CssClass="alt"
            CellPadding="5" CellSpacing="4"
            AutoGenerateColumns="False"
            OnRowDataBound="gvTask_RowDataBound" 
            OnRowCommand="gvTask_RowCommand" PageSize="20">
            <AlternatingRowStyle CssClass="alt" />
            <Columns>
                <asp:BoundField DataField="T_VENDOR_REGISTER_ID" HeaderText="T_VENDOR_REGISTER_ID" ItemStyle-CssClass="hidden-phone">
                <ItemStyle CssClass="hidden" />
                <HeaderStyle CssClass="hidden" />
                </asp:BoundField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <asp:Label ID="lblHeaderRec" runat="server" Text="#"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:ImageButton ID="imbRdoRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Select" ImageUrl="~/images/radio.gif" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <asp:Label ID="lblHeaderEdit" runat="server" Text="EDIT"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:ImageButton ID="imbRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Detail" ImageUrl="~/images/table.gif" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="REGISTER_NO" HeaderText="NO REGISTER" ItemStyle-CssClass="hidden-phone" Visible="false">
                <ItemStyle CssClass="hidden-phone"/>
                </asp:BoundField>
                <asp:BoundField DataField="VENDOR_NAME" HeaderText="VENDOR" ItemStyle-CssClass="hidden-phone" Visible="false">
                <ItemStyle CssClass="hidden-phone"/>
                </asp:BoundField>
                <asp:BoundField DataField="SITE_ADDR" HeaderText="ALAMAT SITUS" ItemStyle-CssClass="hidden-phone"  Visible="false">
                <ItemStyle CssClass="hidden-phone"/>
                </asp:BoundField>
                <asp:BoundField DataField="CREATED_BY" HeaderText="CREATED BY" ItemStyle-CssClass="hidden-phone" Visible="false">
                <ItemStyle CssClass="hidden-phone"/>
                </asp:BoundField>            
                <asp:TemplateField HeaderText="NO REGISTER" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblReg" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="VENDOR" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblVendor" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ENTITY_NAME" HeaderText="ENTITY">                                      
                </asp:BoundField>
                <asp:TemplateField HeaderText="WEBSITE" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblSiteAddr" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" 
                    DataField="COST_LIMIT_AMT" HeaderText="COST LIMIT FROM">                             
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" 
                    DataField="COST_LIMIT_MAX_AMT" HeaderText="COST LIMIT TO">                             
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="hidden-phone" HeaderStyle-CssClass="hidden-phone"
                    DataField="REQUEST_STATUS" HeaderText="STATUS">
                    <ItemStyle HorizontalAlign="Center" />   
                 <ItemStyle CssClass="hidden-phone"/>
                </asp:BoundField>
                <asp:BoundField ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="hidden-phone" HeaderStyle-CssClass="hidden-phone"
                    DataField="CREATION_DATE" HeaderText="TANGGAL">
                   <ItemStyle HorizontalAlign="Center" /> 
                </asp:BoundField>                
            </Columns>
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="3" />
            <PagerStyle CssClass="pgr" />
        </asp:GridView>
       <asp:Label ID="lblNoRow" runat="server"></asp:Label> 
       <div class="Row">
           <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;">              
                <asp:ImageButton ID="imbFirst" runat="server" OnClick="imbFirst_Click" />
                <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click"/>
                &nbsp;
                <asp:Label ID="lblPaging" runat="server"></asp:Label>
                &nbsp;
                <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" style="width: 14px" />
                <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
      </div>
     </div>
    </asp:Panel>
   </div>
  </div>
</div>
<asp:TextBox ID="txtSelectID" class="hidden" runat="server"></asp:TextBox>
<script type="text/javascript">
    function clickEdit(id,type) {
        $('#frmMaint').attr('src', "t_vendor_register_maint.aspx?t_vendor_register_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
    function clickAdd(id, type) {
        debugger;
        $('#frmMaint').attr('src', "t_vendor_register_maint.aspx?t_vendor_register_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
</script>