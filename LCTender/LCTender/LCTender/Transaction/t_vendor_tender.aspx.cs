﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Transaction
{
    public partial class t_vendor_tender : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 8;
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_tender));
        String auser_id_login, retcurr_ctl_id,
               retcreate_doc, retprev_doc_id,
               retprev_ctl_id, retprev_proc_id,
               retcurr_doc_status;
        protected void Page_Load(object sender, EventArgs e)
        {
            //entityControlID.StrEntityID = Session["P_ENTITY_ID"].ToString();

            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["t_vendor_tender_id"] == null)
                {
                    BindDataList(intCurrPage, 0);
                }else if (Request["is_doc"]!=null)
                {
                    if ((Request["is_doc"].ToString()=="Y")&&(Request["t_vendor_tender_id"].ToString() != "0"))
                    {
                        BindDataMaint(Convert.ToInt32(Request["t_vendor_tender_id"]), 2);
                    }else if((Request["is_doc"].ToString() == "N") && (Request["t_vendor_tender_id"].ToString() != "0"))
                    {
                        BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_tender_id"]));
                    }

                }
                else
                {
                    BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_tender_id"]));
                }

            }
            
        }
        protected void BindDataList(int intCurrPage, int intVendorID)
        {
            pnlFilter.Visible = true;
            pnlGridTender.Visible = true;
            imbAdd.Visible = true;
            lblAdd.Visible = true;
            pnlMaintTender.Visible = false;
            load_ddlEntityList(Session["P_ENTITY_ID"].ToString().Trim());

            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_data_tender(intPage, Session["UserName"].ToString(), PER_PAGE, 0, ViewState["filter"].ToString(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;
            gvTask.DataSource = dtresult;
            gvTask.DataBind();


            for (int i = 0; i < dtresult.Rows.Count; i++)
            {

                ImageButton imbRdoSelect = (ImageButton)gvTask.Rows[i].FindControl("imbRdoRow");
                gvTask.Rows[i].Cells[9].Text = String.Format("{0:n0}", Convert.ToDecimal(gvTask.Rows[i].Cells[9].Text));
                gvTask.Rows[i].Cells[10].Text = String.Format("{0:n0}", Convert.ToDecimal(gvTask.Rows[i].Cells[10].Text));

                if (intVendorID == 0)
                {
                    if (i == 0)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        // gvTask.Rows[i].CssClass = "Row";
                    }
                    intVendorID = Convert.ToInt32(gvTask.Rows[i].Cells[0].Text);
                }
                else
                {
                    if (Convert.ToInt32(gvTask.Rows[i].Cells[0].Text) == intVendorID)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        //gvTask.Rows[i].CssClass = "AltRow";
                    }
                }


            }
            txtSelectID.Text = intVendorID.ToString();

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NO TENDER</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PROJECT</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>ESTIMASI BIAYA</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PENAWARAN</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }


            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]), imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblTenderNo = (Label)e.Row.FindControl("lblTenderNo");
                Label lblProjectName = (Label)e.Row.FindControl("lblProjectName");

                String strReg = "";
                String strProject = "";

                String tenderno = (((DataRowView)e.Row.DataItem)["TENDER_NO"]).ToString();

                strReg += String.Format(@"<div class='row'>");
                strReg += String.Format(@"<div class='col-sm-6'>{0}</div>", tenderno.ToUpper());
                strReg += String.Format(@"</div>");

                lblTenderNo.Text = strReg;

                String projectname = (((DataRowView)e.Row.DataItem)["PROJECT_NAME"]).ToString();

                strProject += String.Format(@"<div class='row'>");
                strProject += String.Format(@"<div class='col-sm-6'>{0}</div>", projectname.ToUpper());
                strProject += String.Format(@"</div>");

                lblProjectName.Text = strProject;

            }
        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String TenderId = "";
            TenderId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(TenderId));
                    break;
                case "Detail":
                    BindDataMaint(Convert.ToInt32(TenderId), 2);
                    break;
            }
            txtSelectID.Text = TenderId;

        }
        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0,1);
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridTender.Visible = false;
            imbAdd.Visible = false;
            lblAdd.Visible = false;
            pnlMaintTender.Visible = true;
            load_ddlEntityList(Session["P_ENTITY_ID"].ToString().Trim());
            btnCancel.Attributes.Add("OnClick", "javascript:disablereq()");           

            string strLOV = "";
            string strLOVProject = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/lov_vendor_register.aspx");           
            dbcReader dbcVendorList = new dbcReader();         
           // ltLOVReg.Text = strLOV;
            if (intType == 1)
            {                
                ltWorkFlow.Text = "";
                DataSet dsReg = dbcVendorList.ExecuteSelect("select t_vendor_register_id,register_no,vendor_name from [dbo].[T_VENDOR_REGISTER] where P_APP_USER_ID=" + Session["UserId"].ToString(), 0, 0);
                DataTable dtReg = dsReg.Tables[0];
                if (dtReg.Rows.Count>0)
                {
                    t_vendor_register_id.Text = dtReg.Rows[0]["t_vendor_register_id"].ToString();
                    txtVendReg.Text = dtReg.Rows[0]["register_no"].ToString();
                    txtVendName.Text = dtReg.Rows[0]["vendor_name"].ToString();
                    strLOVProject += String.Format(@"<a href=""javascript:clickLOV('{1}',2)"">{0}</a>", strIMG, "../Lov/p_project_tender.aspx?p_entity_id=" + s_entityId.SelectedValue + "&t_vendor_register_id=" + t_vendor_register_id.Text);
                    ltLOV.Text = strLOVProject;
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                }
                txtProjectName.Text = "";
                p_project_tender_id.Text = "";
                txtOffering.Text = "";
                txtTenderNo.Text = "";
                txtQuantity.Text = "";
                txtMeasurement.Text = "";
                txtDescription.Text = "";

            } else if (intType == 2) {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                int intMaxRec;
                intMaxRec = 0;              
                DataSet dsresult = dbcVendorList.db_get_data_tender(1, "", 1, intIDRow, "", ref intMaxRec);
                DataTable dtresult = dsresult.Tables["dtData"];


                string strSubmit = dbcVendorList.ExecuteReader("SELECT count(*) AMT from sis.dbo.T_W_TENDER_CTL WHERE doc_id=" + intIDRow, "AMT");

                if (Convert.ToInt32(strSubmit) == 0)
                {
                    ltWorkFlow.Text = "<input id=\"btnWorkFlow\" type=\"button\" class=\"btn btn-primary\" value=\"Start Workflow\" onclick=\"javascript: clickLoadSubmitter()\"/>";
                }
                else
                {
                    ltWorkFlow.Text = "";
                }
                                
                t_vendor_tender_id.Text = dtresult.Rows[0]["t_vendor_tender_id"].ToString();
                txtProjectName.Text = dtresult.Rows[0]["project_name"].ToString();
                p_project_tender_id.Text = dtresult.Rows[0]["p_project_tender_id"].ToString();
                t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                txtVendReg.Text = dtresult.Rows[0]["register_no"].ToString();
                txtVendName.Text = dtresult.Rows[0]["vendor_name"].ToString();
                txtEstimate.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["estimated_cost_amt"].ToString()));
                txtEstimateVal.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["estimated_cost_amt"].ToString()));
                txtOffering.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["offering_cost_amt"].ToString()));
                txtTenderNo.Text = dtresult.Rows[0]["tender_no"].ToString();
                if (!DBNull.Value.Equals(dtresult.Rows[0]["quantity"]))
                {
                    txtQuantity.Text = dtresult.Rows[0]["quantity"].ToString();
                }
                if (!DBNull.Value.Equals(dtresult.Rows[0]["measurement"]))
                {
                    txtMeasurement.Text = dtresult.Rows[0]["measurement"].ToString();
                }
                if (!DBNull.Value.Equals(dtresult.Rows[0]["description"]))
                {
                    txtDescription.Attributes.Add("style", "font-size:12px");
                    txtDescription.Text = dtresult.Rows[0]["description"].ToString();
                }                
                p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                p_organization_id.Text = dtresult.Rows[0]["p_organization_id"].ToString();
                request_status.Text = dtresult.Rows[0]["request_status"].ToString();

                String FileName = "";
                String FileMime = "";

                if (!DBNull.Value.Equals(dtresult.Rows[0]["boq_file_name"]))
                {
                    FileName = dtresult.Rows[0]["boq_file_name"].ToString();
                }
                if (!DBNull.Value.Equals(dtresult.Rows[0]["file_type"]))
                {
                    FileMime = dtresult.Rows[0]["file_type"].ToString();
                }
                                    

                strLOVProject += String.Format(@"<a href=""javascript:clickLOV('{1}',2)"">{0}</a>", strIMG, "../Lov/p_project_tender.aspx?p_entity_id=" + s_entityId.SelectedValue + "&t_vendor_register_id=" + t_vendor_register_id.Text);
                ltLOV.Text = strLOVProject;
                if ((FileName != "")&&(FileMime!=""))
                {
                    ltDownloadBoq.Text = String.Format(@"<a href=""javascript:clickView('{0}','{1}')"">BOQ:.<img class='img-responsive' src='../images/icon/Asset 18@4x.png'></a>", FileName, FileMime);
                }else
                {
                    ltDownloadBoq.Text = "";
                }
                
                BindDataSubmitter();
            }
            

            txtSelectID.Text = intIDRow.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ((p_project_tender_id.Text != "") && (t_vendor_register_id.Text != "") 
                && (txtEstimateVal.Text != "") && (txtOffering.Text != "") 
               )
            {
               

                log.Info("Insert Vendor Tender Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_tender";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_project_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEstimateVal.Text.Replace(",",""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtOffering.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", txtDescription.Text, "");
                    

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Saved !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        string uri = Page.Request.Url.AbsoluteUri;
                        Page.Response.Redirect(uri.Replace("Y","N"));
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if ((txtProjectName.Text != "") && (txtVendReg.Text != "")
                && (txtEstimate.Text != "") && (txtOffering.Text != "")
               )
            {

                log.Info("Update Vendor Tender Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_tender";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_project_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEstimateVal.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtOffering.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", txtDescription.Text, "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Updated !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        string uri = Page.Request.Url.AbsoluteUri;
                        Page.Response.Redirect(uri.Replace("Y", "N"));
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            if ((txtProjectName.Text != "") && (txtVendReg.Text != "")
                && (txtEstimate.Text != "") && (txtOffering.Text != "")
               )
            {

                log.Info("Update Vendor Tender Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_tender";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_project_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEstimateVal.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtOffering.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", txtDescription.Text, "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Deleted !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        string uri = Page.Request.Url.AbsolutePath;
                        Page.Response.Redirect(uri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
            string uri = Page.Request.Url.AbsoluteUri;
            Page.Response.Redirect(uri.Replace("Y", "N"));
        }
        private void load_ddlEntityList(string entity_id)
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.db_get_entity_list(Convert.ToInt32(entity_id));
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        protected void BindDataSubmitter()
        {
            auser_id_login = Session["UserId"].ToString();
            if (request_status.Text == "116")
            {
                retcurr_ctl_id = Request["CURR_CTL_ID"];
                retcreate_doc = "N";
                retprev_doc_id = t_vendor_tender_id.Text;
                retprev_ctl_id = Request["PREV_CTL_ID"];
                retprev_proc_id = Request["PREV_PROC_ID"];
                retcurr_doc_status = "1";

            }
            else
            {
                retcurr_ctl_id = auser_id_login;
                retcreate_doc = "Y";
                retprev_doc_id = "";
                retprev_ctl_id = "";
                retprev_proc_id = "342";
                retcurr_doc_status = "1";
            }

            String param_submitter = "";
            String aIS_CREATE_DOC = retcreate_doc;
            String aIS_MANUAL = "N";
            String auser_id_doc = p_app_user_id.Text;
            String auser_id_donor = p_app_user_id.Text;
            String auser_id_taken = auser_id_login;
            String acurr_ctl_id = retcurr_ctl_id;
            String acurr_doc_type_id = "14";
            String acurr_proc_id = "342";
            String acurr_doc_id = t_vendor_tender_id.Text;
            String acurr_doc_status = retcurr_doc_status;
            String acurr_proc_status = "0";
            String aprev_ctl_id = retprev_ctl_id;
            String aprev_doc_type_id = "14";
            String aprev_proc_id = retprev_proc_id;
            String aprev_doc_id = retprev_doc_id;
            String amessage = "";
            String aslot_1 = "";
            String aslot_2 = "";
            String aslot_3 = "";
            String aslot_4 = "";
            String aslot_5 = "";

            param_submitter = String.Format(@"{0}", "../Wf/submitter.aspx?");
            param_submitter += String.Format(@"{0}{1}", "CURR_DOC_ID=", acurr_doc_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_TYPE_ID=", acurr_doc_type_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_ID=", acurr_proc_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_CTL_ID=", acurr_ctl_id);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DOC=", auser_id_doc);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DONOR=", auser_id_donor);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_LOGIN=", auser_id_login);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_TAKEN=", auser_id_taken);
            param_submitter += String.Format(@"{0}{1}", "&IS_CREATE_DOC=", aIS_CREATE_DOC);
            param_submitter += String.Format(@"{0}{1}", "&IS_MANUAL=", aIS_MANUAL);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_STATUS=", acurr_proc_status);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_STATUS=", acurr_doc_status);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_ID=", aprev_doc_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_TYPE_ID=", aprev_doc_type_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_PROC_ID=", aprev_proc_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_CTL_ID=", aprev_ctl_id);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_1=", aslot_1);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_2=", aslot_2);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_3=", aslot_3);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_4=", aslot_4);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_5=", aslot_5);
            param_submitter += String.Format(@"{0}{1}", "&MESSAGE=", amessage);
            param_submitter += String.Format(@"{0}{1}", "&PACKAGE=", "SUBMIT_ENGINE_TENDER");
            

            ltWfSubmitter.Text = Server.HtmlDecode("<div class='modal fade' id='myModalFrmSubmitter' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type = 'button' class='close' data-dismiss='modal'>&times;</button><h3 class='modal-title'>Workflow Started</h3></div><div class='modal-body'><div class='sizer'><div class='embed-responsive embed-responsive-16by9'><iframe id = 'frmSubmitter' src='" + param_submitter + "'></iframe></div></div></div><div class='modal-footer'><button type = 'button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");

        }


    }
}