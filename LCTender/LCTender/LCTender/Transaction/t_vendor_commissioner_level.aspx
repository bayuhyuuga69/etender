﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_commissioner_level.aspx.cs" Inherits="LCTender.Transaction.t_vendor_commissioner_level" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register src="form_vendor_comm_level_plc.ascx" tagname="holderControl" tagprefix="regCtl" %>
<%@ Register src="../Main/content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%--<link href="../Content/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>--%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    /*.btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
    }*/
    .column-in-center{
        float: none;
        margin: 0 auto;
    }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
	    padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    color: #000000;
	}
    .footer{
        position:absolute;
        bottom:15px;
        height:auto;
        padding-top:10px;
        width:98%;
        max-height:70px;
    }
    
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li><a href="javascript:clickTab(1)" style="font:15px Tahoma;">Registrasi Vendor</a></li>
              <li><a href="javascript:clickTab(2)" style="font:15px Tahoma;">Pemegang Saham</a></li>
              <li><a href="javascript:clickTab(3)" style="font:15px Tahoma;">Susunan Direksi</a></li>
              <li class="active"><a data-toggle="tab" href="#commissioner_level" style="font:15px Tahoma;">Susunan Dewan Komisaris</a></li>
              <li><a href="javascript:clickTab(4)" style="font:15px Tahoma;">Dokumen</a></li>              
          </ul>
          <div class="tab-content">   
              <%--</div>--%>
             <div class="main-div"> 
                <%-- <div class="panel">
                        <h2>Admin Login</h2>
                        <p>Please enter your email and password</p>
                    </div>--%>         
                 <div id="commissioner_level" class="tab-pane fade in active">                       
                        <form runat="server" > 
                           <regCtl:holderControl ID="holderControlID" runat="server"/>                                
                       <!-- Modal -->
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-fluid">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Susunan Dewan Komisaris</h4>
                                </div>
                                <div class="modal-body">
                                   <div class="sizer">
                                    <div class="embed-responsive embed-responsive-16by9">
                                      <iframe id="frmMaint" class="embed-responsive-item"></iframe>
                                    </div>
                                   </div>                                             
                                </div>
                                <div class="modal-footer">
                                   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>                               
                              </div>      
                            </div>
                          </div>
                       <!-- Modal --> 
                      </form>                             
                   </div>                       
              </div>
             <%--</div>--%>
             <script type="text/javascript">                
                 var selectID = $("#ContentSection_holderControlID_txtSelectID").val();
                 var url;
                 function clickTab(tab) {
                     if (tab=="1"){
                         url = "t_vendor_register.aspx?t_vendor_register_id=" + selectID;
                     }else if (tab == "2") {
                         url = "t_holder_stock.aspx?t_vendor_register_id=" + selectID;
                     }else if (tab == "3") {
                         url = "t_vendor_director_level.aspx?t_vendor_register_id=" + selectID;
                     }else if (tab == "4") {
                         url = "t_vendor_reg_attachment.aspx?t_vendor_register_id=" + selectID;
                     }
                     $(location).attr('href', url);
                 }
                  window.closeModal = function () {       
                        $("#myModal .close").click();
                   };
                  window.refresh = function () {
                      var selectID = <%= new JavaScriptSerializer().Serialize(Request.QueryString["t_vendor_register_id"]) %>;
                      var url;
                      url = "t_vendor_commissioner_level.aspx?t_vendor_register_id=" + selectID;
                      $(location).attr('href', url);
                  }
            </script>
          </div>
      </div>
    </div>
  <%--  <div class="footer navbar-fixed-bottom" style="background-color:#259fd9;margin-left:16px;margin-right:16px">    
       <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>