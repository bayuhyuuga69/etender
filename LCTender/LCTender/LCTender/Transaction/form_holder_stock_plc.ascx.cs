﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;

namespace LCTender.Transaction
{
    public partial class form_holder_stock_plc : System.Web.UI.UserControl
    {
        private string strCallee;
        private clsutils clsutl = new clsutils();
        int PER_PAGE =20;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                //strCallee = "aceng.koswara2010@gmail.com";
                int intCurrPage = 1;               
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                txtSelectID.Text = Request["t_vendor_register_id"];
                BindDataList(intCurrPage, Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]),0);               
            }

            if (Request["t_vendor_register_id"].ToString() == "0")
            {
                imbAdd.Visible = false;
                lblAdd.Visible = false;
            }
            else
            {
                imbAdd.Visible = true;
                lblAdd.Visible = true;
            }

        }
        protected void BindDataList(int intCurrPage, string strUserName, int intVendorID,int intHolderStockID)
        {
            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            int vendorId = Convert.ToInt32(Request["t_vendor_register_id"]);
            DataSet dsresult = dbcVendorList.db_get_data_holder_stock(intPage, vendorId, strUserName, PER_PAGE, 0, ViewState["filter"].ToString(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;            
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row' style='padding-top:20px'>" +
                  "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NAMA</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>ALAMAT</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>DIBUAT OLEH</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]),imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String VendorRegId = "";
            VendorRegId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    //Response.Write("Vendorid:" + VendorRegId);
                    BindDataList(0, Session["UserName"].ToString(),Convert.ToInt32(Request["t_vendor_register"]), Convert.ToInt32(VendorRegId));
                    break;
                case "Detail":
                    //load_maint(VendorRegId, 2)
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>clickEdit("+ VendorRegId + ",2)</");
                    cstext1.Append("script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    break;
            }
           

        }
       
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblRegVal = (Label)e.Row.FindControl("lblHolderName");
                Label lblVendorVal = (Label)e.Row.FindControl("lblHolderAddr");

                String strReg = "";
                String strVendor = "";

                String regno = (((DataRowView)e.Row.DataItem)["HOLDER_NAME"]).ToString();
                String vendor = (((DataRowView)e.Row.DataItem)["HOLDER_ADDR"]).ToString();
               
                Decimal decStockAmt = Convert.ToDecimal((((DataRowView)e.Row.DataItem)["STOCK_AMT"]).ToString());
                (((DataRowView)e.Row.DataItem)["STOCK_AMT"]) = String.Format("{0:N}", decStockAmt);

                strReg += String.Format(@"<div class='row'>");
                strReg += String.Format(@"<div class='col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4'>{0}</div>", regno.ToUpper());
                strReg += String.Format(@"</div>");

                lblRegVal.Text = strReg;

                strVendor += String.Format(@"<div class='row'>");
                strVendor += String.Format(@"<div class='col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4'>{0}</div>", vendor.ToUpper());
                strVendor += String.Format(@"</div>");

                lblVendorVal.Text = strVendor;               


            }
        }

        public string StrCallee
        {
            get
            {
                return strCallee;
            }
            set
            {
                strCallee = value;
            }
        }

        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]),0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Session["UserName"].ToString(), Convert.ToInt32(Request["t_vendor_register_id"]), 0);
        }

        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>clickAdd(" + 0 + ",1)</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }
    }
}