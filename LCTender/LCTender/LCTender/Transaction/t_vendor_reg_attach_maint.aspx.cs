﻿using System;
using System.Web.UI;
using WsLCTender;
using log4net;
using System.Data;
using System.IO;
using System.Configuration;

namespace LCTender.Transaction
{
    public partial class t_vendor_reg_attach_maint : System.Web.UI.Page
    {
        public static string maxFileSize = ConfigurationManager.AppSettings["maxFileSize"].ToString();      
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_reg_attach_maint));
        private string upDir;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblSize.Text = "Maksimum Ukuran File " + maxFileSize + " MB";
            upDir = Path.Combine(Request.PhysicalApplicationPath, "Upload");
            if (!IsPostBack)
            {

                string strLOV = "";
                string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
                strLOV += String.Format(@"<a href=""javascript:clickLOV()"">{0}</a>", strIMG);

                ltLOV.Text = strLOV;
            }
            
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            // check if a file is being submitted
            if (fileUpload.PostedFile.FileName != "")
            {
                // check extension
                string ext = Path.GetExtension(fileUpload.PostedFile.FileName);
                string contentType = fileUpload.PostedFile.ContentType;
                int fileSize = fileUpload.PostedFile.ContentLength;

                if (fileSize > ((Convert.ToInt32(maxFileSize) * 1024)*1000))
                {
                    lblSize.Text = "Ukuran file terlalu besar. Maksimum ukuran file " + maxFileSize + " MB";
                    return;
                }

                switch (ext.ToLower())
                {
                    case ".png":
                    case ".jpg":
                    case ".jpeg":
                    case ".gif":
                    case ".pdf":
                        break;
                    default:
                        lblError.Text = "Unfortunately the selected file type is not currently supported, sorry...";
                        return;
                }
                // using the following 2 lines of code the file will retain its original name.
                dbcReader dbcSeq = new dbcReader();
                String SeqAttach = dbcSeq.ExecuteReader("Select NEXT VALUE FOR seq_t_reg_attachment_id reg_attach_id", "reg_attach_id");

                string sfn = Path.GetFileName(fileUpload.PostedFile.FileName);
                string NewName = string.Format("{0}{2}-{1}","REGDOC", sfn, SeqAttach);
                string fPath = Path.Combine(upDir, NewName);

                log.Info("Upload Document Started. ");
                try
                {
                    fileUpload.PostedFile.SaveAs(fPath);

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_reg_attachment";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Convert.ToInt32(SeqAttach), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_register_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", fileUpload.PostedFile.FileName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_attachment_type_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", NewName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", fPath, "~#");
                    s_varvalue += String.Format(@"{0}{1}", contentType, "~#");
                    s_varvalue += String.Format(@"{0}{1}", fileSize, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "UPLOAD DOC", " ");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                }
                catch (IOException ex)
                {
                    lblError.Text = "Error uploading file: " + ex.Message;
                    log.ErrorFormat("Error uploading file - Message: {0}", ex.Message);
                    throw;
                }
                catch (Exception er)
                {
                    lblError.Text += "Unknown error: " + er.Message;
                }
            }

        }
    }
}