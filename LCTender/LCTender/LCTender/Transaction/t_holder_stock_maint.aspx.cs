﻿using System;
using System.Web.UI;
using WsLCTender;
using log4net;
using System.Data;

namespace LCTender.Transaction
{
    public partial class t_holder_stock_maint : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_holder_stock_maint));
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                BindDataMaint(Convert.ToInt32(Request["t_vendor_register_id"]), Convert.ToInt32(Request["t_holder_stock_id"]), Convert.ToInt32(Request["type"]));
            }

            
        }
        protected void BindDataMaint(int intVendor, int intIDRow, int intType)
        {
            string strLOV = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV()"">{0}</a>", strIMG);

            ltLOV.Text = strLOV;

            nationality.Attributes.Add("readonly", "readonly");

            btnSave.Attributes.Add("onclick", "return checkValidate();");          

            if (intType==1)
            {
                btnSave.Visible = true;
                percentage.Text = "0";
               // lblRangeValue.Text = percentage.Text + " %";
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                //btnSave.Attributes.Add("onclick", "return checkValidate();");
            }else if (intType == 2)
            {
                int intMaxRec;
                intMaxRec = 0;
                dbcReader dbcStockList = new dbcReader();
                DataSet dsresult = dbcStockList.db_get_data_holder_stock(1, intVendor, Session["UserName"].ToString(), 1, intIDRow, "", ref intMaxRec);
                DataTable dtresult = dsresult.Tables["dtData"];

                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                t_holder_stock_id.Text = dtresult.Rows[0]["t_holder_stock_id"].ToString();
                holder_name.Text = dtresult.Rows[0]["holder_name"].ToString();
                holder_addr.Text = dtresult.Rows[0]["holder_addr"].ToString();
                stock_amt.Text = dtresult.Rows[0]["stock_amt"].ToString();
                percentage.Text = dtresult.Rows[0]["percentage_amt"].ToString();
                //lblRangeValue.Text = percentage.Text + " %";
                nationality.Text = dtresult.Rows[0]["nationality"].ToString();
                p_nationality_id.Text = dtresult.Rows[0]["p_nationality_id"].ToString();

                if (Request["app_level"] != null)
                {
                    btnSave.Visible = false;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                }

            }

            

        }
        protected void percentage_TextChanged(object sender, EventArgs e)
        {
           // lblRangeValue.Text = percentage.Text +" %";
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ( (holder_name.Text != "") && 
                 (stock_amt.Text != "") && 
                 (holder_addr.Text != "") &&
                 (nationality.Text != "") && 
                 (percentage.Text != "") && (Convert.ToInt32(percentage.Text)<=100)
                )
            {

                log.Info("Insert Holder Stock Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_holder_stock";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_register_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_name.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_addr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", stock_amt.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_nationality_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", percentage.Text, "~#");                   
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", description.Text, " ");
                    
                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'","") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
                

            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if ((holder_name.Text != "") &&
                 (stock_amt.Text != "") &&
                 (holder_addr.Text != "") &&
                 (nationality.Text != "") &&
                 (percentage.Text != "") && (Convert.ToInt32(percentage.Text) <= 100)
                )
            {

                log.Info("Update Holder Stock Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_holder_stock";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_holder_stock_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_register_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_name.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_addr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", stock_amt.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_nationality_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", percentage.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", description.Text, "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if ((holder_name.Text != "") &&
                 (stock_amt.Text != "") &&
                 (holder_addr.Text != "") &&
                 (nationality.Text != "") &&
                 (percentage.Text != "") && (Convert.ToInt32(percentage.Text) <= 100)
                )
            {

                log.Info("Update Holder Stock Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_holder_stock";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_holder_stock_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_register_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_name.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", holder_addr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", stock_amt.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_nationality_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", percentage.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", description.Text, "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }
    }
}