﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="t_vendor_commissioner_level_maint.aspx.cs" Inherits="LCTender.Transaction.t_vendor_commissioner_level_maint" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background: #ffffff;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 0px 0px 0px 0px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }
    
    
</style>
<script type="text/javascript">
    window.closeModal = function () {
        $("#myModalFrame .close").click();
    };
    window.closeModalJob = function () {
        $("#myModalFrameJob .close").click();
    };
</script>
</head>
<body>
  <form runat="server"> 
      <!-- Modal --> 
      <div class="form-row">    
          <div class="form-group col-sm-6">
            <label for="lbl_commissioner_name" class="control-label">Nama :</label>
            <asp:TextBox ID="commissioner_name" class="form-control" name="commissioner_name" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvcommissionerName" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Nama" Display="Dynamic" ControlToValidate="commissioner_name"></asp:RequiredFieldValidator>
          </div>          
         <div class="form-group col-sm-6">
             <label for="vendor_job_position" class="control-label">Jabatan:</label>            
             <div class="input-group">
                <asp:TextBox ID="vendor_job_position" class="form-control" name="vendor_job_position" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_vendor_job_position_id" class="hidden" name="p_vendor_job_position_id" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvVendorJobPosition" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Jabatan" Display="Dynamic" ControlToValidate="vendor_job_position"></asp:RequiredFieldValidator>
                <div class="input-group-addon">
                   <asp:Literal ID="ltLOVJob" runat="server"></asp:Literal>
                </div>   
            </div>
        </div>
      </div>
      <div class="form-group col-sm-12">
        <label for="lbl_commissioner_addr" class="control-label">Alamat:</label>          
        <asp:TextBox ID="commissioner_addr" class="form-control" textmode="MultiLine" name="commissioner_addr" runat="server"></asp:TextBox> 
        <asp:RequiredFieldValidator ID="rvcommissionerAddr" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Alamat" Display="Dynamic" ControlToValidate="commissioner_addr"></asp:RequiredFieldValidator>              
       </div>   
      <div class="form-row">
        <div class="form-group col-sm-6">
          <label for="nationality" class="control-label">Kebangsaan:</label>          
           <div class="input-group">
                <asp:TextBox ID="nationality" class="form-control" name="nationality" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_nationality_id" class="hidden" name="p_nationality_id" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvNationality" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Kebangsaan" Display="Dynamic" ControlToValidate="nationality"></asp:RequiredFieldValidator>
                <div class="input-group-addon">
                   <asp:Literal ID="ltLOV" runat="server"></asp:Literal>
                </div>   
            </div>
        </div>
        <div class="form-group col-sm-6">
            <label for="lbl_percent" class="control-label">Persentase:</label>
            <asp:TextBox ID="percentage" class="form-control" name="percentage" textmode="Number" runat="server"></asp:TextBox>
            <asp:Label ID="lblRangeValue" runat="server"></asp:Label>
            <asp:RequiredFieldValidator ID="rfPercentage" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Persentase" Display="Dynamic" ControlToValidate="percentage"></asp:RequiredFieldValidator>     
            <asp:RegularExpressionValidator ID="rePercentage" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Angka dari 0-100" ControlToValidate="percentage" ValidationExpression="(?:\b|-)([1-9]{1,5}[0]?|100)\b"></asp:RegularExpressionValidator>
        </div>       
       </div>
      <div class="form-group col-sm-12">
        <label for="descr" class="control-label">Keterangan:</label>          
        <asp:TextBox ID="description" class="form-control" name="description" textmode="MultiLine" runat="server"></asp:TextBox>   
       </div>         
       <div class="form-row">
        <div class="form-group  col-sm-12">            
            <asp:TextBox ID="t_vendor_register_id" class="hidden" name="t_vendor_register_id" runat="server"></asp:TextBox>
            <asp:TextBox ID="t_vendor_commissioner_level_id" class="hidden" name="t_vendor_commissioner_level_id" runat="server"></asp:TextBox>
            <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Simpan" OnClick="btnSave_Click" />&nbsp; 
            <asp:Button ID="btnUpdate" class="btn btn-primary" runat="server" Text="Update" OnClick="btnUpdate_Click" />&nbsp;
            <asp:Button ID="btnDelete" class="btn btn-primary" runat="server" Text="Hapus" OnClick="btnDelete_Click" />
        </div>
       </div>
    <!-- Modal -->          
  </form>
    <!-- iframe -->
    <div class="modal fade" id="myModalFrame" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Nationality</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOV" class="embed-responsive-item" src="../Lov/p_nationality.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <!-- iframe -->    
     <!-- iframe -->
    <div class="modal fade" id="myModalFrameJob" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Job Position</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOVJob" class="embed-responsive-item" src="../Lov/p_vendor_job_position.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <!-- iframe -->   
</body>
</html>
<script type="text/javascript">
    //debugger;
    var nameVal = false, addrVal = false;
    var percentVal = false;

    $('[id*=myModalFrame]').modal('hide');
   
    function clickLOV() {
        $('#myModalFrame').modal('show');
    }

    $('[id*=myModalFrameJob]').modal('hide');

    function clickLOVJob() {
        $('#myModalFrameJob').modal('show');
    }
   
    function checkValidate() {
            $(function () {

                    var holderVal = $("#commissioner_name").val();
                    $("#commissioner_name").closest('div').toggleClass('has-error', holderVal == "");
                  
                    var addrVal = $("#commissioner_addr").val();
                    $("#commissioner_addr").closest('div').toggleClass('has-error', addrVal == "");

                    var jobVal = $("#vendor_job_position").val();
                    $("#vendor_job_position").closest('div').toggleClass('has-error', jobVal == "");
                                       
                    var nationVal = $("#nationality").val();
                    $("#nationality").closest('div').toggleClass('has-error', nationVal == "");
                   
                    var percentVal = $("#percentage").val();
                    $("#percentage").closest('div').toggleClass('has-error', percentVal == "");
                   
            });
    }
   
</script>