﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Monitoring
{
    public partial class monitoring_qvl : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 10;
        protected static readonly ILog log = LogManager.GetLogger(typeof(monitoring_qvl));
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["t_vendor_register_id"] == null)
                {
                    BindDataList(intCurrPage, 0);

                }
                else
                {
                    BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_register_id"]));
                }

            }
            
        }
        protected void BindDataList(int intCurrPage, int intVendorID)
        {
            pnlFilter.Visible = true;
            pnlGridRegister.Visible = true;

            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_data_vendor_monit(intPage, Session["UserName"].ToString(), PER_PAGE, 0, ViewState["filter"].ToString().Trim(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;
            gvTask.DataSource = dtresult;
            gvTask.DataBind();


            for (int i = 0; i < dtresult.Rows.Count; i++)
            {

                ImageButton imbRdoSelect = (ImageButton)gvTask.Rows[i].FindControl("imbRdoRow");

                Decimal decJmlBatasBawah = Convert.ToDecimal(gvTask.Rows[i].Cells[7].Text);
                gvTask.Rows[i].Cells[7].Text = String.Format("{0:n0}", decJmlBatasBawah);
                Decimal decJmlBatasanAtas = Convert.ToDecimal(gvTask.Rows[i].Cells[8].Text);
                gvTask.Rows[i].Cells[8].Text = String.Format("{0:n0}", decJmlBatasanAtas);

                if (intVendorID == 0)
                {
                    if (i == 0)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        // gvTask.Rows[i].CssClass = "Row";
                    }
                    intVendorID = Convert.ToInt32(gvTask.Rows[i].Cells[0].Text);
                }
                else
                {
                    if (Convert.ToInt32(gvTask.Rows[i].Cells[0].Text) == intVendorID)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        //gvTask.Rows[i].CssClass = "AltRow";
                    }
                }


            }
            txtSelectID.Text = intVendorID.ToString();

            BindDataCtl(intVendorID);

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>REGISTER NO</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VENDOR NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>CATEGORY</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>BATASAN HARGA</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }


            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]), imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }

        protected void BindDataCtl(int intVendorID)
        {
            pnlFilter.Visible = true;
            pnlGridRegister.Visible = true;

            dbcReader dbcVendorList = new dbcReader();
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_register_ctl(intVendorID);
            DataTable dtresult = dsresult.Tables[0];
            gvTaskCtl.DataSource = dtresult;
            gvTaskCtl.DataBind();
            
            if (dtresult.Rows.Count == 0)
            {
                lblNoRowActivity.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>TASK</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PROFILE</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>STATUS</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>RECEIVED BY</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRowActivity.Text = "";
            }
            
        }
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Label lblTenderNo = (Label)e.Row.FindControl("lblTenderNo");
                //Label lblProjectName = (Label)e.Row.FindControl("lblProjectName");
                //Label lblEstimate = (Label)e.Row.FindControl("lblEstimate");
                //Label lblOffering = (Label)e.Row.FindControl("lbOffering");
            }
        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String VendorId = "";
            VendorId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(VendorId));
                    break;
                case "Detail":
                    //load_maint(VendorRegId, 2)
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>clickEdit(" + VendorId + ",2)</");
                    cstext1.Append("script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    break;
            }
            txtSelectID.Text = VendorId;

        }       
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

    }
}