﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="monitoring_register_attach.aspx.cs" Inherits="LCTender.Monitoring.monitoring_register_attach" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register src="../Main/content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
    }
    .column-in-center{
        float: none;
        margin: 0 auto;
    }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
	    padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    color: #000000;
	}
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li><a href="javascript:clickTab(1)" style="font:15px Tahoma;">Monitoring QVL</a></li>            
              <li class="active"><a data-toggle="tab" href="#reg_attachment" style="font:15px Tahoma;">Dokumen</a></li>              
          </ul>
          <div class="tab-content">  
             <div class="main-div"> 
                 <div id="reg_attachment" class="tab-pane fade in active">                       
                      <form runat="server" >
                          <div class="table-responsive">
                             <div class="row">
                                <div class="col-sm-12">
                                    <div class="input-group input-group-sm">
                                        <label for="email" class="col-sm-2">Filter:</label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="btn btn-default glyphicon glyphicon-search">
                                               <asp:Button ID="btnCari" BackColor="Transparent" runat="server" BorderWidth="0" Text="Cari" OnClick="btnCari_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <asp:Panel ID="pnlGridVend" runat="server">
                                        <asp:GridView ID="gvTask" runat="server" Width="100%"
                                            CssClass="mGrid table table-sm"
                                            PagerStyle-CssClass="pgr"
                                            AlternatingRowStyle-CssClass="alt"
                                            CellPadding="5" CellSpacing="4"
                                            AutoGenerateColumns="False"
                                            OnRowDataBound="gvTask_RowDataBound"
                                            OnRowCommand="gvTask_RowCommand" PageSize="3">
                                            <AlternatingRowStyle CssClass="alt" />
                                            <Columns>
                                                <asp:BoundField DataField="T_VENDOR_REG_ATTACHMENT_ID" HeaderText="T_VENDOR_REG_ATTACHMENT_ID" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                                                    <ItemStyle CssClass="hidden" />
                                                    <HeaderStyle CssClass="hidden" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILE_DIR" HeaderText="FILE DIR" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                                                    <ItemStyle CssClass="hidden" />
                                                    <HeaderStyle CssClass="hidden" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FILE_TYPE" HeaderText="FILE TYPE" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                                                    <ItemStyle CssClass="hidden" />
                                                    <HeaderStyle CssClass="hidden" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" visible="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderEdit" runat="server" Text=""></asp:Label>
                                                        <br />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imbRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Hapus" ImageUrl="~/images/delete.gif" alt="hapus" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FILE_NAME" HeaderText="FILE NAME" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                                                    <ItemStyle CssClass="hidden-phone" Wrap="True" />
                                                </asp:BoundField>            
                                                <asp:BoundField DataField="ATTACHMENT_TYPE" HeaderText="ATTACHMENT TYPE" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                                                    <ItemStyle CssClass="hidden-phone" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CREATED_BY" HeaderText="CREATED BY" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                                                    <ItemStyle CssClass="hidden-phone" Wrap="True" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="NAMA FILE" ItemStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFileName" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="visible-desktop" />
                                                    <ItemStyle CssClass="visible-desktop" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="JENIS BERKAS" ItemStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFileType" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="visible-desktop" />
                                                    <ItemStyle CssClass="visible-desktop" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="DIBUAT OLEH" ItemStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="visible-desktop" />
                                                    <ItemStyle CssClass="visible-desktop" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="3" />
                                            <PagerStyle CssClass="pgr" />
                                        </asp:GridView>
                                        <asp:Label ID="lblNoRow" runat="server"></asp:Label>
                                        <div class="Row">
                                             <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse: collapse; padding: 2px; border: solid 1px #c1c1c1; color: #717171;">
                                                 <asp:ImageButton ID="imbFirst" runat="server" visible="false" OnClick="imbFirst_Click"/>
                                                <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click" />
                                                &nbsp;
                                                <asp:Label ID="lblPaging" runat="server"></asp:Label>
                                                &nbsp;
                                                <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" Style="width: 14px" />
                                                <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                   </div>
                                </div>
                              <asp:TextBox ID="txtSelectID" class="hidden" runat="server"></asp:TextBox>
                            </div>
                      </form>                             
                   </div>                       
              </div>
             <script type="text/javascript">                
                 var selectID = "<%= Request.QueryString.ToString() %>";
                 var url;
                 function clickTab(tab) {
                     if (tab=="1"){
                         url = "monitoring_qvl.aspx?" + selectID;
                     }
                     $(location).attr('href', url);
                 }
                 function clickEdit(id, type) {
                        var vendorId = "<%=Request["t_vendor_register_id"]%>";
                        $('#frmMaint').attr('src', "t_vendor_reg_attach_maint.aspx?t_vendor_register_id=" + vendorId + "&t_vendor_commissioner_level_id=" + id + "&type=" + type);
                        $('#myModal').modal('show');
                 }
                 function clickAdd(id, type) {
                        var vendorId = "<%=Request["t_vendor_register_id"]%>";
                        $('#frmMaint').attr('src', "t_vendor_reg_attach_maint.aspx?t_vendor_register_id=" + vendorId + "&t_vendor_commissioner_level_id=" + id + "&type=" + type);
                        $('#myModal').modal('show');
                 }
                 function refresh (RegId) {
                        var url = "t_vendor_reg_attachment.aspx?t_vendor_register_id=" + RegId;
                        $(location).attr('href', url);
                 }
                 function clickView(filename,filetype) {
                        var url = "../Wf/fileviewer.aspx?FILE_NAME=" + filename + "&FILE_TYPE=" + filetype;
                        document.location.href = url;
                 }
            </script>
          </div>
      </div>
    </div>
  <%--  <div class="row">
       <div class="col-sm-12" style="background-color:#259fd9;"><entityCtl:entityControl ID="entityControlID" runat="server"/></div>    
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>