﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="t_vendor_register_maint_ro.aspx.cs" Inherits="LCTender.Monitoring.t_vendor_register_maint_ro" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background: #ffffff;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 0px 0px 0px 0px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }
    
    
</style>
<script type="text/javascript">
    window.closeModal = function () {
        $("#myModalFrame .close").click();
    };
    window.closeModalBank = function () {
        $("#myModalFrmBank .close").click();
    };
    window.closeModalSubmitter = function () {
        $("#myModalFrmSubmitter .close").click();
        window.parent.closeModal();
        window.parent.refreshframe();
    };
    
</script>
</head>
<body>
  <form runat="server"> 
      <!-- Modal --> 
      <div class="form-group col-sm-12">
        <label for="entity" class="control-label">Entity:</label>
        <asp:DropDownList ID="s_entityId" cssclass="form-control" runat="server"></asp:DropDownList><br/>
      </div>
      <div class="form-row">    
          <div class="form-group required col-sm-4">
            <label for="vendName" class="control-label">Nama Vendor:</label>
            <asp:TextBox ID="vendName" cssclass="form-control" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvVendName" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Nama" Display="Dynamic" ControlToValidate="vendName"></asp:RequiredFieldValidator>
          </div>
          <div class="form-group required col-sm-4">
            <label for="npwpNo" class="control-label">No NPWP:</label>          
            <asp:TextBox ID="npwpNo" cssclass="form-control" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfNpwpNo" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan No NPWP" Display="Dynamic" ControlToValidate="npwpNo"></asp:RequiredFieldValidator>  
            <asp:RegularExpressionValidator ID="reNpwpNo" runat="server" ForeColor="#ff0000" ErrorMessage="No NPWP harus terdiri dari 15 Karakter" ControlToValidate="npwpNo" ValidationExpression="^[0-9]{15}$"></asp:RegularExpressionValidator>          
          </div>
         <div class="form-group required col-sm-4">
            <label for="email" class="control-label">Email:</label>         
            <asp:TextBox ID="email" cssclass="form-control" textmode="Email" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvEmail" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Email" Display="Dynamic" ControlToValidate="npwpNo"></asp:RequiredFieldValidator>     
         </div>
      </div>
      <div class="form-group required col-sm-12">
        <label for="officeAddr" class="control-label">Alamat Kantor:</label>
        <asp:TextBox ID="officeAddr" cssclass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rvOfficeAddr" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Alamat Kantor" Display="Dynamic" ControlToValidate="officeAddr"></asp:RequiredFieldValidator>
      </div>    
      <div class="form-group required col-sm-12">
         <label for="siteAddr" class="control-label">Alamat Situs:</label>
         <asp:TextBox ID="siteAddr" cssclass="form-control" runat="server" TextMode="Url"></asp:TextBox>
         <asp:RequiredFieldValidator ID="rvsiteAddr" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Alamat Situs" Display="Dynamic" ControlToValidate="siteAddr"></asp:RequiredFieldValidator>
       </div>
      <div class="form-row">
        <div class="form-group required col-sm-4">
            <label for="phoneNo" class="control-label">Telephone:</label>
            <asp:TextBox ID="phoneNo" cssclass="form-control" runat="server" onkeypress="javascript:return checkIt(event)"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvPhoneNo" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan No.Telephone" Display="Dynamic" ControlToValidate="phoneNo"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group col-sm-4">
            <label for="faxNo" class="control-label">Fax:</label>
            <asp:TextBox ID="faxNo" cssclass="form-control" runat="server" onkeypress="javascript:return checkIt(event)"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rvFaxNo" runat="server"  ForeColor="#ff0000" ErrorMessage="Masukkan No.Fax" Display="Dynamic" ControlToValidate="faxNo"></asp:RequiredFieldValidator>--%>
        </div>
        <div class="form-group required col-sm-4">
            <label for="dtCompany" class="control-label">Tgl Berdiri:</label>            
            <div class="input-group date">
                <asp:TextBox ID="dtNotary" cssclass="form-control" runat="server" ></asp:TextBox>                
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>               
            </div>
            <asp:RequiredFieldValidator ID="rvdtNotary" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Tgl.Berdiri" Display="Dynamic" ControlToValidate="dtNotary"></asp:RequiredFieldValidator>
         </div>
       </div> 
       <div class="form-row">
         <div class="form-group required col-sm-6">
             <label for="typBussiness" class="control-label">Bidang Usaha:<asp:Literal ID="ltLOV" runat="server"></asp:Literal></label><br/>  
             <asp:Label ID="lblCheck" runat="server"></asp:Label>                    
             <asp:TextBox ID="typBussiness" cssclass="form-control" runat="server" style="display:none"></asp:TextBox>                    
             <asp:TextBox ID="typBussiness_id" cssclass="hidden" runat="server"></asp:TextBox>
            <%-- <asp:RequiredFieldValidator ID="rvtypBussiness" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Bidang Usaha" Display="Dynamic" ControlToValidate="typBussiness_id"></asp:RequiredFieldValidator>--%>
         </div> 
         <div class="form-group required col-sm-6">
            <label for="nmBank" class="control-label">Bank:</label>
            <div class="input-group">
                <asp:TextBox ID="nmBank" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="nmBank_id" style="display:none" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvnmBank" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Bank" Display="Dynamic" ControlToValidate="nmBank_id"></asp:RequiredFieldValidator>  
                <div class="input-group-addon">
                    <asp:Literal ID="ltLOVBank" runat="server"></asp:Literal>
                </div>   
            </div>             
         </div> 
       </div>   
       <div class="form-row">    
        <div class="form-group required col-sm-6">
            <label for="accNo" class="control-label">No Rekening:</label>
            <asp:TextBox ID="accNo" cssclass="form-control" runat="server" onkeypress="javascript:return checkIt(event)"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvaccNo" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan No.Rekening" Display="Dynamic" ControlToValidate="accNo"></asp:RequiredFieldValidator>
        </div>   
        <div class="form-group required col-sm-6">
            <label for="accOwner" class="control-label">Pemilik Rekening:</label>
            <asp:TextBox ID="accOwner" cssclass="form-control" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rvaccOwner" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Pemilik Rekening" Display="Dynamic" ControlToValidate="accOwner"></asp:RequiredFieldValidator>
        </div> 
       </div> 
      <div class="form-group required col-sm-12">
        <label for="costlimitMin" class="control-label" style="align-content:center">Cost Limit:</label>
        <asp:DropDownList ID="s_ddlCostLimit" class="form-control" runat="server"></asp:DropDownList>
       <%-- <label for="costlimit" class="control-label" style="align-content:center">Cost Limit:</label>
        <asp:TextBox ID="cost_limit_amt" cssclass="form-control" runat="server" onkeypress="javascript:return checkIt(event)" onkeyup="javascript:formatCurrency(this)"></asp:TextBox>--%>
        <%--<asp:RequiredFieldValidator ID="rvcostlimit" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Cost Limit" Display="Dynamic" ControlToValidate="cost_limit_amt"></asp:RequiredFieldValidator>--%>
       <%-- <asp:RegularExpressionValidator ID="recostlimit" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Angka" ControlToValidate="cost_limit_amt" ValidationExpression="^(\d*\.?\d+|\d*(,\d*)*(\,\d+)?)$"></asp:RegularExpressionValidator>--%>
      </div>
     <%-- <div class="form-row">
        <div class="form-group col-sm-6">               
          <label for="costlimitMin" class="control-label" style="align-content:center">Mulai Dari:</label>
          <asp:DropDownList ID="s_ddlminCostLimit" name="s_ddlminCostLimit" class="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
          <asp:RequiredFieldValidator ID="rvcostlimitMin" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Cost Limit Minimum" Display="Dynamic" ControlToValidate="s_ddlminCostLimit"></asp:RequiredFieldValidator>
        </div>         
        <div class="form-group col-sm-6">
          <label for="costlimitMax" class="control-label">Sampai Dengan:</label>
          <asp:DropDownList ID="s_ddlmaxCostLimit" name="s_ddlmaxCostLimit" class="form-control" runat="server" AutoPostBack="True"></asp:DropDownList>
           <asp:RequiredFieldValidator ID="rvcostlimitMax" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Cost Limit Maximun" Display="Dynamic" ControlToValidate="s_ddlmaxCostLimit"></asp:RequiredFieldValidator>
        </div> 
      </div>--%>
      <div class="form-group col-sm-12">
            <asp:Button ID="btnSave" cssclass="btn btn-primary" runat="server" Text="Simpan" OnClick="btnSave_Click" OnClientClick="return clickCheckBizType()"/>
            <asp:Button ID="btnUpdate" cssclass="btn btn-primary" runat="server" Text="Update" OnClick="btnUpdate_Click" OnClientClick="return clickCheckBizType()"/>
            <asp:Button ID="btnDelete" cssclass="btn btn-primary" runat="server" Text="Hapus" OnClick="btnDelete_Click" OnClientClick="return clickCheckBizType()"/>
            <asp:Literal ID="ltWorkFlow" runat="server"></asp:Literal>
            <asp:TextBox ID="t_vendor_register_id" cssclass="hidden" runat="server"></asp:TextBox>
            <asp:TextBox ID="request_status" cssclass="hidden" runat="server"></asp:TextBox>
            <asp:TextBox ID="p_organization_id" cssclass="hidden" runat="server"></asp:TextBox>
            <asp:TextBox ID="p_app_user_id" cssclass="hidden" runat="server"></asp:TextBox>
            <asp:TextBox ID="user_name" cssclass="hidden" runat="server"></asp:TextBox>
     </div>
    <!-- Modal -->    
  </form>
    <!-- iframe -->
    <div class="modal fade" id="myModalFrame" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">List Business Type</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOV" class="embed-responsive-item" src="../Lov/p_business_type.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <!-- iframe --> 
    <!-- iframe -->
    <div class="modal fade" id="myModalFrmBank" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">List Bank</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOVBank" class="embed-responsive-item" src="../Lov/p_bank.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <asp:Label ID="ltSubmitter" runat="server"></asp:Label>
</body>
</html>
<script type="text/javascript">
    //debugger;
    $('[id*=myModalFrame]').modal('hide');
    $('[id*=myModalFrmBank]').modal('hide');
    $('[id*=myModalFrmSubmitter]').modal('hide');
    
    $('.input-group.date').datepicker({
        format: "dd-M-yyyy",
        autoclose: true
    });
    function clickLOV() {
        $('[id*=myModalFrame]').modal('show');
    }
    function clickLOVBank() {
        $('[id*=myModalFrmBank]').modal('show');
    }
    function clickLoadSubmitter() {
        //debugger;
        bootbox.prompt({
        title: "<table><tr> " +
                        "<td><img class='img-responsive' src='../images/icon/Asset 17@4x.png' width=100% height=100%></td> " +
                        "<td style='font-size:14px;padding:10px 5px'>Vendor dengan ini menyatakan dan menjamin bahwa seluruh dokumen-dokumen " +
                            "dan/atau data-data/informasi yang disampaikan dalam formulir Pengkinian Data " +
                            "Vendor ini adalah benar dan sesuai dengan dokumen-dokumen aslinya, dan dengan demikian " +
                            "Vendor bertanggung jawab sepenuhnya atas kebenaran atau keakuratan data-data, informasi-informasi " +
                            "dan/atau dokumen-dokumen tersebut, serta membebaskan penerima dokumen dari segala tuntutan, gugatan, " +
                            "dan/atau ganti kerugian dalam bentuk apapun, termasuk apabila dikemudian hari terdapat sanggahan atas " +
                            "kewenangan penandatangan formulir Pengkinian Data Vendor ini. " +
                        "</td>  " +
                    "</tr></table>",
            inputType: 'checkbox',
            inputOptions: [
                {
                    text: 'Agreed',
                    value: '1',
                }
            ],
            callback: function (result) {
                if (result=="1") {
                    $('[id*=myModalFrmSubmitter]').modal('show');
                }
            }
        }).find("div.modal-dialog").css({ "width": "100%", "height": "100%" });
        
    }
    function formatCurrency(event) {
      
        var val = event.value;
        val = val.replace(/,/g, "")
        event.value = "";
        val += '';
        x = val.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';

        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }

        event.value = x1 + x2;
    }
   function checkIt(evt) {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57)) {
           return false
       }
       return true
   }
   
   function clickCheckBizType() {
       c = 0;
       i = document.getElementsByName("chkBizType").length;
       if (i == 1) {
           if (document.getElementById("chkBizType").checked) {
               c = 1;
           }
       } else {
           for (x = 0; x < i; x++) {
               if (document.getElementsByName("chkBizType")[x].checked) {
                   c = 1;
                   break;
               }
           }
       }

       if (c == 0) {
           bootbox.alert({
               message: 'Pilih Item Bidang Usaha..!!',
               className: 'bb-alternate-modal'
           });                        
           return false;
       } else {
           return true;
       }

   }

    function checkValidate() {
          $(function () {

                var vnVal = $("#vendName").val();
                $("#vendName").closest('div').toggleClass('has-error', vnVal == "");
                if (vnVal == "") {
                    $("#ContentSection_vendName").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_vendName").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var npVal = $("#npwpNo").val();
                $("#npwpNo").closest('div').toggleClass('has-error', npVal == "");
                if (npVal == "") {
                    $("#ContentSection_npwpNo").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_npwpNo").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var ofAddVal = $("#officeAddr").val();
                $("#officeAddr").closest('div').toggleClass('has-error', ofAddVal == "");
                if (ofAddVal == "") {
                    $("#ContentSection_officeAddr").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_officeAddr").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var mailVal = $("#email").val();
                $("#email").closest('div').toggleClass('has-error', mailVal == "");
                if (mailVal == "") {
                    $("#ContentSection_email").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_email").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var siteAddVal = $("#siteAddr").val();
                $("#siteAddr").closest('div').toggleClass('has-error', siteAddVal == "");
                if (siteAddVal == "") {
                    $("#ContentSection_siteAddr").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_siteAddr").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var phoneNoVal = $("#phoneNo").val();
                $("#phoneNo").closest('div').toggleClass('has-error', phoneNoVal == "");
                if (phoneNoVal == "") {
                    $("#ContentSection_phoneNo").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_phoneNo").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var faxNoVal = $("#faxNo").val();
                $("#faxNo").closest('div').toggleClass('has-error', faxNoVal == "");
                if (faxNoVal == "") {
                    $("#ContentSection_faxNo").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_faxNo").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var dtNotVal = $("#dtNotary").val();
                $("#dtNotary").closest('div').toggleClass('has-error', dtNotVal == "");
                if (dtNotVal == "") {
                    $("#ContentSection_dtNotary").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_dtNotary").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                //var tyBizVal = $("#typBussiness_id").val();
                //$("#typBussiness_id").closest('div').toggleClass('has-error', tyBizVal == "");
                //if (tyBizVal == "") {
                //    $("#ContentSection_typBussiness_id").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                //} else {
                //    $("#ContentSection_typBussiness_id").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                //}

                //var tyBankVal = $("#nmBank_id").val();
                //$("#nmBank_id").closest('div').toggleClass('has-error', tyBankVal == "");
                //if (tyBankVal == "") {
                //    $("#ContentSection_nmBank_id").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                //} else {
                //    $("#ContentSection_nmBank_id").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                //}

                var accVal = $("#accNo").val();
                $("#accNo").closest('div').toggleClass('has-error', accVal == "");
                if (accVal == "") {
                    $("#ContentSection_accNo").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_accNo").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var ownerAccVal = $("#accOwner").val();
                $("#accOwner").closest('div').toggleClass('has-error', ownerAccVal == "");
                if (ownerAccVal == "") {
                    $("#ContentSection_accOwner").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_accOwner").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

                var costVal = $("#cost_limit_amt").val();
                $("#cost_limit_amt").closest('div').toggleClass('has-error', ownerAccVal == "");
                if (ownerAccVal == "") {
                    $("#ContentSection_cost_limit_amt").after('<span class="help-block has-error-required" style="display: block;">fill out this field</span>');
                } else {
                    $("#ContentSection_cost_limit_amt").after('<span class="help-block has-error-required" style="display: none;">fill out this field</span>');
                }

            });
    }
       
</script>