﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Monitoring
{
    public partial class monitoring_tender_attach : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 10;
        protected static readonly ILog log = LogManager.GetLogger(typeof(monitoring_tender_attach));
        private string upDir;
        protected void Page_Load(object sender, EventArgs e)
        {
            upDir = Path.Combine(Request.PhysicalApplicationPath, "Upload");
            if (!IsPostBack)
            {

                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                txtSelectID.Text = Request["t_vendor_tender_id"];
                BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_tender_id"]));

            }
            
        }

        protected void BindDataList(int intCurrPage, int intTenderID)
        {
            pnlFilter.Visible = true;
            pnlGridVend.Visible = true;
            pnlMaintTender.Visible = false;
            dbcReader dbcUserName = new dbcReader();
            string strUserName = dbcUserName.ExecuteReader("select created_by from t_vendor_tender where t_vendor_tender_id=" + Request["t_vendor_tender_id"].ToString() + "", "created_by");
            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            int vendorId = Convert.ToInt32(Request["t_vendor_tender_id"]);
            DataSet dsresult = dbcVendorList.db_get_data_reg_attach_tender(intPage, intTenderID, strUserName, PER_PAGE, 0, ViewState["filter"].ToString(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NAMA FILE</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>DESKRIPSI</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>DIBUAT OLEH</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]), imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String AttachmentId = "";
            AttachmentId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            String FileDir = "";
            FileDir = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[1].Text;
            switch (e.CommandName)
            {
                case "Hapus":

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_tender_attachment";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Convert.ToInt32(AttachmentId), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_tender_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "1", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "DEL", "~#");
                    s_varvalue += String.Format(@"{0}{1}", 0, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "DELETE DOC", " ");

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Terhapus',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());

                        File.Delete(FileDir);
                        FileInfo fInfoEvent;
                        fInfoEvent = new FileInfo(FileDir);
                        fInfoEvent.Delete();                        
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                    break;
            }


        }

        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblFileNameVal = (Label)e.Row.FindControl("lblFileName");
                Label lblFileTypeVal = (Label)e.Row.FindControl("lblFileType");
                Label lblCreatedVal = (Label)e.Row.FindControl("lblCreatedBy");


                String strFileName = "";
                String strFileType = "";
                String strCreated = "";

                String FileName = (((DataRowView)e.Row.DataItem)["FILE_NAME"]).ToString();
                String FileDir = (((DataRowView)e.Row.DataItem)["FILE_DIR"]).ToString();
                String FileMime = (((DataRowView)e.Row.DataItem)["FILE_TYPE"]).ToString();
                String FileType = (((DataRowView)e.Row.DataItem)["ATTACHMENT_TYPE"]).ToString();
                String created = (((DataRowView)e.Row.DataItem)["CREATED_BY"]).ToString();

                strFileName += String.Format(@"<div class='row'>");
                strFileName += String.Format(@"<div class='col-sm-2'><a href=""javascript:clickView('{1}','{2}')""><span class='glyphicon glyphicon-download'>{0}</span></a></div>", FileName.ToUpper(), FileDir, FileMime);
                strFileName += String.Format(@"</div>");

                lblFileNameVal.Text = strFileName;

                strFileType += String.Format(@"<div class='row'>");
                strFileType += String.Format(@"<div class='col-sm-3'>{0}</div>", FileType.ToUpper());
                strFileType += String.Format(@"</div>");

                lblFileTypeVal.Text = strFileType;

                strCreated += String.Format(@"<div class='row'>");
                strCreated += String.Format(@"<div class='col-sm-3'>{0}</div>", created.ToUpper());
                strCreated += String.Format(@"</div>");

                lblCreatedVal.Text = strCreated;


            }
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridVend.Visible = false;
            pnlMaintTender.Visible = true;
            string strLOV = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/p_attachment_type_tender.aspx");

            ltLOV.Text = strLOV;
        }

        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
        }

        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0, 1);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // check if a file is being submitted
            if (fileUpload.PostedFile.FileName != "")
            {
                // check extension
                string ext = Path.GetExtension(fileUpload.PostedFile.FileName);
                string contentType = fileUpload.PostedFile.ContentType;
                int fileSize = fileUpload.PostedFile.ContentLength;

                switch (ext.ToLower())
                {
                    case ".png":
                    case ".jpg":
                    case ".jpeg":
                    case ".gif":
                    case ".pdf":
                        break;
                    default:
                        //lblError.Text = "Unfortunately the selected file type is not currently supported, sorry...";
                        return;
                }
                // using the following 2 lines of code the file will retain its original name.
                dbcReader dbcSeq = new dbcReader();
                String SeqAttach = dbcSeq.ExecuteReader("Select NEXT VALUE FOR seq_t_tdr_attachment_id tdr_attach_id", "tdr_attach_id");

                string sfn = Path.GetFileName(fileUpload.PostedFile.FileName);
                string NewName = string.Format("{0}{2}-{1}", "TENDERDOC", sfn, SeqAttach);
                string fPath = Path.Combine(upDir, NewName);

                log.Info("Upload Document Started. ");
                try
                {
                    fileUpload.PostedFile.SaveAs(fPath);

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_tender_attachment";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Convert.ToInt32(SeqAttach), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["t_vendor_tender_id"], "~#");
                    s_varvalue += String.Format(@"{0}{1}", fileUpload.PostedFile.FileName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_attachment_type_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", NewName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", fPath, "~#");
                    s_varvalue += String.Format(@"{0}{1}", contentType, "~#");
                    s_varvalue += String.Format(@"{0}{1}", fileSize, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "UPLOAD DOC", " ");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["t_vendor_tender_id"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                }
                catch (IOException ex)
                {
                    //lblError.Text = "Error uploading file: " + ex.Message;
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("bootbox.alert({ ");
                    cstext1.Append("message: 'Error uploading file: " + ex.Message.ToString().Replace("'", "") + "',");
                    cstext1.Append("className: 'bb-alternate-modal' });");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    log.ErrorFormat("Error uploading file - Message: {0}", ex.Message);
                    throw;
                }
                catch (Exception er)
                {
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("bootbox.alert({ ");
                    cstext1.Append("message: 'Unknown error: " + er.Message.ToString().Replace("'", "") + "',");
                    cstext1.Append("className: 'bb-alternate-modal' });");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    log.ErrorFormat("Unknown error: {0}", er.Message);
                }
            }

        }

    }
}