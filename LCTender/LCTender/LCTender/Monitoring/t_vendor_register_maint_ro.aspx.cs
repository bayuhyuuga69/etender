﻿using System;
using System.Web.UI;
using WsLCTender;
using log4net;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using Newtonsoft.Json.Linq;
using LCTender.Class;
using System.Linq;
using System.Net;

namespace LCTender.Monitoring
{
   
    public partial class t_vendor_register_maint_ro : System.Web.UI.Page
    {
        dbcReader dbcInsVendReg = new dbcReader();
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_register_maint_ro));
        String auser_id_login, retcurr_ctl_id,
               retcreate_doc, retprev_doc_id,
               retprev_ctl_id, retprev_proc_id,
               retcurr_doc_status;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataMaint(Convert.ToInt32(Request["t_vendor_register_Id"]), Convert.ToInt32(Request["type"]));
                t_vendor_register_id.Text = Request["t_vendor_register_Id"];
                load_ddlEntityList();
            }

        }
        protected void BindDataMaint(int intIDRow, int intType)
        {
            
            string strLOV = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV()"">{0}</a>", strIMG);

            //ltLOV.Text = strLOV;
           

            string strLOVBank = "";
            string strIMGBank = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOVBank += String.Format(@"<a href=""javascript:clickLOVBank()"">{0}</a>", strIMGBank);

            ltLOVBank.Text = strLOVBank;

            typBussiness.Attributes.Add("readonly", "readonly");
            nmBank.Attributes.Add("readonly", "readonly");
            dtNotary.Attributes.Add("readonly", "readonly");

            if (intType==1)
            {
                String strAmt = dbcInsVendReg.ExecuteReader("Select Count(*) Amt from dbo.T_VENDOR_REGISTER where p_app_user_id="+Session["UserId"].ToString(), "Amt");
                if (Convert.ToInt32(strAmt) > 0)
                {
                    btnSave.Visible = false;
                }else
                {
                    btnSave.Visible = true;
                }
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                btnSave.Attributes.Add("onclick", "return checkValidate();");
                ltWorkFlow.Text = "";
                load_chkBizTypeList(0);
                load_ddlCostlimit("0");
            }
            else if (intType == 2)
            {
                log.Info("Bind Data Vendor Register Started. ");
                try
                {
                    int intMaxRec;
                    intMaxRec = 0;
                    DataSet dsresult = dbcInsVendReg.db_get_data_vendor(1, "", 1, intIDRow, "", ref intMaxRec);
                    DataTable dtresult = dsresult.Tables["dtData"];

                    btnSave.Visible = false;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                   
                    string strSubmit = dbcInsVendReg.ExecuteReader("SELECT count(*) AMT from sis.dbo.T_W_REGISTER_CTL WHERE p_w_proc_id=337 and profile_type='OUTBOX' and doc_id=" + intIDRow, "AMT");
                    string strStatus = dbcInsVendReg.ExecuteReader("SELECT request_status from sis.dbo.T_VENDOR_REGISTER WHERE T_VENDOR_REGISTER_ID=" + intIDRow, "request_status");

                    if ((Convert.ToInt32(strSubmit) == 0)||(strStatus =="118"))
                    {
                        //ltWorkFlow.Text = "<input id=\"btnWorkFlow\" type=\"button\" class=\"btn btn-primary\" value=\"Start Workflow\" onclick=\"javascript: clickLoadSubmitter()\"/>";
                        ltWorkFlow.Text = "";
                    }
                    else
                    {
                        ltWorkFlow.Text = "";
                    }

                    t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                    vendName.Text = dtresult.Rows[0]["vendor_name"].ToString();
                    vendName.Attributes.Add("readonly", "readonly");
                    npwpNo.Text = dtresult.Rows[0]["npwp_no"].ToString();
                    npwpNo.Attributes.Add("readonly", "readonly");
                    email.Text = dtresult.Rows[0]["email_addr"].ToString();
                    email.Attributes.Add("readonly", "readonly");
                    officeAddr.Text = dtresult.Rows[0]["office_addr"].ToString();
                    officeAddr.Attributes.Add("readonly", "readonly");
                    siteAddr.Text = dtresult.Rows[0]["site_addr"].ToString();
                    siteAddr.Attributes.Add("readonly", "readonly");
                    phoneNo.Text = dtresult.Rows[0]["phone_no"].ToString();
                    phoneNo.Attributes.Add("readonly", "readonly");
                    faxNo.Text = dtresult.Rows[0]["fax_no"].ToString();
                    faxNo.Attributes.Add("readonly", "readonly");
                    dtNotary.Text = dtresult.Rows[0]["company_date_als"].ToString();
                    dtNotary.Attributes.Add("readonly", "readonly");
                    nmBank.Text = dtresult.Rows[0]["bank_name"].ToString();
                    nmBank.Attributes.Add("readonly", "readonly");
                    nmBank_id.Text = dtresult.Rows[0]["p_bank_id"].ToString();
                    accNo.Text = dtresult.Rows[0]["bank_account_no"].ToString();
                    accNo.Attributes.Add("readonly", "readonly");
                    accOwner.Text = dtresult.Rows[0]["bank_account_owner"].ToString();
                    accOwner.Attributes.Add("readonly", "readonly");
                    s_entityId.Attributes.Add("readonly", "readonly");
                    s_ddlCostLimit.Attributes.Add("readonly", "readonly");
                    p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                    p_organization_id.Text = dtresult.Rows[0]["p_organization_id"].ToString();
                    request_status.Text = dtresult.Rows[0]["request_status"].ToString();
                    load_chkBizTypeList(Convert.ToInt32(t_vendor_register_id.Text));
                    load_ddlCostlimit(dtresult.Rows[0]["p_cost_limit_id"].ToString());
                    BindDataSubmitter();
                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Bind Data Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ((vendName.Text != "") && (npwpNo.Text != "") && (officeAddr.Text != "") && (npwpNo.Text.Length == 15) &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (dtNotary.Text != "") && (nmBank_id.Text != "") && 
                 (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Insert Vendor Register Started. ");
                try
                {

                    
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    string param_bizType_id = Request.Form["chkBizType"] + ",";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", param_bizType_id, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlCostLimit.SelectedValue.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");
                    

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        insertPersonal(npwpNo.Text, o_result_msg);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if ((vendName.Text != "") && (npwpNo.Text != "") && (npwpNo.Text.Length==15)  && 
                 (officeAddr.Text != "") &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (dtNotary.Text != "") && (nmBank_id.Text != "") && 
                 (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Update Vendor Register Started. ");
                try
                {

                    
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    string param_bizType_id = Request.Form["chkBizType"] + ",";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", param_bizType_id, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlCostLimit.SelectedValue.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        insertPersonal(npwpNo.Text, o_result_msg);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if ((vendName.Text != "") && (npwpNo.Text != "") && (officeAddr.Text != "") && (npwpNo.Text.Length == 15) &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (dtNotary.Text != "") && (nmBank_id.Text != "") && 
                 (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Delete Vendor Register Started. ");
                try
                {

                    
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    string param_bizType_id = Request.Form["chkBizType"] + ",";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", param_bizType_id, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["OrganId"].ToString(), "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlCostLimit.SelectedValue.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserId"].ToString(), "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
                    

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("window.parent.closeModal();");
                        cstext1.Append("window.parent.refresh();");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'","") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void BindDataSubmitter()
        {
            auser_id_login = Session["UserId"].ToString();
            if (request_status.Text == "116")
            {
                retcurr_ctl_id = Request["CURR_CTL_ID"];
                retcreate_doc = "N";
                retprev_doc_id = t_vendor_register_id.Text;
                retprev_ctl_id = Request["PREV_CTL_ID"];
                retprev_proc_id = Request["PREV_PROC_ID"];
                retcurr_doc_status = "1";

            }
            else { 
                retcurr_ctl_id = auser_id_login;
                retcreate_doc = "Y";
                retprev_doc_id = "";
                retprev_ctl_id = "";
                retprev_proc_id = "337";
                retcurr_doc_status = "1";
            }

            String param_submitter = "";
            String aIS_CREATE_DOC = retcreate_doc;
            String aIS_MANUAL = "N";
            String auser_id_doc = p_app_user_id.Text;
            String auser_id_donor = p_app_user_id.Text;
            String auser_id_taken = auser_id_login;
            String acurr_ctl_id = retcurr_ctl_id;
            String acurr_doc_type_id = "13";
            String acurr_proc_id = "337";
            String acurr_doc_id = t_vendor_register_id.Text;
            String acurr_doc_status = retcurr_doc_status;
            String acurr_proc_status = "0";
            String aprev_ctl_id = retprev_ctl_id;
            String aprev_doc_type_id = "13";
            String aprev_proc_id = retprev_proc_id;
            String aprev_doc_id = retprev_doc_id;
            String amessage = "";
            String aslot_1 = "";
            String aslot_2 = "";
            String aslot_3 = "";
            String aslot_4 = "";
            String aslot_5 = "";

            param_submitter = String.Format(@"{0}", "../Wf/submitter.aspx?");
            param_submitter += String.Format(@"{0}{1}", "CURR_DOC_ID=", acurr_doc_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_TYPE_ID=", acurr_doc_type_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_ID=", acurr_proc_id);
            param_submitter += String.Format(@"{0}{1}", "&CURR_CTL_ID=", acurr_ctl_id);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DOC=", auser_id_doc);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DONOR=", auser_id_donor);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_LOGIN=", auser_id_login);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_TAKEN=", auser_id_taken);
            param_submitter += String.Format(@"{0}{1}", "&IS_CREATE_DOC=", aIS_CREATE_DOC);
            param_submitter += String.Format(@"{0}{1}", "&IS_MANUAL=", aIS_MANUAL);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_STATUS=", acurr_proc_status);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_STATUS=", acurr_doc_status);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_ID=", aprev_doc_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_TYPE_ID=", aprev_doc_type_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_PROC_ID=", aprev_proc_id);
            param_submitter += String.Format(@"{0}{1}", "&PREV_CTL_ID=", aprev_ctl_id);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_1=", aslot_1);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_2=", aslot_2);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_3=", aslot_3);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_4=", aslot_4);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_5=", aslot_5);
            param_submitter += String.Format(@"{0}{1}", "&MESSAGE=", amessage);
            param_submitter += String.Format(@"{0}{1}", "&PACKAGE=", "SUBMIT_ENGINE_REGISTER");
           
            ltSubmitter.Text = Server.HtmlDecode("<div class='modal fade' id='myModalFrmSubmitter' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type = 'button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Workflow Started</h4></div><div class='modal-body'><div class='sizer'><div class='embed-responsive embed-responsive-16by9'><iframe id = 'frmSubmitter' src='" + param_submitter + "'></iframe></div></div></div><div class='modal-footer'><button type = 'button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");
        }

        private void load_ddlEntityList()
        {
           
            DataSet dsType = dbcInsVendReg.db_get_entity_list(1);
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }

        private void load_chkBizTypeList(int intIDRow)
        {
            
            DataSet dsType = dbcInsVendReg.ExecuteSelect("select P_BUSINESS_TYPE_ID,BUSINESS_TYPE from [dbo].[P_BUSINESS_TYPE]", 0, 0);
            DataTable dtType = dsType.Tables[0];

            Label lblCheckVal = new Label();
            lblCheckVal = (Label)FindControl("lblCheck");
            String strCheckBiztype = "";
            String strChecked = "";

            if (intIDRow != 0)
            {
                DataSet dsTypeDb = dbcInsVendReg.ExecuteSelect("select P_BUSINESS_TYPE_ID from [dbo].[T_VENDOR_BUSINESS_TYPE] where T_VENDOR_REGISTER_ID=" + intIDRow.ToString(), 0, 0);
                DataTable dtTypeDb = dsTypeDb.Tables[0];
                String db_biz_type = "";

                for (int i = 0; i < dtTypeDb.Rows.Count; i++)
                {
                    db_biz_type = db_biz_type + dtTypeDb.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",";
                }

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"] + ",", dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0 || Strings.InStr(db_biz_type, dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",")>0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<div class='checkbox'><label><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label></div>";
                }

                lblCheckVal.Text = strCheckBiztype;

            }
            else
            {

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"] + ",", dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<div class='checkbox'><label><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label></div>";
                }

                lblCheckVal.Text = strCheckBiztype;
            }
           
        }

        private void load_ddlCostlimit(string strId)
        {
            
            DataSet dsType = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_cost_limit", 0,0);
            DataTable dtType = dsType.Tables[0];
            s_ddlCostLimit.DataSource = dtType;
            s_ddlCostLimit.DataTextField = "VALUE";
            s_ddlCostLimit.DataValueField = "P_COST_LIMIT_ID";
            if (strId!="0")
            {
                s_ddlCostLimit.SelectedValue = strId;
            }
            s_ddlCostLimit.DataBind();

        }
        private void insertPersonal(string npwp, string vendorRegID)
        {
            try
            {

                //string str_json = "{\"personalId\":17933,\"personalNo\":\"1802-0025\",\"srPersonalTypeId\":56,\"personalName\":\"PT. JASATAMA POLAMEDIA\",\"amountBalance\":0.0000,\"amountRefund\":0.0000,\"amountReject\":0.0000,\"termOfPayment\":30,\"isRcvPromotion\":false,\"notes\":null,\"active\":true,\"lastUpdated\":1518519323883,\"lastUpdater\":1,\"isParent\":false,\"srItem\":{\"srItemId\":null,\"srGroupNo\":null,\"srItemNo\":null,\"srItemName\":\"INSTITUTION\",\"remark\":null,\"active\":true,\"lastUpdated\":1550631067759,\"lastUpdater\":null,\"srGroup\":null},\"personalInstitution\":{\"personalId\":null,\"srInstitutionTypeId\":747,\"contractNumber\":\"0215494333\",\"contractStart\":null,\"contractEnd\":null,\"contractSummary\":\"\",\"contactPerson\":null,\"isTaxable\":false,\"taxPercentage\":0.0,\"taxRegistrationNo\":null,\"leadTime\":0,\"creditLimit\":0.0000,\"currencyNo\":\"IDR\",\"branchName\":null,\"faxNo\":null,\"phoneNo\":null,\"contactName\":null,\"websiteName\":\"WWW.JASATAMA.COM\",\"personal\":null,\"srItem\":null},\"personalDocument\":{\"personalId\":null,\"documentId\":null,\"srDocumentTypeId\":null,\"documentNo\":null,\"expiredDate\":null,\"imgProfileName\":null,\"remark\":null,\"active\":true,\"lastUpdated\":null,\"lastUpdater\":null,\"srItem\":{\"srItemId\":null,\"srGroupNo\":null,\"srItemNo\":null,\"srItemName\":null,\"remark\":null,\"active\":true,\"lastUpdated\":1550631067759,\"lastUpdater\":null,\"srGroup\":null}},\"personalIdentity\":{\"personalId\":null,\"srIdentityTypeId\":null,\"middleName\":null,\"lastName\":null,\"nickName\":null,\"gender\":null,\"birthDate\":null,\"birthPlace\":null,\"srMaritalStatusId\":null,\"srBloodTypeId\":null,\"bloodRhesus\":null,\"srReligionID\":null,\"srOccupationId\":null,\"srCitizenId\":null,\"height\":null,\"weight\":null,\"countryId\":null,\"imgProfileName\":null,\"srItem\":null,\"personal\":null},\"personalAddress\":{\"prsnlAddrId\":null,\"personalId\":null,\"addressId\":null,\"activeDate\":null,\"srAddrStatusId\":null,\"transCodeNo\":null,\"referenceId\":null,\"remark\":null,\"isActive\":null,\"lastUpdated\":null,\"lastUpdater\":null,\"srItem\":null,\"address\":null,\"personalAddrType\":null},\"personalEmail\":{\"personalId\":null,\"emailId\":null,\"emailAddress\":null,\"srEmailTypeId\":null,\"remark\":null,\"isActive\":true,\"lastUpdated\":null,\"lastUpdater\":null},\"personalMobilePhone\":{\"personalId\":null,\"mobilePhoneId\":null,\"operatorId\":null,\"prefixNo\":null,\"destinationNo\":null,\"macAddress\":null,\"srSMSTypeId\":null,\"remark\":null,\"isActive\":true,\"lastUpdated\":null,\"lastUpdater\":null},\"personalAccount\":{\"personalId\":null,\"accountId\":null,\"accountNo\":null,\"nameOfAccount\":null,\"bankId\":null,\"branchName\":null,\"isActive\":true,\"lastUpdated\":null,\"lastUpdater\":null,\"currencyNo\":null},\"personalLogTime\":{\"personalId\":null,\"accessPoint\":null,\"logTime\":1550631067759},\"personalRelationship\":{\"personalId\":null,\"relationId\":null,\"srRelationGroupId\":null,\"srRelationTypeId\":null,\"remark\":null,\"active\":true,\"lastUpdated\":1550631067759,\"lastUpdater\":null,\"srItem\":null,\"personalRelation\":{\"personalId\":null,\"personalNo\":null,\"sRPersonalTypeId\":null,\"firstName\":null,\"middleName\":null,\"lastName\":null,\"gender\":null,\"birthDate\":null,\"personalIdRelation\":null,\"active\":true,\"lastUpdated\":1550631067759,\"lastUpdater\":null,\"personal\":null,\"srItem\":null},\"personalDocument\":{\"personalId\":null,\"documentId\":null,\"srDocumentTypeId\":null,\"documentNo\":null,\"expiredDate\":null,\"imgProfileName\":null,\"remark\":null,\"active\":true,\"lastUpdated\":null,\"lastUpdater\":null,\"srItem\":{\"srItemId\":null,\"srGroupNo\":null,\"srItemNo\":null,\"srItemName\":null,\"remark\":null,\"active\":true,\"lastUpdated\":1550631067759,\"lastUpdater\":null,\"srGroup\":null}},\"personalAddress\":{\"prsnlAddrId\":null,\"personalId\":null,\"addressId\":null,\"activeDate\":null,\"srAddrStatusId\":null,\"transCodeNo\":null,\"referenceId\":null,\"remark\":null,\"isActive\":null,\"lastUpdated\":null,\"lastUpdater\":null,\"srItem\":null,\"address\":null,\"personalAddrType\":null},\"personal\":null,\"personalRelations\":[],\"personalIdentities\":[],\"personalAddresses\":[],\"personalDocuments\":[]},\"personalInstitutions\":[],\"personalDocuments\":[],\"personalIdentities\":[],\"personalAddresses\":[],\"personalEmails\":[],\"personalMobilePhones\":[],\"personalRelationships\":[],\"personalAccounts\":[],\"personalLogTimes\":[],\"unitReservation\":null}";
                string str_json = callExistingVendor(npwp);
                if ((str_json!="[]"))
                {
                    var json = JToken.Parse(str_json);
                    var fieldsCollector = new JsonFieldsCollector(json);
                    var fields = fieldsCollector.GetAllFields();
                    var result = (from e in fields
                                  where !e.Key.Contains("srItem")
                                        && !e.Key.Contains("personalInstitution")
                                        && !e.Key.Contains("personalDocument")
                                        && !e.Key.Contains("personalIdentity")
                                        && !e.Key.Contains("personalAddress")
                                        && !e.Key.Contains("personalEmail")
                                        && !e.Key.Contains("personalMobilePhone")
                                        && !e.Key.Contains("personalAccount")
                                        && !e.Key.Contains("personalLogTime")
                                        && !e.Key.Contains("personalRelationship")
                                        && !e.Key.Contains("personalDocuments")
                                        && !e.Key.Contains("personalIdentities")
                                        && !e.Key.Contains("personalAddresses")
                                        && !e.Key.Contains("personalEmails")
                                        && !e.Key.Contains("personalMobilePhones")
                                        && !e.Key.Contains("personalRelationships")
                                        && !e.Key.Contains("personalAccounts")
                                        && !e.Key.Contains("personalLogTimes")
                                        && !e.Key.Contains("unitReservation")
                                  select new { respValue = e.Value, respKey = e.Key }).Distinct().ToList();

                    var rsltWebSite = (from e in fields
                                       where e.Key.Contains("personalInstitution.websiteName")
                                       select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strWebSite = "";

                    foreach (var fieldSite in rsltWebSite)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strWebSite = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltBranchName = (from e in fields
                                          where e.Key.Contains("personalInstitution.branchName")
                                          select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strBranchName = "";

                    foreach (var fieldSite in rsltBranchName)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strBranchName = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltContacthName = (from e in fields
                                            where e.Key.Contains("personalInstitution.contactName")
                                            select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strContactName = "";

                    foreach (var fieldSite in rsltContacthName)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strContactName = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltContactPerson = (from e in fields
                                             where e.Key.Contains("personalInstitution.contactPerson")
                                             select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strContactPerson = "";

                    foreach (var fieldSite in rsltContactPerson)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strContactPerson = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltcontractStart = (from e in fields
                                             where e.Key.Contains("personalInstitution.contractStart")
                                             select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strcontractStart = "";

                    foreach (var fieldSite in rsltcontractStart)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strcontractStart = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltcontractEnd = (from e in fields
                                           where e.Key.Contains("personalInstitution.contractEnd")
                                           select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strcontractEnd = "";

                    foreach (var fieldSite in rsltcontractEnd)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strcontractEnd = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltcontractSummary = (from e in fields
                                               where e.Key.Contains("personalInstitution.contractSummary")
                                               select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strcontractSummary = "";

                    foreach (var fieldSite in rsltcontractSummary)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strcontractSummary = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltcontractNumber = (from e in fields
                                              where e.Key.Contains("personalInstitution.contractNumber")
                                              select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strcontractNumber = "";

                    foreach (var fieldSite in rsltcontractNumber)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strcontractNumber = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltdocumentNo = (from e in fields
                                          where e.Key.Contains("personalDocument.documentNo")
                                          select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strdocumentNo = "";

                    foreach (var fieldSite in rsltdocumentNo)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strdocumentNo = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    var rsltexpiredDate = (from e in fields
                                          where e.Key.Contains("personalDocument.expiredDate")
                                          select new { respValSite = e.Value, respKeySite = e.Key }).Distinct().ToList();

                    string strexpiredDate = "";

                    foreach (var fieldSite in rsltdocumentNo)
                    {
                        //Console.WriteLine($"{field.Key}: '{field.Value}'");
                        strexpiredDate = String.Format(@"{0}{1}", fieldSite.respValSite, "");
                    }

                    string s_varvalue = "";

                    foreach (var field in result)
                    {

                        s_varvalue += String.Format(@"{0}{1}", field.respValue, "~#");
                    }
                    s_varvalue += String.Format(@"{0}{1}", strWebSite, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strBranchName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strContactName, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strContactPerson, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strcontractStart, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strcontractEnd, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strcontractSummary, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strcontractNumber, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strdocumentNo, "~#");
                    s_varvalue += String.Format(@"{0}{1}", strexpiredDate, "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendorRegID, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");

                    object outputObject;
                    string s_packname = "p_ins_personal";

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
               }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("getEntityData Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        public static string callExistingVendor(string npwp)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            string str_json = "";
            DataSet dsresult = dbcInsVendReg.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETVENDOR'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            using (var client = new WebClient())
            {
                try
                {
                    string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + npwp + dtresult.Rows[0]["END_FIX_URL"].ToString();
                    System.IO.Stream data = client.OpenRead(baseurl);
                    System.IO.StreamReader reader = new System.IO.StreamReader(data);
                    str_json = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                    return str_json;
                }
                catch (Exception ex)
                {
                    return "ERROR-" + ex.Message.ToString();
                }
            }
        }
    }
}