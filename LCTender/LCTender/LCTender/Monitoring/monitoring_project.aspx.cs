﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Monitoring
{
    public partial class monitoring_project : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        int PER_PAGE = 10;
        protected static readonly ILog log = LogManager.GetLogger(typeof(monitoring_project));
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["t_vendor_tender_id"] == null)
                {
                    BindDataList(intCurrPage, 0);

                }
                else
                {
                    BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_tender_id"]));
                }

            }
            
        }
        protected void BindDataList(int intCurrPage, int intVendorID)
        {
            pnlFilter.Visible = true;
            pnlGridRegister.Visible = true;

            dbcReader dbcVendorList = new dbcReader();
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_data_project_tender(intPage, Session["UserName"].ToString(), PER_PAGE, 0, Convert.ToInt32(Session["P_ENTITY_ID"]),ViewState["filter"].ToString().Trim(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            //if (dtresult.Rows.Count > 0){

            //    var header = (GridViewRow)gvTask.Controls[0].Controls[0];
            //    header.Cells[9].ColumnSpan = 2;
            //    header.Cells[9].Text = "TOTAL TENDER";
            //}


            for (int i = 0; i < dtresult.Rows.Count; i++)
            {

                ImageButton imbRdoSelect = (ImageButton)gvTask.Rows[i].FindControl("imbRdoRow");

               
                if (intVendorID == 0)
                {
                    if (i == 0)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        // gvTask.Rows[i].CssClass = "Row";
                    }
                    intVendorID = Convert.ToInt32(gvTask.Rows[i].Cells[0].Text);
                }
                else
                {
                    if (Convert.ToInt32(gvTask.Rows[i].Cells[0].Text) == intVendorID)
                    {
                        imbRdoSelect.ImageUrl = "../images/radio_s.gif";
                        gvTask.Rows[i].CssClass = "AltRow";
                    }
                    else
                    {
                        imbRdoSelect.ImageUrl = "../images/radio.gif";
                        //gvTask.Rows[i].CssClass = "AltRow";
                    }
                }


            }
            txtSelectID.Text = intVendorID.ToString();
            

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PROJECT</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>ESTIMASI BIAYA</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PENAWARAN</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }


            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]), imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblViewVal = (Label)e.Row.FindControl("lblView");
                string project_id = (((DataRowView)e.Row.DataItem)["P_PROJECT_TENDER_ID"]).ToString();

                String strView= "";

                strView += String.Format(@"<div class='row'>");
                strView += String.Format(@"<div class='col-sm-4'>{0}</div>", "<input id=\"btnView\" type =\"button\" class=\"btn btn-primary btn-xs\" value =\"View All\"/ onclick=\"javascript:showTenderList("+ project_id + ")\">");
                strView += String.Format(@"</div>");

                lblViewVal.Text = strView;
            }
        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String VendorId = "";
            VendorId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(VendorId));
                    break;
            }
            txtSelectID.Text = VendorId;

        }       
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

    }
}