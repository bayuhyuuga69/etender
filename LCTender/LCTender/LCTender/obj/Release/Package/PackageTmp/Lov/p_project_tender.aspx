﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="p_project_tender.aspx.cs" Inherits="LCTender.Lov.p_project_tender" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
.btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
 }
.hidden
     {
         display:none;
     }
</style>
</head>
<body>  
  <div class="container"> 
    <div class="row">
      <div class="col-sm-12">
        <form id="frmLVBussinessType" class="form-inline" runat="server"> 
           <div class="row">
               <div class="col-sm-12">
                 <div class="row">
                     <div class="col-sm-12">
                      <div class="form-group">
                        <label for="email">Filter:</label>
                            <asp:TextBox id="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
                      </div>
                       <asp:Button ID="btnCari" class="btn btn-default" runat="server" Text="Cari" OnClick="btnCari_Click" />
                     </div>
                 </div>                 
               </div>
           </div>
           <div class="row">  
            <div class="col-sm-12">         
                <asp:GridView ID="gvLOV" runat="server" Width="100%"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    CellPadding="5" CellSpacing="4"
                    OnRowDataBound="gvLOV_RowDataBound" 
                    OnPageIndexChanging="gvLOV_PageIndexChanging"
                    AutoGenerateColumns="False"
                    AllowPaging="true">
                    <Columns>          
                    <asp:BoundField DataField="P_PROJECT_TENDER_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_PROJECT_TENDER_ID" visible="false"/>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Center"  ItemStyle-CssClass="visible-desktop">                
                        <ItemTemplate>
                            <asp:Label ID="lblPILIH" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PROJECT_NAME" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PROJECT NAME"/>
                    <asp:BoundField DataField="TENDER_NO" ItemStyle-Wrap="true" ItemStyle-CssClass="hiddephone" HeaderText="TENDER NO"/>  
                    <asp:TemplateField HeaderText="BOQ FILE" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lblFileName" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>      
                    <asp:BoundField DataField="QUANTITY" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="QUANTITY" visible="false"/> 
                    <asp:BoundField DataField="MEASUREMENT" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="MEASURn-EMENT" visible="false"/>                       
                    <asp:BoundField DataField="BUSINESS_TYPE" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="CATEGORY"/>
                    <asp:BoundField DataField="ENTITY_NAME" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="ENTITY"/> 
                    <asp:BoundField DataField="ESTIMATED_COST_AMT" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Right"  HeaderText="ESTIMATED COST" visible="false"/> 
                    <asp:BoundField DataField="DESCRIPTION" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="DESCRIPTION"/> 
                    <asp:BoundField DataField="VALID_FROM_ALS" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Center" HeaderText="OPENING DATE"/>  
                    <asp:BoundField DataField="VALID_TO_ALS" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Center" HeaderText="CLOSING DATE"/>              
                    </Columns>
                </asp:GridView>
             </div>
           </div>
           <asp:Label ID="lblNoRow" runat="server"></asp:Label>
           <asp:TextBox ID="t_vendor_register_id" cssclass="hidden" runat="server"></asp:TextBox>
           <asp:TextBox ID="p_entity_id" cssclass="hidden" runat="server"></asp:TextBox>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
<script type="text/javascript">
    function clickReturn(retVal) {
         //debugger;
         arrVal = retVal.split('#~#');
        //for (x=0;x<arrForm.length;x++) {
            //window.opener.document.getElementById(strForm+arrForm[x]).value = arrVal[x];
          //  window.opener.document.getElementById("ContentSection_registerControlID_typBussiness").value = arrVal[0];
        //}
         parent.document.getElementById("ContentSection_p_project_tender_id").value = arrVal[0];
         parent.document.getElementById("ContentSection_txtProjectName").value = arrVal[1];
         parent.document.getElementById("ContentSection_txtEstimate").value = arrVal[2];
         parent.document.getElementById("ContentSection_txtEstimateVal").value = arrVal[2];
         parent.document.getElementById("ContentSection_txtTenderNo").value = arrVal[3];
         parent.document.getElementById("ContentSection_txtQuantity").value = arrVal[4];
         parent.document.getElementById("ContentSection_txtMeasurement").value = arrVal[5];
         parent.document.getElementById("ContentSection_txtDescription").value = arrVal[6];
         parent.closeModal();
    }
    function clickView(filename, filetype) {
        var url = "../Wf/fileviewer.aspx?FILE_NAME=" + filename + "&FILE_TYPE=" + filetype;
        document.location.href = url;
    }
</script>
