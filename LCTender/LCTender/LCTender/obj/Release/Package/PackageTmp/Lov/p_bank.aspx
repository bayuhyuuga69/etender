﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="p_bank.aspx.cs" Inherits="LCTender.Lov.p_bank" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
.btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
 }
</style>
</head>
<body>  
  <div class="container"> 
    <div class="row">
      <div class="col-sm-12">
        <form id="frmLVBussinessType" class="form-inline" runat="server"> 
           <div class="row">
               <div class="col-sm-12">
                 <div class="row">
                     <div class="col-sm-12">
                      <div class="form-group">
                        <label for="email">Filter:</label>
                            <asp:TextBox id="keyword" name="keyword" cssclass="form-control" runat="server"></asp:TextBox>
                      </div>
                       <asp:Button ID="btnCari" cssclass="btn btn-default" runat="server" Text="Cari" OnClick="btnCari_Click" />
                     </div>
                 </div>
                 
               </div>
           </div>
           <div class="row">  
            <div class="col-sm-12">         
                <asp:GridView ID="gvLOV" runat="server" Width="100%"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    CellPadding="5" CellSpacing="4"
                    OnRowDataBound="gvLOV_RowDataBound" 
                    OnPageIndexChanging="gvLOV_PageIndexChanging"
                    AutoGenerateColumns="False"
                    AllowPaging="true">
                    <Columns>          
                    <asp:BoundField DataField="P_BANK_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_BANK_ID" visible="false"/>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Center"  ItemStyle-CssClass="visible-desktop">                
                        <ItemTemplate>
                            <asp:Label ID="lblPILIH" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BANK_NAME" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="ITEM" />
                    <asp:BoundField DataField="DESCRIPTION" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="DESC"  />    
                    </Columns>
                </asp:GridView>
             </div>
           </div>
           <asp:Label ID="lblNoRow" runat="server"></asp:Label>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
<script type="text/javascript">
    function clickReturn(retVal) {
         //debugger;
         arrVal = retVal.split('#~#');
        //for (x=0;x<arrForm.length;x++) {
            //window.opener.document.getElementById(strForm+arrForm[x]).value = arrVal[x];
          //  window.opener.document.getElementById("ContentSection_registerControlID_typBussiness").value = arrVal[0];
        //}
         window.parent.document.getElementById("nmBank_id").value = arrVal[0];
         window.parent.document.getElementById("nmBank").value = arrVal[1];
         window.parent.closeModalBank();
}
</script>
