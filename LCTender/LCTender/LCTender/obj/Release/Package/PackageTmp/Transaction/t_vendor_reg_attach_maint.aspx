﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="t_vendor_reg_attach_maint.aspx.cs" Inherits="LCTender.Transaction.t_vendor_reg_attach_maint" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background: #ffffff;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 0px 0px 0px 0px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }    
</style>
<script type="text/javascript">
    window.closeModal = function () {
        $("#myModalFrame .close").click();
    };   
</script>
</head>
<body>
   <form runat="server">      
     <!-- Modal -->  
     <asp:Label ID="lblError" runat="server"></asp:Label>    
     <div class="form-row">    
          <div class="form-group required col-sm-6">
            <label for="Jenis_Berkas" class="control-label">Jenis Berkas:</label>&nbsp;<asp:RequiredFieldValidator ID="rvattachment_type" runat="server" ForeColor="#ff0000" ErrorMessage="Masukkan Jenis Berkas" Display="Dynamic" ControlToValidate="attachment_type"></asp:RequiredFieldValidator> 
            <div class="input-group">
                 <asp:TextBox ID="attachment_type" name="attachment_type" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                 <asp:TextBox ID="p_attachment_type_id" class="hidden" name="p_attachment_type_id" runat="server"></asp:TextBox>                 
                <div class="input-group-addon">
                    <asp:Literal ID="ltLOV" runat="server"></asp:Literal>
                </div>   
            </div>   
          </div>          
         <div class="form-group col-sm-6">
            <label for="stockamt" class="control-label">Pilih Berkas:</label>&nbsp;<asp:Label ID="lblSize" runat="server" style="color:red"></asp:Label>                       
            <asp:ImageButton ID="imgUpload" runat="server" cssclass="img-responsive" Height="30px" Width="100px" ImageUrl="~/images/icon/Asset 15@4x.png" OnClientClick="browse();"></asp:ImageButton>
            <asp:RequiredFieldValidator ID="rvfileUpload" runat="server" ForeColor="#ff0000" ErrorMessage="Upload Berkas" Display="Dynamic" ControlToValidate="fileUpload"></asp:RequiredFieldValidator>
            <asp:Label ID="kotakUpload" runat="server" style="color:red"></asp:Label>    
            <asp:FileUpload ID="fileUpload" runat="server" class="form-control-file" style="visibility:hidden" onchange="javascript:fileUpload(event)"/>
         </div>         
     </div>
     <div class="form-row">
          <div class="form-group col-sm-6 pull-left">
          <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Upload" OnClick="btnSave_Click" />
         </div>
     </div>
    </form>
    <!-- Modal -->   
    <!-- iframe -->
    <div class="modal fade" id="myModalFrame" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Attachment Type</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOV" class="embed-responsive-item" src="../Lov/p_attachment_type.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <!-- iframe -->       
</body>
</html>
<script type="text/javascript">
    //debugger;
    var nameVal = false, addrVal = false;
    var percentVal = false;
    $('[id*=myModalFrame]').modal('hide');
    function clickLOV() {
        $('#myModalFrame').modal('show');
    }
    function checkValidate() {
        $(function () {

                var holderVal = $("#commissioner_name").val();
                $("#commissioner_name").closest('div').toggleClass('has-error', holderVal == "");
                  
                var addrVal = $("#commissioner_addr").val();
                $("#commissioner_addr").closest('div').toggleClass('has-error', addrVal == "");

                var jobVal = $("#vendor_job_position").val();
                $("#vendor_job_position").closest('div').toggleClass('has-error', jobVal == "");
                                       
                var nationVal = $("#nationality").val();
                $("#nationality").closest('div').toggleClass('has-error', nationVal == "");
                   
                var percentVal = $("#percentage").val();
                $("#percentage").closest('div').toggleClass('has-error', percentVal == "");
                   
        });
    }
	function browse() {
	    $("#fileUpload").click();
	}
	function fileUpload(event) {
	    $("#kotakUpload").html(event.target.value);
	}
</script>