﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_register.aspx.cs" Inherits="LCTender.Transaction.t_vendor_register" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register src="form_vendor_reg_plc.ascx" tagname="registerControl" tagprefix="regCtl" %>
<%@ Register src="../Main/content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%--<link href="../Content/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>--%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
   <div class="row">
      <div class="col-xs-12">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">Registrasi Vendor</a></li>
              <li><a href="javascript:clickTab(1)" style="font:15px Tahoma;">Pemegang Saham</a></li>
              <li><a href="javascript:clickTab(2)" style="font:15px Tahoma;">Susunan Direksi</a></li>
              <li><a href="javascript:clickTab(3)" style="font:15px Tahoma;">Susunan Dewan Komisaris</a></li>
              <li><a href="javascript:clickTab(4)" style="font:15px Tahoma;">Dokumen</a></li>
          </ul>
          <div class="tab-content">                
             <div class="main-div">            
                 <div id="vendor_reg" class="tab-pane fade in active">
                      <form runat="server"> 
                            <regCtl:registerControl ID="registerControlID" runat="server"/>                                                                                              
                       <!-- Modal -->
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog modal-fluid">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Vendor Register</h4>
                                </div>
                                <div class="modal-body">
                                   <div class="sizer">
                                    <div class="embed-responsive embed-responsive-16by9">
                                      <iframe id="frmMaint" class="embed-responsive-item"></iframe>
                                    </div>
                                   </div>                                             
                                </div>
                                <div class="modal-footer">
                                   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>                               
                              </div>      
                            </div>
                          </div>
                       <!-- Modal -->                        
                      </form> 
                     </div>                                                
                   </div>   
             <script type="text/javascript">                
                 var selectID = $("#ContentSection_registerControlID_txtSelectID").val();                 
                 function clickTab(tab) {
                     if (tab == "1") {
                         url = "t_holder_stock.aspx?t_vendor_register_id=" + selectID;
                     } else if (tab == "2") {
                         url = "t_vendor_director_level.aspx?t_vendor_register_id=" + selectID;
                     } else if (tab == "3") {
                         url = "t_vendor_commissioner_level.aspx?t_vendor_register_id=" + selectID;
                     } else if (tab == "4") {
                         url = "t_vendor_reg_attachment.aspx?t_vendor_register_id=" + selectID;
                     }
                     $(location).attr('href', url);
                 }
                  window.closeModal = function () {       
                        $("#myModal .close").click();
                   };
                  window.refresh = function () {
                      var url;
                      url = "t_vendor_register.aspx";
                      $(location).attr('href', url);
                  }
                  window.refreshframe = function () {
                      var url;
                      url = "../Transaction/t_vendor_register.aspx";
                      $(location).attr('href', url);
                  }
            </script>
          </div> 
        </div>
      </div>
    </div>
   <%-- <div class="footer navbar-fixed-bottom" style="background-color:#259fd9;margin-left:16px;margin-right:16px">     
       <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div> --%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
