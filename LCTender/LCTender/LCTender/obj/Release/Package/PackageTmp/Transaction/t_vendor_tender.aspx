﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_tender.aspx.cs" Inherits="LCTender.Transaction.t_vendor_tender" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register src="../Main/content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<link href="../Content/bootstrap-responsive.css" rel="stylesheet" type="text/css"/>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootbox.js"></script>
<script src="../Scripts/bootbox.min.js"></script>
<script src="../Scripts/bootstrap-datepicker.min.js"></script>
<script src="../Scripts/bootstrap-datepicker.js"></script>
<script src="../Scripts/bootstrap-datepicker.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-grid {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background-color:#fff;
      border-radius: 10px;
      margin: 10px auto 20px;
      padding: 30px 30px 150px 30px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      /*height: 92%;
      width: 40%;
      padding: 0;*/
    }    
    .modal-content {
      height: auto;
      width: auto;
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
     }
    .btn.btn-primary {
      background:#f36e21 none repeat scroll 0 0;
      border-color:#f36e21;
      color: #ffffff;
     }
    .column-in-center{
        float: none;
        margin: 0 auto;
     }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
        font: 13px Tahoma;
        color: #717171;
        padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    /*color: #000000;*/
	}

    .footer{
        position:absolute;
        bottom:15px;
        height:auto;
        padding-top:10px;
        width:98%;
        max-height:70px;
    }


    input:required:invalid, input:focus:invalid {
       box-shadow: 0  0 3px rgba(255,0,0,0.5); 
    }

   input:required:valid {
        /*background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAepJREFUeNrEk79PFEEUx9/uDDd7v/AAQQnEQokmJCRGwc7/QeM/YGVxsZJQYI/EhCChICYmUJigNBSGzobQaI5SaYRw6imne0d2D/bYmZ3dGd+YQKEHYiyc5GUyb3Y+77vfeWNpreFfhvXfAWAAJtbKi7dff1rWK9vPHx3mThP2Iaipk5EzTg8Qmru38H7izmkFHAF4WH1R52654PR0Oamzj2dKxYt/Bbg1OPZuY3d9aU82VGem/5LtnJscLxWzfzRxaWNqWJP0XUadIbSzu5DuvUJpzq7sfYBKsP1GJeLB+PWpt8cCXm4+2+zLXx4guKiLXWA2Nc5ChOuacMEPv20FkT+dIawyenVi5VcAbcigWzXLeNiDRCdwId0LFm5IUMBIBgrp8wOEsFlfeCGm23/zoBZWn9a4C314A1nCoM1OAVccuGyCkPs/P+pIdVIOkG9pIh6YlyqCrwhRKD3GygK9PUBImIQQxRi4b2O+JcCLg8+e8NZiLVEygwCrWpYF0jQJziYU/ho2TUuCPTn8hHcQNuZy1/94sAMOzQHDeqaij7Cd8Dt8CatGhX3iWxgtFW/m29pnUjR7TSQcRCIAVW1FSr6KAVYdi+5Pj8yunviYHq7f72po3Y9dbi7CxzDO1+duzCXH9cEPAQYAhJELY/AqBtwAAAAASUVORK5CYII=);*/
       background-image:url("../images/check.png"); 
       background-position: right top;
       background-repeat: no-repeat;
    }
   @media screen and (min-width: 750px) {
	#myModalFrmSubmitter .modal-dialog  {width:1200px;}
   }
    
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">Join Tender</a></li>             
              <li><a href="javascript:clickTab(4)" style="font:15px Tahoma;">Dokumen</a></li>
          </ul>
          <div class="tab-content">           
            <div id="vendor_reg" class="tab-pane fade in active">
             <div class="main-div">
              <form runat="server"> 
                   <div class="row" style="padding-top:20px">
                       <div class="col-sm-12">
                         <asp:Panel ID="pnlFilter" runat="server">
                            <div class="input-group input-group-sm">
                                <label for="email" class="col-sm-2">Filter:</label>
                                <div class="col-sm-8">
                                    <asp:TextBox id="keyword" cssclass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn btn-default glyphicon glyphicon-search">
                                            <asp:Button ID="btnCari" runat="server" Text="Cari" BackColor="Transparent" BorderWidth="0" />
                                        </div>
                                </div>
                            </div>
                         </asp:Panel>     
                       </div>
                    </div>
                    <div class="row" style="padding-top:20px">
                        <div class="col-sm-12">
                             <a><asp:ImageButton ID="imbAdd" runat="server" ImageUrl="~/images/btn_add.png" OnClick="imbAdd_Click"/>&nbsp;<asp:Label ID="lblAdd" runat="server" Text="Add New Data"></asp:Label></a>
                        </div>
                   </div>
                   <div class="row" style="padding-top:20px">
                      <div class="col-sm-12">  
                          <div class="table-responsive">                      
                            <asp:Panel ID="pnlGridTender" runat="server"> 
                                <asp:GridView ID="gvTask" runat="server" Width="100%"
                                    CssClass="mGrid"
                                    PagerStyle-CssClass="pgr"
                                    AlternatingRowStyle-CssClass="alt"
                                    CellPadding="5" CellSpacing="4"
                                    AutoGenerateColumns="False"
                                    OnRowDataBound="gvTask_RowDataBound"
                                    OnRowCommand="gvTask_RowCommand"
                                    PageSize="8">
                                    <AlternatingRowStyle CssClass="alt" />
                                    <Columns>
                                     <asp:BoundField DataField="T_VENDOR_TENDER_ID" HeaderText="T_VENDOR_TENDER_ID" ItemStyle-CssClass="hidden-phone">
                                       <ItemStyle CssClass="hidden" />
                                       <HeaderStyle CssClass="hidden" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="T_VENDOR_REGISTER_ID" HeaderText="T_VENDOR_REGISTER_ID" ItemStyle-CssClass="hidden-phone">
                                        <ItemStyle CssClass="hidden" />
                                        <HeaderStyle CssClass="hidden" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="TENDER_NO" HeaderText="NO REGISTER" ItemStyle-CssClass="hidden-phone" Visible="false">
                                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                                        </asp:BoundField>
                                      <asp:BoundField DataField="PROJECT_NAME" HeaderText="NAME PROJECT" ItemStyle-CssClass="hidden-phone" Visible="false">
                                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderRec" runat="server" Text="#"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                          <asp:ImageButton ID="imbRdoRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Select" ImageUrl="~/images/radio.gif" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderEdit" runat="server" Text="EDIT"></asp:Label>
                                         </HeaderTemplate>
                                         <ItemTemplate>
                                          <asp:ImageButton ID="imbRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Detail" ImageUrl="~/images/table.gif" />
                                         </ItemTemplate>
                                         <HeaderStyle HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                      </asp:TemplateField>                                                                                  
                                      <asp:TemplateField HeaderText="NAMA VENDOR" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                          <asp:Label ID="lblTenderNo" runat="server"></asp:Label>
                                         </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:BoundField DataField="ENTITY_NAME" HeaderText="ENTITY">                                      
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="PROJECT" ItemStyle-HorizontalAlign="Left">
                                         <ItemTemplate>
                                          <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                                         </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:BoundField DataField="ESTIMATED_COST_AMT" HeaderText="ESTIMATED COST" ItemStyle-HorizontalAlign="Center">
                                       <ItemStyle CssClass="hidden" />
                                       <HeaderStyle CssClass="hidden" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="OFFERING_COST_AMT" HeaderText="OFFERING COST"  ItemStyle-HorizontalAlign="Center">
                                         <ItemStyle HorizontalAlign="Center" />
                                      </asp:BoundField> 
                                      <asp:BoundField DataField="BUSINESS_TYPE" HeaderText="BIDANG USAHA">                                       
                                      </asp:BoundField>               
                                       <asp:BoundField ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="hidden-phone" HeaderStyle-CssClass="hidden-phone"
                                            DataField="REQUEST_STATUS" HeaderText="STATUS">
                                            <ItemStyle HorizontalAlign="Center" />   
                                         <ItemStyle CssClass="hidden-phone"/>
                                       </asp:BoundField>
                                       <asp:BoundField ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="hidden-phone" HeaderStyle-CssClass="hidden-phone"
                                            DataField="CREATION_DATE" HeaderText="TANGGAL">
                                           <ItemStyle HorizontalAlign="Center" Wrap="False"/> 
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="20" />
                                    <PagerStyle CssClass="pgr" />
                                </asp:GridView>
                                 <asp:Label ID="lblNoRow" runat="server"></asp:Label> 
                               <div class="Row">
                                    <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;">                                        
                                        <asp:ImageButton ID="imbFirst" runat="server" OnClick="imbFirst_Click" />
                                        <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click"/>
                                        &nbsp;
                                        <asp:Label ID="lblPaging" runat="server"></asp:Label>
                                        &nbsp;
                                        <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" style="width: 14px" />
                                        <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
                                   </div>
                              </div>
                            </asp:Panel>
                            </div>
                            <asp:Panel ID="pnlMaintTender" runat="server">
                             <div class="form-group col-sm-12">
                                 <asp:Literal ID="ltDownloadBoq" runat="server"></asp:Literal>
                             </div> 
                             <div class="form-group col-sm-12">
                                <label for="entity" class="control-label">Entity:</label>
                                <asp:DropDownList ID="s_entityId" cssclass="form-control" runat="server"></asp:DropDownList><br/>
                             </div>
                             <div class="form-row">
                                <div class="form-group col-sm-6">
                                    <label for="lblVendReg" class="control-label">No Registrasi Perusahaan Anda:</label>   
                                   <%-- <div class="input-group">--%>
                                        <asp:TextBox ID="txtVendReg" cssclass="form-control" runat="server" required="true" ReadOnly="true"></asp:TextBox>                                  
                                        <asp:TextBox ID="t_vendor_register_id" cssclass="hidden" runat="server"></asp:TextBox>
                                      <%--  <div class="input-group-addon">--%>
                                            <asp:Literal ID="ltLOVReg" runat="server"></asp:Literal>
                                      <%--  </div>--%>
                                   <%-- </div>--%>
                                </div> 
                                <div class="form-group col-sm-6">
                                     <label for="lblVendReg" class="control-label">Nama Perusahaan Anda:</label> 
                                     <asp:TextBox ID="txtVendName" cssclass="form-control" runat="server" required="true" ReadOnly="true"></asp:TextBox>                                  
                                </div>                           
                             </div> 
                             <div class="form-row">
                               <div class="form-group required col-sm-6">
                                  <label for="lblProject" class="control-label">Pilih Proyek:</label>                                   
                                  <div class="input-group">
                                    <asp:TextBox ID="txtProjectName" cssclass="form-control" runat="server" required="true" ReadOnly="true"></asp:TextBox>
                                    <asp:TextBox ID="p_project_tender_id" cssclass="hidden" runat="server"></asp:TextBox>
                                    <div class="input-group-addon">
                                       <asp:Literal ID="ltLOV" runat="server"></asp:Literal>
                                    </div> 
                                  </div>
                               </div> 
                               <div class="form-group col-sm-6">
                                  <label for="lblTenderNo" class="control-label">Tender No:</label>
                                  <asp:TextBox ID="txtTenderNo" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>                                
                             </div>
                             <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label for="lblQuantity" class="control-label">Quantity:</label>
                                  <asp:TextBox ID="txtQuantity" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="lblMeasurement" class="control-label">Measurement:</label>
                                  <asp:TextBox ID="txtMeasurement" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                </div>
                             </div>
                             <div class="form-row">
                                <div class="form-group col-sm-6">
                                   <label for="keterangan" class="control-label">Keterangan:</label>
                                   <asp:TextBox ID="txtDescription" cssclass="form-control" runat="server" ReadOnly="true" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="lblOffering" class="control-label">Penawaran:</label>
                                  <asp:TextBox ID="txtOffering" cssclass="form-control" runat="server" required="true" onkeypress="javascript:return checkIt(event)" onkeyup="javascript:formatCurrency(this)"></asp:TextBox>
                                </div>                                
                             </div>                                                    
                             <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label for="lblEstimate" class="control-label hidden">Estimasi Biaya:</label>
                                  <asp:TextBox ID="txtEstimate" cssclass="form-control hidden" readonly="true" runat="server" onkeypress="javascript:return checkIt(event)" onkeyup="javascript:formatCurrency(this)"></asp:TextBox>
                                   <asp:TextBox ID="txtEstimateVal" runat="server" cssclass="hidden"></asp:TextBox>
                                </div>
                             </div>
                              <div class="form-group col-sm-12">
                                    <asp:Button ID="btnSave" cssclass="btn btn-primary" runat="server" Text="Simpan" Onclick="btnSave_Click" />
                                    <asp:Button ID="btnUpdate" cssclass="btn btn-primary" runat="server" Text="Update" Onclick="btnUpdate_Click" />
                                    <asp:Button ID="btnDelete" cssclass="btn btn-primary" runat="server" Text="Hapus" OnClick="btnDelete_Click" />
                                    <asp:Button ID="btnCancel" cssclass="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                                    <asp:Literal ID="ltWorkFlow" runat="server"></asp:Literal>                                    
                                    <asp:TextBox ID="t_vendor_tender_id" cssclass="hidden" runat="server"></asp:TextBox>   
                                    <asp:TextBox ID="request_status" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="p_organization_id" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="p_app_user_id" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="user_name" cssclass="hidden" runat="server"></asp:TextBox>                      
                              </div>
                          </asp:Panel>                                               
                       </div>
                     </div>  
                 <asp:TextBox ID="txtSelectID" cssclass="hidden" runat="server"></asp:TextBox>                  
                </form> 
                 <!-- iframe -->
                 <div class="row">
                    <div class="col-sm-8">
                        <div class="modal fade" id="myModal" role="dialog">
                         <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">List Business Type</h4>             
                                </div>
                                    <div class="modal-body">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe id="frmLOV" class="embed-responsive-item"></iframe>
                                        </div>
                                    </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                         </div>
                        </div>
                      </div>
                    </div>
                <!-- iframe --> 
                <asp:Literal ID="ltWfSubmitter" runat="server"></asp:Literal>   
             </div>                                              
            </div>  
            <script type="text/javascript">    
                $('[id*=myModalFrame]').modal('hide');
                var selectID = $("#ContentSection_txtSelectID").val();
                function clickTab(tab) {
                   if (selectID!="0") {
                        if (tab == "4") {
                            url = "t_vendor_tender_attach.aspx?t_vendor_tender_id=" + selectID;
                        }
                        $(location).attr('href', url);
                    }else {
                        bootbox.alert({ message: "Data Tender Belum  Tersimpan., Pilih Proyek dan Isi Penawaran !!",className: 'bb-alternate-modal' });
                    }
                }
                function clickLOV(url,type) {
                    var $iframe = $('#frmLOV');
                    if(type==1){
                        $('h4.modal-title').text('Daftar Perusahaan');
                    } else if (type == 2) {
                        $('h4.modal-title').text('Daftar Proyek');
                    }
                    if ($iframe.length) {
                        $iframe.attr('src', url);    // here you can change src
                        $('#myModal').modal('show');
                    }                    
                }
                window.closeModal = function () {
                    $("#myModal .close").click();
                };
                function clickLoadSubmitter() {
                    //debugger;
                    bootbox.prompt({
                        title: "<table><tr> " +
                                    "<td><img class='img-responsive' src='../images/icon/Asset 17@4x.png' width=100% height=100%></td> " +
                                    "<td style='font-size:14px;padding:10px 5px'>Vendor dengan ini menyatakan dan menjamin bahwa seluruh dokumen-dokumen " +
                                        "dan/atau data-data/informasi yang disampaikan dalam formulir Pengkinian Data " +
                                        "Vendor ini adalah benar dan sesuai dengan dokumen-dokumen aslinya, dan dengan demikian " +
                                        "Vendor bertanggung jawab sepenuhnya atas kebenaran atau keakuratan data-data, informasi-informasi " +
                                        "dan/atau dokumen-dokumen tersebut, serta membebaskan penerima dokumen dari segala tuntutan, gugatan, " +
                                        "dan/atau ganti kerugian dalam bentuk apapun, termasuk apabila dikemudian hari terdapat sanggahan atas " +
                                        "kewenangan penandatangan formulir Pengkinian Data Vendor ini. " +
                                    "</td>  " +
                                "</tr></table>",                        
                        inputType: 'checkbox',
                        inputOptions: [
                            {
                                text: 'Agreed',
                                value: '1',
                            }
                        ],
                        callback: function (result) {
                            if (result == "1") {
                                $('[id*=myModalFrmSubmitter]').modal('show');
                            }
                        }
                    }).find("div.modal-dialog").css({ "width": "100%", "height": "100%" });
                   
                }
                function formatCurrency(event) {

                    var val = event.value;
                    val = val.replace(/,/g, "")
                    event.value = "";
                    val += '';
                    x = val.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';

                    var rgx = /(\d+)(\d{3})/;

                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }

                    event.value = x1 + x2;
                }
                function checkIt(evt) {
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false
                    }
                    return true
                }
                function disablereq() {
                    $('[id*=txt]').removeAttr('required');
                }
                window.closeModalSubmitter = function () {
                    $("#myModalFrmSubmitter .close").click();
                    url = "../Transaction/t_vendor_tender.aspx?t_vendor_tender_id=" + selectID;
                    $(location).attr('href', url);
                };
                function clickView(filename, filetype) {
                    var url = "../Wf/fileviewer.aspx?FILE_NAME=" + filename + "&FILE_TYPE=" + filetype;
                    document.location.href = url;
                }
           </script>
        </div>
      </div>
    </div>
<%--    <div class="footer navbar-fixed-bottom" style="background-color:#259fd9;margin-left:16px;margin-right:16px">     
       <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div> --%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
