﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="submitter.aspx.cs" Inherits="LCTender.Transaction.submitter" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background: #ffffff;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 0px 0px 0px 0px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }   

    .glyphicon-refresh-animate {
    -animation: spin .7s infinite linear;
    -webkit-animation: spin2 .7s infinite linear;
    }

    @-webkit-keyframes spin2 {
        from { -webkit-transform: rotate(0deg);}
        to { -webkit-transform: rotate(360deg);}
    }

    @keyframes spin {
        from { transform: scale(1) rotate(0deg);}
        to { transform: scale(1) rotate(360deg);}
    }
    
</style>
</head>
<body>
  <form class="form-horizontal" runat="server"> 
    <!-- Modal --> 
  <div class="form-group">
    <label class="control-label col-sm-2" for="time">Waktu Sekarang:</label>
    <div class="col-sm-8">
        <asp:TextBox ID="txtTime" name="txtTime" class="form-control" runat="server"></asp:TextBox>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Pekerjaan Tersedia [Semua]:</label>
    <div class="col-sm-8">
        <asp:Literal ID="ltTask" runat="server"></asp:Literal>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Pesan Akan dikirim:</label>
    <div class="col-sm-8">
       <asp:TextBox ID="txtMsg" name="txtMsg" textmode="MultiLine" class="form-control" runat="server"></asp:TextBox>
    </div>
  </div> 
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Status Dokumen Workflow:</label>
    <div class="col-sm-8">
        <asp:TextBox ID="txtStatus" name="txtTime" class="form-control" runat="server"></asp:TextBox>
    </div>
  </div> 
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <div class="col-sm-5">
           <asp:Button ID="btnSubmit" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>   
      </div>
      <div class="col-sm-5">
          <asp:Literal ID="ltLoading" runat="server"></asp:Literal>
      </div>            
      <asp:TextBox ID="SUBMITTER_ID" class="hidden" name="SUBMITTER_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="RETURN_VAL" class="hidden" name="RETURN_VAL" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_DOC_ID" class="hidden" name="CURR_DOC_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_DOC_TYPE_ID" class="hidden" name="CURR_DOC_TYPE_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_PROC_ID" class="hidden" name="CURR_PROC_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_CTL_ID" class="hidden" name="CURR_CTL_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="PREV_DOC_ID" class="hidden" name="PREV_DOC_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="PREV_DOC_TYPE_ID" class="hidden" name="PREV_DOC_TYPE_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="PREV_PROC_ID" class="hidden" name="PREV_PROC_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="PREV_CTL_ID" class="hidden" name="PREV_CTL_ID" runat="server"></asp:TextBox>
      <asp:TextBox ID="SLOT_1" class="hidden" name="SLOT_1" runat="server"></asp:TextBox>
      <asp:TextBox ID="SLOT_2" class="hidden" name="SLOT_2" runat="server"></asp:TextBox>
      <asp:TextBox ID="SLOT_3" class="hidden" name="SLOT_3" runat="server"></asp:TextBox>
      <asp:TextBox ID="SLOT_4" class="hidden" name="SLOT_4" runat="server"></asp:TextBox>
      <asp:TextBox ID="SLOT_5" class="hidden" name="SLOT_5" runat="server"></asp:TextBox>
      <asp:TextBox ID="USER_ID_DOC" class="hidden" name="USER_ID_DOC" runat="server"></asp:TextBox>
      <asp:TextBox ID="USER_ID_DONOR" class="hidden" name="USER_ID_DONOR" runat="server"></asp:TextBox>
      <asp:TextBox ID="USER_ID_LOGIN" class="hidden" name="USER_ID_LOGIN" runat="server"></asp:TextBox>
      <asp:TextBox ID="USER_ID_TAKEN" class="hidden" name="USER_ID_TAKEN" runat="server"></asp:TextBox>
      <asp:TextBox ID="IS_CREATE_DOC" class="hidden" name="IS_CREATE_DOC" runat="server"></asp:TextBox>
      <asp:TextBox ID="IS_MANUAL" class="hidden" name="IS_MANUAL" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_PROC_STATUS" class="hidden" name="CURR_PROC_STATUS" runat="server"></asp:TextBox>
      <asp:TextBox ID="CURR_DOC_STATUS" class="hidden" name="CURR_DOC_STATUS" runat="server"></asp:TextBox>
      <asp:TextBox ID="MESSAGE" class="hidden" name="MESSAGE" runat="server"></asp:TextBox>
      <asp:TextBox ID="PACKAGE" class="hidden" name="PACKAGE" runat="server"></asp:TextBox>
    </div>    
  </div>
    <!-- Modal -->          
  </form>    
</body>
</html>