﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="form_commisioner_plc_spec.ascx.cs" Inherits="LCTender.Wf.form_commisioner_plc_spec" %>
<div class="table-responsive">
<div class="row">
    <div class="col-sm-12">
        <div class="input-group input-group-sm">
            <label for="email" class="col-sm-2">Filter:</label>
            <div class="col-sm-8">
                <asp:TextBox ID="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <div class="btn btn-default glyphicon glyphicon-search">
                   <asp:Button ID="btnCari" BackColor="Transparent" runat="server" BorderWidth="0" Text="Cari" OnClick="btnCari_Click" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <asp:Panel ID="pnlGridVend" runat="server">
            <asp:GridView ID="gvTask" runat="server" Width="100%"
                CssClass="mGrid table table-sm"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt"
                CellPadding="5" CellSpacing="4"
                AutoGenerateColumns="False"
                OnRowDataBound="gvTask_RowDataBound"
                OnRowCommand="gvTask_RowCommand"
                PageSize="20">
                <AlternatingRowStyle CssClass="alt" />
                <Columns>
                    <asp:BoundField DataField="T_VENDOR_COMMISSIONER_LEVEL_ID" HeaderText="T_VENDOR_COMMISSIONER_LEVEL_ID" ItemStyle-CssClass="hidden-phone">
                        <ItemStyle CssClass="hidden" />
                        <HeaderStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:Label ID="lblHeaderEdit" runat="server" Text="VIEW"></asp:Label>
                            <br />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="imbRow" runat="server" CommandArgument="<%#Container.DataItemIndex%>" CommandName="Detail" ImageUrl="~/images/table.gif" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                   <asp:BoundField DataField="COMMISSIONER_NAME" HeaderText="COMMISSIONER_NAME" ItemStyle-CssClass="hidden-phone" Visible="false">
                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                    </asp:BoundField>   
                   <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="NAMA" ItemStyle-CssClass="visible-desktop">
                        <ItemTemplate>
                            <asp:Label ID="lblDirectorName" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="visible-desktop" />
                        <ItemStyle CssClass="visible-desktop" />
                    </asp:TemplateField>  
                    <asp:BoundField ItemStyle-HorizontalAlign="left" 
                         DataField="VENDOR_JOB_POSITION" HeaderText="JABATAN" >
                        <ItemStyle HorizontalAlign="left" Wrap="False" /> 
                    </asp:BoundField>                  
                    <asp:BoundField ItemStyle-HorizontalAlign="left"
                        DataField="COMMISSIONER_ADDR" HeaderText="ALAMAT">
                        <ItemStyle HorizontalAlign="left" Wrap="False" /> 
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="left"
                        DataField="NATIONALITY" HeaderText="KEBANGSAAN">
                        <ItemStyle HorizontalAlign="left" Wrap="False" /> 
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="center"
                        DataField="PERCENTAGE_AMT" HeaderText="PERSENTASE">
                        <ItemStyle HorizontalAlign="center" Wrap="False" /> 
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="left"
                        DataField="DESCRIPTION" HeaderText="KET">
                        <ItemStyle HorizontalAlign="left" Wrap="False" /> 
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="center"
                        DataField="CREATED_BY" HeaderText="DIBUAT OLEH">
                        <ItemStyle HorizontalAlign="center" Wrap="False" /> 
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="center"
                        DataField="CREATION_DATE" HeaderText="TANGGAL">
                        <ItemStyle HorizontalAlign="center" Wrap="False" /> 
                    </asp:BoundField>                  
                </Columns>
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="3" />
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            <asp:Label ID="lblNoRow" runat="server"></asp:Label>
            <div class="Row">
                <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse: collapse; padding: 2px; border: solid 1px #c1c1c1; color: #717171;">
                    <asp:ImageButton ID="imbFirst" runat="server" OnClick="imbFirst_Click" />
                    <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click" />
                    &nbsp;
                    <asp:Label ID="lblPaging" runat="server"></asp:Label>
                    &nbsp;
                    <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" Style="width: 14px" />
                    <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
                </div>
            </div>
        </asp:Panel>
     </div>
 </div>
<asp:TextBox ID="txtSelectID" class="hidden" runat="server"></asp:TextBox>
</div>
<script type="text/javascript">
    function clickEdit(id, type) {
        var vendorId = "<%=Request["CURR_DOC_ID"]%>";
         var app_level = "<%=Request["app_level"]%>";
        $('#frmMaint').attr('src', "../Transaction/t_vendor_commissioner_level_maint.aspx?t_vendor_register_id=" + vendorId + "&t_vendor_commissioner_level_id=" + id + "&type=" + type + "&app_level=" + app_level);
        $('#myModal').modal('show');
    }
</script>

