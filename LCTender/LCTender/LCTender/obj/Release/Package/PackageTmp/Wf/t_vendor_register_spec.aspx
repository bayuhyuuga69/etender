﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_register_spec.aspx.cs" Inherits="LCTender.Wf.t_vendor_register_spec" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Register src="form_vendor_reg_plc_spec.ascx" tagname="registerControl" tagprefix="regCtl" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootbox.js"></script>
<script src="../Scripts/bootbox.min.js"></script>
<script src="../Scripts/bootstrap-datepicker.min.js"></script>
<script src="../Scripts/bootstrap-datepicker.js"></script>
<script src="../Scripts/bootstrap-datepicker.min.js"></script>
<style type="text/css">
    body {
	font: 13px Tahoma;
	background-color: #ffffff;
    } 
    .modal-dialog {
      width: 98%;    
      padding: 0;
    }
    
    .hidden
     {
         display:none;
     }
   .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }
    .column-in-center{
        float: none;
        margin: 0 auto;
    }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    color: #000000;
	}
    @media screen and (min-width: 750px) {
	
	  #myModal .modal-dialog  {width:900px;}
      #myModalFrmSubmitter .modal-dialog  {width:900px;}

   }
    
</style>
     <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">Registrasi Vendor</a></li>
            <li><a href="javascript:clickTab(1)" style="font:15px Tahoma;">Pemegang Saham</a></li>
            <li><a href="javascript:clickTab(2)" style="font:15px Tahoma;">Susunan Direksi</a></li>
            <li><a href="javascript:clickTab(3)" style="font:15px Tahoma;">Susunan Dewan Komisaris</a></li>
            <li><a href="javascript:clickTab(4)" style="font:15px Tahoma;">Dokumen</a></li>
      </ul>
      <div class="tab-content">
        <%--<div class="main-div-spec">--%>
           <div id="vendor_reg" class="tab-pane fade in active">          
                <regCtl:registerControl ID="registerControlID" runat="server" /> 
          </div>
        <%--</div>--%>
      </div>
<script type="text/javascript">                
    var selectID ="<%= Request.QueryString.ToString() %>";
    var url;
    function clickTab(tab) {
        if (tab == "1") {
            url = "t_holder_stock_spec.aspx?" + selectID;
        } else if (tab == "2") {
            url = "t_director_spec.aspx?" + selectID;
        } else if (tab == "3") {
            url = "t_commisioner_spec.aspx?" + selectID;
        } else if (tab == "4") {
            url = "t_vendor_reg_attach_spec.aspx?" + selectID;
        }
        $(location).attr('href', url);
    }
    window.closeModalSubmitter = function () {
        $("#myModalFrmSubmitter .close").click();
        url = "t_vendor_register_spec.aspx?" + selectID;
        $(location).attr('href', url);
    };
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
