﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_tender_spec.aspx.cs" Inherits="LCTender.Wf.t_vendor_tender_spec" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<script src="../Scripts/bootbox.js"></script>
<script src="../Scripts/bootbox.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-grid {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background-color:#fff;
      border-radius: 10px;
      margin: 10px auto 20px;
      padding: 30px 30px 150px 30px;
    }
    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
     }
    .btn.btn-primary {
      background:#f36e21 none repeat scroll 0 0;
      border-color:#f36e21;
      color: #ffffff;
     }
    .column-in-center{
        float: none;
        margin: 0 auto;
     }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
        font: 13px Tahoma;
        color: #717171;
        padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    /*color: #000000;*/
	}

    .footer{
        position:absolute;
        bottom:15px;
        height:auto;
        padding-top:10px;
        width:98%;
        max-height:70px;
    }


    input:required:invalid, input:focus:invalid {
       box-shadow: 0  0 3px rgba(255,0,0,0.5); 
    }

   input:required:valid {
        /*background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAepJREFUeNrEk79PFEEUx9/uDDd7v/AAQQnEQokmJCRGwc7/QeM/YGVxsZJQYI/EhCChICYmUJigNBSGzobQaI5SaYRw6imne0d2D/bYmZ3dGd+YQKEHYiyc5GUyb3Y+77vfeWNpreFfhvXfAWAAJtbKi7dff1rWK9vPHx3mThP2Iaipk5EzTg8Qmru38H7izmkFHAF4WH1R52654PR0Oamzj2dKxYt/Bbg1OPZuY3d9aU82VGem/5LtnJscLxWzfzRxaWNqWJP0XUadIbSzu5DuvUJpzq7sfYBKsP1GJeLB+PWpt8cCXm4+2+zLXx4guKiLXWA2Nc5ChOuacMEPv20FkT+dIawyenVi5VcAbcigWzXLeNiDRCdwId0LFm5IUMBIBgrp8wOEsFlfeCGm23/zoBZWn9a4C314A1nCoM1OAVccuGyCkPs/P+pIdVIOkG9pIh6YlyqCrwhRKD3GygK9PUBImIQQxRi4b2O+JcCLg8+e8NZiLVEygwCrWpYF0jQJziYU/ho2TUuCPTn8hHcQNuZy1/94sAMOzQHDeqaij7Cd8Dt8CatGhX3iWxgtFW/m29pnUjR7TSQcRCIAVW1FSr6KAVYdi+5Pj8yunviYHq7f72po3Y9dbi7CxzDO1+duzCXH9cEPAQYAhJELY/AqBtwAAAAASUVORK5CYII=);*/
       background-image:url("../images/check.png"); 
       background-position: right top;
       background-repeat: no-repeat;
    }
   @media screen and (min-width: 750px) {
	
	  #myModal .modal-dialog  {width:900px;}
      #myModalFrmSubmitter .modal-dialog  {width:900px;}

   }
    
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">Join Tender</a></li>            
              <li><a href="javascript:clickTab(4)" style="font:15px Tahoma;">Dokumen</a></li>
          </ul>
          <div class="tab-content">           
            <div id="vendor_reg" class="tab-pane fade in active">
             <div class="main-div">
              <form runat="server">       
                 <br/>
                   <div class="row">
                      <div class="col-sm-12">                            
                        <asp:Panel ID="pnlMaintTender" runat="server">
                             <div class="form-group col-sm-12">
                                <label for="entity" class="control-label">Entity:</label>
                                <asp:DropDownList ID="s_entityId" cssclass="form-control" runat="server"></asp:DropDownList><br/>
                             </div>
                             <div class="form-row"> 
                                <div class="form-group col-sm-6">
                                  <label for="lblVendReg" class="control-label">No Registrasi Perusahaan:</label>          
                                  <asp:TextBox ID="txtVendReg" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>                                  
                                  <asp:TextBox ID="t_vendor_register_id" cssclass="hidden" runat="server"></asp:TextBox>
                                </div>  
                                <div class="form-group col-sm-6">
                                  <label for="lblVendReg" class="control-label">Name Perusahaan:</label>          
                                  <asp:TextBox ID="txtVendName" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>   
                                </div>                         
                             </div>
                             <div class="form-group required col-sm-12">
                                 <label for="lblProject" class="control-label">Nama Proyek:</label>
                                 <asp:TextBox ID="txtProjectName" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                                 <asp:TextBox ID="p_project_tender_id" cssclass="hidden" runat="server"></asp:TextBox>
                             </div>                                       
                             <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label for="lblEstimate" class="control-label">Estimasi Biaya:</label>
                                  <asp:TextBox ID="txtEstimate" cssclass="form-control" runat="server" onkeypress="javascript:return checkIt(event)" onkeyup="javascript:formatCurrency(this)"></asp:TextBox>
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="lblOffering" class="control-label">Penawaran</label>
                                  <asp:TextBox ID="txtOffering" cssclass="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                              </div> 
                             </div>
                              <div class="form-group col-sm-12">
                                    <asp:Button ID="btnUpdate" cssclass="btn btn-primary pull-right" runat="server" Text="Simpan" onclick="btnUpdate_Click"/>
                                    <%--<asp:Literal ID="ltWorkFlow" runat="server"></asp:Literal>--%>                                   
                                    <asp:TextBox ID="t_vendor_tender_id" cssclass="hidden" runat="server"></asp:TextBox>   
                                    <asp:TextBox ID="request_status" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="p_organization_id" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="p_app_user_id" cssclass="hidden" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="user_name" cssclass="hidden" runat="server"></asp:TextBox>                      
                              </div>
                              <div class="form-row">
                              <div class="col-sm-12">
                                  <label for="lblSubmit" class="control-label" style="text-align:center">.:SUBMIT PERSETUJUAN:.</label>   
                                  <hr style="min-width:85%; background-color:#a1a1a1 !important; height:1px;"/>
                              </div>
                              </div>
                              <div class="form-row">
                                 <div class="form-group col-sm-12">
                                     <label for="ddlStatusList" class="control-label">Status Persetujuan:</label>                      
                                     <asp:DropDownList ID="s_ddlStatusList" name="s_ddlStatusList" class="form-control" runat="server" AutoPostBack="True"></asp:DropDownList><br>
                                     <input id="btnWorkFlow" type="button" runat="server" class="btn btn-primary" value="Submit flow" onclick="javascript:clickLoadSubmitter()"/>                
                                 </div>  
                              </div>
                          </asp:Panel>                                               
                       </div>
                     </div>  
                 <asp:TextBox ID="txtSelectID" cssclass="hidden" runat="server"></asp:TextBox>                  
                </form> 
                 <!-- iframe -->
                 <div class="row">
                    <div class="col-sm-8">
                        <div class="modal fade" id="myModal" role="dialog">
                         <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">List Business Type</h4>             
                                </div>
                                <div class="modal-body">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe id="frmLOV" class="embed-responsive-item"></iframe>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                         </div>
                        </div>
                     </div>
                  </div>
                <!-- iframe --> 
                <asp:Literal ID="ltWfSubmitter" runat="server"></asp:Literal>   
             </div>                                              
            </div>  
            <script type="text/javascript">    
                $('[id*=myModalFrame]').modal('hide');
                var selectID = "<%= Request.QueryString.ToString() %>";  
                function formatCurrency(event) {

                    var val = event.value;
                    val = val.replace(/,/g, "")
                    event.value = "";
                    val += '';
                    x = val.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';

                    var rgx = /(\d+)(\d{3})/;

                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }

                    event.value = x1 + x2;
                }
                function checkIt(evt) {
                    var charCode = (evt.which) ? evt.which : event.keyCode
                    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                        return false
                    }
                    return true
                }
                function clickTab(tab) {
                    if (tab == "4") {
                        url = "t_vendor_tender_attach_spec.aspx?" + selectID;
                    }
                    $(location).attr('href', url);
                }
                function clickLOV(url,type) {
                    var $iframe = $('#frmLOV');
                    if(type==1){
                        $('h4.modal-title').text('Daftar Perusahaan');
                    } else if (type == 2) {
                        $('h4.modal-title').text('Daftar Proyek');
                    }
                    if ($iframe.length) {
                        $iframe.attr('src', url);    // here you can change src
                        $('#myModal').modal('show');
                    }                    
                }
                window.closeModal = function () {
                    $("#myModal .close").click();
                };
                function clickLoadSubmitter() {
                    //debugger;
                    $('[id*=myModalFrmSubmitter]').modal('show');
                }
                window.closeModalSubmitter = function () {
                    $("#myModalFrmSubmitter .close").click();
                    url = "t_vendor_tender_spec.aspx?" + selectID;
                    $(location).attr('href', url);
                };
           </script>
        </div>
      </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
