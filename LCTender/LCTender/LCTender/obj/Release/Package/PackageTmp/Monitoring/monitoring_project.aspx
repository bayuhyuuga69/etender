﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="monitoring_project.aspx.cs" Inherits="LCTender.Monitoring.monitoring_project" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background-color:#fff;
      border-radius: 10px;
      margin: 10px auto 20px;
      padding: 30px 30px 70px 30px;
    }
    .main-div-grid {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background-color:#fff;
      border-radius: 10px;
      margin: 10px auto 20px;
      padding: 30px 30px 150px 30px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
     }
    .btn.btn-primary {
      background:#f36e21 none repeat scroll 0 0;
      border-color:#f36e21;
      color: #ffffff;
     }
    .column-in-center{
        float: none;
        margin: 0 auto;
     }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
        font: 13px Tahoma;
        color: #717171;
        padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    /*color: #000000;*/
	}

    .footer{
        position:absolute;
        bottom:15px;
        height:auto;
        padding-top:10px;
        width:98%;
        max-height:70px;
    }


    input:required:invalid, input:focus:invalid {
       box-shadow: 0  0 3px rgba(255,0,0,0.5); 
    }

   input:required:valid {
        /*background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAepJREFUeNrEk79PFEEUx9/uDDd7v/AAQQnEQokmJCRGwc7/QeM/YGVxsZJQYI/EhCChICYmUJigNBSGzobQaI5SaYRw6imne0d2D/bYmZ3dGd+YQKEHYiyc5GUyb3Y+77vfeWNpreFfhvXfAWAAJtbKi7dff1rWK9vPHx3mThP2Iaipk5EzTg8Qmru38H7izmkFHAF4WH1R52654PR0Oamzj2dKxYt/Bbg1OPZuY3d9aU82VGem/5LtnJscLxWzfzRxaWNqWJP0XUadIbSzu5DuvUJpzq7sfYBKsP1GJeLB+PWpt8cCXm4+2+zLXx4guKiLXWA2Nc5ChOuacMEPv20FkT+dIawyenVi5VcAbcigWzXLeNiDRCdwId0LFm5IUMBIBgrp8wOEsFlfeCGm23/zoBZWn9a4C314A1nCoM1OAVccuGyCkPs/P+pIdVIOkG9pIh6YlyqCrwhRKD3GygK9PUBImIQQxRi4b2O+JcCLg8+e8NZiLVEygwCrWpYF0jQJziYU/ho2TUuCPTn8hHcQNuZy1/94sAMOzQHDeqaij7Cd8Dt8CatGhX3iWxgtFW/m29pnUjR7TSQcRCIAVW1FSr6KAVYdi+5Pj8yunviYHq7f72po3Y9dbi7CxzDO1+duzCXH9cEPAQYAhJELY/AqBtwAAAAASUVORK5CYII=);*/
       background-image:url("../images/check.png"); 
       background-position: right top;
       background-repeat: no-repeat;
    }

   .Header_div { 
	   padding-left: 20px;
       color: #fff; 
       background:#495767; 
       font-size: 1em;
	}
    
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">Monitoring Tender</a></li> 
          </ul>
          <div class="tab-content">           
            <div id="vendor_reg" class="tab-pane fade in active">
             <div class="main-div">
              <form runat="server">       
                <br/>
                    <div class="row">
                       <div class="col-sm-12">
                         <asp:Panel ID="pnlFilter" runat="server">
                            <div class="input-group input-group-sm">
                                <label for="email" class="col-sm-2">Filter:</label>
                                <div class="col-sm-8">
                                    <asp:TextBox id="keyword" cssclass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn btn-default glyphicon glyphicon-search">
                                            <asp:Button ID="btnCari" runat="server" Text="Search" BackColor="Transparent" BorderWidth="0"  OnClick="btnCari_Click"/>
                                        </div>
                                </div>
                            </div>
                         </asp:Panel>     
                       </div>
                    </div>
                   <br/>
                   <div class="row">
                      <div class="col-sm-12">  
                          <div class="table-responsive">  
                            <div class="row">
                               <div class="col-sm-2 Header_div">PROJECT TENDER</div>
                            </div>                      
                            <asp:Panel ID="pnlGridRegister" runat="server"> 
                                <asp:GridView ID="gvTask" runat="server" Width="100%"
                                    CssClass="mGrid table table-sm"
                                    PagerStyle-CssClass="pgr"
                                    AlternatingRowStyle-CssClass="alt"
                                    CellPadding="5" CellSpacing="4"
                                    AutoGenerateColumns="False"
                                    OnRowDataBound="gvTask_RowDataBound"
                                    OnRowCommand="gvTask_RowCommand"
                                    PageSize="20">
                                    <AlternatingRowStyle CssClass="alt" />
                                    <Columns>
                                      <asp:BoundField DataField="P_PROJECT_TENDER_ID" HeaderText="P_PROJECT_TENDER_ID" ItemStyle-CssClass="hidden-phone">
                                       <ItemStyle CssClass="hidden" />
                                       <HeaderStyle CssClass="hidden" />
                                      </asp:BoundField>                                      
                                      <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderRec" runat="server" Text="#"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                          <asp:ImageButton ID="imbRdoRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Select" ImageUrl="~/images/radio.gif" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                      </asp:TemplateField>                                                                                                                       
                                      <asp:BoundField DataField="PROJECT_NAME" HeaderText="PROJECT NAME">                                       
                                      </asp:BoundField>
                                      <asp:BoundField DataField="TENDER_NO" HeaderText="TENDER NO">                                       
                                      </asp:BoundField>
                                      <asp:BoundField DataField="BUSINESS_TYPE" HeaderText="CATEGORY">                                       
                                      </asp:BoundField>
                                      <asp:BoundField DataField="ENTITY_NAME" HeaderText="ENTITY">                                       
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DESCRIPTION" HeaderText="SUMMARY">                                       
                                      </asp:BoundField> 
                                      <asp:BoundField DataField="VALID_FROM_ALS" HeaderText="OPENING DATE">    
                                        <ItemStyle HorizontalAlign="Center" />                                   
                                      </asp:BoundField>  
                                      <asp:BoundField DataField="VALID_TO_ALS" HeaderText="CLOSING DATE">   
                                        <ItemStyle HorizontalAlign="Center" />                                    
                                      </asp:BoundField> 
                                      <asp:BoundField DataField="PROJECT_AMT" HeaderText="TOTAL TENDER"> 
                                       <ItemStyle HorizontalAlign="Center" />                                     
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="DETAIL">
                                        <ItemTemplate>
                                            <asp:Label ID="lblView" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                      </asp:TemplateField>        
                                    </Columns>
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="20" />
                                    <PagerStyle CssClass="pgr" />
                                </asp:GridView>
                                 <asp:Label ID="lblNoRow" runat="server"></asp:Label> 
                               <div class="Row">
                                    <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;">
                                        <asp:ImageButton ID="imbFirst" runat="server" OnClick="imbFirst_Click" />
                                        <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click"/>
                                        &nbsp;
                                        <asp:Label ID="lblPaging" runat="server"></asp:Label>
                                        &nbsp;
                                        <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" style="width: 14px" />
                                        <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
                                   </div>
                              </div>
                           </asp:Panel>
                         </div>                                   
                       </div>
                     </div>  
                 <asp:TextBox ID="txtSelectID" cssclass="hidden" runat="server"></asp:TextBox>                  
                </form> 
                  
             </div>                                              
            </div>  
            <script type="text/javascript">    
                $('[id*=myModalFrame]').modal('hide');
                var selectID = $("#ContentSection_txtSelectID").val();
               function clickLOV(url,type) {
                    var $iframe = $('#frmLOV');
                    if(type==1){
                        $('h4.modal-title').text('Daftar Perusahaan');
                    } else if (type == 2) {
                        $('h4.modal-title').text('Daftar Proyek');
                    }
                    if ($iframe.length) {
                        $iframe.attr('src', url);    // here you can change src
                        $('#myModal').modal('show');
                    }                    
                }
                window.closeModal = function () {
                    $("#myModal .close").click();
                };
                function clickLoadSubmitter() {
                    debugger;
                    $('[id*=myModalFrmSubmitter]').modal('show');
                }
                function disablereq() {
                    $('[id*=txt]').removeAttr('required');
                }
                window.closeModalSubmitter = function () {
                    $("#myModalFrmSubmitter .close").click();
                    url = "../Transaction/t_vendor_tender.aspx?t_vendor_tender_id=" + selectID;
                    $(location).attr('href', url);
                };
                function showTenderList(project_id) {
                    url = "../Monitoring/monitoring_tender.aspx?p_project_tender_id=" + project_id;
                    $(location).attr('href', url);
                }
           </script>
        </div>
      </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
