﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="LCTender.Home" %>
<%@ Register src="content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <div class="row">
        <form id="form1" runat="server">
         <div class="col-sm-4"><asp:PlaceHolder ID="plcLinks" runat="server"></asp:PlaceHolder></div>
        </form>
        <script src="../Scripts/ChartJS.js"></script>
       <div class="col-sm-8">            
            <asp:PlaceHolder ID="plcChart" runat="server"></asp:PlaceHolder>
            <asp:Literal ID="ltChart" runat="server"></asp:Literal>
        </div>
    </div>
    <div class="footer navbar-fixed-bottom" style="background-color:#259fd9;margin-left:16px;margin-right:16px">     
       <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
