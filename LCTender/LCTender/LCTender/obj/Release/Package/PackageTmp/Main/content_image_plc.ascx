﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_image_plc.ascx.cs" Inherits="LCTender.Main.content_image_plc" %>
<script src="../Scripts/jquery-2.1.4.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>
<link href="../Content/bootstrap.min.css" rel="stylesheet"/>
<link href="../Content/bootstrap-theme.css" rel="stylesheet"/>
<link href="../Content/bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.css" type="text/css" />
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.min.css" type="text/css" />
<script type="text/javascript" src="../Scripts/bootbox.js"></script>
<script type="text/javascript" src="../Scripts/bootbox.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div {
      /*background: #0077bc none repeat scroll 0 0;*/
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 10px 10px 50px 10px;    
      color: #ffffff;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
  
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21;
      color: #ffffff;
    }
    h1, h2, h3, h4, h5, h6 {
        text-align: center;
    }  
</style>
<body>      
     <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
         <asp:Literal ID="ltIndicator" runat="server"></asp:Literal>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" style="max-width:750px; max-height:400px !important;">
          <asp:Literal ID="ltWrapperSlide" runat="server"></asp:Literal>
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</body>
