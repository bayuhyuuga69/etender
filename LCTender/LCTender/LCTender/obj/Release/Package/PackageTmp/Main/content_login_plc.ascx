﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_login_plc.ascx.cs" Inherits="LCTender.Main.content_login_plc" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<script src="../Scripts/jquery-2.1.4.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>
<link href="../Content/bootstrap.min.css" rel="stylesheet"/>
<link href="../Content/bootstrap-theme.css" rel="stylesheet"/>
<link href="../Content/bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.css" type="text/css" />
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.min.css" type="text/css" />
<script type="text/javascript" src="../Scripts/bootbox.js"></script>
<script type="text/javascript" src="../Scripts/bootbox.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
  
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21;
      color: #ffffff;
      border: none;
    }
    h2 {
        text-align: center;
    }
</style>
<body>
      <h2 style="padding-top:20px">Login Form</h2>      
      <div>
          <hr/>
      </div>
      <div class="input-group" style="padding-top:10px">         
          <asp:Literal ID="ltLogin" runat="server"></asp:Literal>
      </div>
      <div class="input-group" style="padding-top:10px">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
        <asp:TextBox ID="username" name="username" class="form-control" runat="server" placeholder="username" style="text-transform:uppercase"></asp:TextBox>
      </div>
      <div class="input-group" style="padding-top:20px">
        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
        <asp:TextBox ID="password" name="password" textmode="password" class="form-control" runat="server" placeholder="PASSWORD"></asp:TextBox>          
      </div>
     <div class="input-group" style="padding-top:10px">
          <label><asp:CheckBox ID="chkRemember" runat="server" />&nbsp;Remember Me?</label> 
     </div>
     <div class="input-group col-sm-offset-10">
          <asp:Button ID="btnLogin" class="btn btn-primary pull-right" runat="server" Text="Login" OnClick="btnLogin_Click" />          
     </div>
     <div class="input-group pull-right" style="padding-top:30px">
          <label for="entity" class="control-label">Not Registered?&nbsp;<a href="javascript:clickRegister()" style="color:#f36e21;">Create an account</a></label>
     </div> 
     <asp:TextBox ID="p_entity_id" class="hidden" name="p_entity_id" runat="server"></asp:TextBox>   
</body>
<script type="text/javascript">
   function clickRegister() {
       var entityID = <%= new JavaScriptSerializer().Serialize(Request.QueryString["P_ENTITY_ID"]) %>;
       var url;
       if(entityID==null){
           entityID ="1";
       }
       url = "Main/register.aspx?P_ENTITY_ID=" + entityID;
       $(location).attr('href', url);
    }
</script>