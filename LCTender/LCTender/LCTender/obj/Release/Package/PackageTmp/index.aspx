﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LCindex.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="LCTender.Main.index" %>
<%@ Register src="Main/content_login_plc.ascx" tagname="loginControl" tagprefix="logCtl" %>
<%@ Register src="Main/content_image_plc.ascx" tagname="imageControl" tagprefix="imageCtl" %>
<%@ Register src="Main/content_project_plc.ascx" tagname="projectControl" tagprefix="projectCtl" %>
<%@ Register src="Main/content_check_status_plc.ascx" tagname="checkControl" tagprefix="checkCtl" %>
<%@ Register src="Main/content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<style type="text/css">
.row [class*="col-"]{
  margin-bottom: -99999px;
  padding-bottom: 99999px;
}

.row{
  overflow: hidden; 
}
.panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
.login-form .form-control {
    background: #f7f7f7 none repeat scroll 0 0;
    border: 1px solid #d4d4d4;
    border-radius: 4px;
    font-size: 14px;
    height: 50px;
    line-height: 50px;
}
.main-div-grid {
    background: #fff none repeat scroll 0 0;
    border-radius: 10px;
    margin: 10px auto 10px;
    padding: 10px 10px 30px 10px;    
}

.login-form .form-group {
    margin-bottom:10px;
}
.login-form{ text-align:left;}
.forgot a {
    color: #777777;
    font-size: 14px;
    text-decoration: underline;
}
  
.forgot {
    text-align: left; margin-bottom:30px;
}
.botto-text {
    color: #ffffff;
    font-size: 14px;
    margin: auto;
}
.login-form .btn.btn-primary.reset {
    background: #ff9900 none repeat scroll 0 0;
}
.back { text-align: left; margin-top:10px;}
.back a {color: #444444; font-size: 13px;text-decoration: none;}
.modal-dialog {
    width: 98%;
    /*height: 92%;*/
    /**width: 40%;*/
    padding: 0;
}
    
.modal-content {
    /*height: 99%;*/
}
.hidden
    {
        display:none;
    }
.sizer {
    /*width: 500px;*/
}
h1, h4, h5, h6 {
    text-align: left;
    color:#fff;
}
</style>
<form runat="server" class="login-form">
  <div class="row">
    <div class="col-sm-8">
        <imageCtl:imageControl ID="imageControlID" runat="server"/>
    </div>
    <div class="col-sm-4" id="divLogin" runat="server">
       <logCtl:loginControl ID="loginControlID" runat="server"/>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6" id="divAnnouncement" runat="server">
      <asp:Literal ID="ltTtlAnnouncement" runat="server"></asp:Literal>     
      <asp:Literal ID="ltAnnouncement" runat="server"></asp:Literal>
    </div>
    <div class="col-sm-6" id="divImage" runat="server">
       <asp:Literal ID="ltImgAnnouncement" runat="server"></asp:Literal>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12" style="background-color:#e8edf0;">
      <h3>Latest Tender:</h3>
      <div class="main-div-grid">
        <div class="table-responsive">
         <projectCtl:projectControl ID="projectControlID" runat="server"/>
        </div>
      </div>
    </div>   
  </div>
  <div class="row">
    <div class="col-sm-12" style="background-color:#e8edf0;">
        <h4>Check Status:</h4>
        <checkCtl:checkControl ID="checkControlID" runat="server"/>
    </div>   
  </div>
  <div class="row">
    <div class="col-sm-12" id="divEntity" runat="server">
      <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div>   
  </div> 
</form> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
