﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="p_app_user_organ.aspx.cs" Inherits="LCTender.Parameter.p_app_user_organ" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-grid {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/
      background-color:#fff;
      border-radius: 10px;
      margin: 10px auto 20px;
      padding: 30px 30px 150px 30px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f0ad4e none repeat scroll 0 0;
      border-color: #f0ad4e;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
     }
    .btn.btn-primary {
      background:#f36e21 none repeat scroll 0 0;
      border-color:#f36e21;
      color: #ffffff;
     }
    .column-in-center{
        float: none;
        margin: 0 auto;
     }
    .hidden
     {
         display:none;
     }

    .AltRow td { 
	    /*font-size: 80%;*/
        font: 13px Tahoma;
        color: #717171;
        padding: 2px;
	    border-top: 1px solid #fde4b0; border-right: 1px solid #fde4b0;
	    vertical-align: top;
	    background-color: #e6fff5; 
	    /*color: #000000;*/
	}

    .footer{
        position:absolute;
        bottom:15px;
        height:auto;
        padding-top:10px;
        width:98%;
        max-height:70px;
    }

    /*input:required:invalid, input:focus:invalid {
       box-shadow: 0  0 3px rgba(255,0,0,0.5); 
    }*/
   /*background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAepJREFUeNrEk79PFEEUx9/uDDd7v/AAQQnEQokmJCRGwc7/QeM/YGVxsZJQYI/EhCChICYmUJigNBSGzobQaI5SaYRw6imne0d2D/bYmZ3dGd+YQKEHYiyc5GUyb3Y+77vfeWNpreFfhvXfAWAAJtbKi7dff1rWK9vPHx3mThP2Iaipk5EzTg8Qmru38H7izmkFHAF4WH1R52654PR0Oamzj2dKxYt/Bbg1OPZuY3d9aU82VGem/5LtnJscLxWzfzRxaWNqWJP0XUadIbSzu5DuvUJpzq7sfYBKsP1GJeLB+PWpt8cCXm4+2+zLXx4guKiLXWA2Nc5ChOuacMEPv20FkT+dIawyenVi5VcAbcigWzXLeNiDRCdwId0LFm5IUMBIBgrp8wOEsFlfeCGm23/zoBZWn9a4C314A1nCoM1OAVccuGyCkPs/P+pIdVIOkG9pIh6YlyqCrwhRKD3GygK9PUBImIQQxRi4b2O+JcCLg8+e8NZiLVEygwCrWpYF0jQJziYU/ho2TUuCPTn8hHcQNuZy1/94sAMOzQHDeqaij7Cd8Dt8CatGhX3iWxgtFW/m29pnUjR7TSQcRCIAVW1FSr6KAVYdi+5Pj8yunviYHq7f72po3Y9dbi7CxzDO1+duzCXH9cEPAQYAhJELY/AqBtwAAAAASUVORK5CYII=);*/
   /*input:required:valid {        
       background-image:url("../images/check.png"); 
       background-position: right top;
       background-repeat: no-repeat;
    }*/
    
</style>
    <div class="row">
      <div class="col-sm-12">
          <ul class="nav nav-tabs">
              <li><a href="javascript:clickTab(1)" style="font:15px Tahoma;">User Application</a></li>
              <li class="active"><a data-toggle="tab" href="#vendor_reg" style="font:15px Tahoma;">User Organ</a></li>  
          </ul>
          <div class="tab-content">           
            <div id="vendor_reg" class="tab-pane fade in active">
             <div class="main-div">
              <form runat="server">       
                <br/>
                    <div class="row">
                       <div class="col-sm-12">
                         <asp:Panel ID="pnlFilter" runat="server">
                            <div class="input-group input-group-sm">
                                <label for="email" class="col-sm-2">Filter:</label>
                                <div class="col-sm-8">
                                    <asp:TextBox id="keyword" cssclass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <div class="btn btn-default glyphicon glyphicon-search">
                                            <asp:Button ID="btnCari" runat="server" Text="Cari" BackColor="Transparent" BorderWidth="0" OnClick="btnCari_Click" />
                                        </div>
                                </div>
                            </div>
                         </asp:Panel>     
                       </div>
                    </div>
                   <br/>
                   <div class="row">
                      <div class="col-sm-12">  
                          <div class="table-responsive">                      
                            <asp:Panel ID="pnlGridTender" runat="server"> 
                                <asp:GridView ID="gvTask" runat="server" Width="100%"
                                    CssClass="mGrid table table-sm"
                                    PagerStyle-CssClass="pgr"
                                    AlternatingRowStyle-CssClass="alt"
                                    CellPadding="5" CellSpacing="4"
                                    AutoGenerateColumns="False"
                                    OnRowCommand="gvTask_RowCommand"
                                    PageSize="10" AllowPaging="True">
                                    <AlternatingRowStyle CssClass="alt" />
                                    <Columns>
                                     <asp:BoundField DataField="P_APP_USER_ORGAN_ID" HeaderText="P_APP_USER_ORGAN_ID" ItemStyle-CssClass="hidden-phone">
                                       <ItemStyle CssClass="hidden" />
                                       <HeaderStyle CssClass="hidden" />
                                     </asp:BoundField>                                     
                                      <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                          <asp:Label ID="lblHeaderEdit" runat="server" Text="EDIT"></asp:Label>
                                         </HeaderTemplate>
                                         <ItemTemplate>
                                          <asp:ImageButton ID="imbRow" runat="server" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Detail" ImageUrl="~/images/table.gif" />
                                         </ItemTemplate>
                                         <HeaderStyle HorizontalAlign="Center" />
                                         <ItemStyle HorizontalAlign="Center" />
                                      </asp:TemplateField>                                                                                  
                                      <asp:BoundField DataField="ORGANIZATION_CODE" HeaderText="ORGANIZATION">
                                      </asp:BoundField>
                                      <asp:BoundField DataField="CODE" HeaderText="JOB POSITION">
                                      </asp:BoundField>
                                      <asp:BoundField DataField="VALID_FROM" HeaderText="VALID FROM">                                      
                                      </asp:BoundField> 
                                       <asp:BoundField DataField="VALID_TO" HeaderText="VALID TO">                                      
                                      </asp:BoundField>                                     
                                    </Columns>
                                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="2" LastPageImageUrl="~/images/FirstOff.gif" NextPageImageUrl="~/images/Next.gif" PreviousPageImageUrl="~/images/Prev.gif" Visible="False" />
                                    <PagerStyle CssClass="pgr" BackColor="White" />
                                    <SortedAscendingHeaderStyle BorderStyle="Inset" />
                                </asp:GridView>
                                 <asp:Label ID="lblNoRow" runat="server"></asp:Label> 
                               <div class="Row">
                                    <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;">
                                       <asp:ImageButton ID="imbAdd" runat="server" ImageUrl="~/images/btn_add.png" OnClick="imbAdd_Click" />
                                        <asp:ImageButton ID="imbFirst" runat="server" ImageUrl="~/images/First.gif" OnClick="imbFirst_Click"/>
                                        <asp:ImageButton ID="imbPrev" runat="server" ImageUrl="~/images/Prev.gif" OnClick="imbPrev_Click"/>
                                        &nbsp;
                                        <asp:Label ID="lblPaging" runat="server"></asp:Label>
                                        &nbsp;
                                        <asp:ImageButton ID="imbNext" runat="server" ImageUrl="~/images/Next.gif" OnClick="imbNext_Click" style="width: 14px" />
                                        <asp:ImageButton ID="imbLast" runat="server" ImageUrl="~/images/Last.gif" OnClick="imbLast_Click" Width="16px" />
                                   </div>
                              </div>
                            </asp:Panel>
                            </div>
                            <asp:Panel ID="pnlMaintTender" runat="server">
                             <div class="form-group col-sm-12">
                                <label for="entity" class="control-label">Entity:</label>
                                <asp:DropDownList ID="s_entityId" cssclass="form-control" runat="server"></asp:DropDownList><br/>
                             </div>
                             <div class="form-row">    
                                <div class="form-group col-sm-12">
                                 <label for="lblOrganization" class="control-label">Organization::.<asp:Literal ID="ltLOVOrgan" runat="server"></asp:Literal></label>
                                 <asp:TextBox ID="txtOrganization" cssclass="form-control" runat="server" required="true" ReadOnly="true"></asp:TextBox>
                                 <asp:TextBox ID="p_organization_id" cssclass="hidden" runat="server"></asp:TextBox>
                                </div>                                                          
                             </div>                                    
                             <div class="form-row">
                                 <div class="form-group col-sm-12">
                                  <label for="lblJobPosition" class="control-label">Job Position::.<asp:Literal ID="ltLOVJobPosition" runat="server"></asp:Literal></label>          
                                  <asp:TextBox ID="txtJobPosition" cssclass="form-control" runat="server" required="true" ReadOnly="true"></asp:TextBox>  
                                  <asp:TextBox ID="p_job_position_id" cssclass="hidden" runat="server"></asp:TextBox>    
                                </div> 
                             </div>
                             <div class="form-row">
                                 <div class="form-group col-sm-6">
                                  <label for="lblValidFrom" class="control-label">Valid From:</label>                
                                   <div class="input-group date">
                                       <asp:TextBox ID="txtValidFrom" cssclass="form-control" runat="server"></asp:TextBox>                
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>               
                                    </div>
                                </div> 
                                 <div class="form-group col-sm-6">
                                  <label for="lblValidTo" class="control-label">Valid To:</label>          
                                  <div class="input-group date">
                                       <asp:TextBox ID="txtValidTo" cssclass="form-control" runat="server"></asp:TextBox>                
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>               
                                    </div>   
                                </div>
                             </div>
                              <div class="form-group col-sm-12">
                                    <asp:Button ID="btnSave" cssclass="btn btn-primary" runat="server" Text="Simpan" Onclick="btnSave_Click" />
                                    <asp:Button ID="btnUpdate" cssclass="btn btn-primary" runat="server" Text="Update" Onclick="btnUpdate_Click" />
                                    <asp:Button ID="btnDelete" cssclass="btn btn-primary" runat="server" Text="Hapus" OnClick="btnDelete_Click" />
                                    <asp:Button ID="btnCancel" cssclass="btn btn-primary" runat="server" Text="Cancel" OnClick="btnCancel_Click" />                                                                     
                                    <asp:TextBox ID="p_app_user_organ_id" cssclass="hidden" runat="server"></asp:TextBox>
                              </div>
                          </asp:Panel>                                               
                       </div>
                     </div>  
                 <asp:TextBox ID="txtSelectID" cssclass="hidden" runat="server"></asp:TextBox> 
                 <asp:TextBox ID="parent_organ_id" cssclass="hidden" runat="server"></asp:TextBox>                   
                 <asp:TextBox ID="p_app_user_id" cssclass="hidden" runat="server"></asp:TextBox>              
                </form> 
                  <!-- iframe -->
                <div class="modal fade" id="myModal" role="dialog">
                 <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">List Business Type</h4>             
                        </div>
                        <div class="modal-body">
                            <div class="sizer">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe id="frmLOV" class="embed-responsive-item"></iframe>
                            </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                 </div>
                </div>
                <!-- iframe --> 
                <asp:Literal ID="ltWfSubmitter" runat="server"></asp:Literal>   
             </div>                                              
            </div>  
            <script type="text/javascript">                 
                $('[id*=myModalFrame]').modal('hide');
                var selectID = "<%= Request.QueryString.ToString() %>";
                function clickTab(tab) {
                    if (tab == "1") {
                        url = "p_app_user.aspx?p_app_user_id=" + selectID;
                    }
                    $(location).attr('href', url);
                }
                function clickLOV(url,type) {
                    var $iframe = $('#frmLOV');
                    if(type==1){
                        $('h4.modal-title').text('Job Position');
                    } else if (type == 2) {
                        $('h4.modal-title').text('Organization');
                    }
                    if ($iframe.length) {
                        $iframe.attr('src', url);    
                        $('#myModal').modal('show');
                    }                    
                }
                window.closeModal = function () {
                    $("#myModal .close").click();
                };
                function clickLoadSubmitter() {
                    debugger;
                    $('[id*=myModalFrmSubmitter]').modal('show');
                }
                function disablereq() {
                    $('[id*=txt]').removeAttr('required');
                }
                window.closeModalSubmitter = function () {
                    $("#myModalFrmSubmitter .close").click();
                    url = "../Transaction/t_vendor_tender.aspx?t_vendor_tender_id=" + selectID;
                    $(location).attr('href', url);
                };
                $('.input-group.date').datepicker({
                    format: "dd-M-yyyy",
                    autoclose: true
                });
           </script>
        </div>
      </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
