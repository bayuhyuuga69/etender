﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Parameter
{
    public partial class p_app_user_role : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        protected static readonly ILog log = LogManager.GetLogger(typeof(p_app_user));
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["p_app_user_id"] == null)
                {
                    BindDataList(intCurrPage, 0);

                }
                else
                {
                    BindDataList(intCurrPage, Convert.ToInt32(Request["p_app_user_id"]));
                }

            }
            
        }
        protected void BindDataList(int intCurrPage, int intUserID)
        {
            pnlFilter.Visible = true;
            pnlGridTender.Visible = true;
            pnlMaintTender.Visible = false;
            load_ddlEntityList();

            dbcReader dbcVendorList = new dbcReader();
            
            DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.V_APP_USER_ROLE where P_APP_USER_ID="+ intUserID + "and upper(ltrim(rtrim(CODE))) " +
                                                           "like '%'+isnull(upper(ltrim(rtrim('" + ViewState["filter"].ToString() + "'))),null)+'%'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            if (dtresult.Rows.Count == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>ORGANIZATION</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>JOB POSITION</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VALID FROM</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VALID TO</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(gvTask.PageIndex + 1, gvTask.PageSize, gvTask.PageCount, imbFirst, imbPrev, imbNext, imbLast, lblPaging);

        }
             
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String UserId = "";
            int intStartRow = gvTask.PageSize * gvTask.PageIndex;
            int intArgumentRow = int.Parse(e.CommandArgument.ToString());
            UserId = gvTask.Rows[intArgumentRow-intStartRow].Cells[0].Text;
            switch (e.CommandName)
            {
                    case "Detail":
                    BindDataMaint(Convert.ToInt32(UserId), 2);
                    break;
            }
            txtSelectID.Text = UserId;

        }
        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0,1);
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridTender.Visible = false;
            pnlMaintTender.Visible = true;


            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";

            string strLOVRole = "";
            strLOVRole += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/p_app_role.aspx");
            ltLOVRole.Text = strLOVRole;


            if (intType == 1)
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                p_app_user_id.Text = "";
                txtAppRole.Text = "";
                txtValidFrom.Text = "";
                txtValidTo.Text = "";

            } else if (intType == 2) {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                dbcReader dbcVendorList = new dbcReader();

                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.V_APP_USER_ROLE where p_app_user_role_id=" + intIDRow, 0,0);
                DataTable dtresult = dsresult.Tables[0];

                p_app_user_role_id.Text = dtresult.Rows[0]["p_app_user_role_id"].ToString();
                p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                p_app_role_id.Text = dtresult.Rows[0]["p_app_role_id"].ToString();
                txtAppRole.Text = dtresult.Rows[0]["code"].ToString();
                txtValidFrom.Text = dtresult.Rows[0]["valid_from"].ToString();
                txtValidTo.Text = dtresult.Rows[0]["valid_to"].ToString();
            }

            txtSelectID.Text = intIDRow.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (p_app_role_id.Text != "")
            {

                log.Info("Insert User Role Started. ");
                try
                {
                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_role";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["p_app_user_id"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_role_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER ROLE", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Saved !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (p_app_role_id.Text != "")
            {

                log.Info("Update User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_role";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_role_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["p_app_user_id"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_role_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER ROLE", "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Updated !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            if (p_app_role_id.Text != "")
            {

                log.Info("Delete User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_role";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_role_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["p_app_user_id"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_role_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER ROLE", "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Deleted !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
           BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }
        private void load_ddlEntityList()
        {
            dbcReader dbcVendorList = new dbcReader();
           
            DataSet dsType = dbcVendorList.db_get_entity_list(Convert.ToInt32(Session["P_ENTITY_ID"]));
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = 0;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex > 0){
                gvTask.PageIndex -= 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex < (gvTask.PageCount - 1))
            {
                gvTask.PageIndex += 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = (gvTask.PageCount - 1);
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text.Trim();
            BindDataList(Convert.ToInt32(ViewState["current_page"]), Convert.ToInt32(Request["p_app_user_id"]));
        }
        

    }
}