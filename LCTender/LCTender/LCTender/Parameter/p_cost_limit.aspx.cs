﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Parameter
{
    public partial class p_cost_limit : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        protected static readonly ILog log = LogManager.GetLogger(typeof(p_cost_limit));
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                BindDataList(intCurrPage);
            }            
        }
        protected void BindDataList(int intCurrPage)
        {
            pnlFilter.Visible = true;
            pnlGridTender.Visible = true;
            pnlMaintTender.Visible = false;

            dbcReader dbcVendorList = new dbcReader();
            
            DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.v_cost_limit where upper(ltrim(rtrim(label_format))) " +
                                                           "like '%'+isnull(upper(ltrim(rtrim('" + ViewState["filter"].ToString() + "'))),null)+'%'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            for (int i = 0; i < dtresult.Rows.Count; i++)
            {
                Decimal decJmlBatasan = Convert.ToDecimal(gvTask.Rows[i].Cells[2].Text);
                gvTask.Rows[i].Cells[2].Text = String.Format("{0:n0}", decJmlBatasan);

                Decimal decJmlBatasan1 = Convert.ToDecimal(gvTask.Rows[i].Cells[3].Text);
                gvTask.Rows[i].Cells[3].Text = String.Format("{0:n0}", decJmlBatasan1);
            }

            if (dtresult.Rows.Count == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VALUE 1</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VALUE 2</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>FORMAT</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(gvTask.PageIndex + 1, gvTask.PageSize, gvTask.PageCount, imbFirst, imbPrev, imbNext, imbLast, lblPaging);

        }
             
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String UserId = "";
            int intStartRow = gvTask.PageSize * gvTask.PageIndex;
            int intArgumentRow = int.Parse(e.CommandArgument.ToString());
            UserId = gvTask.Rows[intArgumentRow-intStartRow].Cells[0].Text;
            switch (e.CommandName)
            {
                    case "Detail":
                    BindDataMaint(Convert.ToInt32(UserId), 2);
                    break;
            }
            txtSelectID.Text = UserId;

        }
        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0,1);
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridTender.Visible = false;
            pnlMaintTender.Visible = true;


            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";

            string strLOVFormat = "";
            strLOVFormat += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/p_cost_format.aspx");
            ltLovFormat.Text = strLOVFormat;

            if (intType == 1)
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                p_cost_limit_id.Text = "";
                p_cost_format_id.Text = "";
                txtValue1.Text = "0";
                txtValue2.Text = "0";
                txtFormat.Text = "";

            } else if (intType == 2) {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                dbcReader dbcVendorList = new dbcReader();

                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.v_cost_limit where p_cost_limit_id=" + intIDRow, 0,0);
                DataTable dtresult = dsresult.Tables[0];

                p_cost_limit_id.Text = dtresult.Rows[0]["p_cost_limit_id"].ToString();
                p_cost_format_id.Text = dtresult.Rows[0]["p_cost_format_id"].ToString();
                txtValue1.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["value1"].ToString())); 
                txtValue2.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["value2"].ToString()));
                txtFormat.Text = dtresult.Rows[0]["label_format"].ToString();
            }

            txtSelectID.Text = intIDRow.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (p_cost_format_id.Text != "")
            {

                log.Info("Insert Cost Limit Started. ");
                try
                {
                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_cost_limit";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_cost_format_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue1.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue2.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", TxtDescription.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");
              
                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Saved !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri, false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]));

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Cost Limit Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if (p_cost_format_id.Text != "")
            {

                log.Info("Update Cost Limit Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_cost_limit";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_cost_limit_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_cost_format_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue1.Text.Replace(",",""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue2.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", TxtDescription.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri, false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Cost Limit Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            if (p_cost_format_id.Text != "")
            {

                log.Info("Delete Cost Limit Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_cost_limit";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_cost_limit_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_cost_format_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue1.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValue2.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", TxtDescription.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Deleted !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri, false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
           BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }
       
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = 0;
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex > 0){
                gvTask.PageIndex -= 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex < (gvTask.PageCount - 1))
            {
                gvTask.PageIndex += 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = (gvTask.PageCount - 1);
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text.Trim();
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }
        

    }
}