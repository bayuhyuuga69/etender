﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Parameter
{
    public partial class p_app_user_all : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        protected static readonly ILog log = LogManager.GetLogger(typeof(p_app_user_all));
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";

                if (Request["t_vendor_tender_id"] == null)
                {
                    BindDataList(intCurrPage, 0);

                }
                else
                {
                    BindDataList(intCurrPage, Convert.ToInt32(Request["t_vendor_tender_id"]));
                }

            }
            
        }
        protected void BindDataList(int intCurrPage, int intVendorID)
        {
            pnlFilter.Visible = true;
            pnlGridTender.Visible = true;
            pnlMaintTender.Visible = false;
            load_ddlEntityList();

            string sql = "";
            if (Session["OrganId"].ToString() != "1")
            {
                sql = "and p_organization_id=" + Session["OrganId"].ToString();
            }

            dbcReader dbcVendorList = new dbcReader();
            DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.v_app_user where upper(ltrim(rtrim(USER_NAME)))+ " +
                                                           "upper(ltrim(rtrim(FULL_NAME)))+ " +
                                                           "upper(ltrim(rtrim(EMAIL_ADDRESS)))+ " +
                                                           "upper(ltrim(rtrim(VENDOR_NAME))) " +
                                                           "like '%'+isnull(upper(ltrim(rtrim('" + ViewState["filter"].ToString() + "'))),null)+'%' "+sql+" order by p_app_user_id desc", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            if (dtresult.Rows.Count == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>USER NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>FULL NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>EMAIL ADDRESS</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(gvTask.PageIndex + 1, gvTask.PageSize, gvTask.PageCount, imbFirst, imbPrev, imbNext, imbLast, lblPaging);

        }
             
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String UserId = "";
            int intStartRow = gvTask.PageSize * gvTask.PageIndex;
            int intArgumentRow = int.Parse(e.CommandArgument.ToString());
            UserId = gvTask.Rows[intArgumentRow-intStartRow].Cells[0].Text;
            switch (e.CommandName)
            {
                    case "Detail":
                        BindDataMaint(Convert.ToInt32(UserId), 2);
                        break;
                    case "Organ":
                        Response.Redirect("../Parameter/p_app_user_organ.aspx?p_app_user_id="+ UserId);
                        break;
                    case "Role":
                        Response.Redirect("../Parameter/p_app_user_role.aspx?p_app_user_id=" + UserId);
                        break;
                    
            }
            txtSelectID.Text = UserId;

        }
        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0,1);
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridTender.Visible = false;
            pnlMaintTender.Visible = true;
            imbAdd.Visible = false;
            lblAdd.Visible = false;

            btnCancel.Attributes.Add("OnClick", "javascript:disablereq()");   
           
            if (intType == 1)
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                p_app_user_id.Text = "";
                txtUserName.Text = "";
                txtFullName.Text = "";
                txtEmail.Text = "";
                load_ddlJobPosition("0");

            } else if (intType == 2) {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                dbcReader dbcVendorList = new dbcReader();
                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.v_app_user where p_app_user_id=" + intIDRow, 0,0);
                DataTable dtresult = dsresult.Tables[0];
                p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                p_job_position_id.Text = dtresult.Rows[0]["p_job_position_id"].ToString();
                txtUserName.Text = dtresult.Rows[0]["user_name"].ToString();
                txtFullName.Text = dtresult.Rows[0]["full_name"].ToString();
                txtEmail.Text = dtresult.Rows[0]["email_address"].ToString();
                txtPassword.Attributes["value"] = dtresult.Rows[0]["rawpwd_base"].ToString();
                s_entityId.SelectedValue = dtresult.Rows[0]["entity_id"].ToString();
                load_ddlJobPosition(p_job_position_id.Text);
            }

            txtSelectID.Text = intIDRow.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ((txtFullName.Text != "") && (txtEmail.Text != "") &&
                (txtPassword.Text != "")
               )
            {

                log.Info("Insert User Started. ");
                try
                {
                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_app_user_approval";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtFullName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEmail.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtPassword.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlJobPosition.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");
                  
                   log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Saved !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if ((txtFullName.Text != "") && (txtEmail.Text != "") &&
                (txtPassword.Text != "")
               )
            {

                log.Info("Update User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_app_user_approval";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtFullName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEmail.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtPassword.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlJobPosition.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Updated !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            if ((txtFullName.Text != "") && (txtEmail.Text != "") &&
                (txtPassword.Text != "")
               )
            {

                log.Info("Delete User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_app_user_approval";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtFullName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEmail.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtPassword.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlJobPosition.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Deleted !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
           txtEmail.Text = "dummy@msn.com";
           BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }
        private void load_ddlEntityList()
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType;
            if (Session["P_ENTITY_ID"].ToString()!="0")
            {
               dsType = dbcVendorList.db_get_entity_list(Convert.ToInt32(Session["P_ENTITY_ID"]));
            }else
            {
                dsType = dbcVendorList.ExecuteSelect("select p_entity_id, " +
                                                     "p_organization_id, entity_code,  " +
                                                     "entity_name, entity_addr, phone_1,  " +
                                                     "phone_2, fax_1, fax_2 from p_entity where is_valid='Y'", 0,0);
            }
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = 0;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex > 0){
                gvTask.PageIndex -= 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex < (gvTask.PageCount - 1))
            {
                gvTask.PageIndex += 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = (gvTask.PageCount - 1);
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0);
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text;
            BindDataList(Convert.ToInt32(ViewState["current_page"]),0);
        }

        private void load_ddlJobPosition(string strId)
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.ExecuteSelect("select P_JOB_POSITION_ID,CODE from dbo.P_JOB_POSITION where P_LEVEL_APPROVAL_TYPE_ID is not null ORDER BY P_LEVEL_APPROVAL_TYPE_ID ASC", 0, 0);
            DataTable dtType = dsType.Tables[0];
            s_ddlJobPosition.DataSource = dtType;
            s_ddlJobPosition.DataTextField = "CODE";
            s_ddlJobPosition.DataValueField = "P_JOB_POSITION_ID";
            if (strId != "0")
            {
                s_ddlJobPosition.SelectedValue = strId;
            }
            s_ddlJobPosition.DataBind();

        }



    }
}