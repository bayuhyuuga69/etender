﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Parameter
{
    public partial class p_app_user_approval_limit : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        protected static readonly ILog log = LogManager.GetLogger(typeof(p_app_user));
     
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int intCurrPage = 1;
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                BindDataList(intCurrPage);
            }
        }
        protected void BindDataList(int intCurrPage)
        {
            pnlFilter.Visible = true;
            pnlGridTender.Visible = true;
            pnlMaintTender.Visible = false; 
                
            load_ddlEntityList();

            dbcReader dbcVendorList = new dbcReader();
            DataSet dsresult = dbcVendorList.ExecuteSelect("exec dbo.p_list_approval_limit 0,"+ Session["OrganId"].ToString() + ","+ Session["P_ENTITY_ID"].ToString() + ",'"+ Session["UserName"].ToString() + "','" + ViewState["filter"].ToString() + "'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            gvTask.DataSource = dtresult;
            gvTask.DataBind();

            //for (int i = 0; i < dtresult.Rows.Count; i++)
            //{
            //    Decimal decJmlBatasan = Convert.ToDecimal(gvTask.Rows[i].Cells[7].Text);
            //    gvTask.Rows[i].Cells[7].Text = String.Format("{0:n0}", decJmlBatasan);

            //    Decimal decJmlBatasan1 = Convert.ToDecimal(gvTask.Rows[i].Cells[8].Text);
            //    gvTask.Rows[i].Cells[8].Text = String.Format("{0:n0}", decJmlBatasan1);
            //}

            if (dtresult.Rows.Count == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>USER NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>FULL NAME</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NOMINAL START</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NOMINAL END</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

            clsutl.load_image_paging(gvTask.PageIndex + 1, gvTask.PageSize, gvTask.PageCount, imbFirst, imbPrev, imbNext, imbLast, lblPaging);

        }
             
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String UserId = "";
            int intStartRow = gvTask.PageSize * gvTask.PageIndex;
            int intArgumentRow = int.Parse(e.CommandArgument.ToString());
            UserId = gvTask.Rows[intArgumentRow-intStartRow].Cells[0].Text;
            switch (e.CommandName)
            {
                    case "Detail":
                    BindDataMaint(Convert.ToInt32(UserId), 2);
                    break;
            }
            txtSelectID.Text = UserId;

        }
        protected void imbAdd_Click(object sender, ImageClickEventArgs e)
        {
            BindDataMaint(0,1);
        }

        protected void BindDataMaint(int intIDRow, int intType)
        {
            pnlFilter.Visible = false;
            pnlGridTender.Visible = false;
            pnlMaintTender.Visible = true;
            imbAdd.Visible = false;
            lblAdd.Visible = false;

            btnCancel.Attributes.Add("OnClick", "javascript:disablereq()");

            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";

            string strLOV = "";
            strLOV += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/p_app_user.aspx?P_ENTITY_ID="+s_entityId.SelectedValue);
            ltLOV.Text = strLOV;

            if (intType == 1)
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                //btnDelete.Visible = false;
                txtListingNo.Text = "-GENERATED-";
                txtListingNo.ReadOnly = true;
                p_app_user_id.Text = ""; 
                txtFullName.Text = "";
                txtUserName.Text = "";
                txtNominalStart.Text = "";
                txtNominalEnd.Text = "";
                txtValidFrom.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                txtValidTo.Text = "";
                load_ddlApproval("0");

            } else if (intType == 2) {
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                btnDelete.Visible = true;

                dbcReader dbcVendorList = new dbcReader();

                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.V_APP_USER_APPROVAL_LIMIT where p_app_user_approval_limit_id=" + intIDRow, 0,0);
                DataTable dtresult = dsresult.Tables[0];

                txtListingNo.TextMode = TextBoxMode.Number;
                p_app_user_approval_limit_id.Text = dtresult.Rows[0]["p_app_user_approval_limit_id"].ToString();
                p_level_approval_type_id.Text = dtresult.Rows[0]["p_level_approval_type_id"].ToString();
                p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                txtUserName.Text = dtresult.Rows[0]["user_name"].ToString();
                txtFullName.Text = dtresult.Rows[0]["full_name"].ToString();
                txtNominalStart.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["acceptable_start_amt"].ToString()));
                txtNominalEnd.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["acceptable_end_amt"].ToString()));
                txtValidFrom.Text = dtresult.Rows[0]["valid_from"].ToString();
                txtValidTo.Text = dtresult.Rows[0]["valid_to"].ToString();
                txtListingNo.Text = dtresult.Rows[0]["listing_no"].ToString();
                s_entityId.SelectedValue = dtresult.Rows[0]["entity_id"].ToString();
                load_ddlApproval(p_level_approval_type_id.Text);
            }

            txtSelectID.Text = intIDRow.ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ((p_app_user_id.Text != "") && (txtNominalStart.Text != "") && (txtNominalEnd.Text != "")
               )
            {

                log.Info("Insert User Organ Started. ");
                try
                {
                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_approval_limit_rev";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlApproval.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalStart.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalEnd.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER APPROVAL", "");
                    
                   log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri,false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]));

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            if ((p_app_user_id.Text != "") && (txtNominalStart.Text != "") && (txtNominalEnd.Text != "")
              )
            {

                log.Info("Update User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_approval_limit_rev";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_approval_limit_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlApproval.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtListingNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalStart.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalEnd.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER APPROVAL", "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");                       
                        if (o_result_msg.Contains("ERROR:")) {
                            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());                        
                        }else{
                            Page.Response.Redirect(Page.Request.Url.AbsoluteUri,false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            pnlFilter.Visible = true;
                            pnlGridTender.Visible = true;
                            pnlMaintTender.Visible = false;
                            BindDataList(Convert.ToInt32(ViewState["current_page"]));
                        }
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            if ((p_app_user_id.Text != "") && (txtNominalStart.Text != "") && (txtNominalEnd.Text != "")
              )
            {

                log.Info("Delete User Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_user_approval_limit_rev";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_approval_limit_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", p_app_user_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_ddlApproval.SelectedValue, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtListingNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalStart.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtNominalEnd.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidFrom.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtValidTo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"].ToString(), "~#");
                    s_varvalue += String.Format(@"{0}{1}", "USER APPROVAL", "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Deleted !!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                        Page.Response.Redirect(Page.Request.Url.AbsoluteUri,false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        pnlFilter.Visible = true;
                        pnlGridTender.Visible = true;
                        pnlMaintTender.Visible = false;
                        BindDataList(Convert.ToInt32(ViewState["current_page"]));
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }


            }

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
           BindDataList(Convert.ToInt32(ViewState["current_page"]));
           Page.Response.Redirect(Page.Request.Url.AbsoluteUri);
        }
        private void load_ddlEntityList()
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType;
            if (Session["P_ENTITY_ID"].ToString() != "0")
            {
                dsType = dbcVendorList.db_get_entity_list(Convert.ToInt32(Session["P_ENTITY_ID"]));
            }
            else
            {
                dsType = dbcVendorList.ExecuteSelect("select p_entity_id, " +
                                                     "p_organization_id, entity_code,  " +
                                                     "entity_name, entity_addr, phone_1,  " +
                                                     "phone_2, fax_1, fax_2 from p_entity where is_valid ='Y'", 0, 0);
            }
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = 0;
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex > 0){
                gvTask.PageIndex -= 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            if (gvTask.PageIndex < (gvTask.PageCount - 1))
            {
                gvTask.PageIndex += 1;
            }
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            gvTask.PageIndex = (gvTask.PageCount - 1);
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            ViewState["filter"] = keyword.Text.Trim();
            BindDataList(Convert.ToInt32(ViewState["current_page"]));
        }

        private void load_ddlApproval(string strId)
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.ExecuteSelect("select P_LEVEL_APPROVAL_TYPE_ID,LEVEL_APPROVAL_DESC from dbo.P_LEVEL_APPROVAL_TYPE", 0, 0);
            DataTable dtType = dsType.Tables[0];
            s_ddlApproval.DataSource = dtType;
            s_ddlApproval.DataTextField = "LEVEL_APPROVAL_DESC";
            s_ddlApproval.DataValueField = "P_LEVEL_APPROVAL_TYPE_ID";
            if (strId != "0")
            {
                s_ddlApproval.SelectedValue = strId;
            }
            s_ddlApproval.DataBind();

        }


    }
}