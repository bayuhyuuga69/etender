﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;

namespace LCTender
{
    public partial class content_summary_plc : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                BindDataList();
            }

        }
        protected void BindDataList()
        {
            dbcReader dbcInbox = new dbcReader();
            DataSet dsresult = dbcInbox.db_get_wf_summary_list(Convert.ToInt32(Request["P_W_DOC_TYPE_ID"]),Session["UserName"].ToString());
            DataTable dtresult = dsresult.Tables[0];
            gvSummary.DataSource = dtresult;
            gvSummary.GridLines = GridLines.None;
            gvSummary.DataBind();

        }


        protected void gvSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSummary.PageIndex = e.NewPageIndex;
            gvSummary.DataBind();           
            BindDataList();
        }

        protected void gvSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String StrHtml = "";
                String task = (((DataRowView)e.Row.DataItem)["display_name"]).ToString();
                String url = (((DataRowView)e.Row.DataItem)["url"]).ToString();
                String element_id = (((DataRowView)e.Row.DataItem)["element_id"]).ToString();
                String profile_type = (((DataRowView)e.Row.DataItem)["profile_type"]).ToString();
                String p_w_proc_id = (((DataRowView)e.Row.DataItem)["p_w_proc_id"]).ToString();
                String scount = (((DataRowView)e.Row.DataItem)["scount"]).ToString();
                String Param = "?" + "ELEMENT_ID=" + element_id
                                   + "&P_W_DOC_TYPE_ID=" + Request["P_W_DOC_TYPE_ID"]
                                   + "&P_W_PROC_ID=" + p_w_proc_id
                                   + "&PROFILE_TYPE=" + profile_type
                                   + "&P_APP_USER_ID=" + Request["P_APP_USER_ID"]
                                   + "&sumworkflowDir=Asc&sumworkflowPageSize=50";
                Label lblButtonVal = (Label)e.Row.FindControl("lblButton");
                string img = "";
                if (profile_type == "INBOX")
                {
                   img = "<button type='button' class='btn btn-primary'>" + profile_type + "<span class='badge'>"+ scount + "</span></button>"; 
                }
                else if (profile_type == "OUTBOX")
                {
                    img = "<button type='button' class='btn btn-info'>" + profile_type + "<span class='badge'>" + scount + "</span></button>";
                }
                StrHtml += String.Format(@"<a href=""javascript:clickTask('{0}','{1}')"">{2}&nbsp;{3}</a><br/>", url, Param, img,task);
                lblButtonVal.Text = StrHtml;

            }
        }


    }
}