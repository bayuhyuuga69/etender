﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_user_task_plc.ascx.cs" Inherits="LCTender.content_user_task_plc" %>
   <div class="Row">
          <div class="col-sm-12 col-md-offset-5"><h5>DAFTAR PEKERJAAN</h5></div>
    </div>
     <div class="row" style="padding-top:20px">
         <div class="col-sm-12">
           <div class="input-group input-group-sm">
                <label for="email" class="col-sm-2">Filter:</label>
                <div class="col-sm-8">
                   <asp:TextBox id="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <div class="btn btn-default glyphicon glyphicon-search">
                            <asp:Button ID="btnCari" runat="server" Text="Cari" BackColor="Transparent" BorderWidth="0" OnClick="btnCari_Click" />
                     </div>
                </div>
            </div>    
         </div>
    </div>
    <div class="Row" style="padding-top:20px">
      <div class="col-sm-12">
          <asp:GridView ID="gvTask" runat="server" Width="100%"
               CssClass="mGrid table table-responsive table-striped table-hover"
               PagerStyle-CssClass="pgr"
               AlternatingRowStyle-CssClass="alt"
               CellPadding="5" CellSpacing="4"
               AutoGenerateColumns="False" 
               OnPageIndexChanging="gvTask_PageIndexChanging" 
               OnRowDataBound="gvTask_RowDataBound" 
               AllowPaging="true">
           <Columns>
               <asp:BoundField DataField="ltask" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="ltask" Visible="false" />
               <asp:BoundField DataField="sender" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="sender" Visible="false" />
               <asp:BoundField DataField="doc_sts" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="doc_sts" Visible="false" />  
               <asp:BoundField DataField="doc_no" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="doc_no" Visible="false" />
               <asp:BoundField DataField="FILENAME" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="FILENAME" Visible="false" /> 
               <asp:BoundField DataField="P_APP_USER_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_APP_USER_ID" Visible="false" /> 
               <asp:BoundField DataField="P_APP_USER_ID_DONOR" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_APP_USER_ID_DONOR" Visible="false" />
               <asp:BoundField DataField="P_APP_USER_ID_TAKEOVER" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_APP_USER_ID_TAKEOVER" Visible="false" />  
               <asp:BoundField DataField="T_CTL_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="T_CTL_ID" Visible="false" /> 
               <asp:BoundField DataField="P_W_DOC_TYPE_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_W_DOC_TYPE_ID" Visible="false" /> 
               <asp:BoundField DataField="P_W_PROC_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="P_W_PROC_ID" Visible="false" /> 
               <asp:BoundField DataField="DOC_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="DOC_ID" Visible="false" />
               <asp:BoundField DataField="DOC_STS" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="DOC_STS" Visible="false" />
               <asp:BoundField DataField="PROC_STS" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PROC_STS" Visible="false" />
               <asp:BoundField DataField="PREV_CTL_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PREV_CTL_ID" Visible="false" />
               <asp:BoundField DataField="PREV_DOC_TYPE_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PREV_DOC_TYPE_ID" Visible="false" />
               <asp:BoundField DataField="PREV_PROC_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PREV_PROC_ID" Visible="false" />
               <asp:BoundField DataField="PREV_DOC_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PREV_DOC_ID" Visible="false" />
               <asp:BoundField DataField="MESSAGE" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="MESSAGE" Visible="false" />
               <asp:BoundField DataField="SLOT_1" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SLOT_1" Visible="false" />
               <asp:BoundField DataField="SLOT_2" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SLOT_2" Visible="false" />
               <asp:BoundField DataField="SLOT_3" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SLOT_3" Visible="false" />
               <asp:BoundField DataField="SLOT_4" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SLOT_4" Visible="false" />
               <asp:BoundField DataField="SLOT_5" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SLOT_5" Visible="false" /> 
               <asp:BoundField DataField="SUBMIT_DATE" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="SUBMIT_DATE" Visible="false" />         
               <asp:TemplateField HeaderText="" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Center"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblButton" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="NO REGISTRASI" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblDoc" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PEKERJAAN" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblTask" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="PENGIRIM" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblSender" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>
            </Columns>
         </asp:GridView> 
      </div>
    </div>
    <br/>
  <asp:Label ID="lblNoRow" runat="server"></asp:Label>