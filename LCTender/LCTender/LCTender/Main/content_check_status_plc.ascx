﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_check_status_plc.ascx.cs" Inherits="LCTender.Main.content_check_status_plc" %>
<script src="../Scripts/jquery-2.1.4.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>
<link href="../Content/bootstrap.min.css" rel="stylesheet"/>
<link href="../Content/bootstrap-theme.css" rel="stylesheet"/>
<link href="../Content/bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.css" type="text/css" />
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.min.css" type="text/css" />
<script type="text/javascript" src="../Scripts/bootbox.js"></script>
<script type="text/javascript" src="../Scripts/bootbox.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-check {
      background: #fff;
      border-radius: 10px;
      margin: 20px auto 20px;
      padding: 15px 10px 30px 15px;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
  
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21;
      color: #ffffff;
    }
</style>
<div class="main-div-check">
    <div class="row">  
         <div class="col-sm-4">     
            <div class="form-group">
                <asp:TextBox id="keyword" cssclass="form-control input-sm" placeholder="Please Input Your Number Registration" runat="server" onkeypress="document.getElementById('regCheck').innerHTML='';"></asp:TextBox>          
            </div>
         </div> 
         <div class="col-sm-2">     
            <div class="form-group">
                <div class="btn btn-default glyphicon glyphicon-search">
                   <asp:Button ID="btnCari" runat="server" Text="Search" BackColor="Transparent" BorderWidth="0" OnClick="btnCari_Click"/>
                </div>           
            </div>
         </div>
         <div class="col-sm-6">  
            <asp:Literal ID="ltApproval" runat="server"></asp:Literal>             
         </div>        
     </div>
     <div class="row">
         <div class="col-sm-12">   
             <a href="Main/register.aspx" style="color:#f36e21;">Nomor Registrasi akan terkirim ke email jika anda sudah melakukan Registrasi</a>    
         </div>
     </div>  
</div> 
