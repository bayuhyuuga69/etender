﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using System.Net;
using System.Web.Security;
using log4net;
using System.Net.Mail;
using LCTender.Class;
using System.IO;
using System.Configuration;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace LCTender.Main
{
    public partial class register : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(register));
        public string main_color_theme = "";
        public string footer_color_theme = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["P_ENTITY_ID"] != null)
            {
                entityControlID.StrEntityID = Request["P_ENTITY_ID"];
                imageControlID.StrEntityID = Request["P_ENTITY_ID"];
                Session["P_ENTITY_ID"] = Request["P_ENTITY_ID"];
                BindAnnouncement(Request["P_ENTITY_ID"]);
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Request["P_ENTITY_ID"]);
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divReg.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
            }
            else
            {
                entityControlID.StrEntityID = "1";
                imageControlID.StrEntityID = "1";
                Session["P_ENTITY_ID"] = "1";
                BindAnnouncement("1");
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme("1");
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divReg.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
            }
            if (!IsPostBack)
            {
                if (Request["P_ENTITY_ID"] != null)
                {
                    entityControlID.StrEntityID = Request["P_ENTITY_ID"];
                    imageControlID.StrEntityID = Request["P_ENTITY_ID"];
                    Session["P_ENTITY_ID"] = Request["P_ENTITY_ID"];
                    BindAnnouncement(Request["P_ENTITY_ID"]);
                    clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Request["P_ENTITY_ID"]);                    
                    main_color_theme = entityTheme.main_color_theme;
                    footer_color_theme = entityTheme.footer_color_theme;
                    divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divReg.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
                }
                else
                {
                    entityControlID.StrEntityID = "1";
                    imageControlID.StrEntityID = "1";
                    Session["P_ENTITY_ID"] = "1";
                    BindAnnouncement("1");
                    clsEntityTheme entityTheme = clsEntityTheme.GetTheme("1");
                    main_color_theme = entityTheme.main_color_theme;
                    footer_color_theme = entityTheme.footer_color_theme;
                    divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divReg.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
                }
                load_ddlEntityList();
                btnValidate.Attributes.Add("OnClick", "javascript:disablereq()");
                btnResend.Text = "Resend OTP";
            }
           
        }

        private void load_ddlEntityList()
        {
            dbcReader dbcEntity = new dbcReader();
            DataSet dsType = dbcEntity.ExecuteSelect("SELECT P_ENTITY_ID,P_ORGANIZATION_ID,ENTITY_CODE,ENTITY_NAME,ENTITY_ADDR FROM P_ENTITY WHERE IS_VALID ='Y'", 0, 0);
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (Request["P_ENTITY_ID"] !=null)
                {
                    if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Request["P_ENTITY_ID"].ToString())
                    {
                        //s_entityId.Items[0].Selected = false;
                        s_entityId.Items[t].Selected = true;
                    }

                }

            }

        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if ((txtFullName.Text != "") && (txtEmail.Text != ""))
            {

                log.Info("Insert User Vendor Started. ");
                try
                {
                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_app_user_vendor";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", txtFullName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEmail.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtPassword.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", s_entityId.SelectedValue, "");


                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        string[] arr_result_msg = o_result_msg.Split(';');
                        string psCode = arr_result_msg[0];                        
                        txtUserName.Text = arr_result_msg[1];
                        string body = getBodyMail(txtFullName.Text,txtUserName.Text, psCode, s_entityId.SelectedItem.ToString());
                        //string body = "Your registration has been done successfully. Your User Name. "+ txtUserName.Text + " .Your Passcode. " + psCode;
                        ltEmail.Text = "<font color=\"#f36e21\">" + arr_result_msg[2] + "</ font >";
                        sendEmail(body, txtEmail.Text);
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("$('#MyOtpModal').modal({backdrop:'static',keyboard:false});");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());

                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

            }
        }
        protected void btnValidate_Click(object sender, EventArgs e)
        {
            dbcReader dbcLog = new dbcReader();
            int o_result_code = 0; int o_user_id = 0; string o_result_msg = "";
            DataSet dsresult = dbcLog.db_get_user_login_otp(txtUserName.Text.Trim().ToUpper(), txtPassword.Text.Trim(), txtPassCode.Text.Trim(), ref o_result_code, ref o_user_id, ref o_result_msg);
            DataTable dtresult = dsresult.Tables[0];

            if (o_result_msg == "sukses")
            {
                string strOrgan = dbcLog.ExecuteReader("select isnull(max(p_organization_id), 0) p_organization_id from p_app_user_organ where p_app_user_id =" + o_user_id.ToString(), "p_organization_id");
                Session["OrganId"] = strOrgan;
                Session["UserName"] = txtUserName.Text.Trim().ToUpper();
                Session["UserId"] = o_user_id;
                Session["Pass"] = txtPassCode.Text.Trim().ToUpper();
                Session["Status"] = "login";
                FormsAuthentication.RedirectFromLoginPage(txtUserName.Text.Trim().ToUpper(), true);
                Application.Lock();
                Application[txtUserName.Text.Trim().ToUpper()] = Session.SessionID;
                Application.UnLock();
                Response.Redirect("Home.aspx");
            }

        }

        protected void btnResend_Click(object sender, EventArgs e)
        {
            log.Info("Resend OTP Started. ");
            try
            {
                dbcReader dbcInsVendReg = new dbcReader();
                object outputObject;
                string s_packname = "p_resend_otp";
                string s_varvalue = "";
                s_varvalue += String.Format(@"{0}{1}", txtUserName.Text, "");

                log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                string s_outcodename = "";
                string s_outmsgname = "O_RESULT_MSG";
                int StartRecs = 0;
                int EndRecs = 0;

                outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                String o_result_code = (String)(pi.GetValue(outputObject, null));

                System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                if (o_result_code == "1")
                {
                    string[] arr_result_msg = o_result_msg.Split(';');
                    string psCode = arr_result_msg[0];
                    txtUserName.Text = arr_result_msg[1];
                    string body = getBodyMail(txtFullName.Text,txtUserName.Text, psCode,s_entityId.SelectedItem.ToString()); 
                    ltEmail.Text = "<font color=\"#f36e21\">" + txtEmail.Text + "</ font >";
                    sendEmail(body, txtEmail.Text);
                    //ltResend.Text = "<span class='badge'>New Passcode Sent</span>";
                    btnResend.Text = "New Passcode Sent";
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("$('#MyOtpModal').modal({backdrop:'static',keyboard:false});");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());

                }
                else if (o_result_code == "-1")
                {
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("bootbox.alert({ ");
                    cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                    cstext1.Append("className: 'bb-alternate-modal' });");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                }

            }
            catch (Exception ex)
            {
                log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }
        public void sendEmail(string body, string email)
        {
            try
            {
                string smtpconfig = ConfigurationManager.AppSettings["smtp"].ToString();
                string user = ConfigurationManager.AppSettings["user_reg"].ToString();
                string password = ConfigurationManager.AppSettings["password_reg"].ToString();
                string port = ConfigurationManager.AppSettings["port"].ToString();
                string subject = ConfigurationManager.AppSettings["subject_reg"].ToString();
                MailMessage mail = new MailMessage();
                mail.To.Add(email);
                mail.From = new MailAddress(user);
                //mail.From = new MailAddress("noreply@lippo-cikarang.com");
                mail.Subject = subject;

                mail.Body = body;

                mail.IsBodyHtml = true;
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                SmtpClient smtp = new SmtpClient();
                smtp.Host = smtpconfig;
                //smtp.Host = "10.128.2.11";
                smtp.Timeout = 10000;//Or Your SMTP Server Address
                smtp.Credentials = new System.Net.NetworkCredential
                     (user, password); // ***use valid credentials***
                smtp.Port = Convert.ToInt32(port);
                //smtp.Credentials = new System.Net.NetworkCredential
                //     ("noreply", "lippo12345"); // ***use valid credentials***
                //smtp.Port = 25;

                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Exception in sendEmail: {0}", ex.Message);
                throw;
            }
        }
        private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            //Console.WriteLine(certificate);
            return true;
        }

        protected void BindAnnouncement(string strEntityID)
        {
            if (strEntityID == "0")
            {
                strEntityID = "1";
            }
            log.Info("Bind Data Entity Started. ");
            try
            {
                dbcReader dbcCarousel = new dbcReader();
                DataSet dsresult = dbcCarousel.ExecuteSelect("Select * from dbo.P_ANNOUNCEMENT where p_entity_id=" + strEntityID, 0, 0);
                DataTable dtresult = dsresult.Tables[0];

                ltTtlAnnouncement.Text = "";
                ltAnnouncement.Text = "";

                if (dtresult.Rows.Count > 0)
                {

                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        ltTtlAnnouncement.Text = String.Format(@"<h1>{0}</h1>", dtresult.Rows[i]["title"].ToString());
                        ltAnnouncement.Text = String.Format(@"{0}", dtresult.Rows[i]["content"].ToString());
                        ltImgAnnouncement.Text = String.Format(@"<img class='img-responsive' src='../images/{0}'>", dtresult.Rows[i]["file_desc"].ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                log.ErrorFormat("Bind Data Entity Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        public string getBodyMail(string fullname, string username, string passcode, string item)
        {
            string roothPathTemplate = ConfigurationManager.AppSettings["RootPathTemplateRegister"].ToString();
            string mailBody = File.ReadAllText(roothPathTemplate);
            mailBody = mailBody.Replace("{FULLNAME}", fullname);
            mailBody = mailBody.Replace("{USERNAME}", username);
            mailBody = mailBody.Replace("{PASSCODE}", passcode);
            mailBody = mailBody.Replace("{ENTITY}", item);
            return mailBody;
        }

    }
}