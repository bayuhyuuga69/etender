﻿using System;
using System.Web.UI;

namespace LCTender
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Control plcInboxCtl = (Control)LoadControl("content_inbox_plc.ascx");
            plcLinks.Controls.Add(plcInboxCtl);
            if (Session["P_ENTITY_ID"] != null)
            {
                entityControlID.StrEntityID = Session["P_ENTITY_ID"].ToString();
            }
            if (!IsPostBack)
            {
                ShowData();
                if (Session["P_ENTITY_ID"] != null)
                {
                    entityControlID.StrEntityID = Session["P_ENTITY_ID"].ToString();
                }
            }

        }

        private void ShowData()
        {           
            String chart = "";
               
            chart = "<canvas id=\"line-chart\" width=\"100%\" height=\"60\"></canvas>";
            chart += "<script> new Chart(document.getElementById(\"line-chart\"),{type:'line',";
            chart += "data: { labels: [\"January\", \"Feburary\", \"March\"],datasets: [{ data: [15, 35, 35],label: \"INBOX\",borderColor: \"#3e95cd\",fill: true},{data: [20, 10, 40],label: \"OUTBOX\",borderColor: \"#F08080\",fill: true}]}, ";
            chart += "options: { title: { display: true,text: 'Inbox Qualified Vendor List'} } ";
            chart += "}); ";
            chart += "</script>";

            ltChart.Text = chart;
        }

    }
}