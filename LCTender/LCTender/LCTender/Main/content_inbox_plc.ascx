﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_inbox_plc.ascx.cs" Inherits="LCTender.content_inbox_plc" %>
   <div class="Row">
      <div class="col-sm-12">
          <asp:GridView ID="gvSummary" runat="server" Width="100%"
               AutoGenerateColumns="False" 
               OnPageIndexChanging="gvSummary_PageIndexChanging" 
               OnRowDataBound="gvSummary_RowDataBound" 
               AllowPaging="true">
           <Columns>
               <asp:BoundField DataField="url" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="url" Visible="false" />
               <asp:BoundField DataField="workflow_name" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="workflow_name" Visible="false" />
               <asp:TemplateField HeaderText="NOTIFIKASI" HeaderStyle-CssClass="visible-desktop" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblWorkflow" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>               
               <asp:BoundField DataField="jumlah" HeaderText="JUMLAH" ItemStyle-CssClass="hidden-phone" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="hidden-phone"   />
            </Columns>
         </asp:GridView> 
      </div>
    </div>
<asp:Literal ID="ltInbox" runat="server"></asp:Literal>
 <%--<div class="jumbotron">
        <h1>Navbar example</h1>
        <p>
            This example is a quick exercise to illustrate how the default, static navbar and
            fixed to top navbar work. It includes the responsive CSS and HTML, so it also adapts
            to your viewport and device.
        </p>
        <p>
            <a class="btn btn-lg btn-primary" href="#" role="button">View navbar docs &raquo;</a>
        </p>       
    </div>  --%>