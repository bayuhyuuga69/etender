﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="LCTender.Dashboard" %>
<%@ Register src="content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
    <style type="text/css">
         .btn.btn-primary {
          background: #f36e21 none repeat scroll 0 0;
          border-color: #f36e21;
          color: #ffffff;
      }
    </style>

    <div class="row">
      <form id="form1" runat="server"> 
        <div class="main-div-dash">
           <div class="col-sm-4"><asp:PlaceHolder ID="plcLinks" runat="server"></asp:PlaceHolder></div>
           <div class="col-sm-8">
              <div class="table-responsive">
                <div class="col-sm-12"><asp:PlaceHolder ID="plcTask" runat="server"></asp:PlaceHolder></div>              
              </div> 
           </div>
        </div>
      </form>
        <script type="text/javascript">
            debugger;
            function clickTask(url,param) {
                document.location.href = url + param;
            }
        </script>
   </div>
   <%--<div class="footer navbar-fixed-bottom" style="background-color:#259fd9;margin-left:16px;margin-right:16px">     
       <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>