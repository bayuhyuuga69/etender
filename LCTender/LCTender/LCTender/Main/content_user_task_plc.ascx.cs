﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;

namespace LCTender
{
    public partial class content_user_task_plc : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                BindDataList();
            }
        }

        protected void BindDataList()
        {
            
            dbcReader dbcInbox = new dbcReader();
            float flt_doc_type = float.Parse(Request["P_W_DOC_TYPE_ID"]);
            float flt_w_proc;
            if (Request["P_W_PROC_ID"] == null)
            {
                flt_w_proc=0;
            }
            else
            {
                flt_w_proc = float.Parse(Request["P_W_PROC_ID"]);
            }

            string str_prof_type = Request["PROFILE_TYPE"];           
            DataSet dsresult = dbcInbox.db_get_user_task_list(flt_doc_type,flt_w_proc,str_prof_type,Session["UserName"].ToString(),keyword.Text);
            DataTable dtresult = dsresult.Tables[0];
            gvTask.DataSource = dtresult;
            gvTask.DataBind();
            gvTask.HeaderRow.TableSection = TableRowSection.TableHeader;

            if (dtresult.Rows.Count == 0)
            {
                lblNoRow.Text = "" +
                "<div class='Row'>" +
                  "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                  "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>NO REGISTRASI/TENDER</div>" +
                  "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PEKERJAAN </div>" +
                  "<div class='col-sm-3' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PENGIRIM</div>" +                  
                "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }

        }

       
        protected void gvTask_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTask.PageIndex = e.NewPageIndex;
            gvTask.DataBind();
            BindDataList();
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
           BindDataList();
        }

        protected void gvTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblButtonVal = (Label) e.Row.FindControl("lblButton");
                Label lblDocVal = (Label)e.Row.FindControl("lblDoc");
                Label lblTaskVal = (Label) e.Row.FindControl("lblTask");
                Label lblSenderVal = (Label)e.Row.FindControl("lblSender");



                String Filenm = (((DataRowView)e.Row.DataItem)["FILENAME"]).ToString();
                String aIS_CREATE_DOC = "N";
                String aIS_MANUAL = "N";
                String auser_id_login = Request["P_APP_USER_ID"];
                String auser_id_doc = Request["P_APP_USER_ID"]; //(((DataRowView)e.Row.DataItem)["P_APP_USER_ID_DONOR"]).ToString();  
                String auser_id_donor  = Request["P_APP_USER_ID"]; //(((DataRowView)e.Row.DataItem)["P_APP_USER_ID_DONOR"]).ToString(); 
                String auser_id_taken  = (((DataRowView)e.Row.DataItem)["P_APP_USER_ID_TAKEOVER"]).ToString(); 
                String acurr_ctl_id   = (((DataRowView)e.Row.DataItem)["T_CTL_ID"]).ToString();
                String acurr_doc_type_id = (((DataRowView)e.Row.DataItem)["P_W_DOC_TYPE_ID"]).ToString();
                String acurr_proc_id  = (((DataRowView)e.Row.DataItem)["P_W_PROC_ID"]).ToString(); 
                String acurr_doc_id  = (((DataRowView)e.Row.DataItem)["DOC_ID"]).ToString(); 
                String acurr_doc_status  = (((DataRowView)e.Row.DataItem)["DOC_STS"]).ToString(); 
                String acurr_proc_status  = (((DataRowView)e.Row.DataItem)["PROC_STS"]).ToString(); 
                String aprev_ctl_id       = (((DataRowView)e.Row.DataItem)["PREV_CTL_ID"]).ToString(); 
                String aprev_doc_type_id  = (((DataRowView)e.Row.DataItem)["PREV_DOC_TYPE_ID"]).ToString(); 
                String aprev_proc_id      = (((DataRowView)e.Row.DataItem)["PREV_PROC_ID"]).ToString(); 
                String aprev_doc_id       = (((DataRowView)e.Row.DataItem)["PREV_DOC_ID"]).ToString(); 
                String amessage  = (((DataRowView)e.Row.DataItem)["MESSAGE"]).ToString(); 
                String aslot_1   = (((DataRowView)e.Row.DataItem)["SLOT_1"]).ToString(); 
                String aslot_2   = (((DataRowView)e.Row.DataItem)["SLOT_2"]).ToString();
                String aslot_3   = (((DataRowView)e.Row.DataItem)["SLOT_3"]).ToString(); 
                String aslot_4   = (((DataRowView)e.Row.DataItem)["SLOT_4"]).ToString(); 
                String aslot_5   = (((DataRowView)e.Row.DataItem)["SLOT_5"]).ToString();
                String submit_date = (((DataRowView)e.Row.DataItem)["SUBMIT_DATE"]).ToString();

                String paramtr = "";
                paramtr += String.Format(@"&CURR_DOC_ID={0}", acurr_doc_id);
                paramtr += String.Format(@"&CURR_PROC_ID={0}", acurr_proc_id);
                paramtr += String.Format(@"&CURR_DOC_TYPE_ID={0}", acurr_doc_type_id);
                paramtr += String.Format(@"&CURR_CTL_ID={0}", acurr_ctl_id);
                paramtr += String.Format(@"&USER_ID_DOC={0}", auser_id_doc);
                paramtr += String.Format(@"&USER_ID_DONOR={0}", auser_id_donor);
                paramtr += String.Format(@"&USER_ID_LOGIN={0}", auser_id_login);
                paramtr += String.Format(@"&USER_ID_TAKEN={0}", auser_id_taken);
                paramtr += String.Format(@"&IS_CREATE_DOC={0}", aIS_CREATE_DOC);
                paramtr += String.Format(@"&IS_MANUAL={0}", aIS_MANUAL);
                paramtr += String.Format(@"&CURR_PROC_STATUS={0}", acurr_proc_status);
                paramtr += String.Format(@"&CURR_DOC_STATUS={0}", acurr_doc_status);
                paramtr += String.Format(@"&PREV_DOC_ID={0}", aprev_doc_id);
                paramtr += String.Format(@"&PREV_DOC_TYPE_ID={0}", aprev_doc_type_id);
                paramtr += String.Format(@"&PREV_PROC_ID={0}", aprev_proc_id);
                paramtr += String.Format(@"&PREV_CTL_ID={0}", aprev_ctl_id);
                paramtr += String.Format(@"&SLOT_1={0}", aslot_1);
                paramtr += String.Format(@"&SLOT_2={0}", aslot_2);
                paramtr += String.Format(@"&SLOT_3={0}", aslot_3);
                paramtr += String.Format(@"&SLOT_4={0}", aslot_4);
                paramtr += String.Format(@"&SLOT_5={0}", aslot_5);
                paramtr += String.Format(@"&MESSAGE={0}", amessage);

                lblButtonVal.Text = "<a href='"+ Filenm+paramtr + "'><button type='button' class='btn btn-primary btn-xs'>BUKA</button></a>";
                String strDoc = "";
                String strTask   = "";
                String strSender = "";

                String doc = (((DataRowView)e.Row.DataItem)["doc_no"]).ToString();
                String task = (((DataRowView)e.Row.DataItem)["ltask"]).ToString();
                String senderTask = (((DataRowView)e.Row.DataItem)["sender"]).ToString();
                String doc_sts = (((DataRowView)e.Row.DataItem)["doc_sts"]).ToString();

                strDoc += String.Format(@"<div class='row'>");
                strDoc += String.Format(@"<div class='col-sm-3'>{0}</div>", doc);
                strDoc += String.Format(@"</div>");

                lblDocVal.Text = strDoc;

                strTask += String.Format(@"<div class='row'>");
                strTask += String.Format(@"<div class='col-sm-3'>{0}</div>", task);
                strTask += String.Format(@"</div>");     
                         
                lblTaskVal.Text = strTask;

                strSender += String.Format(@"<div class='row'>");
                strSender += String.Format(@"<div class='col-sm-3'>{0}</div>", senderTask);
                strSender += String.Format(@"</div>");

                lblSenderVal.Text = strSender;
            }
        }
    }
}