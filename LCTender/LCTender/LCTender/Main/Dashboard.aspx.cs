﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender.Class;
namespace LCTender
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Control plcLinksCtl = (Control)LoadControl("content_summary_plc.ascx");
            plcLinks.Controls.Add(plcLinksCtl);

            Control plcTaskCtl = (Control)LoadControl("content_user_task_plc.ascx");
            plcTask.Controls.Add(plcTaskCtl);

            //entityControlID.StrEntityID = Session["P_ENTITY_ID"].ToString();

        }
    }
}