﻿using System;
using System.Linq;
using WsLCTender;
using log4net;
using System.Data;

namespace LCTender.Main
{
    public partial class content_image_reg_plc : System.Web.UI.UserControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(content_image_plc));
        private string strEntityID;
        protected void Page_Load(object sender, EventArgs e)
        {
            BindCarousel(strEntityID);
        }
        protected void BindCarousel(string strEntityID)
        {
            if (strEntityID == "0")
            {
                strEntityID = "1";
            }
            log.Info("Bind Data Entity Started. ");
            try
            {
                dbcReader dbcCarousel = new dbcReader();
                DataSet dsresult = dbcCarousel.ExecuteSelect("Select * from dbo.P_CAROUSEL where getDate() between valid_from and isnull(valid_to,getDate()+1) and p_entity_id=" + strEntityID, 0, 0);
                DataTable dtresult = dsresult.Tables[0];

                ltIndicator.Text = "";
                String strIndicator = "";
                ltWrapperSlide.Text = "";
                String strWrapper = "";

                if (dtresult.Rows.Count > 0)
                {

                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        String ClassActive = "";
                        if (dtresult.Rows[i]["is_active_class"].ToString() == "Y")
                        {
                            ClassActive = "class='active'";
                        }
                        strIndicator += String.Format(@"<li data-target='#myCarousel' data-slide-to='{0}' {1}></li>", dtresult.Rows[i]["listing_no"].ToString(), ClassActive);
                    }
                }
                ltIndicator.Text = strIndicator;
                if (dtresult.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        String ClassActive = "";
                        if (dtresult.Rows[i]["is_active_class"].ToString() == "Y")
                        {
                            ClassActive = "class='item active'";
                        }
                        else
                        {
                            ClassActive = "class='item'";
                        }

                        strWrapper += String.Format(@"<div {1}><img class='img-responsive' src='../images/{0}'></div>", dtresult.Rows[i]["file_name"].ToString(), ClassActive);
                    }
                }
                ltWrapperSlide.Text = strWrapper;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Bind Data Entity Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        public string StrEntityID
        {
            get
            {
                return strEntityID;
            }
            set
            {
                strEntityID = value;
            }
        }
    }
}