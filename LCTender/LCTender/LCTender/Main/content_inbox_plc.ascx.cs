﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;

namespace LCTender
{
    public partial class content_inbox_plc : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                BindDataList();
            }

        }

        protected void BindDataList()
        {
            dbcReader dbcInbox = new dbcReader();
            DataSet dsresult = dbcInbox.db_get_workflowname(Session["UserName"].ToString());
            DataTable dtresult = dsresult.Tables[0];
            gvSummary.DataSource = dtresult;
            gvSummary.GridLines = GridLines.None;
            gvSummary.DataBind();


        }


        protected void gvSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSummary.PageIndex = e.NewPageIndex;
            gvSummary.DataBind();
            BindDataList();
        }

        protected void gvSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String url = (((DataRowView)e.Row.DataItem)["url"]).ToString();
                String workflow_name = (((DataRowView)e.Row.DataItem)["workflow_name"]).ToString();

                Label lblWFVal = (Label)e.Row.FindControl("lblWorkflow");
                lblWFVal.Text = "<a href='" + url + "'>" + workflow_name + "</a>";
            }
        }

    }
}