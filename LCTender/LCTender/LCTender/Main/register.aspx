﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LCindex.Master" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="LCTender.Main.register" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Register src="content_image_reg_plc.ascx" tagname="imageControl" tagprefix="imageCtl" %>
<%@ Register src="content_project_plc.ascx" tagname="projectControl" tagprefix="projectCtl" %>
<%@ Register src="content_check_status_plc.ascx" tagname="checkControl" tagprefix="checkCtl" %>
<%@ Register src="content_entity_plc.ascx" tagname="entityControl" tagprefix="entityCtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<style type="text/css">
.row [class*="col-"]{
  margin-bottom: -99999px;
  padding-bottom: 99999px;
}

.row{
  overflow: hidden; 
}
.panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
.login-form .form-control {
    background: #f7f7f7 none repeat scroll 0 0;
    border: 1px solid #d4d4d4;
    border-radius: 4px;
    font-size: 14px;
    height: 50px;
    line-height: 50px;
}
.main-div-grid {
    background: #fff none repeat scroll 0 0;
    border-radius: 10px;
    margin: 10px auto 10px;
    padding: 10px 10px 30px 10px;    
}

.login-form .form-group {
    margin-bottom:10px;
}
.login-form{ text-align:left;}
.forgot a {
    color: #777777;
    font-size: 14px;
    text-decoration: underline;
}
  
.forgot {
    text-align: left; margin-bottom:30px;
}
.botto-text {
    color: #ffffff;
    font-size: 14px;
    margin: auto;
}
.login-form .btn.btn-primary.reset {
    background: #f36e21 none repeat scroll 0 0;
}
.back { text-align: left; margin-top:10px;}
.back a {color: #444444; font-size: 13px;text-decoration: none;}
.modal-dialog {
    height: 40%;
    width: 40%;
    padding-left:30px;
    padding-right:30px;
}
    
.modal-content {
    /*height: 99%;*/
}
.hidden
    {
        display:none;
    }
.sizer {
  /*width: 500px;*/
}
.btn.btn-primary {
    background: #f36e21;
    color: #ffffff;
    border: none;
}
.btnValidate{
    background:#2bb673;
     color: #ffffff;
}
.btnResendOTP{
    background:#fff;
    color: #444444;
}
h1, h4, h5, h6 {
    text-align: left;
}
</style>
<script type="text/javascript">                
    function show() {
        //$('#MyOtpModal').modal('show');
        $('#MyOtpModal').modal({ backdrop: 'static', keyboard: false })
    }
    function disablereq() {
        $('[id*=txt]').removeAttr('required');
    }
    function backToLogin() {
        var entityID = <%= new JavaScriptSerializer().Serialize(Request.QueryString["P_ENTITY_ID"]) %>;
        $(location).attr('href', "../index.aspx?P_ENTITY_ID="+entityID);
    }
</script>
<form runat="server" class="login-form">
  <div class="row">
    <div class="col-sm-8">
        <imageCtl:imageControl ID="imageControlID" runat="server"/>
    </div>
    <div id="divReg" runat="server" class="col-sm-4" style="background-color:#0077bc;color:#fff">
        <%--<div class="main-div"> --%>  
          <h2  style="padding-top:5px">Vendor Register</h2>
          <div>
              <hr/>
          </div>
          <div class="input-group" style="padding-top:5px">
            <span class="input-group-addon"><i class="glyphicon glyphicon-cloud"></i>&nbsp;Entity</span>
            <asp:DropDownList ID="s_entityId" cssclass="form-control" runat="server"></asp:DropDownList><br/>
          </div>
          <div class="input-group" style="padding-top:5px">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <asp:TextBox ID="txtFullName" cssclass="form-control" runat="server" placeholder="fullname"></asp:TextBox>
          </div>
          <div class="input-group" style="padding-top:5px">
            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
            <asp:TextBox ID="txtEmail" textmode="Email" cssclass="form-control" runat="server" placeholder="email"></asp:TextBox>
          </div>
          <div class="input-group" style="padding-top:5px">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <asp:TextBox ID="txtPassword" textmode="password" cssclass="form-control" runat="server" placeholder="password"></asp:TextBox>          
          </div>
        <div class="input-group pull-right" style="padding-top: 40px">
            <button type="button" class="btn btn-info" onclick="javascript:backToLogin()"><span class="glyphicon glyphicon-lock"></span>&nbsp;Back To Login</button>&nbsp;<asp:Button ID="btnRegister" cssclass="btn btn-primary pull-right" runat="server" Text="Apply" OnClick="btnRegister_Click" />
        </div>        
         <asp:TextBox ID="p_entity_id" class="hidden" name="p_entity_id" runat="server"></asp:TextBox>
         <asp:TextBox ID="txtUserName" class="hidden" runat="server"></asp:TextBox>
 <%-- </div>--%>
      <!-- Modal -->
       <div class="modal fade" id="MyOtpModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12" style="background-color:#495767;color:#fff">
                          <h3>Validate OTP (On Time Passcode)</h3>  
                         </div>                         
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-12" style="background-color:#fff;color:#444444;padding-left:30px;padding-right:30px">
                             A one time passcode has been sent to <asp:Literal ID="ltEmail" runat="server"></asp:Literal><br>
                             Please enter the OTP below to verify your Email Address. If you cannot see the email from your inbox. Make sure to check your SPAM folder
                         </div>                         
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-12" style="background-color:#fff;color:#444444;padding-left:30px;padding-right:30px">
                            <asp:TextBox ID="txtPassCode" cssclass="form-control" runat="server"></asp:TextBox>
                         </div>                         
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-12" style="background-color:#fff;color:#444444;padding-left:30px;padding-right:30px">
                            <asp:Button ID="btnValidate" cssclass="btn btnValidate" runat="server" Text="Validate" OnClick="btnValidate_Click"/>                              
                            <asp:Button ID="btnResend"  cssclass="btn btn-info pull-right" runat="server" OnClick="btnResend_Click"></asp:Button><asp:Literal ID="ltResend" runat="server"></asp:Literal>
                         </div>                         
                    </div>
                </div>                
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>                               
            </div>      
        </div>
       </div>
     <!-- Modal -->
    </div>
  </div>
   <div class="row">
    <div class="col-sm-6" id="divAnnouncement" runat="server">
      <asp:Literal ID="ltTtlAnnouncement" runat="server"></asp:Literal>     
      <asp:Literal ID="ltAnnouncement" runat="server"></asp:Literal>
    </div>
    <div class="col-sm-6" id="divImage" runat="server">
       <asp:Literal ID="ltImgAnnouncement" runat="server"></asp:Literal>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12" style="background-color:#e8edf0;">
      <h3>Latest Tender:</h3>
      <div class="main-div-grid">
        <div class="table-responsive">
         <projectCtl:projectControl ID="projectControlID" runat="server"/>
        </div>
      </div>
    </div>   
  </div>
  <div class="row">
    <div class="col-sm-12" style="background-color:#e8edf0;">
        <h4>Check Status:</h4>
        <checkCtl:checkControl ID="checkControlID" runat="server"/>
    </div>   
  </div>
  <div class="row">
    <div class="col-sm-12" id="divEntity" runat="server">
      <entityCtl:entityControl ID="entityControlID" runat="server"/>
    </div>   
  </div> 
</form> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>
