﻿using System;
using WsLCTender;
using log4net;
using System.Data;

namespace LCTender.Main
{
    public partial class content_entity_plc : System.Web.UI.UserControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(content_entity_plc));
        private string strEntityID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataEntity(Convert.ToInt32(strEntityID));
            }
        }

        protected void BindDataEntity(int intEntity)
        {
                if (intEntity==0)
                {
                    intEntity = 1;
                }
                log.Info("Bind Data Entity Started. ");
                try
                {
                   
                    dbcReader dbcShowEntity = new dbcReader();
                    DataSet dsresult = dbcShowEntity.db_get_entity_list(intEntity);
                    DataTable dtresult = dsresult.Tables[0];

                    if (dtresult.Rows.Count >0)
                    {

                        ltEntityName.Text = dtresult.Rows[0]["entity_name"].ToString();
                        ltEntityAddr.Text = dtresult.Rows[0]["entity_addr"].ToString();
                        ltPhone.Text = "T." + dtresult.Rows[0]["phone_1"].ToString() + "," + dtresult.Rows[0]["phone_2"].ToString();
                        ltFax.Text = "F." + dtresult.Rows[0]["phone_1"].ToString() + "," + dtresult.Rows[0]["phone_2"].ToString();
                    }
                  
                   
               }
                catch (Exception ex)
                {
                    log.ErrorFormat("Bind Data Entity Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }

        }

        public string StrEntityID
        {
            get
            {
                return strEntityID;
            }
            set
            {
                strEntityID = value;
            }
        }

    }

   
}