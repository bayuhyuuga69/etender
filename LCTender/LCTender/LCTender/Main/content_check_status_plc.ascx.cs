﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;

namespace LCTender.Main
{
    public partial class content_check_status_plc : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ltApproval.Text = ltApproval.Text;
        }
        protected void btnCari_Click(object sender, EventArgs e)
        {
            if (keyword.Text!="") {
                dbcReader clsReader = new dbcReader();
                String appStatus = clsReader.ExecuteReader("select x.request_status from sis.dbo.T_VENDOR_REGISTER x, dbo.P_APP_USER y where x.p_app_user_id=y.p_app_user_id AND upper(ltrim(rtrim(x.register_no)))+upper(ltrim(rtrim(y.user_name))) like '%'+isnull(upper(ltrim(rtrim('" + keyword.Text.Trim() + "'))),null)+'%'", "request_status");
                if (appStatus == "1")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-info\">submitted</button></span>";
                }
                else if (appStatus == "114")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-success\">approved</button></span>";
                }
                else if (appStatus == "115")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-danger\">rejected</button></span>";
                }
                else if (appStatus == "118")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-success\">join tender</button></span>";
                }
                else if (appStatus == "0")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-info\">register</button></span>";
                }
                else
                {
                    ltApproval.Text = "<span id='regCheck'>Check Again Your Register No</span>";
                }
            }else
            {
                ltApproval.Text = "";
            }
            this.keyword.Focus();
        }
        protected void keyword_TextChanged(object sender, EventArgs e)
        {
            if (keyword.Text == "")
            {
                ltApproval.Text = "";
            }
                if (keyword.Text != "")
            {
                dbcReader clsReader = new dbcReader();
                String appStatus = clsReader.ExecuteReader("select request_status from sis.dbo.T_VENDOR_REGISTER where register_no='" + keyword.Text.Trim() + "'", "request_status");
                if (appStatus == "1")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-info\">submitted</button></span>";
                }
                else if (appStatus == "114")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-success\">approved</button></span>";
                }
                else if (appStatus == "115")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-danger\">rejected</button></span>";
                }
                else if (appStatus == "0")
                {
                    ltApproval.Text = "<span id='regCheck'>Your status has been :<button type=\"button\" class=\"btn btn-info\">register</button></span>";
                }
                else
                {
                    ltApproval.Text = "<span id='regCheck'>Check Again Your Register No</span>";
                    Page_Load(sender, e);
                }
            }
            this.keyword.Focus();
        }
    }
}