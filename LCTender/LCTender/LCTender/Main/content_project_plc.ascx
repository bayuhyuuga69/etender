﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_project_plc.ascx.cs" Inherits="LCTender.Transaction.content_project_plc" %> 
<%--<asp:Panel ID="pnlGridVend" runat="server"> --%>   
    <asp:GridView ID="gvTask" runat="server" Width="100%"
        CssClass="mGrid"
        PagerStyle-CssClass="pgr"
        AlternatingRowStyle-CssClass="alt"
        CellPadding="5" CellSpacing="4"
        AutoGenerateColumns="False"
        PageSize="10">
        <AlternatingRowStyle CssClass="alt" />
        <Columns>
            <asp:BoundField DataField="P_PROJECT_TENDER_ID" HeaderText="P_PROJECT_TENDER_ID" ItemStyle-CssClass="hidden-phone">
            <ItemStyle CssClass="hidden" />
            <HeaderStyle CssClass="hidden" />
            </asp:BoundField>                                                           
            <asp:BoundField DataField="PROJECT_NAME" HeaderText="PROJECT NAME">                                       
            </asp:BoundField>
            <asp:BoundField DataField="BUSINESS_TYPE" HeaderText="CATEGORY">                                       
            </asp:BoundField>
            <asp:BoundField DataField="ENTITY_NAME" HeaderText="ENTITY">                                       
            </asp:BoundField>
            <asp:BoundField DataField="DESCRIPTION" HeaderText="SUMMARY">                                       
            </asp:BoundField> 
            <asp:BoundField DataField="VALID_FROM_ALS" HeaderText="OPENING DATE">    
            <ItemStyle HorizontalAlign="Center" />                                   
            </asp:BoundField>  
            <asp:BoundField DataField="VALID_TO_ALS" HeaderText="CLOSING DATE">   
            <ItemStyle HorizontalAlign="Center" />                                    
            </asp:BoundField> 
        </Columns>
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="20" />
        <PagerStyle CssClass="pgr" />
    </asp:GridView>
   <asp:Label ID="lblNoRow" runat="server"></asp:Label> 
    <div class="Row">
        <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;">
            <asp:ImageButton ID="imbFirst" runat="server" OnClick="imbFirst_Click" />
            <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click"/>
            &nbsp;
            <asp:Label ID="lblPaging" runat="server"></asp:Label>
            &nbsp;
            <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" style="width: 14px" />
            <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
        </div>
    </div>
<%--</asp:Panel>--%>
<asp:TextBox ID="txtSelectID" cssclass="hidden" runat="server"></asp:TextBox>
<script type="text/javascript">
    function clickEdit(id,type) {
        $('#frmMaint').attr('src', "t_vendor_register_maint.aspx?t_vendor_register_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
    function clickAdd(id, type) {
        debugger;
        $('#frmMaint').attr('src', "t_vendor_register_maint.aspx?t_vendor_register_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
    function disablereq() {
        $('[id*=txt]').removeAttr('required');
    }
</script>