﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_chart inbox_plc.ascx.cs" Inherits="LCTender.content_chart_inbox_plc" %>
<script src="../Scripts/Chart.min.js"></script>
    <canvas id="barChartLoc" height="300" width="300"></canvas>
    <script>
            var barChartLocData = {
                labels: ["January", "Feburary", "March"],
                datasets: [{ fillColor: "lightblue", strokeColor: "blue", data: [15, 20, 35] }]
            };
            var mybarChartLoc = new Chart(document.getElementById("barChartLoc").getContext("2d")).Bar
            (barChartLocData);
    </script>