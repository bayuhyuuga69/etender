﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_summary_plc.ascx.cs" Inherits="LCTender.content_summary_plc" %>
    <div class="Row">
      <div class="col-sm-12">
          <asp:GridView ID="gvSummary" runat="server" Width="100%"
               AutoGenerateColumns="False" 
               OnPageIndexChanging="gvSummary_PageIndexChanging" 
               OnRowDataBound="gvSummary_RowDataBound" 
               AllowPaging="true">
           <Columns>
               <asp:BoundField DataField="display_name" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="PEKERJAAN" Visible="false" />
               <asp:BoundField DataField="url" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="url" Visible="false" />
               <asp:BoundField DataField="element_id" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="element_id" Visible="false" />
               <asp:BoundField DataField="profile_type" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="profile_type" Visible="false" />
               <asp:BoundField DataField="p_w_proc_id" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="p_w_proc_id" Visible="false" />
               <asp:TemplateField HeaderText="" HeaderStyle-CssClass="visible-desktop" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left"  ItemStyle-CssClass="visible-desktop">                
                   <ItemTemplate>
                       <asp:Label ID="lblButton" runat="server"></asp:Label>
                   </ItemTemplate>
               </asp:TemplateField>               
               <asp:BoundField DataField="scount" HeaderText="scount" ItemStyle-CssClass="hidden-phone" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="hidden-phone" Visible="false" />
            </Columns>
         </asp:GridView> 
      </div>
    </div>


