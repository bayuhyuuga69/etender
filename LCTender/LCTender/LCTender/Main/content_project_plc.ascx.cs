﻿using System;
using System.Linq;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using System.Collections.Generic;
using Newtonsoft.Json;
using log4net;
using System.Net;
using System.Text;
using System.IO;

namespace LCTender.Transaction
{
    public partial class content_project_plc : System.Web.UI.UserControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(content_project_plc));
        private string strEntityID;
        private clsutils clsutl = new clsutils();
        public string main_color_theme = "";
        public string footer_color_theme = "";
        int PER_PAGE = 10;
        dbcReader dbcVendorList = new dbcReader();
        class ResponOpenTender
        {
            public string entityNo { get; set; }
            public string createDate { get; set; }
            public string serviceUnitName { get; set; }
            public string serviceUnitNo { get; set; }
            public string departmentName { get; set; }
            public string divisionNo { get; set; }
            public string entityName { get; set; }
            public string departmentNo { get; set; }
            public string measurement { get; set; }
            public string categoryName { get; set; }
            public string tenderNo { get; set; }
            public string divisionName { get; set; }
            public string description { get; set; }
            public string closingDate { get; set; }
            public string tenderName { get; set; }
            public string quantity { get; set; }
            public string costEstimate { get; set; }
            public string categoryNo { get; set; }
            public string openDate { get; set; }

        }
        class ResBoqDoc {
            public string filedescription { get; set; }
            public string filename { get; set; }
            public string documentfile { get; set; }
        }

        public string StrEntityID
        {
            get
            {
                return strEntityID;
            }
            set
            {
                strEntityID = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                //strCallee = "aceng.koswara2010@gmail.com";
                int intCurrPage = 1;               
                ViewState["current_page"] = intCurrPage;
                ViewState["filter"] = "";
                string entityno = dbcVendorList.ExecuteReader("Select ENTITYNO from dbo.P_ENTITY where P_ENTITY_ID=" + Session["P_ENTITY_ID"].ToString(), "ENTITYNO");
                getOpenTender(entityno);
                //getBOQTender();
                BindDataList(intCurrPage, 0, Convert.ToInt32(Session["P_ENTITY_ID"]));
            }
         

        }
        protected void BindDataList(int intCurrPage,int intVendorID,int intEntityID)
        {
            int intMaxRec, intMaxPage;
            intMaxRec = 0;
            int intPage = Convert.ToInt32(ViewState["current_page"]);
            DataSet dsresult = dbcVendorList.db_get_data_project_tender(intPage, "", PER_PAGE, 0, intEntityID, ViewState["filter"].ToString().Trim(), ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];
            intMaxPage = clsutl.get_max_page(PER_PAGE, intMaxRec);
            ViewState["max_page"] = intMaxPage;
            gvTask.DataSource = dtresult;
            gvTask.DataBind();
            clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Session["P_ENTITY_ID"].ToString());
            main_color_theme = entityTheme.main_color_theme;
            footer_color_theme = entityTheme.footer_color_theme;           
            gvTask.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(main_color_theme);
            

            if (intMaxRec == 0)
            {
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:"+ main_color_theme + ";font-size: 0.9em;color: #fff;'>PROJECT</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:"+ main_color_theme + ";font-size: 0.9em;color: #fff;'>ESTIMASI BIAYA</div>" +
                   "<div class='col-sm-3' style='padding: 4px 2px;background-color:"+ main_color_theme + ";font-size: 0.9em;color: #fff;'>PENAWARAN</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Text = "";
            }


            clsutl.load_image_paging(Convert.ToInt32(ViewState["current_page"]), PER_PAGE,
                                    Convert.ToInt32(ViewState["max_page"]), imbFirst, imbPrev, imbNext, imbLast, lblPaging);


        }
        protected void gvTask_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            String VendorRegId = "";
            VendorRegId = gvTask.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text;
            switch (e.CommandName)
            {
                case "Select":
                    //Response.Write("Vendorid:" + VendorRegId);
                    BindDataList(0, Convert.ToInt32(VendorRegId), Convert.ToInt32(StrEntityID));
                    break;
                case "Detail":
                    //load_maint(VendorRegId, 2)
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>clickEdit("+ VendorRegId + ",2)</");
                    cstext1.Append("script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    break;
            }
            txtSelectID.Text = VendorRegId;

        }
        
        protected void imbFirst_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0, Convert.ToInt32(StrEntityID));
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>disablereq;$('html,body').animate({ scrollTop: document.body.scrollHeight},'slow');</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }

        protected void imbPrev_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) - 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0, Convert.ToInt32(StrEntityID));
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>disablereq;$('html,body').animate({ scrollTop: document.body.scrollHeight},'slow');</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }

        protected void imbNext_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = Convert.ToInt32(ViewState["current_page"]) + 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0, Convert.ToInt32(StrEntityID));            
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>disablereq;$('html,body').animate({ scrollTop: document.body.scrollHeight},'slow');</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }

        protected void imbLast_Click(object sender, ImageClickEventArgs e)
        {
            ViewState["current_page"] = ViewState["max_page"];
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0, Convert.ToInt32(StrEntityID));
            ClientScriptManager cs = Page.ClientScript;
            System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
            cstext1.Append("<script type=text/javascript>disablereq;$('html,body').animate({ scrollTop: document.body.scrollHeight},'slow');</");
            cstext1.Append("script>");
            cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
        }

        protected void btnCari_Click(object sender, EventArgs e)
        {
            ViewState["current_page"] = 1;
            BindDataList(Convert.ToInt32(ViewState["current_page"]), 0, Convert.ToInt32(StrEntityID));
        }

        protected void getOpenTender(string entityno)
        {
            try
            {
                //string json = "[{\"entityNo\":\"36500\",\"create Date\":1549440959270,\"serviceUnitName\":\"GENERAL SERVICES DEPARTMENT\",\"serviceUnitNo\":\"GSDLC001\",\"departmentName\":\"HLD LC GENERAL SERVICES DEPARTMENT\",\"divisionNo\":\"102015000000\",\"entityName\":\"LIPPO CIKARANG\",\"departmentNo\":\"102015200000\",\"measurement\":null,\"categoryName\":\"PEKERJAAN\",\"tenderNo\":\"OPT1902-000003\",\"divisionName\":\"HLD LC HUMAN RESOURCES & SERVICES DIVISION\",\"description\":\"test\",\"closingDate\":1551200400000,\"tenderName\":\"testt tender\",\"quantity\":null,\"costEstimate\":22333333.0000,\"categoryNo\":\"PR0009\",\"openDate\":1549299600000}]";
                string json = "";
                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETOPENTENDER'", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                using (var client = new WebClient())
                {
                    try
                    {
                        string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + entityno + "&" + dtresult.Rows[0]["WHERE_START_URL"].ToString() + dtresult.Rows[0]["START_ROW"].ToString() + "&" + dtresult.Rows[0]["WHERE_END_URL"].ToString() + dtresult.Rows[0]["END_ROW"].ToString() + dtresult.Rows[0]["END_FIX_URL"].ToString();
                        System.IO.Stream data = client.OpenRead(baseurl);
                        System.IO.StreamReader reader = new System.IO.StreamReader(data);
                        json = reader.ReadToEnd();
                        data.Close();
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                List<ResponOpenTender> resOpenTender = JsonConvert.DeserializeObject<List<ResponOpenTender>>(json);

                if (resOpenTender.Count > 0)
                {
                    foreach (ResponOpenTender res in resOpenTender)
                    {
                        object outputObject;
                        string s_packname = "p_ins_project_tender";
                        string s_varvalue = "";
                        string boqfilename = "";
                        string contenttype = "";
                        string strQuantity;
                        if (res.quantity == null)
                        {
                            strQuantity = "0";
                        }else
                        {
                            strQuantity = res.quantity.ToString();
                        }
                        
                        boqfilename = getBOQTender(res.tenderNo);
                        contenttype = GetContentType(boqfilename);

                        s_varvalue += String.Format(@"{0}{1}", res.entityNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.createDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.serviceUnitName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.serviceUnitNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.departmentName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.divisionNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.entityName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.departmentNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.measurement, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.categoryName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.tenderNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.divisionName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.description, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.closingDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.tenderName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", strQuantity, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.costEstimate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.categoryNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.openDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", boqfilename, "~#");
                        s_varvalue += String.Format(@"{0}{1}", contenttype, "");

                        log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                        string s_outcodename = "";
                        string s_outmsgname = "O_RESULT_MSG";
                        int StartRecs = 0;
                        int EndRecs = 0;

                        outputObject = dbcVendorList.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                        System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                        String o_result_code = (String)(pi.GetValue(outputObject, null));

                        System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                        String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("getOpenTender Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        protected string getBOQTender(string tenderno)
        {
            string json = "";
            string fileboq = "";
            DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETBOQ'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            using (var client = new WebClient())
            {
                try
                {
                    
                    string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + tenderno + dtresult.Rows[0]["END_FIX_URL"].ToString();
                    System.IO.Stream data = client.OpenRead(baseurl);
                    System.IO.StreamReader reader = new System.IO.StreamReader(data);
                    json = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            if (json!="[]") { 
                    List < ResBoqDoc > resBoqDoc = JsonConvert.DeserializeObject<List<ResBoqDoc>>(json);
                    if (resBoqDoc.Count > 0)
                    {
                        foreach (ResBoqDoc res in resBoqDoc)
                        {

                            Byte[] bytes = Convert.FromBase64String(res.documentfile);
                            string upDir = Path.Combine(Request.PhysicalApplicationPath, "Boq");
                            fileboq = Path.Combine(upDir, res.filename);

                            if (!File.Exists(upDir + "\\" + res.filename)){
                                     File.WriteAllBytes(upDir + "\\" + res.filename, bytes);
                                }
                            }
                    }
             }
            return fileboq;
        }

        private string GetContentType(string fileName)
        {
            string strcontentType = "application/octetstream";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (registryKey != null && registryKey.GetValue("Content Type") != null)
                strcontentType = registryKey.GetValue("Content Type").ToString();
            return strcontentType;
        }
    }
}