﻿using System;
using WsLCTender;
using System.Data;
using System.Web.Security;

namespace LCTender.Main
{
    public partial class content_login_plc : System.Web.UI.UserControl
    {
        private string strEntityID;
        protected void Page_Load(object sender, EventArgs e)
        {
            p_entity_id.Text = strEntityID;
        }

        public string StrEntityID
        {
            get
            {
                return strEntityID;
            }
            set
            {
                strEntityID = value;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            dbcReader dbcLog = new dbcReader();
            int o_result_code=0; int o_user_id=0; string o_result_msg="";
            DataSet dsresult = dbcLog.db_get_user_login(username.Text.Trim().ToUpper(),password.Text, ref o_result_code,ref o_user_id, ref o_result_msg);
            DataTable dtresult = dsresult.Tables[0];
            if (o_result_msg == "sukses")
            {
                string strOrgan = dbcLog.ExecuteReader("select isnull(max(p_organization_id), 0) p_organization_id from p_app_user_organ where p_app_user_id =" + o_user_id.ToString(), "p_organization_id");
                string strEntity = dbcLog.ExecuteReader("select [dbo].[f_get_entity]("+ strOrgan + ") as p_entity_id", "p_entity_id");
                Session["P_ENTITY_ID"]= strEntity;
                Session["OrganId"] = strOrgan;
                Session["UserName"] = username.Text.Trim().ToUpper();
                Session["UserId"] = o_user_id;
                Session["Pass"] = password.Text.Trim();
                Session["Status"] = "login";
                FormsAuthentication.RedirectFromLoginPage(username.Text.Trim().ToUpper(), chkRemember.Checked);
                Application.Lock();
                Application[username.Text.Trim().ToUpper()] = Session.SessionID;
                Application.UnLock();
                Response.Redirect("Main/Home.aspx");
            }else
            {
                ltLogin.Text = o_result_msg;
            }

        }
    }
}