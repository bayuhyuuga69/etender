﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="content_entity_plc.ascx.cs" Inherits="LCTender.Main.content_entity_plc" %>
<script src="../Scripts/jquery-2.1.4.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>
<link href="../Content/bootstrap.min.css" rel="stylesheet"/>
<link href="../Content/bootstrap-theme.css" rel="stylesheet"/>
<link href="../Content/bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../Scripts/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.css" type="text/css" />
<link rel="stylesheet" href="../Contents/bootstrap-datepicker.min.css" type="text/css" />
<script type="text/javascript" src="../Scripts/bootbox.js"></script>
<script type="text/javascript" src="../Scripts/bootbox.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-entity {
      border-radius: 10px;
      color:#fff;
      margin-bottom: 0px;
      padding: 15px 10px 15px 15px;
      font: 13px Berlin Sans FB Demi, sans-serif;
    }

    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
  
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21;
      color: #ffffff;
    }
</style>
<div class="main-div-entity">
    <div class="row">  
        <div class="col-sm-4">     
            <asp:Literal ID="ltEntityName" runat="server"></asp:Literal>
        </div> 
        <div class="col-sm-4">     
            <asp:Literal ID="ltEntityAddr" runat="server"></asp:Literal>
        </div>  
        <div class="col-sm-2">   
            <span class="glyphicon glyphicon-earphone fa-lg"></span>
        </div> 
        <div class="col-sm-2">                 
            <div><asp:Literal ID="ltPhone" runat="server"></asp:Literal></div>               
            <div><asp:Literal ID="ltFax" runat="server"></asp:Literal></div>
        </div>
    </div> 
</div>     
