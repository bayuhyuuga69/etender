﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using WsLCTender;
using LCTender.Class;
using log4net;

namespace LCTender.Wf
{
    public partial class t_vendor_tender_spec : System.Web.UI.Page
    {
        private clsutils clsutl = new clsutils();
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_tender_spec));       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataMaint(Convert.ToInt32(Request["CURR_DOC_ID"]),2);
            }

            BindDataSubmitter();

        }
       
        protected void BindDataMaint(int intIDRow, int intType)
        {
           
            pnlMaintTender.Visible = true;

            int intMaxRec;
            intMaxRec = 0;
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsresult = dbcVendorList.db_get_data_tender(1, "", 1, intIDRow, "", ref intMaxRec);
            DataTable dtresult = dsresult.Tables["dtData"];

            string strLOV = "";
            string strLOVProject = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV('{1}',1)"">{0}</a>", strIMG, "../Lov/lov_vendor_register.aspx");
            strLOVProject += String.Format(@"<a href=""javascript:clickLOV('{1}',2)"">{0}</a>", strIMG, "../Lov/p_project_tender.aspx");

            //ltLOV.Text = strLOVProject;
            //ltLOVReg.Text = strLOV;
            if (intType == 1)
            {
                //btnUpdate.Visible = false;
                //ltWorkFlow.Text = "";
            } else if (intType == 2) {
                //btnUpdate.Visible = true;
                //ltWorkFlow.Text = "<input id=\"btnWorkFlow\" type=\"button\" class=\"btn btn-primary\" value=\"Start Workflow\" onclick=\"javascript: clickLoadSubmitter()\"/>";
                string strSubmit = dbcVendorList.ExecuteReader("SELECT count(*) AMT from sis.dbo.T_W_TENDER_CTL WHERE p_w_proc_id=343 " +
                                                                   "AND p_app_user_id_takeover is not null AND doc_id=" + Request["CURR_DOC_ID"], "AMT");

                if (Convert.ToInt32(strSubmit) > 0)
                {
                    btnWorkFlow.Visible = false;
                }
                t_vendor_tender_id.Text = dtresult.Rows[0]["t_vendor_tender_id"].ToString();
                txtProjectName.Text = dtresult.Rows[0]["project_name"].ToString();
                p_project_tender_id.Text = dtresult.Rows[0]["p_project_tender_id"].ToString();
                t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                txtVendReg.Text = dtresult.Rows[0]["register_no"].ToString();
                txtVendName.Text = dtresult.Rows[0]["vendor_name"].ToString();
                txtEstimate.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["estimated_cost_amt"].ToString()));
                txtOffering.Text = String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["offering_cost_amt"].ToString()));
                load_ddlEntityList();
                load_ddlStatusList();
            }
            

            txtSelectID.Text = intIDRow.ToString();
        }
        private void load_ddlEntityList()
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.db_get_entity_list(1);
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_entityId.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_entityId.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_entityId.Items[1].Value;
                    liType.Text = s_entityId.Items[1].Text;
                    s_entityId.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_entity_id"].ToString();
                liType.Text = dtType.Rows[t]["entity_code"].ToString();
                s_entityId.Items.Add(liType);
                if (dtType.Rows[t]["p_entity_id"].ToString().Trim() == Session["P_ENTITY_ID"].ToString().Trim())
                {
                    //s_entityId.Items[0].Selected = false;
                    s_entityId.Items[t].Selected = true;
                }

            }

        }

        protected void BindDataSubmitter()
        {
            String param_submitter = "";

            String aIS_CREATE_DOC = "";
            String aIS_MANUAL = "";

            aIS_CREATE_DOC = Request["IS_CREATE_DOC"];
            aIS_MANUAL = Request["IS_MANUAL"];

            param_submitter = String.Format(@"{0}", "../Wf/submitter.aspx?");
            param_submitter += String.Format(@"{0}{1}", "CURR_DOC_ID=", Request["CURR_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_TYPE_ID=", Request["CURR_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_ID=", Request["CURR_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_CTL_ID=", Request["CURR_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DOC=", Request["USER_ID_DOC"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DONOR=", Request["USER_ID_DONOR"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_LOGIN=", Request["USER_ID_LOGIN"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_TAKEN=", Request["USER_ID_TAKEN"]);
            param_submitter += String.Format(@"{0}{1}", "&IS_CREATE_DOC=", aIS_CREATE_DOC);
            param_submitter += String.Format(@"{0}{1}", "&IS_MANUAL=", aIS_MANUAL);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_STATUS=", Request["CURR_PROC_STATUS"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_STATUS=", s_ddlStatusList.SelectedValue);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_ID=", Request["PREV_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_TYPE_ID=", Request["PREV_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_PROC_ID=", Request["PREV_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_CTL_ID=", Request["PREV_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_1", Request["SLOT_1"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_2=", Request["SLOT_2"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_3=", Request["SLOT_3"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_4=", Request["SLOT_4"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_5=", Request["SLOT_5"]);
            param_submitter += String.Format(@"{0}{1}", "&MESSAGE=", Request["MESSAGE"]);
            param_submitter += String.Format(@"{0}{1}", "&PACKAGE=", "SUBMIT_ENGINE_TENDER");

            ltWfSubmitter.Text = Server.HtmlDecode("<div class='modal fade' id='myModalFrmSubmitter' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type = 'button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Workflow Started</h4></div><div class='modal-body'><div class='sizer'><div class='embed-responsive embed-responsive-16by9'><iframe id = 'frmSubmitter' src='" + param_submitter + "'></iframe></div></div></div><div class='modal-footer'><button type = 'button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");

        }

        private void load_ddlStatusList()
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.db_get_status_list(1, 1);
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_ddlStatusList.Items.Count > 1))
            {
                for (int i = 1; (i <= (s_ddlStatusList.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_ddlStatusList.Items[1].Value;
                    liType.Text = s_ddlStatusList.Items[1].Text;
                    s_ddlStatusList.Items.Remove(liType);
                }

            }

            for (t = 0; (t <= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                if (dtType.Rows[t]["p_status_list_id"].ToString() != "117")
                {
                    liType.Value = dtType.Rows[t]["p_status_list_id"].ToString();
                    liType.Text = dtType.Rows[t]["code"].ToString();
                    s_ddlStatusList.Items.Add(liType);
                }

                //if ((dtType.Rows[t]["p_status_list_id"].ToString().Trim()==Request.QueryString["p_status_list_id"].Trim()))
                //{
                //    s_ddlStatusList.Items[0].Selected = false;
                //    s_ddlStatusList.Items[(t + 1)].Selected = true;
                //}

            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if ((txtEstimate.Text != ""))
            {

                log.Info("Update Tender Spec Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_upd_vend_tender_spec";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_tender_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", txtEstimate.Text.Replace(",", ""), "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"], "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Updated..!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }


    }
}