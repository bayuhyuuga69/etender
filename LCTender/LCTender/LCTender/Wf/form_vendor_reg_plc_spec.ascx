﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="form_vendor_reg_plc_spec.ascx.cs" Inherits="LCTender.Transaction.form_vendor_reg_plc_spec" %> 
 <form runat="server" style="padding-top:20px;"> 
        <div class="form-row">    
              <div class="form-group col-sm-4">
                <label for="vendName" class="control-label">Nama Vendor:</label>
                <asp:TextBox ID="vendName" cssclass="form-control" name="vendName" runat="server" ReadOnly="true"></asp:TextBox>
              </div>
              <div class="form-group col-sm-4">
                <label for="npwpNo" class="control-label">No NPWP:</label>          
                <asp:TextBox ID="npwpNo" cssclass="form-control" name="npwpNo" runat="server" ReadOnly="true"></asp:TextBox>   
              </div>
             <div class="form-group col-sm-4">
                <label for="email" class="control-label">Email:</label>         
                <asp:TextBox ID="email" type="email" cssclass="form-control" name="email" runat="server" ReadOnly="true"></asp:TextBox>
             </div>
          </div>
          <div class="form-row">
              <div class="form-group col-sm-6">
                <label for="officeAddr" class="control-label">Alamat Kantor:</label>
                <asp:TextBox ID="officeAddr" cssclass="form-control" name="officeAddr" runat="server" ReadOnly="true"></asp:TextBox>
              </div>    
              <div class="form-group col-sm-6">
                 <label for="siteAddr" class="control-label">Alamat Situs:</label>
                 <asp:TextBox ID="siteAddr" cssclass="form-control" name="siteAddr" runat="server" ReadOnly="true"></asp:TextBox>
               </div>
          </div>
          <div class="form-row">
            <div class="form-group col-sm-4">
             <label for="phoneNo" class="control-label">Telephone:</label>
             <asp:TextBox ID="phoneNo" cssclass="form-control" name="phoneNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group col-sm-4">
                <label for="faxNo" class="control-label">Fax:</label>
                <asp:TextBox ID="faxNo" cssclass="form-control" name="faxNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group col-sm-4">
                <label for="dtCompany" class="control-label">Tgl Berdiri:</label>            
                <div class="input-group date">
                    <asp:TextBox ID="dtNotary" cssclass="form-control" name="dtNotary" runat="server" ReadOnly="true"></asp:TextBox>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
             </div>
            </div> 
           <div class="form-row">
             <div class="form-group col-sm-4">
                 <label for="typBussiness" class="control-label">Bidang Usaha:&nbsp;&nbsp;<asp:Literal ID="ltLOV" runat="server"></asp:Literal></label>   
                 <asp:Literal ID="lblCheck" runat="server"></asp:Literal>                   
                 <asp:TextBox ID="typBussiness" cssclass="form-control" name="typBussiness" runat="server" ReadOnly="true" style="display:none"></asp:TextBox>                    
                 <asp:TextBox ID="typBussiness_id" cssclass="hidden" name="typBussiness_id" runat="server"></asp:TextBox>
             </div> 
             <div class="form-group col-sm-4">
                <label for="nmBank" class="control-label">Bank::.<asp:Literal ID="ltLOVBank" runat="server"></asp:Literal></label>
                <asp:TextBox ID="nmBank" cssclass="form-control" name="nmBank" runat="server"  ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="nmBank_id" cssclass="hidden" name="nmBank_id" runat="server"></asp:TextBox>            
             </div> 
             <div class="form-group col-sm-4">
                <label for="costLimit" class="control-label">Cost Limit</label>
                <asp:TextBox ID="cost_limit_amt" cssclass="form-control" name="costLimitAmt" runat="server" ReadOnly="true"></asp:TextBox>
             </div>
            </div>   
          <div class="form-row">    
            <div class="form-group col-sm-4">
                <label for="accNo" class="control-label">No Rekening:</label>
                <asp:TextBox ID="accNo" cssclass="form-control" name="accNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>   
            <div class="form-group col-sm-4">
                <label for="accOwner" class="control-label">Pemilik Rekening:</label>
                <asp:TextBox ID="accOwner" cssclass="form-control" name="accOwner" runat="server" ReadOnly="true"></asp:TextBox>
            </div> 
            <div class="form-group col-sm-4">
                <label for="note" class="control-label">Note:&nbsp;<asp:Literal ID="ltNote" runat="server"></asp:Literal></label>
                <asp:TextBox ID="txtNote" name="txtNote" cssclass="form-control" runat="server"></asp:TextBox>
            </div>
          </div> 
         <div class="form-group col-sm-12">               
                <asp:Button ID="btnUpdate" class="btn btn-primary pull-right" runat="server" Text="Simpan" onclick="btnUpdate_Click"/>
         </div>
         <div class="col-sm-12">
                  <label for="lblSubmit" class="control-label" style="text-align:center">.:SUBMIT PERSETUJUAN:.</label>   
                  <hr style="min-width:85%; background-color:#a1a1a1 !important; height:1px;"/>
         </div>
         <div class="form-group col-sm-12">
                 <label for="ddlStatusList" class="control-label">Status Persetujuan:</label>                      
                 <asp:DropDownList ID="s_ddlStatusList" name="s_ddlStatusList" class="form-control" runat="server" AutoPostBack="True"></asp:DropDownList><br>
                 <input id="btnWorkFlow" runat="server" type="button" class="btn btn-primary" value="Submit flow" onclick="javascript:clickLoadSubmitter()"/>                
          </div>  
         <div class="form-group col-sm-12">
                <asp:TextBox ID="t_vendor_register_id" class="hidden" name="t_vendor_register_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="request_status" class="hidden" name="request_status" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_organization_id" class="hidden" name="p_organization_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_app_user_id" class="hidden" name="p_app_user_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="user_name" class="hidden" name="user_name" runat="server"></asp:TextBox>
         </div>   
        <asp:Label ID="ltSubmitter" runat="server"></asp:Label>
  </form>
<script type="text/javascript">
    //debugger;
    $('[id*=myModalFrame]').modal('hide');
    $('[id*=myModalFrmBank]').modal('hide');
    $('[id*=myModalFrmSubmitter]').modal('hide');
    
    $('.input-group.date').datepicker({
        format: "dd-M-yyyy",
        autoclose: true
    });
    function clickLOV() {
        $('[id*=myModalFrame]').modal('show');
    }
    function clickLOVBank() {
        $('[id*=myModalFrmBank]').modal('show');
    }
    function clickLoadSubmitter() {
        debugger;
        $('[id*=myModalFrmSubmitter]').modal('show');
    }
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'top'
        });
    });
</script>