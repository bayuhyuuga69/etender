﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using WsLCTender;
using log4net;
using System.Data;

namespace LCTender.Wf
{
    public partial class t_vendor_register_agreed : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_register_agreed));
        //String auser_id_login, retcurr_ctl_id,
        //       retcreate_doc, retprev_doc_id,
        //       retprev_ctl_id, retprev_proc_id,
        //       retcurr_doc_status;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataMaint(Convert.ToInt32(Request["CURR_DOC_ID"]),2);
                t_vendor_register_id.Text = Request["CURR_DOC_ID"];
            }

            
        }
        protected void BindDataMaint(int intIDRow, int intType)
        {
            string strLOV = "";
            string strIMG = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOV += String.Format(@"<a href=""javascript:clickLOV()"">{0}</a>", strIMG);

            //ltLOV.Text = strLOV;

            string strLOVBank = "";
            string strIMGBank = "<img src=\"../images/view.gif\" width=\"17\" height=\"17\">";
            strLOVBank += String.Format(@"<a href=""javascript:clickLOVBank()"">{0}</a>", strIMGBank);

            //ltLOVBank.Text = strLOVBank;

            typBussiness.Attributes.Add("readonly", "readonly");
            nmBank.Attributes.Add("readonly", "readonly");
            dtNotary.Attributes.Add("readonly", "readonly");

            if (intType==1)
            {
                btnSave.Visible = true;
                btnUpdate.Visible = false;
                btnDelete.Visible = false;
                //btnSave.Attributes.Add("onclick", "return checkValidate();");
            }else if (intType == 2)
            {
                log.Info("Bind Data Vendor Register Started. ");
                try
                {
                    int intMaxRec;
                    intMaxRec = 0;
                    dbcReader dbcVendorList = new dbcReader();
                    DataSet dsresult = dbcVendorList.db_get_data_vendor(1, "", 1, intIDRow, "", ref intMaxRec);
                    DataTable dtresult = dsresult.Tables["dtData"];

                    btnSave.Visible = false;
                    btnUpdate.Visible = false;
                    btnDelete.Visible = false;
                    if (Request["confirm"].ToString()=="2")
                    {
                        btnStartTender.Visible = false;
                        ltHeader.Text = "<div class=\"h4\" style=\"text-align:center;color:#4d96de\">KONFIRMASI PENOLAKAN REGISTRASI <label style=\"color:red\"> " + dtresult.Rows[0]["register_no"].ToString() + "</label></div>";
                    }
                    else
                    {
                        ltHeader.Text = "<div class=\"h4\" style=\"text-align:center;color:#4d96de\">KONFIRMASI PERSETUJUAN REGISTRASI <label style=\"color:red\"> " + dtresult.Rows[0]["register_no"].ToString() + "</label></div>";
                    }
                    t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                    vendName.Text = dtresult.Rows[0]["vendor_name"].ToString();
                    npwpNo.Text = dtresult.Rows[0]["npwp_no"].ToString();
                    email.Text = dtresult.Rows[0]["email_addr"].ToString();
                    officeAddr.Text = dtresult.Rows[0]["office_addr"].ToString();
                    siteAddr.Text = dtresult.Rows[0]["site_addr"].ToString();
                    phoneNo.Text = dtresult.Rows[0]["phone_no"].ToString();
                    faxNo.Text = dtresult.Rows[0]["fax_no"].ToString();
                    dtNotary.Text = dtresult.Rows[0]["company_date_als"].ToString();
                    nmBank.Text = dtresult.Rows[0]["bank_name"].ToString();
                    nmBank_id.Text = dtresult.Rows[0]["p_bank_id"].ToString();
                    accNo.Text = dtresult.Rows[0]["bank_account_no"].ToString();
                    accOwner.Text = dtresult.Rows[0]["bank_account_owner"].ToString();
                    p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                    p_organization_id.Text = dtresult.Rows[0]["p_organization_id"].ToString();
                    load_chkBizTypeList(intIDRow);
                    BindDataSubmitter();
                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Bind Data Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }


            }

            

        }
        protected void btnStartTender_Click(object sender, EventArgs e)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            object outputObject;
            string s_packname = "p_update_control";
            string s_varvalue = "";
            s_varvalue += String.Format(@"{0}{1}", Request["CURR_CTL_ID"].ToString(), "~#");
            s_varvalue += String.Format(@"{0}{1}", Request["USER_ID_TAKEN"].ToString(), "");

            log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

            string s_outcodename = "";
            string s_outmsgname = "O_RESULT_MSG";
            int StartRecs = 0;
            int EndRecs = 0;

            outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

            System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
            String o_result_code = (String)(pi.GetValue(outputObject, null));

            System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
            String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
            Response.Redirect("../Transaction/t_vendor_tender.aspx");
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

            if ((vendName.Text != "") && (npwpNo.Text != "") && (officeAddr.Text != "") &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (faxNo.Text != "") && (dtNotary.Text != "") && (typBussiness_id.Text != "") &&
                 (nmBank_id.Text != "") && (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Insert Vendor Register Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "A", "~#");
                    s_varvalue += String.Format(@"{0}{1}", "0", "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", typBussiness_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", "231", "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", "10343", "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");                       
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if ((vendName.Text != "") && (npwpNo.Text != "") && (officeAddr.Text != "") &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (faxNo.Text != "") && (dtNotary.Text != "") && (typBussiness_id.Text != "") &&
                 (nmBank_id.Text != "") && (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Update Vendor Register Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "U", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", typBussiness_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", "231", "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", "10343", "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if ((vendName.Text != "") && (npwpNo.Text != "") && (officeAddr.Text != "") &&
                 (email.Text != "") && (siteAddr.Text != "") && (phoneNo.Text != "") &&
                 (faxNo.Text != "") && (dtNotary.Text != "") && (typBussiness_id.Text != "") &&
                 (nmBank_id.Text != "") && (accNo.Text != "") && (accOwner.Text != "")
                 )
            {

                log.Info("Delete Vendor Register Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_ins_vendor_register";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", "D", "~#");
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", vendName.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", npwpNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", officeAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", email.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", siteAddr.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", phoneNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", faxNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", dtNotary.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", typBussiness_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accNo.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", nmBank_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", accOwner.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", "231", "~#");//org_id
                    s_varvalue += String.Format(@"{0}{1}", "10343", "~#");//user_id
                    s_varvalue += String.Format(@"{0}{1}", "INPUT VENDOR", "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Sukses',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }
        private void load_chkBizTypeList(int intIDRow)
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.ExecuteSelect("select P_BUSINESS_TYPE_ID,BUSINESS_TYPE from [dbo].[P_BUSINESS_TYPE]", 0, 0);
            DataTable dtType = dsType.Tables[0];

            String strCheckBiztype = "";
            String strChecked = "";

            if (intIDRow != 0)
            {
                DataSet dsTypeDb = dbcVendorList.ExecuteSelect("select P_BUSINESS_TYPE_ID from [dbo].[T_VENDOR_BUSINESS_TYPE] where T_VENDOR_REGISTER_ID=" + intIDRow.ToString(), 0, 0);
                DataTable dtTypeDb = dsTypeDb.Tables[0];
                String db_biz_type = "";

                for (int i = 0; i < dtTypeDb.Rows.Count; i++)
                {
                    db_biz_type = db_biz_type + dtTypeDb.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",";
                }

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"], dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0 || Strings.InStr(db_biz_type, dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<label class='checkbox-inline'><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label>";
                }

                lblCheck.Text = strCheckBiztype;

            }
            else
            {

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"], dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<label class='checkbox-inline'><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label>";
                }

                lblCheck.Text = strCheckBiztype;
            }

        }
        protected void BindDataSubmitter()
        {
            String param_submitter = "";

            String aIS_CREATE_DOC = "";
            String aIS_MANUAL = "";

            aIS_CREATE_DOC = Request["IS_CREATE_DOC"];
            aIS_MANUAL = Request["IS_MANUAL"];

            param_submitter = String.Format(@"{0}", "../Wf/submitter.aspx?");
            param_submitter += String.Format(@"{0}{1}", "CURR_DOC_ID=", Request["CURR_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_TYPE_ID=", Request["CURR_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_ID=", Request["CURR_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_CTL_ID=", Request["CURR_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DOC=", Request["USER_ID_DOC"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DONOR=", Request["USER_ID_DONOR"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_LOGIN=", Request["USER_ID_LOGIN"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_TAKEN=", Request["USER_ID_TAKEN"]);
            param_submitter += String.Format(@"{0}{1}", "&IS_CREATE_DOC=", aIS_CREATE_DOC);
            param_submitter += String.Format(@"{0}{1}", "&IS_MANUAL=", aIS_MANUAL);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_STATUS=", Request["CURR_PROC_STATUS"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_STATUS=", Request["CURR_DOC_STATUS"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_ID=", Request["PREV_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_TYPE_ID=", Request["PREV_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_PROC_ID=", Request["PREV_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_CTL_ID=", Request["PREV_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_1", Request["SLOT_1"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_2=", Request["SLOT_2"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_3=", Request["SLOT_3"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_4=", Request["SLOT_4"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_5=", Request["SLOT_5"]);
            param_submitter += String.Format(@"{0}{1}", "&MESSAGE=", Request["MESSAGE"]);
            param_submitter += String.Format(@"{0}{1}", "&PACKAGE=", "SUBMIT_ENGINE_REGISTER");

            ltSubmitter.Text = Server.HtmlDecode("<div class='modal fade' id='myModalFrmSubmitter' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type = 'button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Workflow Started</h4></div><div class='modal-body'><div class='sizer'><div class='embed-responsive embed-responsive-16by9'><iframe id = 'frmSubmitter' src='" + param_submitter + "'></iframe></div></div></div><div class='modal-footer'><button type = 'button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");

        }
        
    }
}