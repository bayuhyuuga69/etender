﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="form_vendor_attach_plc_spec.ascx.cs" Inherits="LCTender.Transaction.form_vendor_attach_plc_spec" %>
<div class="table-responsive">
<div class="row">
    <div class="col-sm-12">
        <div class="input-group input-group-sm">
            <label for="email" class="col-sm-2">Filter:</label>
            <div class="col-sm-8">
                <asp:TextBox ID="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <div class="btn btn-default glyphicon glyphicon-search">
                   <asp:Button ID="btnCari" BackColor="Transparent" runat="server" BorderWidth="0" Text="Cari" OnClick="btnCari_Click" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <asp:Panel ID="pnlGridVend" runat="server">
            <asp:GridView ID="gvTask" runat="server" Width="100%"
                CssClass="mGrid table table-sm"
                PagerStyle-CssClass="pgr"
                AlternatingRowStyle-CssClass="alt"
                CellPadding="5" CellSpacing="4"
                AutoGenerateColumns="False"
                OnRowDataBound="gvTask_RowDataBound"
                OnRowCommand="gvTask_RowCommand" PageSize="3">
                <AlternatingRowStyle CssClass="alt" />
                <Columns>
                    <asp:BoundField DataField="T_VENDOR_REG_ATTACHMENT_ID" HeaderText="T_VENDOR_REG_ATTACHMENT_ID" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                        <ItemStyle CssClass="hidden" />
                        <HeaderStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FILE_DIR" HeaderText="FILE DIR" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                        <ItemStyle CssClass="hidden" />
                        <HeaderStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FILE_TYPE" HeaderText="FILE TYPE" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true">
                        <ItemStyle CssClass="hidden" />
                        <HeaderStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" visible="false">
                        <HeaderTemplate>
                            <asp:Label ID="lblHeaderEdit" runat="server" Text="HAPUS"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="imbRow" runat="server" Height="20px" width="20px" CommandArgument="<%# Container.DataItemIndex%>" CommandName="Hapus" ImageUrl="~/images/icon/Asset 2@4x.png" alt="hapus" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="FILE_NAME" HeaderText="FILE NAME" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                    </asp:BoundField>            
                    <asp:BoundField DataField="ATTACHMENT_TYPE" HeaderText="ATTACHMENT TYPE" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CREATED_BY" HeaderText="CREATED BY" ItemStyle-CssClass="hidden-phone" ItemStyle-Wrap="true" Visible="false">
                        <ItemStyle CssClass="hidden-phone" Wrap="True" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="NAMA FILE" ItemStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblFileName" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="visible-desktop" />
                        <ItemStyle CssClass="visible-desktop" HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="visible-desktop" HeaderText="JENIS BERKAS" ItemStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblFileType" runat="server"></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle CssClass="visible-desktop" />
                        <ItemStyle CssClass="visible-desktop" HorizontalAlign="Left" />
                    </asp:TemplateField>
                     <asp:BoundField ItemStyle-HorizontalAlign="Center" 
                           DataField="CREATED_BY" HeaderText="DIBUAT OLEH">
                          <ItemStyle HorizontalAlign="Center" />   
                     <ItemStyle CssClass="hidden-phone"/>
                    </asp:BoundField>
                    <asp:BoundField ItemStyle-HorizontalAlign="Center"
                           DataField="CREATION_DATE" HeaderText="TANGGAL">
                           <ItemStyle HorizontalAlign="Center" /> 
                    </asp:BoundField>
                </Columns>
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" PageButtonCount="3" />
                <PagerStyle CssClass="pgr" />
            </asp:GridView>
            <asp:Label ID="lblNoRow" runat="server"></asp:Label>
            <div class="Row">
                 <div class="col-sm-12" style="background-color: #fff; margin: 0px 0 0px 0; border: solid 1px #525252; border-collapse: collapse; padding: 2px; border: solid 1px #c1c1c1; color: #717171;">
                     <asp:ImageButton ID="imbFirst" runat="server" visible="false" OnClick="imbFirst_Click"/>
                    <asp:ImageButton ID="imbPrev" runat="server" OnClick="imbPrev_Click" />
                    &nbsp;
                    <asp:Label ID="lblPaging" runat="server"></asp:Label>
                    &nbsp;
                    <asp:ImageButton ID="imbNext" runat="server" OnClick="imbNext_Click" Style="width: 14px" />
                    <asp:ImageButton ID="imbLast" runat="server" OnClick="imbLast_Click" Width="16px" />
                </div>
            </div>
        </asp:Panel>
       </div>
    </div>
<asp:TextBox ID="txtSelectID" class="hidden" runat="server"></asp:TextBox>
</div>
<script type="text/javascript">
    function clickEdit(id, type) {
        var vendorId = "<%=Request["t_vendor_register_id"]%>";
        $('#frmMaint').attr('src', "t_vendor_reg_attach_maint.aspx?t_vendor_register_id=" + vendorId + "&t_vendor_commissioner_level_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
    function clickAdd(id, type) {
        var vendorId = "<%=Request["t_vendor_register_id"]%>";
        $('#frmMaint').attr('src', "t_vendor_reg_attach_maint.aspx?t_vendor_register_id=" + vendorId + "&t_vendor_commissioner_level_id=" + id + "&type=" + type);
        $('#myModal').modal('show');
    }
    function refresh (RegId) {
        var url = "t_vendor_reg_attachment.aspx?t_vendor_register_id=" + RegId;
        $(location).attr('href', url);
    }
    function clickView(filename,filetype) {
        var url = "../Wf/fileviewer.aspx?FILE_NAME=" + filename + "&FILE_TYPE=" + filetype;
        document.location.href = url;
    }
</script>
