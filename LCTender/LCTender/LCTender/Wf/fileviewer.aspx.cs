﻿using System.Web.UI;
using System.IO;
using Scripting;


namespace LCTender.Wf
{
    public partial class fileviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            viewFile(Request["FILE_NAME"],Request["FILE_TYPE"]);
            
        }

        public void viewFile(string filename, string filetype)
        {
            
            FileSystemObject fso = new FileSystemObject();
            if (fso.FileExists(filename))
            {
                Response.Clear();
                Response.ContentType = filetype;
                Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(filename)).Name);                //Write the file directly to the HTTP content output stream.
                Response.WriteFile(filename);
                Response.End();
            }
            else
                Response.Write("<h1>File tidak ditemukan !</h1> <br>Hubungi Administrator jika terjadi perubahan lokasi UPLOAD.");
        }

    }
}