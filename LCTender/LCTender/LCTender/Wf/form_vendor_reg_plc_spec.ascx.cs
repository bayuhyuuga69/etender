﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using WsLCTender;
using log4net;
namespace LCTender.Transaction
{
    public partial class form_vendor_reg_plc_spec : System.Web.UI.UserControl
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_register_maint));
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                log.Info("Bind Data Register Spec Started. ");
                try
                {
                    dbcReader dbcVendorList = new dbcReader();
                    DataSet dsresult = dbcVendorList.db_get_vender_register_spec(Convert.ToInt32(Request["CURR_DOC_ID"]), Convert.ToInt32(Request["app_level"]));
                    DataTable dtresult = dsresult.Tables[0];
                    string strSubmit = "";
                    string strStatus = "";
                    strSubmit = dbcVendorList.ExecuteReader("select dbo.f_cek_curr_wf(" + Request["CURR_DOC_ID"].ToString() + ", " + Request["CURR_PROC_ID"].ToString() + ") amt", "amt");
                        if (Request["CURR_DOC_TYPE_ID"].ToString()=="13")
                        {
                            strStatus = dbcVendorList.ExecuteReader("SELECT request_status from sis.dbo.T_VENDOR_REGISTER WHERE T_VENDOR_REGISTER_ID=" + Request["CURR_DOC_ID"].ToString(), "request_status");
                        }

                        if (Convert.ToInt32(strSubmit) > 0)
                        {
                            btnWorkFlow.Visible = false;
                        }

                        if (strStatus=="118")
                        {
                            btnWorkFlow.Visible = true;
                        }

                        btnUpdate.Visible = true;
                        t_vendor_register_id.Text = dtresult.Rows[0]["t_vendor_register_id"].ToString();
                        vendName.Text = dtresult.Rows[0]["vendor_name"].ToString();
                        npwpNo.Text = dtresult.Rows[0]["npwp_no"].ToString();
                        email.Text = dtresult.Rows[0]["email_addr"].ToString();
                        officeAddr.Text = dtresult.Rows[0]["office_addr"].ToString();
                        siteAddr.Text = dtresult.Rows[0]["site_addr"].ToString();
                        phoneNo.Text = dtresult.Rows[0]["phone_no"].ToString();
                        faxNo.Text = dtresult.Rows[0]["fax_no"].ToString();
                        dtNotary.Text = dtresult.Rows[0]["notary_company_date"].ToString();
                        typBussiness.Text = dtresult.Rows[0]["business_type"].ToString();
                        typBussiness_id.Text = dtresult.Rows[0]["p_business_type_id"].ToString();
                        nmBank.Text = dtresult.Rows[0]["bank_name"].ToString();
                        nmBank_id.Text = dtresult.Rows[0]["p_bank_id"].ToString();
                        accNo.Text = dtresult.Rows[0]["bank_account_no"].ToString();
                        accOwner.Text = dtresult.Rows[0]["bank_account_owner"].ToString();
                        p_app_user_id.Text = dtresult.Rows[0]["p_app_user_id"].ToString();
                        cost_limit_amt.Text = "Rp. "+ String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["cost_limit_amt"].ToString())) +" - Rp. "+ String.Format("{0:n0}", Convert.ToDecimal(dtresult.Rows[0]["cost_limit_max_amt"].ToString()));
                        load_ddlStatusList();
                        DataSet dsNote = dbcVendorList.ExecuteSelect("select isnull(a.notes,'TIDAK ADA CATATAN') notes,a.p_level_approval_type_id,b.level_approval_type from dbo.t_vendor_register_spec a,dbo.p_level_approval_type b " +
                                                                     "where a.t_vendor_register_id=" + Request["CURR_DOC_ID"].ToString() + " and " +
                                                                     "a.p_level_approval_type_id=b.p_level_approval_type_id and  " +
                                                                     "a.p_level_approval_type_id<" + Request["app_level"].ToString(), 0, 0);
                        DataTable dtNote = dsNote.Tables[0];
                        String note = "";
                        note = String.Format(@"<ul class='list-inline'>");
                        for (int i = 0; i < dtNote.Rows.Count; i++)
                        {
                              note += String.Format(@"<li><a href='#' data-toggle='tooltip' title='{0}'>{1} NOTE</a></li>", dtNote.Rows[i]["notes"].ToString(), dtNote.Rows[i]["level_approval_type"].ToString());
                        }
                       note += String.Format(@"</ul>");
                       ltNote.Text = note;
                       load_chkBizTypeList(Convert.ToInt32(t_vendor_register_id.Text));
                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Bind Data Register Spec Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }
            }
            BindDataSubmitter();

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if ((txtNote.Text != ""))
            {

                log.Info("Update Reg Spec Started. ");
                try
                {

                    dbcReader dbcInsVendReg = new dbcReader();
                    object outputObject;
                    string s_packname = "p_upd_vend_reg_spec";
                    string s_varvalue = "";
                    s_varvalue += String.Format(@"{0}{1}", t_vendor_register_id.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Request["app_level"].ToString(), "~#");                    
                    s_varvalue += String.Format(@"{0}{1}", txtNote.Text, "~#");
                    s_varvalue += String.Format(@"{0}{1}", Session["UserName"], "");

                    log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                    string s_outcodename = "";
                    string s_outmsgname = "O_RESULT_MSG";
                    int StartRecs = 0;
                    int EndRecs = 0;

                    outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                    System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                    String o_result_code = (String)(pi.GetValue(outputObject, null));

                    System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                    String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));


                    if (o_result_code == "1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: 'Updated..!',");
                        cstext1.Append("size: 'small',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }
                    else if (o_result_code == "-1")
                    {
                        ClientScriptManager cs = Page.ClientScript;
                        System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                        cstext1.Append("<script type=text/javascript>");
                        cstext1.Append("bootbox.alert({ ");
                        cstext1.Append("message: '" + o_result_msg.ToString().Replace("'", "") + "',");
                        cstext1.Append("className: 'bb-alternate-modal' });");
                        cstext1.Append("</script>");
                        cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    }

                }
                catch (Exception ex)
                {
                    log.ErrorFormat("Insert Vendor Register Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    Console.WriteLine("Control flow reaches finally");
                }



            }
        }
        private void load_ddlStatusList()
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.db_get_status_list(1, 1);
            DataTable dtType = dsType.Tables[0];
            int t;
            ListItem liType = null;
            if ((s_ddlStatusList.Items.Count > 1))
            {
                for (int i = 1; (i<= (s_ddlStatusList.Items.Count - 1)); i++)
                {
                    liType = new ListItem();
                    liType.Value = s_ddlStatusList.Items[1].Value;
                    liType.Text = s_ddlStatusList.Items[1].Text;
                    s_ddlStatusList.Items.Remove(liType);
                }

            }

            for (t = 0; (t<= (dtType.Rows.Count - 1)); t++)
            {
                liType = new ListItem();
                liType.Value = dtType.Rows[t]["p_status_list_id"].ToString();
                liType.Text = dtType.Rows[t]["code"].ToString();
                s_ddlStatusList.Items.Add(liType);
                //if ((dtType.Rows[t]["p_status_list_id"].ToString().Trim()==Request.QueryString["p_status_list_id"].Trim()))
                //{
                //    s_ddlStatusList.Items[0].Selected = false;
                //    s_ddlStatusList.Items[(t + 1)].Selected = true;
                //}

            }

        }

        protected void BindDataSubmitter()
        {

            String param_submitter = "";

            String aIS_CREATE_DOC = "";
            String aIS_MANUAL = "";

            aIS_CREATE_DOC = Request["IS_CREATE_DOC"];
            aIS_MANUAL = Request["IS_MANUAL"];

            param_submitter = String.Format(@"{0}", "../Wf/submitter.aspx?");
            param_submitter += String.Format(@"{0}{1}", "CURR_DOC_ID=", Request["CURR_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_TYPE_ID=", Request["CURR_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_ID=", Request["CURR_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_CTL_ID=", Request["CURR_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DOC=", Request["USER_ID_DOC"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_DONOR=", Request["USER_ID_DONOR"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_LOGIN=", Request["USER_ID_LOGIN"]);
            param_submitter += String.Format(@"{0}{1}", "&USER_ID_TAKEN=", Request["USER_ID_TAKEN"]);
            param_submitter += String.Format(@"{0}{1}", "&IS_CREATE_DOC=", aIS_CREATE_DOC);
            param_submitter += String.Format(@"{0}{1}", "&IS_MANUAL=", aIS_MANUAL);
            param_submitter += String.Format(@"{0}{1}", "&CURR_PROC_STATUS=", Request["CURR_PROC_STATUS"]);
            param_submitter += String.Format(@"{0}{1}", "&CURR_DOC_STATUS=", s_ddlStatusList.SelectedValue);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_ID=", Request["PREV_DOC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_DOC_TYPE_ID=", Request["PREV_DOC_TYPE_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_PROC_ID=", Request["PREV_PROC_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&PREV_CTL_ID=", Request["PREV_CTL_ID"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_1", Request["SLOT_1"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_2=", Request["SLOT_2"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_3=", Request["SLOT_3"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_4=", Request["SLOT_4"]);
            param_submitter += String.Format(@"{0}{1}", "&SLOT_5=", Request["SLOT_5"]);
            param_submitter += String.Format(@"{0}{1}", "&MESSAGE=", Request["MESSAGE"]);
            param_submitter += String.Format(@"{0}{1}", "&PACKAGE=", "SUBMIT_ENGINE_REGISTER");

            ltSubmitter.Text = Server.HtmlDecode("<div class='modal fade' id='myModalFrmSubmitter' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><button type = 'button' class='close' data-dismiss='modal'>&times;</button><h4 class='modal-title'>Workflow Started</h4></div><div class='modal-body'><div class='sizer'><div class='embed-responsive embed-responsive-16by9'><iframe id = 'frmSubmitter' src='" + param_submitter + "'></iframe></div></div></div><div class='modal-footer'><button type = 'button' class='btn btn-default' data-dismiss='modal'>Close</button></div></div></div></div>");

        }

        private void load_chkBizTypeList(int intIDRow)
        {
            dbcReader dbcVendorList = new dbcReader();
            DataSet dsType = dbcVendorList.ExecuteSelect("select P_BUSINESS_TYPE_ID,BUSINESS_TYPE from [dbo].[P_BUSINESS_TYPE]", 0, 0);
            DataTable dtType = dsType.Tables[0];

            String strCheckBiztype = "";
            String strChecked = "";

            if (intIDRow != 0)
            {
                DataSet dsTypeDb = dbcVendorList.ExecuteSelect("select P_BUSINESS_TYPE_ID from [dbo].[T_VENDOR_BUSINESS_TYPE] where T_VENDOR_REGISTER_ID=" + intIDRow.ToString(), 0, 0);
                DataTable dtTypeDb = dsTypeDb.Tables[0];
                String db_biz_type = "";

                for (int i = 0; i < dtTypeDb.Rows.Count; i++)
                {
                    db_biz_type = db_biz_type + dtTypeDb.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",";
                }

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"], dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0 || Strings.InStr(db_biz_type, dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<label class='checkbox-inline'><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label>";
                }

                lblCheck.Text = strCheckBiztype;

            }
            else
            {

                for (int i = 0; i < dtType.Rows.Count; i++)
                {

                    if (Strings.InStr(Request.Form["chkBizType"], dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + ",") > 0)
                    {
                        strChecked = "checked";
                    }
                    else
                    {
                        strChecked = "";
                    }

                    strCheckBiztype = strCheckBiztype + "<label class='checkbox-inline'><input type='checkbox' " + strChecked + " name='chkBizType' value='" + dtType.Rows[i]["P_BUSINESS_TYPE_ID"].ToString().Trim() + "'>" + dtType.Rows[i]["BUSINESS_TYPE"].ToString().Trim() + "</label>";
                }

                lblCheck.Text = strCheckBiztype;
            }

        }


    }
}