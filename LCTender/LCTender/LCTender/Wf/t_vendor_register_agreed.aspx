﻿<%@ Page Language="C#" MasterPageFile="~/LC.Master" AutoEventWireup="true" CodeBehind="t_vendor_register_agreed.aspx.cs" Inherits="LCTender.Wf.t_vendor_register_agreed" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentSection" runat="server">
<%: Styles.Render("~/bundles/BootstrapCss")%>
<script src="../Scripts/jquery-1.11.2.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
<style type="text/css">
    .panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
    .login-form .form-control {
      background: #f7f7f7 none repeat scroll 0 0;
      border: 1px solid #d4d4d4;
      border-radius: 4px;
      font-size: 14px;
      height: 50px;
      line-height: 50px;
    }
    .main-div-agreed {
      /*background: #ffffff none repeat scroll 0 0;
      border-radius: 2px;
      margin: 10px auto 30px;
      padding: 50px 70px 70px 71px;*/

      background-color:#fff;
      border-radius: 10px;
      /*margin: 10px auto 10px;*/
      margin:10px 40px 75px 0px;
      padding: 10px 25px 20px 10px;
    }
    .login-form .form-group {
      margin-bottom:10px;
    }
    .login-form{ text-align:left;}
    .forgot a {
      color: #777777;
      font-size: 14px;
      text-decoration: underline;
    }
    .login-form  .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
      font-size: 14px;
      width: 5%;
      height: 30px;
      line-height: 30px;
      padding: 0;
    }
    .forgot {
      text-align: left; margin-bottom:30px;
    }
    .botto-text {
      color: #ffffff;
      font-size: 14px;
      margin: auto;
    }
    .login-form .btn.btn-primary.reset {
      background: #ff9900 none repeat scroll 0 0;
    }
    .back { text-align: left; margin-top:10px;}
    .back a {color: #444444; font-size: 13px;text-decoration: none;}
    .modal-dialog {
      width: 98%;
      /*height: 92%;*/
      /**width: 40%;*/
      padding: 0;
    }
    
    .modal-content {
      /*height: 99%;*/
    }
    .hidden
     {
         display:none;
     }
    .sizer {
      /*width: 500px;*/
    }
    .btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
    }
    .h4{ 
	    color: #4d96de;	 
	}
    
</style>
<script type="text/javascript">
    window.closeModal = function () {
        $("#myModalFrame .close").click();
    };
    window.closeModalBank = function () {
        $("#myModalFrmBank .close").click();
    };
</script>
<div class="row">
 <div class="col-sm-12">
  <div class="main-div-agreed">
      <asp:Literal ID="ltHeader" runat="server"></asp:Literal>
      <form runat="server"> 
          <!-- Modal --> 
          <div class="form-row">    
              <div class="form-group col-sm-4">
                <label for="vendName" class="control-label">Nama Vendor:</label>
                <asp:TextBox ID="vendName" class="form-control" name="vendName" runat="server" ReadOnly="true"></asp:TextBox>
              </div>
              <div class="form-group col-sm-4">
                <label for="npwpNo" class="control-label">No NPWP:</label>          
                <asp:TextBox ID="npwpNo" class="form-control" name="npwpNo" runat="server" ReadOnly="true"></asp:TextBox>   
              </div>
             <div class="form-group col-sm-4">
                <label for="email" class="control-label">Email:</label>         
                <asp:TextBox ID="email" type="email" class="form-control" name="email" runat="server" ReadOnly="true"></asp:TextBox>
             </div>
          </div>
          <div class="form-group col-sm-12">
            <label for="officeAddr" class="control-label">Alamat Kantor:</label>
            <asp:TextBox ID="officeAddr" class="form-control" name="officeAddr" runat="server" ReadOnly="true"></asp:TextBox>
          </div>    
          <div class="form-group col-sm-12">
             <label for="siteAddr" class="control-label">Alamat Situs:</label>
             <asp:TextBox ID="siteAddr" class="form-control" name="siteAddr" runat="server" ReadOnly="true"></asp:TextBox>
           </div>
          <div class="form-row">
            <div class="form-group col-sm-4">
             <label for="phoneNo" class="control-label">Telephone:</label>
             <asp:TextBox ID="phoneNo" class="form-control" name="phoneNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group col-sm-4">
                <label for="faxNo" class="control-label">Fax:</label>
                <asp:TextBox ID="faxNo" class="form-control" name="faxNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="form-group col-sm-4">
                <label for="dtCompany" class="control-label">Tgl Berdiri:</label>            
                <div class="input-group date">
                    <asp:TextBox ID="dtNotary" class="form-control" name="dtNotary" runat="server" ReadOnly="true"></asp:TextBox>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
             </div>
            </div> 
           <div class="form-row">
             <div class="form-group col-sm-6">
                 <label for="typBussiness" class="control-label">Bidang Usaha:<asp:Literal ID="ltLOV" runat="server"></asp:Literal></label><br/>    
                 <asp:Literal ID="lblCheck" runat="server"></asp:Literal>                
                 <asp:TextBox ID="typBussiness" class="form-control" name="typBussiness" runat="server" style="display:none"></asp:TextBox>                    
                 <asp:TextBox ID="typBussiness_id" class="hidden" name="typBussiness_id" runat="server"></asp:TextBox>
             </div> 
             <div class="form-group col-sm-6">
                <label for="nmBank" class="control-label">Bank::.<asp:Literal ID="ltLOVBank" runat="server"></asp:Literal></label>
                <asp:TextBox ID="nmBank" class="form-control" name="nmBank" runat="server"  ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="nmBank_id" class="hidden" name="nmBank_id" runat="server"></asp:TextBox>            
             </div> 
            </div>   
           <div class="form-row">    
            <div class="form-group col-sm-6">
                <label for="accNo" class="control-label">No Rekening:</label>
                <asp:TextBox ID="accNo" class="form-control" name="accNo" runat="server" ReadOnly="true"></asp:TextBox>
            </div>   
            <div class="form-group col-sm-6">
                <label for="accOwner" class="control-label">Pemilik Rekening:</label>
                <asp:TextBox ID="accOwner" class="form-control" name="accOwner" runat="server" ReadOnly="true"></asp:TextBox>
            </div> 
           </div> 
          <div class="form-row">
            <div class="form-group">
                <asp:Button ID="btnSave" class="btn btn-primary" runat="server" Text="Simpan" OnClick="btnSave_Click" />&nbsp; 
                <asp:Button ID="btnUpdate" class="btn btn-primary" runat="server" Text="Update" OnClick="btnUpdate_Click" />&nbsp;
                <asp:Button ID="btnDelete" class="btn btn-primary" runat="server" Text="Hapus" OnClick="btnDelete_Click" />&nbsp;
                <asp:Button ID="btnStartTender" class="btn btn-primary" runat="server" Text="Start Tender" OnClick="btnStartTender_Click" />&nbsp; 
                <asp:TextBox ID="t_vendor_register_id" class="hidden" name="t_vendor_register_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="request_status" class="hidden" name="request_status" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_organization_id" class="hidden" name="p_organization_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="p_app_user_id" class="hidden" name="p_app_user_id" runat="server"></asp:TextBox>
                <asp:TextBox ID="user_name" class="hidden" name="user_name" runat="server"></asp:TextBox>
            </div>
           </div>
        <!-- Modal -->    
     </form>
    <!-- iframe -->
    <div class="modal fade" id="myModalFrame" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">List Business Type</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOV" class="embed-responsive-item" src="../Lov/p_business_type.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
    <!-- iframe --> 
    <!-- iframe -->
    <div class="modal fade" id="myModalFrmBank" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">List Bank</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe id="frmLOVBank" class="embed-responsive-item" src="../Lov/p_bank.aspx"></iframe>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>
   <!-- iframe -->
   <!-- iframe -->
   <%-- <div class="modal fade" id="myModalFrmSubmitter" role="dialog">
     <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Workflow Started</h4>             
            </div>
            <div class="modal-body">
                <div class="sizer">
                <div class="embed-responsive embed-responsive-16by9">                    
                    <asp:Label ID="ltSubmitter" runat="server" name="ltSubmitter" Text="Label"></asp:Label>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
    </div>--%>
    <!-- iframe -->
   <asp:Label ID="ltSubmitter" runat="server"></asp:Label>
  </div> 
 </div>
</div>
<script type="text/javascript">
    //debugger;
    $('[id*=myModalFrame]').modal('hide');
    $('[id*=myModalFrmBank]').modal('hide');
    $('[id*=myModalFrmSubmitter]').modal('hide');
    
    //$('.input-group.date').datepicker({
    //    format: "dd-M-yyyy",
    //    autoclose: true
    //});
    function clickLOV() {
        $('[id*=myModalFrame]').modal('show');
    }
    function clickLOVBank() {
        $('[id*=myModalFrmBank]').modal('show');
    }
    function clickStartTender() {
        var url = "";
        $(location).attr('href', url);
    }
    function clickLoadSubmitter() {
        debugger;
        $('[id*=myModalFrmSubmitter]').modal('show');
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="server">
</asp:Content>