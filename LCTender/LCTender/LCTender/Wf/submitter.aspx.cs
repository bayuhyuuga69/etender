﻿using System;
using System.Web.UI;
using WsLCTender;
using log4net;
using System.Data;
using System.Net;
using Newtonsoft.Json;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace LCTender.Transaction
{
    public partial class submitter : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(t_vendor_register_maint));
        public class bodyInsert
        {
          public string expiredDate { get; set; }
          public string personalNo { get; set; }
          public string documentNo { get; set; }
          public string phoneNo { get; set; }
          public string taxRegistrationNo { get; set; }
          public string websiteName { get; set; }
          public string contractSummary { get; set; }
          public string contactName { get; set; }
          public string personalName { get; set; }
          public string branchName { get; set; }
          public string faxNo { get; set; }
          public string notes { get; set; }
          public string contactPerson { get; set; }
          public string contractNumber { get; set; }
          public string contractEnd { get; set; }
        }
        public class responInsert
        {
            public string status { get; set; }
            public string vendorNo { get; set; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CURR_DOC_ID.Text = Request["CURR_DOC_ID"];
                CURR_DOC_TYPE_ID.Text = Request["CURR_DOC_TYPE_ID"];
                CURR_PROC_ID.Text = Request["CURR_PROC_ID"];
                CURR_CTL_ID.Text = Request["CURR_CTL_ID"];
                PREV_DOC_ID.Text = Request["PREV_DOC_ID"];
                PREV_DOC_TYPE_ID.Text = Request["PREV_DOC_TYPE_ID"];
                PREV_PROC_ID.Text = Request["PREV_PROC_ID"];
                PREV_CTL_ID.Text = Request["PREV_CTL_ID"];
                SLOT_1.Text = Request["SLOT_1"];
                SLOT_2.Text = Request["SLOT_2"];
                SLOT_3.Text = Request["SLOT_3"];
                SLOT_4.Text = Request["SLOT_4"];
                SLOT_5.Text = Request["SLOT_5"];
                USER_ID_DOC.Text = Request["USER_ID_DOC"];
                USER_ID_DONOR.Text = Request["USER_ID_DONOR"];
                USER_ID_LOGIN.Text = Request["USER_ID_LOGIN"];
                USER_ID_TAKEN.Text = Request["USER_ID_TAKEN"];
                IS_CREATE_DOC.Text = Request["IS_CREATE_DOC"];
                IS_MANUAL.Text = Request["IS_MANUAL"];
                CURR_PROC_STATUS.Text = Request["CURR_PROC_STATUS"];
                CURR_DOC_STATUS.Text = Request["CURR_DOC_STATUS"];
                MESSAGE.Text = Request["MESSAGE"];
                RETURN_VAL.Text = "1";
                PACKAGE.Text = Request["PACKAGE"];

                if (IS_CREATE_DOC.Text == "")
                {
                    IS_CREATE_DOC.Text = "N";
                }
                if (IS_MANUAL.Text == "")
                {
                    IS_MANUAL.Text = "N";
                }
                if (CURR_DOC_STATUS.Text == "")
                {
                    CURR_DOC_STATUS.Text = "1";
                }
                if (CURR_PROC_STATUS.Text=="")
                {
                    CURR_PROC_STATUS.Text = "0";
                }
                
                dbcReader dtbTask = new dbcReader();
                ltTask.Text = Server.HtmlDecode(dtbTask.db_get_next_info(Convert.ToInt32(CURR_PROC_ID.Text), Convert.ToInt32(CURR_DOC_TYPE_ID.Text)));
                txtStatus.Text = Server.HtmlDecode(dtbTask.db_get_next_status(Convert.ToInt32(CURR_DOC_STATUS.Text)));
                ltLoading.Text ="Loading...<img class=\"img-responsive\" src=\"../images/spinner.gif\" height=\"70\" width=\"70\"/>";
                ltLoading.Visible = false;
                txtTime.Text = DateTime.Now.ToString("dddd dd MMMM yyyy");
            }
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            btnSubmit.Visible = false;
            ltLoading.Visible = true;

            log.Info("Submitter Started. ");
            try
            {
                CURR_DOC_ID.Text = Request["CURR_DOC_ID"];
                CURR_DOC_TYPE_ID.Text = Request["CURR_DOC_TYPE_ID"];
                CURR_PROC_ID.Text = Request["CURR_PROC_ID"]; 
                CURR_CTL_ID.Text = Request["CURR_CTL_ID"];
                PREV_DOC_ID.Text = Request["PREV_DOC_ID"];
                PREV_DOC_TYPE_ID.Text = Request["PREV_DOC_TYPE_ID"];
                PREV_PROC_ID.Text = Request["PREV_PROC_ID"];
                PREV_CTL_ID.Text = Request["PREV_CTL_ID"];
                SLOT_1.Text = Request["SLOT_1"];
                SLOT_2.Text = Request["SLOT_2"];
                SLOT_3.Text = Request["SLOT_3"];
                SLOT_4.Text = Request["SLOT_4"];
                SLOT_5.Text = Request["SLOT_5"];
                USER_ID_DOC.Text = Request["USER_ID_DOC"];
                USER_ID_DONOR.Text = Request["USER_ID_DONOR"];
                USER_ID_LOGIN.Text = Request["USER_ID_LOGIN"];
                USER_ID_TAKEN.Text = Request["USER_ID_TAKEN"];
                IS_CREATE_DOC.Text = Request["IS_CREATE_DOC"];
                IS_MANUAL.Text = Request["IS_MANUAL"];
                CURR_PROC_STATUS.Text = Request["CURR_PROC_STATUS"];
                CURR_DOC_STATUS.Text = Request["CURR_DOC_STATUS"];
                MESSAGE.Text = Request["MESSAGE"];
                RETURN_VAL.Text = "1";
                PACKAGE.Text = Request["PACKAGE"];

                if (IS_CREATE_DOC.Text == "")
                {
                    IS_CREATE_DOC.Text = "N";
                }
                if (IS_MANUAL.Text == "")
                {
                    IS_MANUAL.Text = "N";
                }
                if (CURR_DOC_STATUS.Text == "")
                {
                    CURR_DOC_STATUS.Text = "1";
                }
                if (CURR_PROC_STATUS.Text == "")
                {
                    CURR_PROC_STATUS.Text = "0";
                }
                

                dbcReader dbcInsVendReg = new dbcReader();

                string sql = "Select NEXT VALUE FOR seq_submitter_id as submitter_id";
                SUBMITTER_ID.Text = dbcInsVendReg.ExecuteReader(sql, "submitter_id");

                object outputObject;
                string s_packname = PACKAGE.Text;
                string s_varvalue = "";
                s_varvalue += String.Format(@"{0}{1}", SUBMITTER_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", IS_CREATE_DOC.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", IS_MANUAL.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", USER_ID_DOC.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", USER_ID_DONOR.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", USER_ID_LOGIN.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", USER_ID_TAKEN.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_CTL_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_DOC_TYPE_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_PROC_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_DOC_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_DOC_STATUS.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", CURR_PROC_STATUS.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", PREV_CTL_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", PREV_DOC_TYPE_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", PREV_PROC_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", PREV_DOC_ID.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", MESSAGE.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", SLOT_1.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", SLOT_2.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", SLOT_3.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", SLOT_4.Text, "~#");
                s_varvalue += String.Format(@"{0}{1}", SLOT_5.Text, "");

                log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                string s_outcodename = "";
                string s_outmsgname = "";
                int StartRecs = 0;
                int EndRecs = 0;

                outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                String o_result_code = (String)(pi.GetValue(outputObject, null));

                System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));

                if (o_result_code == "1")
                {
                    ltLoading.Visible = false;
                    DataSet dsSubmit = dbcInsVendReg.ExecuteSelect("select RETURN_MESSAGE,ERROR_MESSAGE,MAIL_ADDRESS,USER_ID_DONOR,USER_NAME_NEXT from SUBMITTER where SUBMITTER_ID=" + SUBMITTER_ID.Text, 0, 0);
                    DataTable dtSubmit = dsSubmit.Tables[0];
                    String retMsg = dtSubmit.Rows[0]["RETURN_MESSAGE"].ToString(); 
                    String errMsg = dtSubmit.Rows[0]["ERROR_MESSAGE"].ToString(); 
                    String userIdDonor = dtSubmit.Rows[0]["USER_ID_DONOR"].ToString();
                    String mailAddr = dtSubmit.Rows[0]["MAIL_ADDRESS"].ToString();
                    String userDonor = dbcInsVendReg.ExecuteReader("select FULL_NAME from P_APP_USER Where P_APP_USER_ID=" + userIdDonor, "FULL_NAME");
                    String userReceive = dtSubmit.Rows[0]["USER_NAME_NEXT"].ToString();

                    String msgPopUp = "";
                    if (CURR_DOC_TYPE_ID.Text == "13") {
                        if (retMsg.Trim().Replace("'", "") == "0")
                        {
                            msgPopUp = errMsg.Trim().Replace("'", "");
                        } else
                        {
                            msgPopUp = retMsg.Trim().Replace("'", "");
                        }
                    }else if (CURR_DOC_TYPE_ID.Text == "14")
                    {
                       if (!errMsg.Trim().Replace("'", "").Contains("ERROR:")){
                            msgPopUp = "SUBMITTED";
                        }else
                        {
                            msgPopUp = errMsg.Trim().Replace("'", "");
                        }
                    }
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("bootbox.alert({ ");
                    cstext1.Append("message: '" + msgPopUp + "',");
                    cstext1.Append("size: 'small',");
                    cstext1.Append("callback:function(){window.parent.closeModalSubmitter();}");
                    cstext1.Append("});");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                    String agreed = dbcInsVendReg.ExecuteReader("select REQUEST_STATUS from T_VENDOR_REGISTER Where T_VENDOR_REGISTER_ID=" + CURR_DOC_ID.Text, "REQUEST_STATUS");
                    String vendMail = dbcInsVendReg.ExecuteReader("select email_addr from T_VENDOR_REGISTER Where T_VENDOR_REGISTER_ID=" + CURR_DOC_ID.Text, "email_addr");

                    if ((agreed == "114") && (CURR_DOC_TYPE_ID.Text == "13"))
                    {
                       if (!msgPopUp.Contains("ERROR:")){

                            String amt_personal = dbcInsVendReg.ExecuteReader("select count(*) amt_personal from P_PERSONAL Where T_VENDOR_REGISTER_ID=" + CURR_DOC_ID.Text, "amt_personal");
                            if (Convert.ToInt32(amt_personal)==0)
                            {
                               
                                string strInsCynergy = insertVendorToCinergy(CURR_DOC_ID.Text);
                                responInsert rest = new responInsert();
                                rest = JsonConvert.DeserializeObject<responInsert>(strInsCynergy);
                                string retValid = dbcInsVendReg.ExecuteReader("declare @str varchar(max); " +
                                                                 "exec p_upd_vendorno "+ CURR_DOC_ID.Text + ",'"+ rest.vendorNo + "',@str OUTPUT " +
                                                                 "select @str ret", "ret");
                            }
                           
                            string bodyvendor = getBodyMailVendor(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                            sendEmailVendor(bodyvendor, vendMail);
                            DataSet dsApproval = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_approval_mail " + CURR_DOC_ID.Text, 0, 0);
                            DataTable dtApproval = dsApproval.Tables[0];
                            if (dtApproval.Rows.Count>0)
                            {
                                for (int i=0;i<= dtApproval.Rows.Count-1;i++)
                                {
                                    string body = getBodyMailApprove(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                                    sendEmail(body, dtApproval.Rows[i]["mail_address"].ToString());
                                }
                           
                            }                          
                        }
                    } else if ((agreed != "114") && (CURR_DOC_TYPE_ID.Text == "13"))
                    {
                       if (!msgPopUp.Contains("ERROR:")) { 
                           string body = getBodyMail(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                           sendEmail(body, mailAddr);
                            if (CURR_PROC_ID.Text == "337")
                            {
                              string bodyvendor = getBodyMailRegVendor(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                              sendEmailVendor(bodyvendor, vendMail);
                            }
                        }
                    } else if ((agreed == "115") && (CURR_DOC_TYPE_ID.Text == "13"))
                    {
                        if (!msgPopUp.Contains("ERROR:"))
                        {
                            string bodyvendor = getBodyMailVendor(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                            sendEmailVendor(bodyvendor, vendMail);
                            DataSet dsApproval = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_approval_mail " + CURR_DOC_ID.Text, 0, 0);
                            DataTable dtApproval = dsApproval.Tables[0];
                            if (dtApproval.Rows.Count > 0)
                            {
                                for (int i = 0; i <= dtApproval.Rows.Count-1; i++)
                                {
                                    string body = getBodyMailApprove(CURR_DOC_ID.Text, retMsg, userDonor, userReceive);
                                    sendEmail(body, dtApproval.Rows[i]["mail_address"].ToString());
                                }

                            }
                        }
                    }

                }
                else if (o_result_code == "-1")
                {
                    ltLoading.Visible = false;
                    String retMsg = dbcInsVendReg.ExecuteReader("select RETURN_MESSAGE from SUBMITTER Where SUBMITTER_ID=" + SUBMITTER_ID.Text, "RETURN_MESSAGE");
                    String errMsg = dbcInsVendReg.ExecuteReader("select ERROR_MESSAGE from SUBMITTER Where SUBMITTER_ID=" + SUBMITTER_ID.Text, "ERROR_MESSAGE");
                    ClientScriptManager cs = Page.ClientScript;
                    System.Text.StringBuilder cstext1 = new System.Text.StringBuilder();
                    cstext1.Append("<script type=text/javascript>");
                    cstext1.Append("bootbox.alert({ ");
                    cstext1.Append("message: '" + retMsg.Trim().Replace("'", "") + "-" + errMsg.Trim() + "',");
                    cstext1.Append("className: 'bb-alternate-modal' });");
                    cstext1.Append("</script>");
                    cs.RegisterStartupScript(this.GetType(), "PopupScript", cstext1.ToString());
                }

            }
            catch (Exception ex)
            {
                log.ErrorFormat("Submitter Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        public static string insertVendorToCinergy(string curr_doc_id)
        {
            dbcReader dbcInsVendReg = new dbcReader();
          
            DataSet dsBodyInsert = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_vendor_cinergy "+ curr_doc_id, 0, 0);
            DataTable dtBodyInsert = dsBodyInsert.Tables[0];
            string expiredDate;
            if (dtBodyInsert.Rows[0]["expiredDate"].ToString() == "")
            {
                expiredDate = null;
            }else
            {
                expiredDate = dtBodyInsert.Rows[0]["expiredDate"].ToString();
            }
            string personalNo;
            if (dtBodyInsert.Rows[0]["personalNo"].ToString() == "")
            {
                personalNo = null;
            }
            else
            {
                personalNo = dtBodyInsert.Rows[0]["personalNo"].ToString();
            }
            string taxRegistrationNo;
            if (dtBodyInsert.Rows[0]["taxRegistrationNo"].ToString() == "")
            {
                taxRegistrationNo = null;
            }
            else
            {
                taxRegistrationNo = dtBodyInsert.Rows[0]["taxRegistrationNo"].ToString();
            }
            string contractSummary;
            if (dtBodyInsert.Rows[0]["contractSummary"].ToString() == "")
            {
                contractSummary = null;
            }
            else
            {
                contractSummary = dtBodyInsert.Rows[0]["contractSummary"].ToString();
            }
            string contractNumber;
            if (dtBodyInsert.Rows[0]["contractNumber"].ToString() == "")
            {
                contractNumber = null;
            }
            else
            {
                contractNumber = dtBodyInsert.Rows[0]["contractNumber"].ToString();
            }
            string contractEnd;
            if (dtBodyInsert.Rows[0]["contractEnd"].ToString() == "")
            {
                contractEnd = null;
            }
            else
            {
                contractEnd = dtBodyInsert.Rows[0]["contractEnd"].ToString();
            }
            var bodyInsert = new
                  {
                    expiredDate = expiredDate,// dtBodyInsert.Rows[0]["expiredDate"].ToString(),
                    personalNo = personalNo, //dtBodyInsert.Rows[0]["personalNo"].ToString(),
                    documentNo = dtBodyInsert.Rows[0]["documentNo"].ToString(),
                    phoneNo = dtBodyInsert.Rows[0]["phoneNo"].ToString(),
                    taxRegistrationNo = taxRegistrationNo,// dtBodyInsert.Rows[0]["taxRegistrationNo"].ToString(),
                    websiteName = dtBodyInsert.Rows[0]["websiteName"].ToString(),
                    contractSummary = contractSummary, //dtBodyInsert.Rows[0]["contractSummary"].ToString(),
                    contactName = dtBodyInsert.Rows[0]["contactName"].ToString(),
                    personalName = dtBodyInsert.Rows[0]["personalName"].ToString(),
                    branchName = dtBodyInsert.Rows[0]["branchName"].ToString(),
                    faxNo = dtBodyInsert.Rows[0]["faxNo"].ToString(),
                    notes = dtBodyInsert.Rows[0]["notes"].ToString(),
                    contactPerson = dtBodyInsert.Rows[0]["contactPerson"].ToString(),
                    contractNumber = contractNumber,//dtBodyInsert.Rows[0]["contractNumber"].ToString(),
                    contractEnd = contractEnd,//dtBodyInsert.Rows[0]["contractEnd"].ToString()
                 };
            string result = "";
            DataSet dsresult = dbcInsVendReg.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='INSERTVENDOR'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            using (var client = new WebClient())
            {
                try
                {
                    string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString();
                    var dataString = JsonConvert.SerializeObject(bodyInsert);
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    result = client.UploadString(new Uri(baseurl), "POST", dataString);
                    return result;
                }
                catch (Exception ex)
                {
                    return "ERROR-" + ex.Message.ToString();
                }
            }
        }
        public void sendEmail(string body, string email)
        {
            try
            {
                string smtpconfig = ConfigurationManager.AppSettings["smtp"].ToString();
                string user = ConfigurationManager.AppSettings["user_notify"].ToString();
                string password = ConfigurationManager.AppSettings["password_notify"].ToString();
                string port = ConfigurationManager.AppSettings["port"].ToString();
                string subject = ConfigurationManager.AppSettings["subject_notify"].ToString();
                MailMessage mail = new MailMessage();
                mail.To.Add(email);
                //mail.From = new MailAddress("e-notification@lippo-cikarang.com");
                mail.From = new MailAddress(user);
                mail.Subject = subject;

                mail.Body = body;

                mail.IsBodyHtml = true;
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                SmtpClient smtp = new SmtpClient();
                //smtp.Host = "10.128.2.11";
                smtp.Host = smtpconfig;
                smtp.Timeout = 10000;//Or Your SMTP Server Address
                //smtp.Credentials = new System.Net.NetworkCredential
                //     ("e-notification@lippo-cikarang.com", "lippo678"); // ***use valid credentials***
                //smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential
                    (user, password); // ***use valid credentials***
                smtp.Port = Convert.ToInt32(port);

                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Exception in sendEmail: {0}", ex.Message);
                throw;
            }
        }
        public void sendEmailVendor(string body, string email)
        {
            try
            {
                string smtpconfig = ConfigurationManager.AppSettings["smtp"].ToString();
                string user = ConfigurationManager.AppSettings["user_reg"].ToString();
                string password = ConfigurationManager.AppSettings["password_reg"].ToString();
                string port = ConfigurationManager.AppSettings["port"].ToString();
                string subject = ConfigurationManager.AppSettings["subject_reg"].ToString();
                MailMessage mail = new MailMessage();
                mail.To.Add(email);
                //mail.From = new MailAddress("e-notification@lippo-cikarang.com");
                mail.From = new MailAddress(user);
                mail.Subject = subject;

                mail.Body = body;

                mail.IsBodyHtml = true;
                //System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
                SmtpClient smtp = new SmtpClient();
                //smtp.Host = "10.128.2.11";
                smtp.Host = smtpconfig;
                smtp.Timeout = 10000;//Or Your SMTP Server Address
                //smtp.Credentials = new System.Net.NetworkCredential
                //     ("e-notification@lippo-cikarang.com", "lippo678"); // ***use valid credentials***
                //smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential
                    (user, password); // ***use valid credentials***
                smtp.Port = Convert.ToInt32(port);

                //Or your Smtp Email ID and Password
                smtp.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate,
                X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Exception in sendEmail: {0}", ex.Message);
                throw;
            }
        }
        private bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            //Console.WriteLine(certificate);
            return true;
        }
        public string getBodyMail(string curr_doc_id, string retMsg, string userDonor, string userReceive)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            DataSet dsBodyMail = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_vendor_approve " + curr_doc_id, 0, 0);
            DataTable dtBodyMail = dsBodyMail.Tables[0];
            string roothPathTemplate = ConfigurationManager.AppSettings["RootPathTemplate"].ToString();
            string task = "";
            if (retMsg!="0")
            {
                int i = retMsg.IndexOf(',');
                task = retMsg.Substring(i+1);
            }
            else
            {
                task = "";
            }
            string mailBody = File.ReadAllText(roothPathTemplate);
            mailBody = mailBody.Replace("{FULL_NAME}", userReceive);
            mailBody = mailBody.Replace("{TANGGAL}", DateTime.Now.ToString("dd MMMM yyyy"));
            mailBody = mailBody.Replace("{TASK}", task);
            mailBody = mailBody.Replace("{VENDOR_NAME}", dtBodyMail.Rows[0]["vendor_name"].ToString());
            mailBody = mailBody.Replace("{REGISTER_NO}", dtBodyMail.Rows[0]["register_no"].ToString());
            mailBody = mailBody.Replace("{COST_LIMIT}", "Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_amt"].ToString())) + " - Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_max_amt"].ToString())));
            mailBody = mailBody.Replace("{SENDER}", userDonor);
            return mailBody;
        }
        public string getBodyMailVendor(string curr_doc_id, string retMsg, string userDonor, string userReceive)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            DataSet dsBodyMail = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_vendor_approve " + curr_doc_id, 0, 0);
            DataTable dtBodyMail = dsBodyMail.Tables[0];
            string roothPathTemplate = ConfigurationManager.AppSettings["RootPathTemplateVendor"].ToString();
            string task = "";
            if (retMsg != "0")
            {
                int i = retMsg.IndexOf(',');
                task = retMsg.Substring(i + 1);
            }
            else
            {
                task = "";
            }
            string mailBody = File.ReadAllText(roothPathTemplate);
            mailBody = mailBody.Replace("{FULL_NAME}", userReceive);
            mailBody = mailBody.Replace("{TANGGAL}", DateTime.Now.ToString("dd MMMM yyyy"));
            mailBody = mailBody.Replace("{VENDOR_NAME}", dtBodyMail.Rows[0]["vendor_name"].ToString());
            mailBody = mailBody.Replace("{REGISTER_NO}", dtBodyMail.Rows[0]["register_no"].ToString());
            mailBody = mailBody.Replace("{COST_LIMIT}", "Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_amt"].ToString())) + " - Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_max_amt"].ToString())));
            mailBody = mailBody.Replace("{STATUS}", dtBodyMail.Rows[0]["request_status"].ToString());
            mailBody = mailBody.Replace("{APPROVAL}", userDonor);
            return mailBody;
        }
        public string getBodyMailRegVendor(string curr_doc_id, string retMsg, string userDonor, string userReceive)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            DataSet dsBodyMail = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_vendor_approve " + curr_doc_id, 0, 0);
            DataTable dtBodyMail = dsBodyMail.Tables[0];
            string roothPathTemplate = ConfigurationManager.AppSettings["RootPathTemplateVendor"].ToString();
            string task = "";
            if (retMsg != "0")
            {
                int i = retMsg.IndexOf(',');
                task = retMsg.Substring(i + 1);
            }
            else
            {
                task = "";
            }
            string mailBody = File.ReadAllText(roothPathTemplate);
            mailBody = mailBody.Replace("{FULL_NAME}", userDonor);
            mailBody = mailBody.Replace("{TANGGAL}", DateTime.Now.ToString("dd MMMM yyyy"));
            mailBody = mailBody.Replace("{VENDOR_NAME}", dtBodyMail.Rows[0]["vendor_name"].ToString());
            mailBody = mailBody.Replace("{REGISTER_NO}", dtBodyMail.Rows[0]["register_no"].ToString());
            mailBody = mailBody.Replace("{COST_LIMIT}", "Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_amt"].ToString())) + " - Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_max_amt"].ToString())));
            mailBody = mailBody.Replace("{STATUS}", dtBodyMail.Rows[0]["request_status"].ToString());
            mailBody = mailBody.Replace("{APPROVAL}", userReceive);
            return mailBody;
        }
        public string getBodyMailApprove(string curr_doc_id, string retMsg, string userDonor, string userReceive)
        {
            dbcReader dbcInsVendReg = new dbcReader();
            DataSet dsBodyMail = dbcInsVendReg.ExecuteSelect("exec dbo.p_list_vendor_approve " + curr_doc_id, 0, 0);
            DataTable dtBodyMail = dsBodyMail.Tables[0];
            string roothPathTemplate = ConfigurationManager.AppSettings["RootPathTemplateApprove"].ToString();
            string task = "";
            if (retMsg != "0")
            {
                int i = retMsg.IndexOf(',');
                task = retMsg.Substring(i + 1);
            }
            else
            {
                task = "";
            }
            string mailBody = File.ReadAllText(roothPathTemplate);
            mailBody = mailBody.Replace("{TANGGAL}", DateTime.Now.ToString("dd MMMM yyyy"));
            mailBody = mailBody.Replace("{VENDOR_NAME}", dtBodyMail.Rows[0]["vendor_name"].ToString());
            mailBody = mailBody.Replace("{REGISTER_NO}", dtBodyMail.Rows[0]["register_no"].ToString());
            mailBody = mailBody.Replace("{COST_LIMIT}", "Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_amt"].ToString())) + " - Rp. " + String.Format("{0:n0}", Convert.ToDecimal(dtBodyMail.Rows[0]["cost_limit_max_amt"].ToString())));
            mailBody = mailBody.Replace("{STATUS}", dtBodyMail.Rows[0]["request_status"].ToString());
            mailBody = mailBody.Replace("{APPROVAL}", userDonor);
            return mailBody;
        }

    }
}