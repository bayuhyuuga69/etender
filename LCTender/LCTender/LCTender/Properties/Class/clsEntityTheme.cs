﻿using System;
using System.Data;
using WsLCTender;
using log4net;

namespace LCTender.Class
{
       public class clsEntityTheme
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(clsEntityTheme));
        public string main_color_theme { get; set; }
        public string footer_color_theme { get; set; }
        public static clsEntityTheme GetTheme(string strEntityID)
        {
            log.Info("Bind Data clsEntityTheme Started. ");
            try
            {
                clsEntityTheme oEntity = new clsEntityTheme();
                dbcReader dbcEntity = new dbcReader();
                DataSet dsresult = dbcEntity.ExecuteSelect("Select * from dbo.P_ENTITY where getDate() between valid_from and isnull(valid_to,getDate()+1) and p_entity_id=" + strEntityID, 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                if (dtresult.Rows.Count > 0)
                {

                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        oEntity.main_color_theme=dtresult.Rows[i]["main_color_theme"].ToString();
                        oEntity.footer_color_theme = dtresult.Rows[i]["footer_color_theme"].ToString();
                    }
                }
               
                return oEntity;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Bind Data Entity Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }
    }
}