﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LCTender.Class
{
    public class clsutils
    {
            public int get_first_row(int intPage, int intPerPage)
            {
                return (intPerPage * intPage) - (intPerPage - 1);
            }

            public int get_max_page(int intPerPage, int intMaxRow)
            {
                int intMod, intMaxPage;
                if (intMaxRow == 0)
                    intMaxPage = 1;
                else
                {
                    intMod = intMaxRow % intPerPage;
                    if (intMod == 0)
                        intMaxPage = intMaxRow / intPerPage;
                    else
                        intMaxPage = (intMaxRow / intPerPage) + 1;
                }

                return intMaxPage;
            }

            public void load_image_paging(int intCurrPage, int intPerPage, long intMaxPage, ImageButton objImbFirst, ImageButton objImbPrev, ImageButton objImbNext, ImageButton objImbLast, Label objLblPaging)
            {
                if (intMaxPage == 1)
                {
                    objImbFirst.ImageUrl = "../images/FirstOff.gif";
                    objImbFirst.Enabled = false;
                    objImbPrev.ImageUrl = "../images/PrevOff.gif";
                    objImbPrev.Enabled = false;

                    objImbNext.ImageUrl = "../images/NextOff.gif";
                    objImbNext.Enabled = false;
                    objImbLast.ImageUrl = "../images/LastOff.gif";
                    objImbLast.Enabled = false;
                }
                else if (intCurrPage == intMaxPage)
                {
                    objImbFirst.ImageUrl = "../images/First.gif";
                    objImbFirst.Enabled = true;
                    objImbPrev.ImageUrl = "../images/Prev.gif";
                    objImbPrev.Enabled = true;

                    objImbNext.ImageUrl = "../images/NextOff.gif";
                    objImbNext.Enabled = false;
                    objImbLast.ImageUrl = "../images/LastOff.gif";
                    objImbLast.Enabled = false;
                }
                else if (intCurrPage == 1)
                {
                    objImbFirst.ImageUrl = "../images/FirstOff.gif";
                    objImbFirst.Enabled = false;
                    objImbPrev.ImageUrl = "../images/PrevOff.gif";
                    objImbPrev.Enabled = false;

                    objImbNext.ImageUrl = "../images/Next.gif";
                    objImbNext.Enabled = true;
                    objImbLast.ImageUrl = "../images/Last.gif";
                    objImbLast.Enabled = true;
                }
                else
                {
                    objImbFirst.ImageUrl = "../images/First.gif";
                    objImbFirst.Enabled = true;
                    objImbPrev.ImageUrl = "../images/Prev.gif";
                    objImbPrev.Enabled = true;

                    objImbNext.ImageUrl = "../images/Next.gif";
                    objImbNext.Enabled = true;
                    objImbLast.ImageUrl = "../images/Last.gif";
                    objImbLast.Enabled = true;
                }

                objLblPaging.Text = "Halaman " + intCurrPage + " dari " + intMaxPage;
           }
            public static bool CheckSessionLogin()
            {
                bool result = true;

                if (HttpContext.Current.Session["OrganId"] == null || HttpContext.Current.Session["UserName"] == null
                    || HttpContext.Current.Session["UserId"] == null || HttpContext.Current.Session["Pass"] == null
                    || HttpContext.Current.Session["Status"] == null || HttpContext.Current.Session["P_ENTITY_ID"] == null
                    )
                {
                    result = false;
                }

                return result;
            }
            public static void JQueryErrMsg(string StrMessage, object Page)
            {
                Control Ctr = (Control)Page;
                ScriptManager.RegisterStartupScript(Ctr, Ctr.GetType(), "BootboxAlert" + StrMessage, "bootbox.alert('" + StrMessage.Replace("'", "\\'") + "');", true);
            }

            public static void JQueryRedirect(string url, object Page)
            {
                //Control Ctr = (Control)Page;
                Page Url = new Page();
                //ScriptManager.RegisterStartupScript(Ctr, Ctr.GetType(), "Redirect " + url, "window.location = '" + url + "';", true);
                HttpContext.Current.Response.Redirect("Main/logout.aspx");
            }
   }
}