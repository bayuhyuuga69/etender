﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WsLCTender;

namespace LCTender.Lov
{
    public partial class p_business_type : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataList();
            }
        }

        protected void BindDataList()
        {
            dbcReader clsReader = new dbcReader();
            DataSet dsresult = clsReader.ExecuteSelect("Select * From SIS.dbo.P_BUSINESS_TYPE where upper(ltrim(rtrim(BUSINESS_TYPE))) like '%'+isnull(upper(ltrim(rtrim('"+keyword.Text.Trim()+"'))),null)+'%'", 0,0);
            DataTable dtresult = dsresult.Tables[0];
            gvLOV.DataSource = dtresult;
            gvLOV.DataBind();

            if (dtresult.Rows.Count == 0)
            {

                lblNoRow.Visible = true;
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;border-left: solid 2px #fff; font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;border-left: solid 2px #fff; font-size: 0.9em;color: #fff;'>BUSINESS TYPE</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;border-left: solid 2px #fff; font-size: 0.9em;color: #fff;'>DESCRIPTION</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Visible = false;
            }


        }

        protected void gvLOV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblPILIHVal = (Label)e.Row.FindControl("lblPILIH");
                String strLOV = "";

                String idLOV = (((DataRowView)e.Row.DataItem)["P_BUSINESS_TYPE_ID"]).ToString();
                String idITEM = (((DataRowView)e.Row.DataItem)["BUSINESS_TYPE"]).ToString();

                strLOV += String.Format(@"<button type='button'  class='btn btn-primary' onclick=""javascript:clickReturn('{0}#~#{1}')"">PILIH</button>", idLOV, idITEM);
                lblPILIHVal.Text = strLOV;

            }
        }

        protected void gvLOV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvLOV.PageIndex = e.NewPageIndex;
            gvLOV.DataBind();
            BindDataList();
        }
        protected void btnCari_Click(object sender, EventArgs e)
        {
            BindDataList();
        }
    }
}