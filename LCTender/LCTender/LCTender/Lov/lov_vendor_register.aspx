﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lov_vendor_register.aspx.cs" Inherits="LCTender.Lov.lov_vendor_register" %>
<%@ Import Namespace="System.Web.Optimization" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<%: Styles.Render("~/bundles/BootstrapCss")%>
<%: Scripts.Render("~/bundles/jQuery")%>
<style type="text/css">
.btn.btn-primary {
      background: #f36e21 none repeat scroll 0 0;
      border-color: #f36e21;
      color: #ffffff;
 }
</style>
</head>
<body>  
  <div class="container"> 
    <div class="row">
      <div class="col-sm-12">
        <form id="frmLVBussinessType" class="form-inline" runat="server"> 
           <div class="row">
               <div class="col-sm-12">
                 <div class="row">
                     <div class="col-sm-12">
                      <div class="form-group">
                        <label for="email">Filter:</label>
                            <asp:TextBox id="keyword" name="keyword" class="form-control" runat="server"></asp:TextBox>
                      </div>
                       <asp:Button ID="btnCari" class="btn btn-default" runat="server" Text="Cari" OnClick="btnCari_Click" />
                     </div>
                 </div>
                 
               </div>
           </div>
           <div class="row">  
            <div class="col-sm-12">         
                <asp:GridView ID="gvLOV" runat="server" Width="100%"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt"
                    CellPadding="5" CellSpacing="4"
                    OnRowDataBound="gvLOV_RowDataBound" 
                    OnPageIndexChanging="gvLOV_PageIndexChanging"
                    AutoGenerateColumns="False"
                    AllowPaging="true">
                    <Columns>          
                    <asp:BoundField DataField="T_VENDOR_REGISTER_ID" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="T_VENDOR_REGISTER_ID" visible="false"/>
                    <asp:TemplateField HeaderText="" HeaderStyle-CssClass="visible-desktop" ItemStyle-HorizontalAlign="Center"  ItemStyle-CssClass="visible-desktop">                
                        <ItemTemplate>
                            <asp:Label ID="lblPILIH" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="REGISTER_NO" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="NO REGISTER" />
                    <asp:BoundField DataField="VENDOR_NAME" ItemStyle-Wrap="true" ItemStyle-CssClass="hidden-phone" HeaderText="NAMA"  />    
                    </Columns>
                </asp:GridView>
             </div>
           </div>
           <asp:Label ID="lblNoRow" runat="server"></asp:Label>
        </form>
      </div>
    </div>
  </div>
</body>
</html>
<script type="text/javascript">
    function clickReturn(retVal) {
         //debugger;
         arrVal = retVal.split('#~#');
        //for (x=0;x<arrForm.length;x++) {
            //window.opener.document.getElementById(strForm+arrForm[x]).value = arrVal[x];
          //  window.opener.document.getElementById("ContentSection_registerControlID_typBussiness").value = arrVal[0];
        //}
         parent.document.getElementById("ContentSection_t_vendor_register_id").value = arrVal[0];
         parent.document.getElementById("ContentSection_txtVendReg").value = arrVal[1];
         parent.closeModal();
}
</script>
