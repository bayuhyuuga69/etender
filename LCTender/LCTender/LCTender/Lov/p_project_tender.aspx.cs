﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WsLCTender;
using log4net;
using LCTender.Class;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.IO;


namespace LCTender.Lov
{
    public partial class p_project_tender : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(p_project_tender));
        dbcReader dbcVendorList = new dbcReader();
        class ResponOpenTender
        {
            public string entityNo { get; set; }
            public string createDate { get; set; }
            public string serviceUnitName { get; set; }
            public string serviceUnitNo { get; set; }
            public string departmentName { get; set; }
            public string divisionNo { get; set; }
            public string entityName { get; set; }
            public string departmentNo { get; set; }
            public string measurement { get; set; }
            public string categoryName { get; set; }
            public string tenderNo { get; set; }
            public string divisionName { get; set; }
            public string description { get; set; }
            public string closingDate { get; set; }
            public string tenderName { get; set; }
            public string quantity { get; set; }
            public string costEstimate { get; set; }
            public string categoryNo { get; set; }
            public string openDate { get; set; }

        }
        class ResBoqDoc
        {
            public string filedescription { get; set; }
            public string filename { get; set; }
            public string documentfile { get; set; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            t_vendor_register_id.Text = Request["t_vendor_register_id"].ToString();
            p_entity_id.Text = Request["p_entity_id"].ToString();
            if (!IsPostBack)
            {
                string entityno = dbcVendorList.ExecuteReader("Select ENTITYNO from dbo.P_ENTITY where P_ENTITY_ID=" + Session["P_ENTITY_ID"].ToString(), "ENTITYNO");
                getOpenTender(entityno);
                BindDataList();
            }
        }

        protected void BindDataList()
        {
            dbcReader clsReader = new dbcReader();
            DataSet dsresult = clsReader.ExecuteSelect("select a.p_project_tender_id, a.project_name, a.tender_no, c.entity_name, a.p_business_type_id, a.quantity, a.boq_file_name, a.file_type, a.measurement, " +
                                                       "b.business_type, convert(varchar,dbo.getJavaTimeStampAsDate(a.open_date), 105) valid_from_als,convert(varchar,dbo.getJavaTimeStampAsDate(a.closing_date), 105) valid_to_als,replace(replace(rtrim(ltrim(a.description)),'-',''),CHAR(13)+CHAR(10),'') description, replace(convert(varchar,convert(Money, a.cost_estimate),1),'.00','') as estimated_cost_amt  " +
                                                       "from p_project_tender a, p_business_type b, p_entity c " +
                                                       "where a.p_business_type_id = b.p_business_type_id " +
                                                       "and exists(select 1 from t_vendor_business_type_spec x, t_vendor_register_spec x1 " +
                                                             "where " +
                                                             "x.p_business_type_id = a.p_business_type_id " +
                                                             "and x.t_vendor_register_id = x1.t_vendor_register_id " +
                                                             "and x1.p_level_approval_type_id=1 " +
                                                             "and (a.cost_estimate <= x1.cost_limit_amt or a.cost_estimate <= x1.cost_limit_max_amt) " +
                                                             "and x.t_vendor_register_id = " + t_vendor_register_id.Text + " " +
                                                            ") " +
                                                       "and not exists( " +
                                                             "select 1 from t_vendor_tender y " +
                                                             "where " +
                                                             "y.p_project_tender_id = a.p_project_tender_id " +
                                                             "and y.t_vendor_register_id = " + t_vendor_register_id.Text + " " +
                                                      ") " +
                                                       "and a.p_entity_id="+ p_entity_id.Text + " " +
                                                       "and a.p_entity_id=c.p_entity_id and upper(ltrim(rtrim(a.project_name)))+UPPER(b.business_type) " +
                                                       "like '%'+isnull(upper(ltrim(rtrim('" + keyword.Text.Trim()+"'))),null)+'%'", 0,0);
            DataTable dtresult = dsresult.Tables[0];
            gvLOV.DataSource = dtresult;
            gvLOV.DataBind();

            if (dtresult.Rows.Count == 0)
            {

                lblNoRow.Visible = true;
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>PROJECT NAME</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>ENTITY</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Visible = false;
            }


        }

        protected void gvLOV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblPILIHVal = (Label)e.Row.FindControl("lblPILIH");
                String strLOV = "";

                String idLOV = (((DataRowView)e.Row.DataItem)["P_PROJECT_TENDER_ID"]).ToString();
                String idItem = (((DataRowView)e.Row.DataItem)["PROJECT_NAME"]).ToString();
                String estimatedCost = String.Format("{0:n0}", Convert.ToDecimal((((DataRowView)e.Row.DataItem)["ESTIMATED_COST_AMT"]).ToString()));
                String tenderNO = (((DataRowView)e.Row.DataItem)["TENDER_NO"]).ToString();
                String quantity = (((DataRowView)e.Row.DataItem)["QUANTITY"]).ToString();
                String measurement = (((DataRowView)e.Row.DataItem)["MEASUREMENT"]).ToString();
                String description = (((DataRowView)e.Row.DataItem)["DESCRIPTION"]).ToString();

                strLOV += String.Format(@"<button type='button'  class='btn btn-primary' onclick=""javascript:clickReturn('{0}#~#{1}#~#{2}#~#{3}#~#{4}#~#{5}#~#{6}')"">PILIH</button>", idLOV, idItem, estimatedCost, tenderNO, quantity, measurement, description.Replace("\n","").Replace("'",""));
                lblPILIHVal.Text = strLOV;

                Label lblFileNameVal = (Label)e.Row.FindControl("lblFileName");

                String strFileName = "";
                String FileName = "-";
                String FileMime = "-";

                FileName = (((DataRowView)e.Row.DataItem)["BOQ_FILE_NAME"]).ToString();
                FileMime = (((DataRowView)e.Row.DataItem)["FILE_TYPE"]).ToString();

                strFileName += String.Format(@"<div class='row'>");
                if (FileName!="")
                {
                    strFileName += String.Format(@"<div class='col-sm-2'><a href=""javascript:clickView('{0}','{1}')""><span class='glyphicon glyphicon-download'>FILE</span></a></div>", FileName, FileMime);
                }else
                {
                    strFileName += String.Format(@"<div class='col-sm-2'>{0}</div>", "NO FILE");
                }
                
                strFileName += String.Format(@"</div>");

                lblFileNameVal.Text = strFileName;

            }
        }
        protected void gvLOV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvLOV.PageIndex = e.NewPageIndex;
            gvLOV.DataBind();
            BindDataList();
        }
        protected void btnCari_Click(object sender, EventArgs e)
        {
            BindDataList();
        }

        protected void getOpenTender(string entityno)
        {
            try
            {
                //string json = "[{\"entityNo\":\"36500\",\"create Date\":1549440959270,\"serviceUnitName\":\"GENERAL SERVICES DEPARTMENT\",\"serviceUnitNo\":\"GSDLC001\",\"departmentName\":\"HLD LC GENERAL SERVICES DEPARTMENT\",\"divisionNo\":\"102015000000\",\"entityName\":\"LIPPO CIKARANG\",\"departmentNo\":\"102015200000\",\"measurement\":null,\"categoryName\":\"PEKERJAAN\",\"tenderNo\":\"OPT1902-000003\",\"divisionName\":\"HLD LC HUMAN RESOURCES & SERVICES DIVISION\",\"description\":\"test\",\"closingDate\":1551200400000,\"tenderName\":\"testt tender\",\"quantity\":null,\"costEstimate\":22333333.0000,\"categoryNo\":\"PR0009\",\"openDate\":1549299600000}]";
                string json = "";
                DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETOPENTENDER'", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                using (var client = new WebClient())
                {
                    try
                    {
                        string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + entityno + "&" + dtresult.Rows[0]["WHERE_START_URL"].ToString() + dtresult.Rows[0]["START_ROW"].ToString() + "&" + dtresult.Rows[0]["WHERE_END_URL"].ToString() + dtresult.Rows[0]["END_ROW"].ToString() + dtresult.Rows[0]["END_FIX_URL"].ToString();
                        System.IO.Stream data = client.OpenRead(baseurl);
                        System.IO.StreamReader reader = new System.IO.StreamReader(data);
                        json = reader.ReadToEnd();
                        data.Close();
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                List<ResponOpenTender> resOpenTender = JsonConvert.DeserializeObject<List<ResponOpenTender>>(json);

                if (resOpenTender.Count > 0)
                {
                    foreach (ResponOpenTender res in resOpenTender)
                    {
                        object outputObject;
                        string s_packname = "p_ins_project_tender";
                        string s_varvalue = "";
                        string boqfilename = "";
                        string contenttype = "";
                        string strQuantity;
                        if (res.quantity == null)
                        {
                            strQuantity = "0";
                        }
                        else
                        {
                            strQuantity = res.quantity.ToString();
                        }

                        boqfilename = getBOQTender(res.tenderNo);
                        contenttype = GetContentType(boqfilename);

                        s_varvalue += String.Format(@"{0}{1}", res.entityNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.createDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.serviceUnitName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.serviceUnitNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.departmentName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.divisionNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.entityName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.departmentNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.measurement, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.categoryName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.tenderNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.divisionName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.description, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.closingDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.tenderName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", strQuantity, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.costEstimate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.categoryNo, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.openDate, "~#");
                        s_varvalue += String.Format(@"{0}{1}", boqfilename, "~#");
                        s_varvalue += String.Format(@"{0}{1}", contenttype, "");

                        log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                        string s_outcodename = "";
                        string s_outmsgname = "O_RESULT_MSG";
                        int StartRecs = 0;
                        int EndRecs = 0;

                        outputObject = dbcVendorList.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                        System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                        String o_result_code = (String)(pi.GetValue(outputObject, null));

                        System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                        String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("getEntityData Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        protected string getBOQTender(string tenderno)
        {
            string json = "";
            string fileboq = "";
            DataSet dsresult = dbcVendorList.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETBOQ'", 0, 0);
            DataTable dtresult = dsresult.Tables[0];
            using (var client = new WebClient())
            {
                try
                {

                    string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + tenderno + dtresult.Rows[0]["END_FIX_URL"].ToString();
                    System.IO.Stream data = client.OpenRead(baseurl);
                    System.IO.StreamReader reader = new System.IO.StreamReader(data);
                    json = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            if (json != "[]")
            {
                List<ResBoqDoc> resBoqDoc = JsonConvert.DeserializeObject<List<ResBoqDoc>>(json);
                if (resBoqDoc.Count > 0)
                {
                    foreach (ResBoqDoc res in resBoqDoc)
                    {

                        Byte[] bytes = Convert.FromBase64String(res.documentfile);
                        string upDir = Path.Combine(Request.PhysicalApplicationPath, "Boq");
                        fileboq = Path.Combine(upDir, res.filename);

                        if (!File.Exists(upDir + "\\" + res.filename))
                        {
                            File.WriteAllBytes(upDir + "\\" + res.filename, bytes);
                        }
                    }
                }
            }
            return fileboq;
        }

        private string GetContentType(string fileName)
        {
            string strcontentType = "application/octetstream";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (registryKey != null && registryKey.GetValue("Content Type") != null)
                strcontentType = registryKey.GetValue("Content Type").ToString();
            return strcontentType;
        }
    }
}