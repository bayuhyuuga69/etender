﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WsLCTender;

namespace LCTender.Lov
{
    public partial class lov_vendor_register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataList();
            }
        }

        protected void BindDataList()
        {
            dbcReader clsReader = new dbcReader();
            DataSet dsresult = clsReader.ExecuteSelect("select t_vendor_register_id,register_no,vendor_name " +
                                                      "from v_lov_vendor_register where created_by='" + Session["UserName"].ToString()+"' and upper(ltrim(rtrim(vendor_name)))+UPPER(register_no) " +
                                                      "like '%'+isnull(upper(ltrim(rtrim('" + keyword.Text.Trim()+"'))),null)+'%'", 0,0);
            DataTable dtresult = dsresult.Tables[0];
            gvLOV.DataSource = dtresult;
            gvLOV.DataBind();

            if (dtresult.Rows.Count == 0)
            {

                lblNoRow.Visible = true;
                lblNoRow.Text = "" +
                 "<div class='Row'>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>#</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>REGISTER NO</div>" +
                   "<div class='col-sm-4' style='padding: 4px 2px;background-color:#0077bc;font-size: 0.9em;color: #fff;'>VENDOR NAME</div>" +
                 "</div>" +
                "<div class='Row'>" +
                   "<div class='col-sm-12' style='background-color: #fff; margin: 5px 0 10px 0; border: solid 1px #525252; border-collapse:collapse;padding: 2px; border: solid 1px #c1c1c1; color:#717171;'>DATA TIDAK DITEMUKAN</div>" +
                 "</div>";
            }
            else
            {
                lblNoRow.Visible = false;
            }


        }

        protected void gvLOV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblPILIHVal = (Label)e.Row.FindControl("lblPILIH");
                String strLOV = "";

                String idLOV = (((DataRowView)e.Row.DataItem)["T_VENDOR_REGISTER_ID"]).ToString();
                String idITEM = (((DataRowView)e.Row.DataItem)["REGISTER_NO"]).ToString();
                String idVENDOR = (((DataRowView)e.Row.DataItem)["VENDOR_NAME"]).ToString();

                strLOV += String.Format(@"<button type='button'  class='btn btn-primary' onclick=""javascript:clickReturn('{0}#~#{1}#~#{2}')"">PILIH</button>", idLOV, idITEM, idVENDOR);
                lblPILIHVal.Text = strLOV;

            }
        }
        protected void gvLOV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvLOV.PageIndex = e.NewPageIndex;
            gvLOV.DataBind();
            BindDataList();
        }
        protected void btnCari_Click(object sender, EventArgs e)
        {
            BindDataList();
        }
    }
}