﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LCTender.Models
{
    public class RecomVendor
    {
        public string project_name { get; set; }
        public string register_no { get; set; }
        public string vendor_name { get; set; }
        public string tender_no { get; set; }
        public string personalid { get; set; }
        public string personalno { get; set; }
        public string tender_date { get; set; }
        public string tender_date_epoch { get; set; }
        public string estimated_cost_amt { get; set; }
        public string offering_cost_amt { get; set; }
        public string file_penawaran { get; set; }
        public string file_type { get; set; }
        public string vendorNo { get; set; }
    }
}