﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LCTender.Models
{
    public class JoinTender
    {
        public int t_vendor_register_id { get; set; }
        public string personal_id { get; set; }
        public string personal_no { get; set; }
        public int p_project_tender_id { get; set; }
        public string tender_no { get; set; }
        public string vendor_name { get; set; }
        public int t_vendor_tender_id { get; set; }
        public string join_date { get; set; }
        public double join_date_epoch { get; set; }
        public string project_name { get; set; }
        public string request_status { get; set; }
	    public int amt_submitted { get; set; }
	    public string status { get; set; }
        public string service_unit_name { get; set; }
	    public string service_unit_no { get; set; }
	    public string entity_name { get; set; }
	    public string department_name { get; set; }
        public string department_no { get; set; }
	    public string division_no { get; set; }
        public string vendorNo { get; set; }
    }
}