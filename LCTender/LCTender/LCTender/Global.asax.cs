﻿using System;
using System.Web.UI;
using System.Web.Optimization;
using System.Web.Http;
using LCTender.App_Start;

namespace LCTender
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
        void Session_Start(object sender, EventArgs e)
        {
            Page Url = new Page();
            if (Session["OrganId"] == null || Session["UserName"] == null
                || Session["UserId"] == null || Session["Pass"] == null
                || Session["Status"] == null || Session["P_ENTITY_ID"] == null
                )
            {
               Response.Redirect(Url.ResolveClientUrl("~/index.aspx").ToString());
            }
            else
            {
               Response.Redirect(Url.ResolveClientUrl("~/Main/Home.aspx").ToString());
            }
        }
    }
 }