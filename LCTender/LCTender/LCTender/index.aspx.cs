﻿using System;
using WsLCTender;
using log4net;
using System.Data;
using LCTender.Class;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net;
using System.Linq;

namespace LCTender.Main
{
    public partial class index : System.Web.UI.Page
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(index));
        public string main_color_theme = "";
        public string footer_color_theme = "";
        dbcReader dbcInsVendReg = new dbcReader();
        class ResponEntity
        {
            public string entityNo { get; set; }
            public string entityName { get; set; }

        }
        class ResponCategory
        {
            public string itemName { get; set; }
            public string itemNo { get; set; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Request["P_ENTITY_ID"] != null)
            {
                entityControlID.StrEntityID = Request["P_ENTITY_ID"];
                loginControlID.StrEntityID = Request["P_ENTITY_ID"];
                imageControlID.StrEntityID = Request["P_ENTITY_ID"];
                projectControlID.StrEntityID = Request["P_ENTITY_ID"];
                Session["P_ENTITY_ID"] = Request["P_ENTITY_ID"];
                BindAnnouncement(Request["P_ENTITY_ID"]);
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Request["P_ENTITY_ID"]);
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divLogin.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
            }
            else
            {
                loginControlID.StrEntityID = "1";
                entityControlID.StrEntityID = "1";
                imageControlID.StrEntityID = "1";
                projectControlID.StrEntityID = "1";
                Session["P_ENTITY_ID"] = "1";
                BindAnnouncement("1");
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme("1");
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divLogin.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
            }

            if (!IsPostBack)
            {
                if (Request["P_ENTITY_ID"] != null)
                {
                    entityControlID.StrEntityID = Request["P_ENTITY_ID"];
                    loginControlID.StrEntityID = Request["P_ENTITY_ID"];
                    imageControlID.StrEntityID = Request["P_ENTITY_ID"];
                    projectControlID.StrEntityID = Request["P_ENTITY_ID"];
                    Session["P_ENTITY_ID"] = Request["P_ENTITY_ID"];
                    BindAnnouncement(Request["P_ENTITY_ID"]);
                    clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Request["P_ENTITY_ID"]);
                    main_color_theme = entityTheme.main_color_theme;
                    footer_color_theme = entityTheme.footer_color_theme;
                    divImage.Attributes.Add("style", "background-color:"+ main_color_theme + ";color:#fff");
                    divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divLogin.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
                    getEntityData();
                    getCategoryData();
                }
                else
                {
                    loginControlID.StrEntityID = "1";
                    entityControlID.StrEntityID = "1";
                    imageControlID.StrEntityID = "1";
                    projectControlID.StrEntityID = "1";
                    Session["P_ENTITY_ID"] = "1";
                    BindAnnouncement("1");
                    clsEntityTheme entityTheme = clsEntityTheme.GetTheme("1");
                    main_color_theme = entityTheme.main_color_theme;
                    footer_color_theme = entityTheme.footer_color_theme;
                    divImage.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divAnnouncement.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divLogin.Attributes.Add("style", "background-color:" + main_color_theme + ";color:#fff");
                    divEntity.Attributes.Add("style", "background-color:" + footer_color_theme + ";");
                    getEntityData();
                    getCategoryData();
                }

            }
        }
        protected void BindAnnouncement(string strEntityID)
        {
            if (strEntityID == "0")
            {
                strEntityID = "1";
            }
            log.Info("Bind Data Entity Started. ");
            try
            {
                
                DataSet dsresult = dbcInsVendReg.ExecuteSelect("Select * from dbo.P_ANNOUNCEMENT where p_entity_id=" + strEntityID, 0, 0);
                DataTable dtresult = dsresult.Tables[0];

                ltTtlAnnouncement.Text = "";
                ltAnnouncement.Text = "";

                if (dtresult.Rows.Count > 0)
                {

                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        ltTtlAnnouncement.Text=String.Format(@"<h1>{0}</h1>", dtresult.Rows[i]["title"].ToString());
                        ltAnnouncement.Text = String.Format(@"{0}", dtresult.Rows[i]["content"].ToString());
                        ltImgAnnouncement.Text = String.Format(@"<img class='img-responsive' src='images/{0}'>", dtresult.Rows[i]["file_desc"].ToString());
                    }
                }
                
            }
            catch (Exception ex)
            {
                log.ErrorFormat("BindAnnouncement Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        protected void getEntityData()
        {
            try
            {
                //string json = "[{\"entityNo\":\"0\",\"entityName\":\"TES\"},{\"entityNo\":\"00100\",\"entityName\":\"PT. LIPPO KARAWACI TBK\"},{\"entityNo\":\"01400\",\"entityName\":\"PT. TATA MANDIRI DAERAH VILLA PERMATA\"},{\"entityNo\":\"02500\",\"entityName\":\"PT. SENTRA ASRITAMA REALTY DEVELOPMENT\"},{\"entityNo\":\"02501\",\"entityName\":\"PT. SENTRA ASRITAMA REALTY DEVELOPMENT\"},{\"entityNo\":\"02700\",\"entityName\":\"PT. TATA MANDIRI DAERAH LIPPO KARAWACI\"},{\"entityNo\":\"02701\",\"entityName\":\"PT. TATA MANDIRI DAERAH LIPPO KARAWACI\"},{\"entityNo\":\"02702\",\"entityName\":\"PT. TATA MANDIRI DAERAH LIPPO KARAWACI\"},{\"entityNo\":\"12900\",\"entityName\":\"PT. KEMANG VILLAGE MANAGEMENT\"},{\"entityNo\":\"21400\",\"entityName\":\"PT CHANDRA MULIA ADHIDARMA\r\n\"},{\"entityNo\":\"21500\",\"entityName\":\"PT. TUNAS PUNDI BUMI\"},{\"entityNo\":\"22700\",\"entityName\":\"PT. MANUNGGAL UTAMA MAKMUR\"},{\"entityNo\":\"23400\",\"entityName\":\"PT.SINAR SURYA TIMUR\"},{\"entityNo\":\"24700\",\"entityName\":\"PT. ST.MORITZ MANAGEMENT\"},{\"entityNo\":\"35300\",\"entityName\":\"PT. CAHAYA INA PERMAI\"},{\"entityNo\":\"35400\",\"entityName\":\"PT. MAHKOTA SENTOSA EKANUSA\"},{\"entityNo\":\"35800\",\"entityName\":\"PT. TMD DEPOK MANAJEMEN\"},{\"entityNo\":\"36500\",\"entityName\":\"PT. LIPPO CIKARANG, TBK\"},{\"entityNo\":\"36501\",\"entityName\":\"PT. LIPPO CIKARANG - ORANGE COUNTY\"},{\"entityNo\":\"36700\",\"entityName\":\"PT. ERA BARU REALTINDO\"},{\"entityNo\":\"36800\",\"entityName\":\"PT ZEUS KARYA PRIMA\r\n\"},{\"entityNo\":\"36900\",\"entityName\":\"PT. ASTANA ARTHA MAS\"},{\"entityNo\":\"37000\",\"entityName\":\"PT. MEGA KREASI TEKNIKA\"},{\"entityNo\":\"37100\",\"entityName\":\"PT. MENARA INTI DEVELOPMENT\"},{\"entityNo\":\"37200\",\"entityName\":\"PT. WASKA SENTANA\"},{\"entityNo\":\"37300\",\"entityName\":\"PT. DUNIA AIR INDAH\"},{\"entityNo\":\"37400\",\"entityName\":\"PT. BEKASI MEGA POWER\"},{\"entityNo\":\"37600\",\"entityName\":\"PT. GREAT JAKARTA INTI DEVELOPMENT\"},{\"entityNo\":\"37700\",\"entityName\":\"PT. TIRTASARI NIRMALA\"},{\"entityNo\":\"37800\",\"entityName\":\"PT. KREASI DUNIA KELUARGA\"},{\"entityNo\":\"37900\",\"entityName\":\"PT. DIAN CITY MARGA\"},{\"entityNo\":\"38900\",\"entityName\":\"PT PONDERA PRIMA SARANA\r\n\"},{\"entityNo\":\"39000\",\"entityName\":\"PT. TELAGA BANYU MURNI\"},{\"entityNo\":\"41900\",\"entityName\":\"PT. KARIMATA ALAM DAMAI\"},{\"entityNo\":\"43900\",\"entityName\":\"PT. MEGA KREASI CIKARANG DAMAI\"},{\"entityNo\":\"44000\",\"entityName\":\"PT. MEGA KREASI PERMAI\"},{\"entityNo\":\"44400\",\"entityName\":\"PT. MEGA KREASI PROPERTINDO UTAMA\"},{\"entityNo\":\"44500\",\"entityName\":\"PT. MEGA KREASI CIKARANG ASRI\"},{\"entityNo\":\"45200\",\"entityName\":\"PT. MEGAKREASI NUSANTARA TEKNOLOGI\"},{\"entityNo\":\"47800\",\"entityName\":\"PT. MEGA KREASI CIKARANG REALTINDO\"},{\"entityNo\":\"49900\",\"entityName\":\"PT. LIPPO DIAMOND DEVELOPMENT\"},{\"entityNo\":\"50000\",\"entityName\":\"LIPPO GROUP & PT.MITSUBISHI CORPORATION\"},{\"entityNo\":\"50500\",\"entityName\":\"PT. MAHKOTA SENTOSA UTAMA\"},{\"entityNo\":\"52300\",\"entityName\":\"PT.MEGA PRIMA KREASI\"},{\"entityNo\":\"C0400\",\"entityName\":\"PT SWADAYA TEKNOPOLIS\r\n\"},{\"entityNo\":\"E0700\",\"entityName\":\"PT. BUMI LEMAHABANG PERMAI\"},{\"entityNo\":\"JIC\",\"entityName\":\"JAPANESE INFORMATION CENTER\"},{\"entityNo\":\"LK\",\"entityName\":\"LK TESTING\"},{\"entityNo\":\"M0760\",\"entityName\":\"LC & PT.MANDIRI GROUP\"},{\"entityNo\":\"M0930\",\"entityName\":\"PT. INDO DHAMMA REZEKI\"},{\"entityNo\":\"O0093\",\"entityName\":\"PT. BALADHIKA KARYA RAHARJA\"},{\"entityNo\":\"O0099\",\"entityName\":\"PT.KENCANA KEMILAU BINTANG\"},{\"entityNo\":\"O0100\",\"entityName\":\"PT. MEGA PROFITA ABADI\"},{\"entityNo\":\"O0120\",\"entityName\":\"PT.PANCA SURYA ENERGI\"},{\"entityNo\":\"O0130\",\"entityName\":\"PT.TRIMULIA UTAMA SUKSES\"},{\"entityNo\":\"O0140\",\"entityName\":\"PT.ADYAKASA ADIL SEJAHTERA\"},{\"entityNo\":\"O0141\",\"entityName\":\"PT.PUSPITA MANDIRA LOKA\"},{\"entityNo\":\"O0142\",\"entityName\":\"PT.MITRA KHARISMA LUHUR\"},{\"entityNo\":\"O0160\",\"entityName\":\"PT.SINAR SAFIRA SEMESTA\"},{\"entityNo\":\"O0170\",\"entityName\":\"PT.RAJATA ANDAKARSA MULIA\"},{\"entityNo\":\"O0180\",\"entityName\":\"PT.BANGUN SINERGI KATULISTIWA\"},{\"entityNo\":\"O0190\",\"entityName\":\"PT.CAKRAWALA BINTANG ABADI \"},{\"entityNo\":\"O0200\",\"entityName\":\"PT.ALPHA SENTRA PRIMA\"},{\"entityNo\":\"O0201\",\"entityName\":\"PT.MEGAPILAR PERKASA MANDIRI \"},{\"entityNo\":\"O0202\",\"entityName\":\"PT.MEGAH BERKATINDO NUSANTARA\"},{\"entityNo\":\"O0203\",\"entityName\":\"PT. LESTARI PRIMA PRAKARSA\"},{\"entityNo\":\"O0204\",\"entityName\":\"PT.CAHAYA KURNIA PRAKARSA\"},{\"entityNo\":\"O0205\",\"entityName\":\"PT. PRIMANDA SEJAHTERA ABADI\"},{\"entityNo\":\"O0206\",\"entityName\":\"PT. AGRA SURYA UNGGUL\"},{\"entityNo\":\"O0207\",\"entityName\":\"PT.BANGUN CIPTA DARMA\"},{\"entityNo\":\"O0208\",\"entityName\":\"PT.MEGAH OMEGA ABADI\"},{\"entityNo\":\"O0209\",\"entityName\":\"PT.PELANGI INDO SAKTI\"},{\"entityNo\":\"O0210\",\"entityName\":\"PT.INTAN TANAH GEMILANG\"},{\"entityNo\":\"O0211\",\"entityName\":\"PT. BUKIT MARGA PRAKARSA\"},{\"entityNo\":\"O0212\",\"entityName\":\"PT. BUMI CATUR JAYA\"},{\"entityNo\":\"O0213\",\"entityName\":\"PT. KURNIA SEMESTA PRIMA\"},{\"entityNo\":\"OVO\",\"entityName\":\"PT.OVO VISIONET INTERNATIONAL\"},{\"entityNo\":\"TBM\",\"entityName\":\"PT. TANJUNG BUNGA MAKASSAR\"}]";
                string json = "";
                DataSet dsresult = dbcInsVendReg.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETENTITY'", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                using (var client = new WebClient())
                {
                    try
                    {
                        string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_START_URL"].ToString() + dtresult.Rows[0]["START_ROW"].ToString() + "&" + dtresult.Rows[0]["WHERE_END_URL"].ToString() + dtresult.Rows[0]["END_ROW"].ToString() + dtresult.Rows[0]["END_FIX_URL"].ToString();
                        System.IO.Stream data = client.OpenRead(baseurl);
                        System.IO.StreamReader reader = new System.IO.StreamReader(data);
                        json = reader.ReadToEnd();
                        data.Close();
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                List<ResponEntity> resEntity = JsonConvert.DeserializeObject<List<ResponEntity>>(json);

                if (resEntity.Count > 0)
                {
                    string retValid = dbcInsVendReg.ExecuteReader("declare @str varchar(max); " +
                                                                  "exec p_upd_invalid_entity @str OUTPUT " +
                                                                  "select @str ret", "ret");
                    var result = (from e in resEntity
                                  //where e.entityNo == "00100"
                                  //      || e.entityNo == "36500"
                                  select new { entityName = e.entityName, entityNo = e.entityNo }).Distinct().ToList();
                    foreach (var res in result)
                    {
                       
                        object outputObject;
                        string s_packname = "p_ins_entity";
                        string s_varvalue = "";
                        s_varvalue += String.Format(@"{0}{1}", res.entityName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.entityNo, "");
                      
                        log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                        string s_outcodename = "";
                        string s_outmsgname = "O_RESULT_MSG";
                        int StartRecs = 0;
                        int EndRecs = 0;

                        outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                        System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                        String o_result_code = (String)(pi.GetValue(outputObject, null));

                        System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                        String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("getEntityData Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        protected void getCategoryData()
        {
            try
            {
                //string json = "[{\"itemName\": \"BARANG ATK\",\"itemNo\": \"PRO001\"},{\"itemName\": \"COMPUTER / PERANGKAT IT\",\"itemNo\": \"PRO003\"},{\"itemName\": \"BAHAN BANGUNAN\",\"itemNo\": \"PRO007\"},{\"itemName\": \"PEKERJAAN\",\"itemNo\": \"PR0009\"}]";
                string json = "";
                DataSet dsresult = dbcInsVendReg.ExecuteSelect("Select * from dbo.P_CINERGY_URL where URL_CODE='GETMASTERCATEGORY'", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                using (var client = new WebClient())
                {
                    try
                    {
                        string baseurl = dtresult.Rows[0]["CINERGY_URL"].ToString() + dtresult.Rows[0]["WHERE_URL"].ToString() + dtresult.Rows[0]["END_FIX_URL"].ToString();
                        System.IO.Stream data = client.OpenRead(baseurl);
                        System.IO.StreamReader reader = new System.IO.StreamReader(data);
                        json = reader.ReadToEnd();
                        data.Close();
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                List<ResponCategory> resCategory = JsonConvert.DeserializeObject<List<ResponCategory>>(json);

                if (resCategory.Count > 0)
                {
                    foreach (ResponCategory res in resCategory)
                    {
                       
                        object outputObject;
                        string s_packname = "p_ins_biz_type";
                        string s_varvalue = "";
                        s_varvalue += String.Format(@"{0}{1}", res.itemName, "~#");
                        s_varvalue += String.Format(@"{0}{1}", res.itemNo, "");

                        log.DebugFormat("s_varvalue.-DEBUG: packname: {0}>> s_varvalue: {1}", s_packname, s_varvalue);

                        string s_outcodename = "";
                        string s_outmsgname = "O_RESULT_MSG";
                        int StartRecs = 0;
                        int EndRecs = 0;

                        outputObject = dbcInsVendReg.pack_cmd_exec(s_packname, s_varvalue, s_outcodename, s_outmsgname, StartRecs, EndRecs);

                        System.Reflection.PropertyInfo pi = outputObject.GetType().GetProperty("o_result_code");
                        String o_result_code = (String)(pi.GetValue(outputObject, null));

                        System.Reflection.PropertyInfo pi_msg = outputObject.GetType().GetProperty("o_result_msg");
                        String o_result_msg = (String)(pi_msg.GetValue(outputObject, null));
                    }
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("getCategoryData Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }


    }
}