﻿using System;
using WsLCTender;
using log4net;
using System.Data;

namespace LCTender
{
    public partial class LC : System.Web.UI.MasterPage
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(LC));
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                writeMenu();
            }
         

        }
        private string loadEntity()
        {
            log.Info("load Entity. ");
            try
            {
                dbcReader dbcEntity = new dbcReader();
                DataSet dsresult = dbcEntity.ExecuteSelect("SELECT P_ENTITY_ID,P_ORGANIZATION_ID,ENTITY_CODE,ENTITY_NAME,ENTITY_ADDR FROM P_ENTITY", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                string strEntity = "";
                if (dtresult.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        if (dtresult.Rows[i]["p_entity_id"].ToString() == Session["P_ENTITY_ID"].ToString())
                        {
                            strEntity = "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">" + dtresult.Rows[i]["entity_name"].ToString() + "&nbsp;<span class=\"caret\"></span></a>";
                        }
                    }
                }

                return strEntity;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("load Entity Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        private string loadMenu()
        {

            dbcReader dbcLog = new dbcReader();
            string o_result_msg = "";
            DataSet dsresult = dbcLog.db_get_user_start_app(Convert.ToInt32(Session["UserId"]), ref o_result_msg);
            DataTable dtresult = dsresult.Tables[0];

            String strMenu="";
            strMenu += String.Format(@"<li class='active'><a href='../Main/Home.aspx'><span class='fa-stack fa-lg pull-left'><i class='fa fa-home fa-stack-1x'></i></span>HOME</a></li>");
            if (dtresult.Rows.Count>0)
            {                
                int i;
                for (i=0; i<=dtresult.Rows.Count-1;i++)
                {
                    string strGlyphicon = "fa-th-list";
                    if (dtresult.Rows[i]["application_code"].ToString().Contains("MONITORING")){
                        strGlyphicon = "fa-flag";
                    }
                    
                    strMenu += String.Format(@"<li>");
                    strMenu += String.Format(@"<a href='#'><span class='fa-stack fa-lg pull-left'><i class='fa "+ strGlyphicon + " fa-stack-1x'></i></span> {0}</a>", dtresult.Rows[i]["application_code"].ToString());
                    strMenu += String.Format(@"<ul class='nav-pills nav-stacked' style='list-style-type:none;'");
                    DataSet dsrsmenu = dbcLog.db_get_user_menu(Session["UserName"].ToString(), Convert.ToInt32(dtresult.Rows[i]["p_application_id"].ToString()));
                    DataTable dtrsmenu = dsrsmenu.Tables[0];
                    if (dtrsmenu.Rows.Count > 0) {
                        int k;
                        for (k=0;k<=dtrsmenu.Rows.Count-1;k++)
                        {
                            strMenu += String.Format(@"<li><a href='{1}'>{0}</a></li>", dtrsmenu.Rows[k]["menu"].ToString(), dtrsmenu.Rows[k]["path_file_name"].ToString());
                        }
                    }
                    strMenu += String.Format(@"</ul></li>");
                    //strMenu += String.Format(@"<li><a>{0}</a></li>", dtresult.Rows[i]["application_code"].ToString());
                    //strMenu += String.Format(@"<li id='home'\><a href=javascript:clickShow('/Main/Home.aspx#','#','home')>Home</a></li>",);
                    //strMenu += String.Format(@"<li id='register'><a href=javascript:clickShow('/Transaction/t_vendor_register.aspx#','#','register')>Vendor Register</a></li>");
                }

            }
           

            return strMenu;
        }
        private string loadNews(string strEntityId)
        {
            log.Info("load news. ");
            try
            {
                if (strEntityId=="0")
                {
                    strEntityId = "1";
                }
                dbcReader dbcEntity = new dbcReader();
                DataSet dsresult = dbcEntity.ExecuteSelect("select * from P_NEWS where p_entity_id=" + strEntityId, 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                String strNews = "";

                if (dtresult.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                        strNews += String.Format(@"<li class='li-border-right'><a href='{0}'>{1}</a></li>", dtresult.Rows[i]["file_name"].ToString(), dtresult.Rows[i]["title"].ToString());
                    }

                }

                return strNews;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("load news Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }
        private void writeMenu()
        {
            ltMenu.Text = loadMenu();
            ltUser.Text = loadUser();
            ltNews.Text = loadNews(Session["P_ENTITY_ID"].ToString());
            ltCaret.Text = loadEntity();

        }
        private string loadUser()
        {
            string strUser="";
            dbcReader dbcUser = new dbcReader();
            if (Session["UserId"]!=null)
            {
                strUser = dbcUser.ExecuteReader("select full_name from p_app_user where p_app_user_id=" + Session["UserId"].ToString(), "full_name");
            }           
            return strUser;
        }
    }
}