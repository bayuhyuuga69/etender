﻿using System;
using log4net;
using WsLCTender;
using System.Data;
using LCTender.Class;
using System.Web.UI;

namespace LCTender
{
    public partial class LCindex : System.Web.UI.MasterPage
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(LCindex));
        public string main_color_theme = "";
        public string footer_color_theme = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
            {
                writeEntity();                
            }
           
        }

        private string loadEntity()
        {
            log.Info("load Entity. ");
            try
            {
                dbcReader dbcEntity = new dbcReader();
                DataSet dsresult = dbcEntity.ExecuteSelect("SELECT P_ENTITY_ID,P_ORGANIZATION_ID,ENTITY_CODE,ENTITY_NAME,ENTITY_ADDR FROM P_ENTITY WHERE IS_VALID='Y'", 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                String strEntity = "";

                if (dtresult.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                      
                        strEntity += String.Format(@"<li><a href='../index.aspx?P_ENTITY_ID={1}'>{0}</a></li>", dtresult.Rows[i]["entity_name"].ToString(), dtresult.Rows[i]["p_entity_id"].ToString());
                        if (Request["P_ENTITY_ID"] != null)
                        {
                          if (dtresult.Rows[i]["p_entity_id"].ToString() == Request["P_ENTITY_ID"]) {
                            ltCaret.Text = "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+ dtresult.Rows[i]["entity_name"].ToString() + "&nbsp;<span class=\"caret\"></span></a>";
                          }
                        }else
                        {
                            ltCaret.Text = "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">ENTITY&nbsp;<span class=\"caret\"></span></a>";
                        }
                    }

                }

                return strEntity;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("load Entity Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }

        private string loadNews(string strEntityId)
        {
            log.Info("load news. ");
            try
            {
                dbcReader dbcEntity = new dbcReader();
                DataSet dsresult = dbcEntity.ExecuteSelect("select * from P_NEWS where p_entity_id="+ strEntityId, 0, 0);
                DataTable dtresult = dsresult.Tables[0];
                String strNews = "";

                if (dtresult.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtresult.Rows.Count - 1; i++)
                    {
                     
                        strNews += String.Format(@"<li class='li-border-right'><a href='{0}'>{1}</a></li>", dtresult.Rows[i]["file_name"].ToString(), dtresult.Rows[i]["title"].ToString());
                    }

                   
               }
               return strNews;
            }
            catch (Exception ex)
            {
                log.ErrorFormat("load news Error - Message: {0}", ex.Message);
                throw;
            }
            finally
            {
                Console.WriteLine("Control flow reaches finally");
            }
        }
        private void writeEntity()
        {
            ltEntity.Text = loadEntity();
            if (Request["P_ENTITY_ID"] != null)
            {
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme(Request["P_ENTITY_ID"]);
                ltNews.Text = loadNews(Request["P_ENTITY_ID"]);
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                aBrand.Attributes.Add("style", "color:" + main_color_theme + ";font-size:30px");
            }
            else
            {
                clsEntityTheme entityTheme = clsEntityTheme.GetTheme("1");
                ltNews.Text = loadNews("1");
                main_color_theme = entityTheme.main_color_theme;
                footer_color_theme = entityTheme.footer_color_theme;
                aBrand.Attributes.Add("style", "color:" + main_color_theme + ";font-size:30px");
            }
           
        }
    }
}