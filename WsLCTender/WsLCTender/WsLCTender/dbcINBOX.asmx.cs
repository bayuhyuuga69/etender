﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using WsLCTender.Class;

namespace WsLCTender
{
    /// <summary>
    /// Summary description for dbcINBOX
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class dbcINBOX : System.Web.Services.WebService
    {

        [WebMethod]
        public DataSet GetProfileTask()
        {
            var result = clsWFInbox.GetProfile();
            return result;
        }

        [WebMethod]
        public DataSet GetWorkflowName(string puser)
        {
            var result = clsWFInbox.GetWorkflowName(puser);
            return result;
        }

        [WebMethod]
        public DataSet GetWorkflowSummaryList(float pdoc_type_id, string puser)
        {
            var result = clsWFInbox.GetWorkflowSummaryList(pdoc_type_id,puser);
            return result;
        }


        [WebMethod]
        public DataSet GetUserTaskList(float pdoc_type_id, float pproc_id, string pprof_type, string puser, string keyword)
        {
            var result = clsWFInbox.GetUserTaskList(pdoc_type_id,pproc_id,pprof_type,puser,keyword);
            return result;
        }

        [WebMethod]
        public DataSet GetTaskProfileList(string puser)
        {
            var result = clsWFInbox.GetTaskProfileList(puser);
            return result;
        }
    }
}
