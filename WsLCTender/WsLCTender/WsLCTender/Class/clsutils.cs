﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WsLCTender.Class
{
    public class clsutils
    {
        public int get_first_row(int intPage, int intPerPage)
        {
            return (intPerPage * intPage) - (intPerPage - 1);
        }

        public int get_max_page(int intPerPage, int intMaxRow)
        {
            int intMod, intMaxPage;
            if (intMaxRow == 0)
                intMaxPage = 1;
            else
            {
                intMod = intMaxRow % intPerPage;
                if (intMod == 0)
                    intMaxPage = intMaxRow / intPerPage;
                else
                    intMaxPage = (intMaxRow / intPerPage) + 1;
            }

            return intMaxPage;
        }

    }
}