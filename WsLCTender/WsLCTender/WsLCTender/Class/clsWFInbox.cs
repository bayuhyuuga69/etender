﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using log4net;
using log4net.Config;
using log4net.Core;
using log4net.Repository.Hierarchy;
using log4net.Appender;

namespace WsLCTender.Class
{
    public class clsWFInbox
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(clsWFInbox));
        public static DataSet GetProfile()
        {
            log.Info("GetProfile() started");

            string conString = ConfigurationManager.ConnectionStrings["LCTenderConn"].ToString();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(conString))
            {
                try
                {
                    
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"SELECT *
                                            FROM [SIS].[dbo].[v_task_profile_list] ";

                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(ds);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                  
                }
                finally
                {
                    conn.Close();
                }
            }

           
            return ds;
        }

        public static DataSet GetWorkflowName(string puser)
        {
            string conString = ConfigurationManager.ConnectionStrings["LCTenderConn"].ToString();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(conString))
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"select * from sis.dbo.f_workflow_name(@puser) ";

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("puser", puser));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(ds);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    conn.Close();
                }
            }


            return ds;
        }

        public static DataSet GetWorkflowSummaryList(float pdoc_type_id, string puser)
        {
            string conString = ConfigurationManager.ConnectionStrings["LCTenderConn"].ToString();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(conString))
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"select * from sis.dbo.f_workflow_summary_list(@pdoc_type_id,@puser) ";

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("pdoc_type_id", pdoc_type_id));
                    cmd.Parameters.Add(new SqlParameter("puser", puser));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(ds);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    conn.Close();
                }
            }


            return ds;
        }

        public static DataSet GetUserTaskList(float pdoc_type_id,float pproc_id,string pprof_type,string puser,string keyword)
        {
            string conString = ConfigurationManager.ConnectionStrings["LCTenderConn"].ToString();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(conString))
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"select * from sis.dbo.f_user_task_list(@pdoc_type_id,@pproc_id,@pprof_type,@puser,@keyword) ";

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("pdoc_type_id", pdoc_type_id));
                    cmd.Parameters.Add(new SqlParameter("pproc_id", pproc_id));
                    cmd.Parameters.Add(new SqlParameter("pprof_type", pprof_type));
                    cmd.Parameters.Add(new SqlParameter("puser", puser));
                    cmd.Parameters.Add(new SqlParameter("keyword", keyword));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(ds);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    conn.Close();
                }
            }


            return ds;
        }

        public static DataSet GetTaskProfileList(string puser)
        {
            string conString = ConfigurationManager.ConnectionStrings["LCTenderConn"].ToString();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(conString))
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"select * from sis.dbo.f_task_profile_list(@puser) ";

                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("puser", puser));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(ds);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    conn.Close();
                }
            }


            return ds;
        }


    }
}