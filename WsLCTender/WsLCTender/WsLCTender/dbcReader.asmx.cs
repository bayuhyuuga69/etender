﻿using System;
using System.Web.Services;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using Microsoft.VisualBasic;
using log4net;
using WsLCTender.Class;

namespace WsLCTender
{
    /// <summary>
    /// Summary description for dbcReader
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class dbcReader : System.Web.Services.WebService
    {
        
       public struct sOutput_ds
        {
            public string o_result_code;
            public string o_result_msg;
            public DataSet o_rc_data;
        }
        protected static readonly ILog log = LogManager.GetLogger(typeof(dbcReader));
        private string ConnLC = ConfigurationManager.AppSettings["LCOLEConn"].ToString();
        private clsutils clsutl = new clsutils();

        [WebMethod()]
        public sOutput_ds pack_cmd_exec(string s_packname, string s_varvalue, string s_outcodename, string s_outmsgname, int StartRecs, int EndRecs)
        {
            // '-------------------------------------------------

            log.Info(s_packname + " Started. ");
            log.DebugFormat(s_packname +">>[s_varvalue] : {0} ", s_varvalue);
            OleDbCommand mCommand;

            OleDbDataAdapter mAdapter;
            DataSet ds = null/* TODO Change to default(_) if this is not a reference type */;

            string o_result_code = "-1";
            string o_result_msg = "-";
            string[] s_array;
            OleDbParameter[] p = new OleDbParameter[101];
            OleDbParameter[] o = new OleDbParameter[101];
            int i;

            using (OleDbConnection mConnection = new OleDbConnection(ConnLC)) {
                try
                {

                    mCommand = new OleDbCommand(s_packname, mConnection);
                    mCommand.CommandType = CommandType.StoredProcedure;
                    if (mConnection.State != ConnectionState.Open)
                        mConnection.Open();

                    int aneh = 0;
                    string Qry = "";
                    OleDbDataReader mReader;
                    OleDbCommand mCommandok;

                    s_array = Strings.Split(s_varvalue, "~#");
                    for (i = 0; i <= s_array.Length - 1; i++)
                    {
                        // '--- handle convert format dat DD-MON-YYYY di database 
                        if (Strings.Len(s_array[i]) >= 7 & Strings.Len(s_array[i].ToUpper()) <= 11 & (Strings.InStr(s_array[i].ToUpper(), "-JAN-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-PEB-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-FEB-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-MAR-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-APR-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-MAY-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-MEI-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-JUN-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-JUL-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-AUG-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-AGU-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-SEP-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-OCT-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-OKT-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-NOV-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-NOP-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-DEC-") != 0 | Strings.InStr(s_array[i].ToUpper(), "-DES-") != 0))
                        {
                            Qry = "select count(fnc.ok) ok from (select [dbo].[f_isok_date]('" + Strings.Trim(s_array[i].ToUpper()) + "') ok) fnc where fnc.ok = 1 ";
                            mCommandok = new OleDbCommand(Qry, mConnection);
                            mReader = mCommandok.ExecuteReader();
                            if (mReader.Read())
                                aneh = mReader.GetInt32(0);//System.Convert.ToInt32(mReader.Item("ok"));
                            else
                                aneh = 0;
                            mReader.Dispose();
                            mCommandok.Dispose();
                        }
                        else
                            aneh = 0;

                        if (aneh == 1)
                        {
                            p[i] = new OleDbParameter("p" + i, OleDbType.Date);
                            p[i].Value = s_array[i];
                            mCommand.Parameters.Add(p[i]);
                        }
                        else if (Strings.InStr(s_array[i].ToUpper(), "OUT_O_") == 0)
                        {
                            p[i] = new OleDbParameter("p" + i, OleDbType.VarChar, 3000);
                            p[i].Value = s_array[i];
                            mCommand.Parameters.Add(p[i]);
                        }
                        else
                        {
                            o[0] = new OleDbParameter("O_" + Strings.Replace(s_array[i], "OUT_O_", ""), OleDbType.VarChar, 3000, "");
                            mCommand.Parameters.Add(o[0]);
                            mCommand.Parameters["O_" + Strings.Replace(s_array[i], "OUT_O_", "")].Direction = ParameterDirection.Output;
                        }
                    }

                    // '------ Output Message ----------  
                    if (s_outcodename != "")
                    {
                        OleDbParameter o1 = new OleDbParameter(s_outcodename, OleDbType.VarChar, 3000, "");
                        mCommand.Parameters.Add(o1);
                        mCommand.Parameters[s_outcodename].Direction = ParameterDirection.Output;
                    }
                    if (s_outmsgname != "")
                    {
                        OleDbParameter o2 = new OleDbParameter(s_outmsgname, OleDbType.VarChar, 3000, "");
                        mCommand.Parameters.Add(o2);
                        mCommand.Parameters[s_outmsgname].Direction = ParameterDirection.Output;
                    }

                    // '-------------------------------- 
                    mCommand.ExecuteNonQuery();

                    if (s_outcodename != "")
                        o_result_code = mCommand.Parameters[s_outcodename].Value.ToString();
                    if (s_outmsgname != "")
                    {
                        if (Convert.IsDBNull(mCommand.Parameters[s_outmsgname].Value))
                            o_result_msg = "SUKSES";
                        else
                            o_result_msg = System.Convert.ToString(mCommand.Parameters[s_outmsgname].Value);
                    }
                    if (o_result_msg != "" & Strings.InStr(o_result_msg.ToUpper(), "ERROR") == 0)
                        o_result_code = "1";

                    if (EndRecs > 0)
                    {
                        ds = new DataSet();
                        mAdapter = new OleDbDataAdapter(mCommand);
                        if (StartRecs > 0)
                            mAdapter.Fill(ds, StartRecs, EndRecs, "Procedure");
                        else
                            mAdapter.Fill(ds, "Procedure");
                        mAdapter.Dispose();
                    }
                    else if (StartRecs > 0 & EndRecs == 0)
                    {
                        ds = new DataSet();
                        mAdapter = new OleDbDataAdapter(mCommand);
                        mAdapter.Fill(ds, StartRecs, EndRecs + 1000000, "Procedure");
                        mAdapter.Dispose();
                    }
                    else
                        ds = null/* TODO Change to default(_) if this is not a reference type */;

                    if (o_result_msg == "")
                        o_result_msg = "SUKSES";
                    // '-------- Output Object ----------------
                    sOutput_ds ReturnOut = new sOutput_ds();
                    ReturnOut.o_result_code = o_result_code;
                    ReturnOut.o_result_msg = o_result_msg;
                    ReturnOut.o_rc_data = ds;

                    mConnection.Close();
                    mCommand.Dispose();
                    return ReturnOut;
                }
                catch (Exception ex)
                {
                    log.ErrorFormat(s_packname +" Error - Message: {0}", ex.Message);
                    sOutput_ds ReturnOut = new sOutput_ds();
                    ReturnOut.o_result_code = "0";
                    ReturnOut.o_result_msg = ex.Message;
                    throw;
                }
                finally
                {
                    mConnection.Close();
                }
            }
        }

        [WebMethod()]
        public DataSet ExecuteSelect(string Qry, int StartRecs, int EndRecs)
        {
            log.Info(Qry + " Started. ");
            log.DebugFormat("[Query] : {0} ", Qry);
            OleDbCommand mCommand;
            OleDbDataAdapter mAdapter;
            DataSet ds;

            using (OleDbConnection mConnection = new OleDbConnection(ConnLC)) {
                try {
                    mCommand = new OleDbCommand(Qry, mConnection);
                    if (mConnection.State != ConnectionState.Open)
                        mConnection.Open();

                    mAdapter = new OleDbDataAdapter();
                    ds = new DataSet("DS");
                    mCommand.CommandType = CommandType.Text;
                    mAdapter.SelectCommand = mCommand;
                    if (StartRecs > 0)
                        mAdapter.Fill(ds, StartRecs, EndRecs, "Table");
                    else
                        mAdapter.Fill(ds, "Table");
                    mAdapter.Dispose();
                    ds.Dispose();
                    mConnection.Close();
                    mCommand.Dispose();

                    return ds;
                 }
                catch (Exception ex)
                {
                    log.ErrorFormat(Qry + " Error - Message: {0}", ex.Message);
                    throw;
                }
                finally
                {
                    mConnection.Close();
                }
            }

        }

        [WebMethod()]
        public DataSet db_get_data_vendor(int intPage, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_register", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param2.Value = user;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param3.Value = strSearch;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param4);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_holder_stock(int intPage, int intVendor, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_holder_stock", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_holder_stock_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param2.Value = intVendor;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }


        [WebMethod()]
        public DataSet db_get_data_director_level(int intPage, int intVendor, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_direct_lvl", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_direct_lvl_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param2.Value = intVendor;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_commissioner_level(int intPage, int intVendor, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_commissioner_lvl", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_direct_lvl_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param2.Value = intVendor;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_reg_attachment(int intPage, int intVendor, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_reg_attachment", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_reg_attachment_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param2.Value = intVendor;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public string db_get_next_info(int vcurr_proc_id, int vcurr_doc_type_id)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select sis.dbo.f_get_next_info(" + vcurr_proc_id + "," + vcurr_doc_type_id + ")";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            String strVal = "";
            myConnection.Open();
            strVal = myCommand.ExecuteScalar().ToString();
            myConnection.Close();
            return strVal;
        }

        [WebMethod()]
        public string db_get_next_status(int vstatus_list_id)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select code from sis.dbo.v_document_workflow_status where p_status_list_id=" + vstatus_list_id + "";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            String strVal = "";
            myConnection.Open();
            strVal = myCommand.ExecuteScalar().ToString();
            myConnection.Close();
            return strVal;
        }

        [WebMethod()]
        public string ExecuteReader(string Qry, string sItem)
        {
            OleDbConnection mConnection;
            OleDbCommand mCommand;
            OleDbDataReader mReader;
            string Rtn;

            mConnection = new OleDbConnection(ConnLC);
            mCommand = new OleDbCommand(Qry, mConnection);

            if (mConnection.State != ConnectionState.Open)
                mConnection.Open();
            mReader = mCommand.ExecuteReader();

            if (mReader.Read())
            {
                if (sItem != "")
                    Rtn = mReader[sItem].ToString();
                else
                    Rtn = "1";
            }
            else
                Rtn = "";
            mCommand.Dispose();
            mReader.Close();
            mConnection.Close();
            return Rtn;
        }

        [WebMethod()]
        public DataSet db_get_vender_register_spec(int vendor_register_id, int appr_type_id)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.f_vendor_register_spec(" + vendor_register_id + "," + appr_type_id + ")";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_status_list(int vendor_register_id, int appr_type_id)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_task_status_list", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_reg", OleDbType.Numeric, 32);
            param1.Value = vendor_register_id;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_appr_level", OleDbType.Numeric, 32);
            param2.Value = appr_type_id;
            myCommand.Parameters.Add(param2);

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();
            myAdapter.Fill(ds);

            ds.Dispose();
            myConnection.Close();

            return ds;

        }

        [WebMethod()]
        public DataSet db_get_entity_list(int entity_id)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select p_entity_id,p_organization_id,entity_code,entity_name,entity_addr,phone_1,phone_2,fax_1,fax_2 from p_entity where p_entity_id="+ entity_id;
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_user_login(string i_user_name, string i_user_pwd, ref int o_result_code, ref int o_user_id, ref string o_result_msg)
        {
            
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_user_login", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_user_name", OleDbType.VarChar, 32);
            param1.Value = i_user_name;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_user_pwd ", OleDbType.VarChar, 32);
            param2.Value = i_user_pwd;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("o_result_code", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param3);
            myCommand.Parameters["o_result_code"].Direction = ParameterDirection.Output;

            OleDbParameter param4 = new OleDbParameter("o_user_id", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param4);
            myCommand.Parameters["o_user_id"].Direction = ParameterDirection.Output;

            OleDbParameter param5 = new OleDbParameter("o_result_msg", OleDbType.VarChar, 1000);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_result_msg"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();
            o_result_code = Convert.ToInt32(myCommand.Parameters["o_result_code"].Value);
            o_user_id = Convert.ToInt32(myCommand.Parameters["o_user_id"].Value);
            o_result_msg = myCommand.Parameters["o_result_msg"].Value.ToString();
            myAdapter.Fill(ds);

            ds.Dispose();
            myConnection.Close();

            return ds;

        }

        [WebMethod()]
        public DataSet db_get_user_login_otp(string i_user_name, string i_user_pwd, string i_user_otp_pwd, ref int o_result_code, ref int o_user_id, ref string o_result_msg)
        {

            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_user_login_otp", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_user_name", OleDbType.VarChar, 32);
            param1.Value = i_user_name;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_user_pwd ", OleDbType.VarChar, 32);
            param2.Value = i_user_pwd;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("i_user_otp_pwd ", OleDbType.VarChar, 32);
            param3.Value = i_user_otp_pwd;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("o_result_code", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param4);
            myCommand.Parameters["o_result_code"].Direction = ParameterDirection.Output;

            OleDbParameter param5 = new OleDbParameter("o_user_id", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_user_id"].Direction = ParameterDirection.Output;

            OleDbParameter param6 = new OleDbParameter("o_result_msg", OleDbType.VarChar, 1000);
            myCommand.Parameters.Add(param6);
            myCommand.Parameters["o_result_msg"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();
            o_result_code = Convert.ToInt32(myCommand.Parameters["o_result_code"].Value);
            o_user_id = Convert.ToInt32(myCommand.Parameters["o_user_id"].Value);
            o_result_msg = myCommand.Parameters["o_result_msg"].Value.ToString();
            myAdapter.Fill(ds);

            ds.Dispose();
            myConnection.Close();

            return ds;

        }

        [WebMethod()]
        public DataSet db_get_user_start_app(int i_user_id, ref string o_result_msg)
        {

            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_user_start_app", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_user_id", OleDbType.Numeric, 10);
            param1.Value = i_user_id;
            myCommand.Parameters.Add(param1);
          
            OleDbParameter param2 = new OleDbParameter("o_result_msg", OleDbType.VarChar, 1000);
            myCommand.Parameters.Add(param2);
            myCommand.Parameters["o_result_msg"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();
            o_result_msg = myCommand.Parameters["o_result_msg"].Value.ToString();
            myAdapter.Fill(ds);

            ds.Dispose();
            myConnection.Close();

            return ds;

        }

        [WebMethod()]
        public DataSet db_get_user_menu(string i_user_name, int i_application_id)
        {

            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_menu_tree", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_user_name", OleDbType.VarChar, 50);
            param1.Value = i_user_name;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_application_id ", OleDbType.Numeric, 5);
            param2.Value = i_application_id;
            myCommand.Parameters.Add(param2);

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();
            myAdapter.Fill(ds);

            ds.Dispose();
            myConnection.Close();

            return ds;

        }

        [WebMethod()]
        public DataSet db_get_workflowname(string username)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.f_workflow_name('"+username+"')";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_user_task_list(float doc_type, float w_proc, string prof_type, string username, string keyword)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.f_user_task_list(" + doc_type + "," + w_proc + ", '" + prof_type + "','"+username+ "','"+keyword+"')";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_wf_summary_list(float doc_type, string username)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.f_workflow_summary_list("+doc_type+",'" + username + "')";
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_tender(int intPage, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_tender", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_tender_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param2.Value = user;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param3.Value = strSearch;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param4);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_reg_attach_tender(int intPage, int intTender, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_tender_attachment", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_tdr_attachment_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_tender_id", OleDbType.Numeric, 32);
            param2.Value = intTender;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_vendor_monit(int intPage, string user, int intPerPage, int intIDRow, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_register_monit", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param2.Value = user;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param3.Value = strSearch;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param4);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_register_ctl(int intVendor)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.v_t_w_register_ctl where t_vendor_register_id="+ intVendor;
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_project_tender(int intPage, string user, int intPerPage, int intIDRow, int intEntity, string strSearch, ref int intMaxRow)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_project_tender", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_project_tender_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_p_entity_id", OleDbType.Numeric, 32);
            param2.Value = intEntity;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            intMaxRow = Convert.ToInt32(myCommand.Parameters["o_rec_qty"].Value);
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_tender_monit(int intPage, string user, int intPerPage, int intIDRow, int intIDProject, string strSearch, ref string stringMax)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_vendor_tender_monit", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_tender_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_project_tender_id", OleDbType.Numeric, 32);
            param2.Value = intIDProject;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            stringMax = myCommand.Parameters["o_rec_qty"].Value.ToString();
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_tender_ctl(int intVendor)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);

            String strQuery;
            strQuery = "select * from sis.dbo.v_t_w_tender_ctl where t_vendor_tender_id=" + intVendor;
            OleDbCommand myCommand = new OleDbCommand(strQuery, myConnection);
            OleDbDataAdapter myAdapter = new OleDbDataAdapter();
            DataSet ds = new DataSet("DS");

            myCommand.CommandType = CommandType.Text;
            myAdapter.SelectCommand = myCommand;
            myAdapter.Fill(ds);
            myAdapter.Dispose();
            ds.Dispose();
            myConnection.Close();

            return ds;
        }

        [WebMethod()]
        public DataSet db_get_data_recommend_tender(int intPage, string user, int intPerPage, int intIDRow, int intIDProject, string strSearch, ref string stringMax)
        {
            OleDbConnection myConnection = new OleDbConnection(ConnLC);
            int intFirstRow;
            myConnection.Open();

            OleDbCommand myCommand = new OleDbCommand("p_list_recommend_tender", myConnection);
            myCommand.CommandType = CommandType.StoredProcedure;

            OleDbParameter param1 = new OleDbParameter("i_vendor_tender_id", OleDbType.Numeric, 32);
            param1.Value = intIDRow;
            myCommand.Parameters.Add(param1);

            OleDbParameter param2 = new OleDbParameter("i_project_tender_id", OleDbType.Numeric, 32);
            param2.Value = intIDProject;
            myCommand.Parameters.Add(param2);

            OleDbParameter param3 = new OleDbParameter("user", OleDbType.VarChar, 32);
            param3.Value = user;
            myCommand.Parameters.Add(param3);

            OleDbParameter param4 = new OleDbParameter("i_search", OleDbType.VarChar, 32);
            param4.Value = strSearch;
            myCommand.Parameters.Add(param4);

            OleDbParameter param5 = new OleDbParameter("o_rec_qty", OleDbType.Numeric, 5);
            myCommand.Parameters.Add(param5);
            myCommand.Parameters["o_rec_qty"].Direction = ParameterDirection.Output;

            OleDbDataAdapter myAdapter = new OleDbDataAdapter();

            DataSet ds = new DataSet("DS");

            myAdapter.SelectCommand = myCommand;
            myCommand.ExecuteNonQuery();

            intFirstRow = clsutl.get_first_row(intPage, intPerPage);

            stringMax = myCommand.Parameters["o_rec_qty"].Value.ToString();
            myAdapter.Fill(ds, intFirstRow - 1, intPerPage, "dtData");

            ds.Dispose();
            myConnection.Close();

            return ds;
        }

    }
}
